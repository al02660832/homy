package nieucrm.nieu.mx.skola.Fragments;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import info.hoang8f.android.segmented.SegmentedGroup;
import nieucrm.nieu.mx.skola.Adapter.ServicioAdminAdapter;
import nieucrm.nieu.mx.skola.DataModel.ServiciosData;
import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class ServiciosAdminFragment extends Fragment {

    TextView numSolicitado,numProgreso,numCompletado,numPagado,numCancelado;
    ListView listAdminServices;
    SegmentedGroup segmentedServicios;
    ParseQuery<ParseObject>service;
    ProgressBar progressServicio;
    RadioButton radioSolicitados,radioProgreso,radioCompletados,radioPagados,radioCancelados;
    String avenirBold = "font/avenir-next-bold.ttf", avenirMedium = "font/avenir-next-medium.ttf",demibold="font/avenir-next-demi-bold.ttf";

    ServicioAdminAdapter adapter;
    ArrayList<ServiciosData> array;
    ArrayList<ServiciosData> arraySolicitado;
    ArrayList<ServiciosData> arrayProgreso;
    ArrayList<ServiciosData> arrayCompletado;
    ArrayList<ServiciosData> arrayPagado;
    ArrayList<ServiciosData> arrayCancelado;

    String escuelaId;
    ParseObject escuela;

    public ServiciosAdminFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        setHasOptionsMenu(true);


        return inflater.inflate(R.layout.fragment_servicios_admin, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);

        numSolicitado=(TextView)v.findViewById(R.id.numSolicitado);
        numProgreso=(TextView)v.findViewById(R.id.numProgreso);
        numCompletado=(TextView)v.findViewById(R.id.numCompletado);
        numPagado=(TextView)v.findViewById(R.id.numPagado);
        numCancelado=(TextView)v.findViewById(R.id.numCancelado);

        progressServicio=(ProgressBar)v.findViewById(R.id.progressServicio);

        radioSolicitados=(RadioButton)v.findViewById(R.id.radioSolicitados);
        radioProgreso=(RadioButton)v.findViewById(R.id.radioProgreso);
        radioCompletados=(RadioButton)v.findViewById(R.id.radioCompletados);
        radioPagados=(RadioButton)v.findViewById(R.id.radioPagados);
        radioCancelados=(RadioButton)v.findViewById(R.id.radioCancelados);

        listAdminServices=(ListView)v.findViewById(R.id.listAdminServices);

        segmentedServicios=(SegmentedGroup)v.findViewById(R.id.segmentedServicios);


        fontFace();

        getServicios();



    }

    public void getServicios(){
        array=new ArrayList<>();
        arraySolicitado=new ArrayList<>();
        arrayProgreso=new ArrayList<>();
        arrayCompletado=new ArrayList<>();
        arrayPagado=new ArrayList<>();
        arrayCancelado=new ArrayList<>();

        escuelaId=((ParseApplication)getActivity().getApplication()).getEscuelaId();
        escuela=ParseObject.createWithoutData("Escuela",escuelaId);

        service=ParseQuery.getQuery("ServicioSolicitado");
        service.include("estudiante").include("servicio").include("personaAutorizada");
        service.include("personaAutorizada.escuela");
        service.orderByDescending("fechaSolicitud");
        service.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                progressServicio.setVisibility(View.GONE);
                listAdminServices.setVisibility(View.VISIBLE);
                if (e==null){
                    for (ParseObject o:objects){
                        ParseObject estObjects=o.getParseObject("estudiante");
                        ParseObject service=o.getParseObject("servicio");
                        ParseUser user=o.getParseUser("personaAutorizada");

                        ServiciosData data=new ServiciosData();

                        if (user==null){

                        }else {
                            if (user.getParseObject("escuela")==null){

                            }else {
                                if(user.getParseObject("escuela").getObjectId().equals(escuelaId)){
                                    if(estObjects==null){
                                        data.setAlumno("Alumno");
                                    }else{
                                        data.setAlumno(estObjects.getString("NOMBRE")+" "+estObjects.getString("APELLIDO"));
                                    }

                                    if (service==null){
                                        data.setNombre("Servicio");
                                    }else {
                                        data.setNombre(service.getString("nombre"));
                                        data.setDescripcion(service.getString("descripcion"));
                                        data.setPrecio("$"+String.valueOf(service.getNumber("precio").intValue()));

                                    }

                                    data.setStatus(o.getNumber("status").intValue());
                                    data.setObjectId(o.getObjectId());
                                    data.setComentarios(o.getString("comentarios"));

                                    data.setDate(o.getDate("fechaSolicitud"));

                                    if (o.getNumber("status").intValue()==0){
                                        arraySolicitado.add(data);
                                    }else {
                                        if (o.getNumber("status").intValue()==1){
                                            arrayProgreso.add(data);
                                        }else {
                                            if (o.getNumber("status").intValue()==2){
                                                arrayCompletado.add(data);
                                            }else {
                                                if (o.getNumber("status").intValue()==3){
                                                    arrayPagado.add(data);
                                                }else {
                                                    if (o.getNumber("status").intValue()==9||o.getNumber("status").intValue()==8){
                                                        arrayCancelado.add(data);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }



                    }
                    try{
                    adapter=new ServicioAdminAdapter(getContext(),arraySolicitado);
                    listAdminServices.setAdapter(adapter);
                    }catch (Exception e1){
                        Log.d("Error",e1.getMessage());

                    }
                    numSolicitado.setText(String.valueOf(arraySolicitado.size()));
                    numProgreso.setText(String.valueOf(arrayProgreso.size()));
                    numCompletado.setText(String.valueOf(arrayCompletado.size()));
                    numCancelado.setText(String.valueOf(arrayCancelado.size()));
                    numPagado.setText(String.valueOf(arrayPagado.size()));
                    segmentedServicios.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                            switch (checkedId){
                                case R.id.radioSolicitados:
                                    adapter=new ServicioAdminAdapter(getContext(),arraySolicitado);
                                    listAdminServices.setAdapter(adapter);
                                    break;
                                case R.id.radioProgreso:
                                    adapter=new ServicioAdminAdapter(getContext(),arrayProgreso);
                                    listAdminServices.setAdapter(adapter);

                                    break;
                                case R.id.radioCompletados:
                                    adapter=new ServicioAdminAdapter(getContext(),arrayCompletado);
                                    listAdminServices.setAdapter(adapter);

                                    break;
                                case R.id.radioPagados:
                                    adapter=new ServicioAdminAdapter(getContext(),arrayPagado);
                                    listAdminServices.setAdapter(adapter);

                                    break;
                                case R.id.radioCancelados:
                                    adapter=new ServicioAdminAdapter(getContext(),arrayCancelado);
                                    listAdminServices.setAdapter(adapter);

                                    break;
                            }
                        }
                    });
                }else {
                    Log.d("Error",e.getMessage());
                }
            }
        });

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.new_service,menu);
        inflater.inflate(R.menu.precio,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id=item.getItemId();
        if (id==android.R.id.home){
            getFragmentManager().popBackStack();
        }

        if(id==R.id.nuevoServicio){

            Fragment fragment = new ServiciosActivosFragment();
            Bundle bundle=new Bundle();
            fragment.setArguments(bundle);
            FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
            transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();

        }

        if (id==R.id.nuevoPrecio){

            Fragment fragment = new ContabilidadFragment();
            Bundle b=new Bundle();

            fragment.setArguments(b);
            getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
            /*
            FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
            transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
            */


        }

        return super.onOptionsItemSelected(item);

    }

    public void fontFace(){
        Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), avenirBold);
        Typeface medium = Typeface.createFromAsset(getActivity().getAssets(), avenirMedium);
        Typeface demi=Typeface.createFromAsset(getActivity().getAssets(),demibold);

        radioSolicitados.setTypeface(medium);
        radioProgreso.setTypeface(medium);
        radioCompletados.setTypeface(medium);
        radioPagados.setTypeface(medium);
        radioCancelados.setTypeface(medium);

        numSolicitado.setTypeface(medium);
        numProgreso.setTypeface(medium);
        numCompletado.setTypeface(medium);
        numPagado.setTypeface(medium);
        numCancelado.setTypeface(medium);


    }

}

