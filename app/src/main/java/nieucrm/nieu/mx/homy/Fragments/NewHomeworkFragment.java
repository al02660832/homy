package nieucrm.nieu.mx.skola.Fragments;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickResult;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import nieucrm.nieu.mx.skola.Helpers.Constants;
import nieucrm.nieu.mx.skola.Helpers.ImagePicker;
import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.Helpers.Util;
import nieucrm.nieu.mx.skola.R;


/**
 * Created by Carlos Romero
 */

public class NewHomeworkFragment extends Fragment {

    TextView fechaAsignada, fechaEntrega, textHomeworkTarea, materias,textHomeworkPara,fechaTarea,fechaText,materiaTxt,descripTarea;
    EditText contenido;
    Button crearTarea;
    Date now;
    ParseObject tipoTarea;
    int usertype;
    DatePickerDialog.OnDateSetListener date;
    Calendar myCalendar = Calendar.getInstance();
    String homeworkId;
    ParseObject homework;
    HashMap<String, Object> params;
    ProgressBar progressTarea;
    ImageView imagenTarea;

    boolean hasPhoto;

    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf",demibold="font/avenir-next-demi-bold.ttf";

    ArrayList<ParseObject> grupoTarea;

    String[]arrayMateria;

    String descripcion,materia;

    String nombreM;

    ArrayList<String> nombres;
    ArrayList<String>mSelectedItems;
    ArrayList<ParseObject>estudiantes;
    String[] n;

    ArrayList<String> nombreMateria;
    String[] m;
    ProgressDialog dialog,dialogs;

    TransferUtility utility;
    TransferObserver observe;
    String camino,pagoId;
    private static final int PICK_IMAGE_ID = 234;
    private String mPath;
    ImageButton adjuntarTarea;
    String escuelaId;
    ParseObject defaultGroup;
    String grupoNombre,groupId;
    Activity a;

    public NewHomeworkFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            a=(Activity) context;
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        utility= Util.getTransferUtility(a);
        Bundle b=this.getArguments();
        if (b!=null){
            groupId=b.getString("groupId");
            grupoNombre=b.getString("grupoNombre");

        }

        return inflater.inflate(R.layout.fragment_new_homework, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        crearTarea = (Button) view.findViewById(R.id.crearTareaN);
        contenido = (EditText) view.findViewById(R.id.contenidoTarea);
        materias = (TextView) view.findViewById(R.id.materiaTarea);
        fechaAsignada = (TextView) view.findViewById(R.id.fechaAsignada);
        fechaEntrega = (TextView) view.findViewById(R.id.fechaEntrega);
        textHomeworkTarea=(TextView)view.findViewById(R.id.grupoTarea);

        textHomeworkPara=(TextView)view.findViewById(R.id.textHomeworkPara);
        fechaTarea=(TextView)view.findViewById(R.id.fechaTarea);
        fechaText=(TextView)view.findViewById(R.id.fechaText);
        materiaTxt=(TextView)view.findViewById(R.id.materiaTxt);

        descripTarea=(TextView)view.findViewById(R.id.descripTarea);

        adjuntarTarea=(ImageButton)view.findViewById(R.id.adjuntarTarea);

        imagenTarea=(ImageView)view.findViewById(R.id.fotoTarea);
        progressTarea=(ProgressBar)view.findViewById(R.id.progressTarea);


        usertype=ParseUser.getCurrentUser().getNumber("usertype").intValue();

        if (grupoNombre==null){
            Log.e("Nombre nulo","Nulo");
        }else {
            textHomeworkTarea.setText(grupoNombre);

        }

        setTypeface();

        setFecha();

        setListeners();



    }

    public void setListeners(){
        fechaEntrega.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fechaTarea();
            }
        });

        textHomeworkTarea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog=new ProgressDialog(a);
                dialog.setTitle("Cargando");
                dialog.setMessage("Por favor espere...");
                dialog.setCancelable(false);
                dialog.show();
                grupos();
            }
        });

        materias.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogs=new ProgressDialog(a);
                dialogs.setTitle("Cargando");
                dialogs.setMessage("Por favor espere...");
                dialogs.setCancelable(false);
                dialogs.show();
                setMaterias();
            }
        });

        adjuntarTarea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPicker();
            }
        });

        crearTarea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getTipo();
            }
        });
    }

    public void fechaTarea() {
        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        fechaEntrega.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(a, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });
    }

    public void updateLabel() {
        String myFormat = "dd/MMM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        fechaEntrega.setText(sdf.format(myCalendar.getTime()));

    }

    public void setFecha() {
        now = new Date();
        String format = "dd/MMM/yyyy"; //In which you need put here
        SimpleDateFormat df = new SimpleDateFormat(format, Locale.US);
        fechaAsignada.setText(df.format(myCalendar.getTime()));
    }

    public void setMaterias() {
        nombreMateria=new ArrayList<>();

        ParseQuery<ParseObject>asunto=ParseQuery.getQuery("materia");
        asunto.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                dialogs.hide();
                if(e==null){
                    for (ParseObject o:objects){
                        nombreMateria.add(o.getString("nombre"));
                    }
                }else {
                    Toast.makeText(a,"Error",Toast.LENGTH_SHORT).show();
                }

                arrayMateria=new String[nombreMateria.size()];
                arrayMateria=nombreMateria.toArray(arrayMateria);

                AlertDialog.Builder builder = new AlertDialog.Builder(a);

                // Set the dialog title

                builder.setTitle("Seleccione una materia: ")

                        // Specify the list array, the items to be selected by default (null for none),

                        // and the listener through which to receive callbacks when items are selected

                        .setItems(arrayMateria, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                nombreM=nombreMateria.get(which);
                                materias.setText(nombreM);
                            }

                            // Set the action buttons
                        }).create().show();
            }
        });
    }

    public void grupos(){

        nombres=new ArrayList<>();
        estudiantes=new ArrayList<>();
        grupoTarea=new ArrayList<>();

        escuelaId=((ParseApplication)a.getApplication()).getEscuelaId();

        ParseObject escuela=ParseObject.createWithoutData("Escuela",escuelaId);

        ParseQuery<ParseObject> est= ParseQuery.getQuery("grupo").include("Maestros").include("escuela").whereEqualTo("Maestros",ParseUser.getCurrentUser());
        est.whereEqualTo("escuela",escuela);
        est.findInBackground(new FindCallback<ParseObject>() {

            @Override

            public void done(List<ParseObject> objects, ParseException e) {
                dialog.hide();
                if (e==null){

                    for (ParseObject o:objects){

                        nombres.add(o.getString("name"));
                        estudiantes.add(o);
                    }

                }else {

                    Toast.makeText(a,"Error",Toast.LENGTH_SHORT).show();

                }


                n=new String[nombres.size()];

                n=nombres.toArray(n);


                boolean []elementoSeleccionado=new boolean[nombres.size()];

                for(int i=0;i<elementoSeleccionado.length;i++){

                    elementoSeleccionado[i]=false;

                }


                mSelectedItems = new ArrayList<>();  // Where we track the selected items

                AlertDialog.Builder builder = new AlertDialog.Builder(a);

                // Set the dialog title

                builder.setTitle("Para: ")

                        // Specify the list array, the items to be selected by default (null for none),

                        // and the listener through which to receive callbacks when items are selected

                        .setMultiChoiceItems(n, elementoSeleccionado,

                                new DialogInterface.OnMultiChoiceClickListener() {

                                    @Override

                                    public void onClick(DialogInterface dialog, int which,

                                                        boolean isChecked) {

                                        if (isChecked) {

                                            // If the user checked the item, add it to the selected items

                                            mSelectedItems.add(n[which]);
                                            grupoTarea.add(estudiantes.get(which));

                                        } else if (mSelectedItems.contains(n[which])) {

                                            // Else, if the item is already in the array, remove it

                                            mSelectedItems.remove(n[which]);
                                            grupoTarea.remove(estudiantes.get(which));
                                        }

                                    }

                                })

                        // Set the action buttons

                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                            @Override

                            public void onClick(DialogInterface dialog, int id) {
                                // User clicked OK, so save the mSelectedItems results somewhere
                                // or return them to the component that opened the dialog
                                textHomeworkTarea.setText("");

                                for (int j = 0; j < mSelectedItems.size(); j++){
                                    if (j==mSelectedItems.size()-1){
                                        textHomeworkTarea.append(mSelectedItems.get(j));
                                    }else{
                                        textHomeworkTarea.append(mSelectedItems.get(j) + ", ");
                                    }
                                }
                            }

                        })

                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                            @Override

                            public void onClick(DialogInterface dialog, int id) {


                            }

                        })


                        .create().show();



            }

        });

    }

    public void getTipo(){
        ParseQuery<ParseObject>tipos=ParseQuery.getQuery("tipoAnuncio");
        tipos.whereEqualTo("nombre","Tarea");
        tipos.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (e==null){
                    tipoTarea=object;
                    saveHomework();
                }
            }
        });
    }

    public void saveHomework(){
        defaultGroup=ParseObject.createWithoutData("grupo",groupId);
        params=new HashMap<>();


        if (grupoTarea==null){
            grupoTarea=new ArrayList<>();
            grupoTarea.add(defaultGroup);

        }else {
            if (grupoTarea.size()==0){
                grupoTarea.add(defaultGroup);
            }
        }

        descripcion=contenido.getText().toString().trim();
        materia=materias.getText().toString().trim();
        homework=new ParseObject("anuncio");

        escuelaId=((ParseApplication)a.getApplication()).getEscuelaId();



        homework.put("autor", ParseUser.getCurrentUser());
        homework.put("grupos",grupoTarea);
        homework.put("materia",materia);
        homework.put("tipo",tipoTarea);
        if (ParseUser.getCurrentUser().getNumber("usertype").intValue()==0){
            homework.put("aprobado",true);
        }else {
            homework.put("aprobado",false);
        }
        homework.put("descripcion",descripcion);
        homework.put("fechaEntrega",myCalendar.getTime());

        homework.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e==null){

                    pagoId=homework.getObjectId();

                    if (hasPhoto){
                        saveToAnuncioPhoto(homework);
                    }
                    homeworkId=homework.getObjectId();
                    params.put("anuncioObjectId",homeworkId);

                    switch (usertype){
                        case 0:
                            //Admin
                            homeworkId=homework.getObjectId();
                            params.put("anuncioObjectId",homeworkId);
                            params.put("escuelaObjId",escuelaId);

                            ParseCloud.callFunctionInBackground("adminApprovedAnuncio", params, new FunctionCallback<Object>() {

                                @Override
                                public void done(Object object, ParseException e) {
                                    if (e == null){
                                        Log.d("Cloudcode","Success");
                                    }else {
                                        Log.d("Error",e.getMessage());
                                    }
                                }
                            });
                            break;
                        case 1:
                            //Teacher
                            homeworkId=homework.getObjectId();
                            params.put("anuncioObjectId",homeworkId);
                            params.put("escuelaObjId",escuelaId);

                            ParseCloud.callFunctionInBackground("teacherAnuncioToBeApproved", params, new FunctionCallback<Object>() {

                                @Override
                                public void done(Object object, ParseException e) {
                                    if (e == null){
                                        Log.d("Cloudcode","Success");
                                    }else {
                                        Log.d("Error",e.getMessage());
                                    }
                                }
                            });
                            break;
                        case 2:
                            //Parent
                            homeworkId=homework.getObjectId();
                            params.put("anuncioObjectId",homeworkId);
                            params.put("escuelaObjId",escuelaId);

                            ParseCloud.callFunctionInBackground("parentAnuncioCreated", params, new FunctionCallback<Object>() {

                                @Override
                                public void done(Object object, ParseException e) {
                                    if (e == null){
                                        Log.d("Cloudcode","Success");
                                    }else {
                                        Log.d("Error",e.getMessage());
                                    }
                                }
                            });
                            break;

                        default:
                            break;
                    }
                    Toast.makeText(a,"Tarea creada",Toast.LENGTH_SHORT).show();
                }

            }
        });
    }


    public void showPicker(){
        PickImageDialog.build(new PickSetup().setCameraButtonText("Cámara").setGalleryButtonText("Galería").setTitle("Agregar foto").setCancelText("Cancelar"))
                .setOnPickResult(new IPickResult() {
                    @Override
                    public void onPickResult(PickResult r) {
                        //TODO: do what you have to...
                        if (r.getError() == null) {
                            //If you want the Uri.
                            //Mandatory to refresh image from Uri.
                            //getImageView().setImageURI(null);

                            //Setting the real returned image.
                            //getImageView().setImageURI(r.getUri());

                            //If you want the Bitmap.

                            Bitmap bitmap = r.getBitmap();
                            // TODO use bitmap

                            camino=r.getPath();
                            //imagenMensaje.setImageBitmap(bitmap);
                            hasPhoto=true;
                            imagenTarea.setImageBitmap(bitmap);
                            progressTarea.setVisibility(View.VISIBLE);


                            //Image path
                            //r.getPath();
                        } else {
                            //Handle possible errors
                            //TODO: do what you have to do with r.getError();
                            Toast.makeText(a, r.getError().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                }).show(getActivity().getSupportFragmentManager());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case PICK_IMAGE_ID:
                Bitmap bitmap = ImagePicker.getImageFromResult(a, resultCode, data);
                // TODO use bitmap



                camino=mPath;
                //imagenMensaje.setImageBitmap(bitmap);
                imagenTarea.setImageBitmap(bitmap);
                hasPhoto=true;
                progressTarea.setVisibility(View.VISIBLE);
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    public void setTypeface(){
        Typeface bold=Typeface.createFromAsset(a.getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(a.getAssets(),avenirMedium);
        Typeface dem=Typeface.createFromAsset(a.getAssets(),demibold);

        fechaAsignada.setTypeface(medium);
        fechaEntrega.setTypeface(medium);
        materias.setTypeface(medium);
        textHomeworkTarea.setTypeface(medium);
        textHomeworkPara.setTypeface(medium);
        fechaTarea.setTypeface(medium);
        fechaText.setTypeface(medium);
        materiaTxt.setTypeface(medium);
        descripTarea.setTypeface(medium);
        crearTarea.setTypeface(dem);
    }

    public void saveToAnuncioPhoto(ParseObject anuncio){
        final ParseObject anuncioPhoto=new ParseObject("AnuncioPhoto");
        anuncioPhoto.put("anuncio",anuncio);
        anuncioPhoto.put("aws",true);
        anuncioPhoto.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e==null){
                    Log.d("AnuncioPhoto","Foto guardada");
                    attachMessage(anuncioPhoto.getObjectId());
                }else {
                    Toast.makeText(a, "Verifique su conexión a internet e intente de nuevo",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void attachMessage(String anuncioId){
        Toast.makeText(a,"Enviando foto",Toast.LENGTH_SHORT).show();
        observe= utility.upload(Constants.BUCKET_NAME,anuncioId,new java.io.File(camino ));
        observe.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (TransferState.COMPLETED.equals(observe.getState())) {

                    //progressMessage.setVisibility(View.GONE);
                    progressTarea.setVisibility(View.GONE);
                    getActivity().getSupportFragmentManager().popBackStack();
                    Toast.makeText(a, "Foto guardada", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                long _bytesCurrent = bytesCurrent;
                long _bytesTotal = bytesTotal;

                float percentage =  ((float)_bytesCurrent /(float)_bytesTotal * 100);
                Log.d("percentage","" +percentage);
                //progressMessage.setProgress((int) percentage);
                progressTarea.setProgress((int) percentage);
            }

            @Override
            public void onError(int id, Exception ex) {
                Log.d("Error de Amazon",ex.getMessage());
                Toast.makeText(a,"Error al subir la foto, intente de nuevo",Toast.LENGTH_SHORT).show();
            }
        });
    }


}



