package nieucrm.nieu.mx.skola.Fragments;


import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amazonaws.services.kms.model.ExpiredImportTokenException;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;


import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */


public class VisitantesFragment extends Fragment {

    EditText editEscuela,editNombre,editCorreo,editTelefono;
    TextView txtEstado,siguienteVisitante,bienvenidoTag,skolaTag,linkGuest,estadosTag,datosTag,escuelaTag;
    RadioButton btnContacto;
    String[] arrayEstado;
    ImageButton imgSiguiente;
    String avenirBold = "font/avenir-next-bold.ttf", avenirMedium = "font/avenir-next-medium.ttf",avenirDemi="font/avenir-next-demi-bold.ttf",nanum="font/NanumPenScript-Regular.ttf";


    boolean contact;

    Fragment fragment;

    String escuela,nombre,correo,telefono,estado;


    public VisitantesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_visitantes, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);

        editEscuela=(EditText)v.findViewById(R.id.editEscuela);
        editNombre=(EditText)v.findViewById(R.id.editNombre);
        editCorreo=(EditText)v.findViewById(R.id.editCorreo);
        editTelefono=(EditText)v.findViewById(R.id.editTelefono);


        btnContacto=(RadioButton)v.findViewById(R.id.btnContacto);

        imgSiguiente=(ImageButton)v.findViewById(R.id.imgSiguiente);

        txtEstado=(TextView)v.findViewById(R.id.txtEstado);
        siguienteVisitante=(TextView)v.findViewById(R.id.siguienteVisitante);
        bienvenidoTag=(TextView)v.findViewById(R.id.bienvenidoTag);
        skolaTag=(TextView)v.findViewById(R.id.skolaTag);
        linkGuest=(TextView)v.findViewById(R.id.linkGuest);
        estadosTag=(TextView)v.findViewById(R.id.estadosTag);
        datosTag=(TextView)v.findViewById(R.id.datosTag);
        escuelaTag=(TextView)v.findViewById(R.id.escuelaTag);

        linkGuest.setText("http://skola.nieu.mx/");



        setType();
        setListener();

    }

    public void setListener(){

        arrayEstado= getActivity().getResources().getStringArray(R.array.estados_array);
        txtEstado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              setEstados();
            }
        });

        imgSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //saveObject();
                try {
                    fragment=new VisitantesQRFragment();
                    FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                    transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                    transaction.replace(R.id.content_guest,fragment,"fragment").addToBackStack(null).commit();
                }catch (Exception e){
                  Log.e("Error",e.getMessage());
                }

            }
        });

        btnContacto.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                contact=btnContacto.isChecked();
            }
        });

        linkGuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url ="http://skola.nieu.mx/" ;
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

    }

    public void setType(){
        Typeface bold = Typeface.createFromAsset(getContext().getAssets(), avenirBold);
        Typeface medium = Typeface.createFromAsset(getContext().getAssets(), avenirMedium);
        Typeface demi=Typeface.createFromAsset(getContext().getAssets(),avenirDemi);
        Typeface nan=Typeface.createFromAsset(getContext().getAssets(),nanum);


        editEscuela.setTypeface(medium);
        editNombre.setTypeface(medium);
        editCorreo.setTypeface(medium);
        editTelefono.setTypeface(medium);
        btnContacto.setTypeface(medium);
        txtEstado.setTypeface(medium);
        siguienteVisitante.setTypeface(demi);
        bienvenidoTag.setTypeface(nan);
        skolaTag.setTypeface(medium);
        linkGuest.setTypeface(medium);
        estadosTag.setTypeface(medium);
        datosTag.setTypeface(medium);
        escuelaTag.setTypeface(medium);

    }

    public void saveObject(){

        escuela=editEscuela.getText().toString().trim();
        nombre=editNombre.getText().toString().trim();
        correo=editCorreo.getText().toString().trim();
        telefono=editTelefono.getText().toString().trim();
        estado=txtEstado.getText().toString().trim();

        ParseObject prospecto=new ParseObject("Prospectos");

        prospecto.put("Escuela",escuela);
        prospecto.put("Nombre",nombre);
        prospecto.put("Correo",correo);
        prospecto.put("Telefono",telefono);
        prospecto.put("Estado",estado);
        prospecto.put("Contacto",contact);

        prospecto.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                fragment=new VisitantesQRFragment();
                getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_guest,fragment,"fragment").addToBackStack(null).commit();
                if (e==null){
                    Log.d("Prospecto:","Guardado exitosamente");
                }else {
                    Log.d("Prospecto:","Errores");
                }
            }
        });


    }

    public void setEstados(){
        txtEstado.setText("");
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());

        // Set the dialog title

        builder.setTitle("Seleccione un estado: ")

                .setItems(R.array.estados_array, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        txtEstado.setText(arrayEstado[which]);
                    }

                    // Set the action buttons
                }).create().show();
    }

}
