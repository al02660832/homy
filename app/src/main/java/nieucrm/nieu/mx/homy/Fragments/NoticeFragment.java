package nieucrm.nieu.mx.skola.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import nieucrm.nieu.mx.skola.Adapter.NoticeAdapter;
import nieucrm.nieu.mx.skola.DataModel.InfoData;
import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class NoticeFragment extends Fragment {

    ListView listaCircular;
    ParseObject escuela;
    String escuelaId, fecha;

    NoticeAdapter a;
    InfoData data;
    ArrayList<InfoData> array;

    public NoticeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_notice, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        listaCircular=(ListView)view.findViewById(R.id.listaCircular);

        getInfo();

    }

    public void getInfo(){
        array=new ArrayList<>();
        ParseQuery<ParseObject>infoQuery=ParseQuery.getQuery("Informacion");
        escuelaId=((ParseApplication)getActivity().getApplication()).getEscuelaId();
        escuela=ParseObject.createWithoutData("Escuela",escuelaId);
        infoQuery.whereEqualTo("escuela",escuela);
        infoQuery.orderByDescending("createdAt");
        infoQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e==null){
                    for (ParseObject o:objects){
                        data=new InfoData();
                        if (o.getObjectId()==null){

                        }else {
                            data.setObjectId(o.getObjectId());
                        }

                        if (o.getString("contenido")==null){
                            data.setTipo("");
                        }else {
                            data.setContenido(o.getString("contenido"));
                        }

                        if (o.getString("tipo")==null){
                            data.setTipo("General");
                        }else {
                            data.setTipo(o.getString("tipo"));
                        }

                        data.setAws(o.getBoolean("aws"));

                        array.add(data);

                    }
                    a=new NoticeAdapter(getActivity(),array);
                    listaCircular.setAdapter(a);
                }else {
                    Log.e("error",e.getMessage());
                }
            }
        });

    }

}
