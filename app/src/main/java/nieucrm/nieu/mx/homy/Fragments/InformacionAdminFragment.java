package nieucrm.nieu.mx.skola.Fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import nieucrm.nieu.mx.skola.BuildConfig;
import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class InformacionAdminFragment extends Fragment {

    String avenirBold = "font/avenir-next-bold.ttf", avenirMedium = "font/avenir-next-medium.ttf", demibold = "font/avenir-next-demi-bold.ttf";
    ImageButton imgMenu,imgDirectorio,imgReglamento,imgAvisos,imgPrograma,imgActividad;
    TextView txtMenu,txtDirectorio,txtReglamento,txtAvisos,txtPrograma,txtActividad,txtInformacion;
    LinearLayout layoutMom;

    Bundle b;
    ProgressDialog p;

    Fragment fragment=null;
    Activity a;


    public InformacionAdminFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            a=(Activity) context;
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_informacion_admin, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        txtMenu=(TextView)view.findViewById(R.id.menuTag);
        txtDirectorio=(TextView)view.findViewById(R.id.txtDirectorio);
        txtReglamento=(TextView)view.findViewById(R.id.txtReglamento);
        txtAvisos=(TextView)view.findViewById(R.id.txtAvisos);
        txtPrograma=(TextView)view.findViewById(R.id.txtPrograma);
        txtActividad=(TextView)view.findViewById(R.id.txtActividad);
        txtInformacion=(TextView)view.findViewById(R.id.txtInformacion);

        imgMenu=(ImageButton)view.findViewById(R.id.imgMenu);
        imgDirectorio=(ImageButton)view.findViewById(R.id.imgDirectorio);
        imgReglamento=(ImageButton)view.findViewById(R.id.imgReglamento);
        imgAvisos=(ImageButton)view.findViewById(R.id.imgAvisos);
        imgPrograma=(ImageButton)view.findViewById(R.id.imgPrograma);
        imgActividad=(ImageButton)view.findViewById(R.id.imgActividad);

        layoutMom=(LinearLayout)view.findViewById(R.id.layoutExtra);

        b=new Bundle();
        p=new ProgressDialog(a);
        p.setTitle("Cargando...");
        p.setMessage("Por favor espere");
        p.setCancelable(false);

        if (BuildConfig.FLAVOR.equals("momsTotsTol")||BuildConfig.FLAVOR.equals("momsTotsMet")) {
            layoutMom.setVisibility(View.VISIBLE);
        }

        fontFace();
        setListeners();
    }

    public void setListeners(){
        imgMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                p.show();
                fragment=new InformationAdminFragment();
                // app.setTipoInformacion(1);
                b.putInt("tipo",3);
                fragment.setArguments(b);
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                p.hide();
            }
        });

        imgDirectorio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                p.show();
                fragment=new InformationAdminFragment();
                // app.setTipoInformacion(1);
                b.putInt("tipo",2);
                fragment.setArguments(b);
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                p.hide();
            }
        });

        imgReglamento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                p.show();
                fragment=new InformationAdminFragment();
                // app.setTipoInformacion(1);
                b.putInt("tipo",1);
                fragment.setArguments(b);
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                p.hide();
            }
        });

        imgPrograma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                p.show();
                fragment=new InformationAdminFragment();
                // app.setTipoInformacion(1);
                b.putInt("tipo",4);
                fragment.setArguments(b);
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                p.hide();
            }
        });

        imgActividad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                p.show();
                fragment=new InformationAdminFragment();
                // app.setTipoInformacion(1);
                b.putInt("tipo",5);
                fragment.setArguments(b);
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                p.hide();
            }
        });

        imgAvisos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                p.show();
                fragment=new InformationAdminFragment();
                // app.setTipoInformacion(1);
                b.putInt("tipo",6);
                fragment.setArguments(b);
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                p.hide();
            }
        });
    }

    public void fontFace(){
        Typeface bold = Typeface.createFromAsset(a.getAssets(), avenirBold);
        Typeface medium = Typeface.createFromAsset(a.getAssets(), avenirMedium);
        Typeface demi=Typeface.createFromAsset(a.getAssets(),demibold);

        txtMenu.setTypeface(demi);
        txtDirectorio.setTypeface(demi);
        txtReglamento.setTypeface(demi);
        txtAvisos.setTypeface(demi);
        txtPrograma.setTypeface(demi);
        txtActividad.setTypeface(demi);
        txtInformacion.setTypeface(bold);



    }
}
