package nieucrm.nieu.mx.skola.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import nieucrm.nieu.mx.skola.DataModel.AnuncioData;
import nieucrm.nieu.mx.skola.DataModel.MensajeData;
import nieucrm.nieu.mx.skola.Fragments.AnuncioDetailFragment;
import nieucrm.nieu.mx.skola.Fragments.MessageDetailFragment;
import nieucrm.nieu.mx.skola.R;


/**
 * Created by Carlos Romero on 14/03/2017.
 */

public class MensajeAdapter extends BaseAdapter {

    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf",avenirDemibold="font/avenir-next-demi-bold.ttf";



    private Context context;
    LayoutInflater inflater;
    String obId;
    private List<MensajeData> mensajeData=null;
    private ArrayList<MensajeData> array=null;

    public MensajeAdapter(Context context, List<MensajeData> mensajeData) {
        this.context = context;
        this.mensajeData=mensajeData;
        try {
            inflater=LayoutInflater.from(context);

        }catch (Exception e){
            Log.d("Error",e.getMessage());
        }
        this.array=new ArrayList<>();
        this.array.addAll(mensajeData);
    }

    public class ViewHolder{
        TextView hora;
        TextView fecha;
        TextView parentesco;
        TextView nombre;
        TextView contenido;
        TextView para;
        TextView paraPara;
        ImageView viewClip;
    }


    @Override
    public int getCount() {
        return mensajeData.size();
    }

    @Override
    public Object getItem(int position) {
        return mensajeData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Typeface bold=Typeface.createFromAsset(context.getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(context.getAssets(),avenirMedium);
        Typeface demibold=Typeface.createFromAsset(context.getAssets(),avenirDemibold);

        ViewHolder holder;
        // TextView nombreEstudiante=(TextView)v.findViewById(R.id.nombreEstudiante);

        if(convertView==null){
            holder=new ViewHolder();
            convertView=inflater.inflate(R.layout.mensaje_item_layout,null);
            holder.fecha=(TextView)convertView.findViewById(R.id.fechaMensaje);
            holder.hora=(TextView)convertView.findViewById(R.id.horaMensaje);
            holder.parentesco=(TextView)convertView.findViewById(R.id.parentescoMensaje);
            holder.nombre=(TextView)convertView.findViewById(R.id.nombreMensaje);
            holder.contenido=(TextView)convertView.findViewById(R.id.contenidoMensaje);
            holder.para=(TextView)convertView.findViewById(R.id.paraMensaje);
            holder.viewClip=(ImageView)convertView.findViewById(R.id.viewClip);
            holder.paraPara=(TextView)convertView.findViewById(R.id.paraPara);

            holder.para.setTypeface(demibold);
            holder.fecha.setTypeface(medium);
            holder.contenido.setTypeface(medium);
            holder.hora.setTypeface(medium);
            holder.nombre.setTypeface(demibold);
            holder.parentesco.setTypeface(demibold);
            convertView.setTag(holder);
        }else {
            holder=(ViewHolder)convertView.getTag();
        }

        if (mensajeData.get(position).getParseUri()!=null||mensajeData.get(position).isAwsAttachment()||mensajeData.get(position).isTablaAnuncio()){
            holder.viewClip.setVisibility(View.VISIBLE);
        }else {
            holder.viewClip.setVisibility(View.GONE);
        }

        /*
        if (mensajeData.get(position).getTipo().equals("Anuncio")){
            holder.tipo.setTextColor(Color.parseColor("#4FC1E9"));
        }else {
            if (anuncioData.get(position).getTipo().equals("Tarea")){
                holder.tipo.setTextColor(Color.parseColor("#A0D468"));
            }else {
                if (anuncioData.get(position).getTipo().equals("Enfermería")){
                    holder.tipo.setTextColor(Color.parseColor("#ED5565"));
                }else {
                    holder.tipo.setTextColor(Color.parseColor("#48CFAD"));
                }
            }
        }
        */





        holder.fecha.setText(mensajeData.get(position).getFechaEntrega());
        holder.hora.setText(mensajeData.get(position).getHora());
        holder.parentesco.setText(mensajeData.get(position).getParentesco());
        holder.nombre.setText(mensajeData.get(position).getEmisor());
        holder.paraPara.setText(mensajeData.get(position).getPara());

        if (mensajeData.get(position).getReceptor()==null){
            holder.para.setVisibility(View.GONE);
        }else {
            holder.para.setText(mensajeData.get(position).getReceptor());
        }

        holder.contenido.setText(mensajeData.get(position).getDescripcion());


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment=new MessageDetailFragment();
                Bundle b=new Bundle();
                b.putString("parentesco",mensajeData.get(position).getParentesco());
                if (mensajeData.get(position).getPara()==null){
                    b.putString("para","");
                }else {
                    b.putString("para"," Para: ");

                }
                b.putString("hora",mensajeData.get(position).getHora());
                b.putString("studentId",mensajeData.get(position).getStudentId());
                b.putString("fecha",mensajeData.get(position).getFechaEntrega());
                b.putString("nombreAlumno",mensajeData.get(position).getEmisor());
                b.putString("asunto",mensajeData.get(position).getTipo());
                b.putString("descripcion",mensajeData.get(position).getDescripcion());
                b.putString("seleccion",mensajeData.get(position).getSel());
                b.putString("id",mensajeData.get(position).getID());

                if (mensajeData.get(position).getParseUri()!=null){
                    b.putString("parseUri",mensajeData.get(position).getParseUri());
                }else {
                    if (mensajeData.get(position).isAwsAttachment()){
                        b.putBoolean("aws",true);
                    }else {
                        if (mensajeData.get(position).isTablaAnuncio()){
                            b.putBoolean("tablaAnuncio",true);
                        }
                    }
                }

                fragment.setArguments(b);
                FragmentTransaction transaction= ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
            }
        });


        return convertView;
    }





}

