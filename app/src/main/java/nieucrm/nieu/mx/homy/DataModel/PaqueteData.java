package nieucrm.nieu.mx.skola.DataModel;

import java.util.Date;

/**
 * Created by mmac1 on 26/09/2017.
 */

public class PaqueteData {

    private String num,horas,nivel,precio,tipo,prontoPago,alimento,hourE,hourS, nivelId,objectId;
    private Date horaEntrada,horaSalida;
    private boolean taller,nuevo;
    private int horasServicio;

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getHoras() {
        return horas;
    }

    public void setHoras(String horas) {
        this.horas = horas;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getProntoPago() {
        return prontoPago;
    }

    public void setProntoPago(String prontoPago) {
        this.prontoPago = prontoPago;
    }

    public String getAlimento() {
        return alimento;
    }

    public void setAlimento(String alimento) {
        this.alimento = alimento;
    }

    public Date getHoraEntrada() {
        return horaEntrada;
    }

    public void setHoraEntrada(Date horaEntrada) {
        this.horaEntrada = horaEntrada;
    }

    public Date getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(Date horaSalida) {
        this.horaSalida = horaSalida;
    }

    public boolean isTaller() {
        return taller;
    }

    public void setTaller(boolean taller) {
        this.taller = taller;
    }

    public String getNivelId() {
        return nivelId;
    }

    public void setNivelId(String nivelId) {
        this.nivelId = nivelId;
    }

    public boolean isNuevo() {
        return nuevo;
    }

    public void setNuevo(boolean nuevo) {
        this.nuevo = nuevo;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getHorasServicio() {
        return horasServicio;
    }

    public void setHorasServicio(int horasServicio) {
        this.horasServicio = horasServicio;
    }
}
