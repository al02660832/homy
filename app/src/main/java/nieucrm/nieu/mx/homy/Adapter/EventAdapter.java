package nieucrm.nieu.mx.skola.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import nieucrm.nieu.mx.skola.DataModel.EventData;
import nieucrm.nieu.mx.skola.Fragments.EventDetailsFragment;
import nieucrm.nieu.mx.skola.R;


/**
 * Created by Carlos Romero on 10/12/2016.
 */

public class EventAdapter extends BaseAdapter {

    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf";
    String day,month,year,fechaComp,hour;
    Long dateE;


    private Context context;
    LayoutInflater inflater;
    private List<EventData> eventData=null;
    private ArrayList<EventData>arrayList=null;
    private LinearLayout backgroundEvent;
    int usertype=ParseUser.getCurrentUser().getNumber("usertype").intValue();


    public EventAdapter(Context context, List<EventData> eventData) {
        this.context = context;
        this.eventData = eventData;
        try {
            inflater=LayoutInflater.from(context);

        }catch (Exception e){
            Log.d("Error",e.getMessage());
        }
        this.arrayList=new ArrayList<>();
        this.arrayList.addAll(eventData);
    }

    public class ViewHolder{
        TextView textFecha;
        TextView textTitulo;
    }


    @Override
    public int getCount() {
        return eventData.size();
    }

    @Override
    public Object getItem(int position) {
        return eventData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Typeface bold=Typeface.createFromAsset(context.getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(context.getAssets(),avenirMedium);

        final ViewHolder holder;

        if(convertView==null){
            holder=new ViewHolder();
            convertView=inflater.inflate(R.layout.evento_item_layout,null);
            holder.textFecha = (TextView) convertView.findViewById(R.id.textFecha);
            holder.textTitulo = (TextView) convertView.findViewById(R.id.txtTitulo);
            backgroundEvent=(LinearLayout)convertView.findViewById(R.id.backgroundEvent);

            if ( usertype==0||usertype==1){
                backgroundEvent.setBackgroundColor((Color.parseColor("#4A89DC")));
            }

            holder.textFecha.setTypeface(bold);
            holder.textTitulo.setTypeface(medium);

            convertView.setTag(holder);
        }else {
            holder=(ViewHolder)convertView.getTag();
        }

        day=(String)android.text.format.DateFormat.format("dd",eventData.get(position).getFecha());
        month=(String)android.text.format.DateFormat.format("MMM",eventData.get(position).getFecha());
        year=(String)android.text.format.DateFormat.format("yyyy",eventData.get(position).getFecha());
        hour=(String)android.text.format.DateFormat.format("hh:mm a",eventData.get(position).getFecha());

        eventData.get(position).setFec(day+" "+month+" "+year);
        eventData.get(position).setHora(hour);

        holder.textFecha.setText(day+" "+month);
        fechaComp=day+""+month+" "+year;

        holder.textTitulo.setText(eventData.get(position).getNombre());

       dateE=eventData.get(position).getFecha().getTime();
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new EventDetailsFragment();
                Bundle bundle=new Bundle();
                bundle.putString("nombreEvento",eventData.get(position).getNombre());
                bundle.putString("fechaEvento",eventData.get(position).getFec());
                bundle.putString("horaEvento",eventData.get(position).getHora());
                bundle.putString("descripcionEvento",eventData.get(position).getDescripcion());
                bundle.putString("lugarEvento",eventData.get(position).getLugar());
                bundle.putSerializable("fE",eventData.get(position).getFecha());
                bundle.putString("eventId",eventData.get(position).getObjectId());
                bundle.putLong("dateEvento",dateE);

                fragment.setArguments(bundle);
                FragmentTransaction transaction= ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();

            }
        });
        return convertView;
    }

}
