package nieucrm.nieu.mx.skola.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;

import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import info.hoang8f.android.segmented.SegmentedGroup;
import nieucrm.nieu.mx.skola.DataModel.MomentoData;
import nieucrm.nieu.mx.skola.R;


/**
 * Created by Carlos Romero on 26/12/2016.
 */

public class MomentosAdapter extends BaseAdapter {

    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf",avenirDemibold="font/avenir-next-demi-bold.ttf";



    private Context context;
    LayoutInflater inflater;
    String obId;
    private List<MomentoData> momentoData=null;
    private ArrayList<MomentoData> array=null;
    ViewHolder holder;

    public MomentosAdapter(Context context, List<MomentoData> momentoData) {
        this.context = context;
        this.momentoData=momentoData;
        try {
            inflater=LayoutInflater.from(context);

        }catch (Exception e){
            Log.d("Error",e.getMessage());
        }
        this.array=new ArrayList<>();
        this.array.addAll(momentoData);
    }

    public class ViewHolder{
        TextView nombreMomento,desTag,comTag,colTag,merTag,lecheTag,descaTag,durTag,timeTag,horarioTag,functTag,aviTag,pipTag,popTag,comentTag;
        SegmentedGroup segmentedDesayuno,segmentedComida,segmentedColacion,segmentedMerienda;
        RadioButton desNadaRadio,desPocoRadio,desTodoRadio,desMasRadio,
                comNadaRadio,comPocoRadio,comTodoRadio,comMasRadi,
                colNadaRadio,colPocoRadio,colTodoRadio,colMasRadio,
                merNadaRadio,merPocoRadio,merTodoRadio,merMasRadio;
        EditText editMili,editMinu,editHora,editPip,editPop,editComGen;
        Switch switchDurmio,switchAviso;
    }


    @Override
    public int getCount() {
        return momentoData.size();
    }

    @Override
    public Object getItem(int position) {
        return momentoData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    ////////////////////////////////////////////////
    ///Evita que las celdas sean recicladas

    @Override
    public int getViewTypeCount() {

        return getCount();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    ///////////////////////////////////////////////
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final Typeface bold=Typeface.createFromAsset(context.getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(context.getAssets(),avenirMedium);
        Typeface demibold=Typeface.createFromAsset(context.getAssets(),avenirDemibold);


        // TextView nombreEstudiante=(TextView)v.findViewById(R.id.nombreEstudiante);

        if(convertView==null){
            holder=new ViewHolder();
            convertView=inflater.inflate(R.layout.momentos_item_layout,null);

            holder.nombreMomento=(TextView)convertView.findViewById(R.id.nombreMomento);
            holder.desTag=(TextView)convertView.findViewById(R.id.desTag);
            holder.comTag=(TextView)convertView.findViewById(R.id.comTag);
            holder.colTag=(TextView)convertView.findViewById(R.id.colTag);
            holder.merTag=(TextView)convertView.findViewById(R.id.merTag);
            holder.lecheTag=(TextView)convertView.findViewById(R.id.lecheTag);
            holder.descaTag=(TextView)convertView.findViewById(R.id.descaTag);
            holder.durTag=(TextView)convertView.findViewById(R.id.durTag);
            holder.timeTag=(TextView)convertView.findViewById(R.id.timeTag);
            holder.horarioTag=(TextView)convertView.findViewById(R.id.horarioTag);
            holder.functTag=(TextView)convertView.findViewById(R.id.functTag);
            holder.aviTag=(TextView)convertView.findViewById(R.id.aviTag);
            holder.pipTag=(TextView)convertView.findViewById(R.id.pipTag);
            holder.popTag=(TextView)convertView.findViewById(R.id.popTag);
            holder.comentTag=(TextView)convertView.findViewById(R.id.comentTag);

            holder.segmentedColacion=(SegmentedGroup)convertView.findViewById(R.id.segmentedColacion);
            holder.segmentedComida=(SegmentedGroup)convertView.findViewById(R.id.segmentedComida);
            holder.segmentedDesayuno=(SegmentedGroup)convertView.findViewById(R.id.segmentedDesayuno);
            holder.segmentedMerienda=(SegmentedGroup)convertView.findViewById(R.id.segmentedMerienda);

            holder.desNadaRadio=(RadioButton) convertView.findViewById(R.id.desNadaRadio);
            holder.desPocoRadio=(RadioButton) convertView.findViewById(R.id.desPocoRadio);
            holder.desTodoRadio=(RadioButton) convertView.findViewById(R.id.desTodoRadio);
            holder.desMasRadio=(RadioButton) convertView.findViewById(R.id.desMasRadio);
            holder.comNadaRadio=(RadioButton) convertView.findViewById(R.id.comNadaRadio);
            holder.comPocoRadio=(RadioButton) convertView.findViewById(R.id.comPocoRadio);
            holder.comTodoRadio=(RadioButton) convertView.findViewById(R.id.comTodoRadio);
            holder.comMasRadi=(RadioButton) convertView.findViewById(R.id.comMasRadio);
            holder.colNadaRadio=(RadioButton) convertView.findViewById(R.id.colNadaRadio);
            holder.colPocoRadio=(RadioButton) convertView.findViewById(R.id.colPocoRadio);
            holder.colTodoRadio=(RadioButton) convertView.findViewById(R.id.colTodoRadio);
            holder.colMasRadio=(RadioButton) convertView.findViewById(R.id.colMasRadio);
            holder.merNadaRadio=(RadioButton) convertView.findViewById(R.id.merNadaRadio);
            holder.merPocoRadio=(RadioButton) convertView.findViewById(R.id.merPocoRadio);
            holder.merTodoRadio=(RadioButton) convertView.findViewById(R.id.merTodoRadio);
            holder.merMasRadio=(RadioButton) convertView.findViewById(R.id.merMasRadio);

            holder.editMili=(EditText)convertView.findViewById(R.id.editMili);
            holder.editMinu=(EditText)convertView.findViewById(R.id.editMinu);
            holder.editHora=(EditText)convertView.findViewById(R.id.editHora);
            holder.editPip=(EditText)convertView.findViewById(R.id.editPip);
            holder.editPop=(EditText)convertView.findViewById(R.id.editPop);
            holder.editComGen=(EditText)convertView.findViewById(R.id.editComGen);

            holder.switchAviso=(Switch)convertView.findViewById(R.id.switchAviso);
            holder.switchDurmio=(Switch)convertView.findViewById(R.id.switchDurmio);

            if (position%2==0){
                convertView.setBackgroundColor(ContextCompat.getColor(context,R.color.purple_moment));
            }



            holder.nombreMomento.setTypeface(medium);
            holder.nombreMomento.setTypeface(medium);
            holder.desTag.setTypeface(medium);
            holder.comTag.setTypeface(medium);
            holder.colTag.setTypeface(medium);
            holder.merTag.setTypeface(medium);
            holder.lecheTag.setTypeface(medium);
            holder.descaTag.setTypeface(medium);
            holder.durTag.setTypeface(medium);
            holder.timeTag.setTypeface(medium);
            holder.horarioTag.setTypeface(medium);
            holder.functTag.setTypeface(medium);
            holder.aviTag.setTypeface(medium);
            holder.pipTag.setTypeface(medium);
            holder.popTag.setTypeface(medium);
            holder.comentTag.setTypeface(medium);

            holder.desNadaRadio.setTypeface(medium);
            holder.desPocoRadio.setTypeface(medium);
            holder.desTodoRadio.setTypeface(medium);
            holder.desMasRadio.setTypeface(medium);
            holder.comNadaRadio.setTypeface(medium);
            holder.comPocoRadio.setTypeface(medium);
            holder.comTodoRadio.setTypeface(medium);
            holder.comMasRadi.setTypeface(medium);
            holder.colNadaRadio.setTypeface(medium);
            holder.colPocoRadio.setTypeface(medium);
            holder.colTodoRadio.setTypeface(medium);
            holder.colMasRadio.setTypeface(medium);
            holder.merNadaRadio.setTypeface(medium);
            holder.merPocoRadio.setTypeface(medium);
            holder.merTodoRadio.setTypeface(medium);
            holder.merMasRadio.setTypeface(medium);

            holder.editMili.setTypeface(medium);
            holder.editMinu.setTypeface(medium);
            holder.editHora.setTypeface(medium);
            holder.editPip.setTypeface(medium);
            holder.editPop.setTypeface(medium);
            holder.editComGen.setTypeface(medium);

            holder.switchDurmio.setChecked(false);
            holder.switchAviso.setChecked(false);

            holder.nombreMomento.setText(momentoData.get(position).getNombre());
            holder.segmentedDesayuno.setTintColor(ContextCompat.getColor(context,R.color.purple_access),ContextCompat.getColor(context,R.color.white));
            holder.segmentedComida.setTintColor(ContextCompat.getColor(context,R.color.purple_access),ContextCompat.getColor(context,R.color.white));
            holder.segmentedColacion.setTintColor(ContextCompat.getColor(context,R.color.purple_access),ContextCompat.getColor(context,R.color.white));
            holder.segmentedMerienda.setTintColor(ContextCompat.getColor(context,R.color.purple_access),ContextCompat.getColor(context,R.color.white));

            holder.segmentedDesayuno.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    switch (checkedId){
                        case R.id.desPocoRadio:
                            momentoData.get(position).setDesayuno(holder.desPocoRadio.getText().toString());
                            break;
                        case R.id.desNadaRadio:
                            momentoData.get(position).setDesayuno(holder.desNadaRadio.getText().toString());
                            break;
                        case R.id.desTodoRadio:
                            momentoData.get(position).setDesayuno(holder.desTodoRadio.getText().toString());
                            break;
                        case R.id.desMasRadio:
                            momentoData.get(position).setDesayuno(holder.desMasRadio.getText().toString());
                            break;
                    }
                }
            });

            holder.segmentedComida.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    switch (checkedId){
                        case R.id.comPocoRadio:
                            momentoData.get(position).setComida(holder.comPocoRadio.getText().toString());
                            break;
                        case R.id.comNadaRadio:
                            momentoData.get(position).setComida(holder.comNadaRadio.getText().toString());
                            break;
                        case R.id.comTodoRadio:
                            momentoData.get(position).setComida(holder.comTodoRadio.getText().toString());
                            break;
                        case R.id.comMasRadio:
                            momentoData.get(position).setComida(holder.comMasRadi.getText().toString());
                            break;
                    }
                }
            });

            holder.segmentedColacion.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    switch (checkedId){
                        case R.id.colPocoRadio:
                            momentoData.get(position).setColacion(holder.colPocoRadio.getText().toString());
                            break;
                        case R.id.colNadaRadio:
                            momentoData.get(position).setColacion(holder.colNadaRadio.getText().toString());
                            break;
                        case R.id.colTodoRadio:
                            momentoData.get(position).setColacion(holder.colTodoRadio.getText().toString());
                            break;
                        case R.id.colMasRadio:
                            momentoData.get(position).setColacion(holder.colMasRadio.getText().toString());
                            break;
                    }
                }
            });

            holder.segmentedMerienda.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    switch (checkedId){
                        case R.id.merPocoRadio:
                            momentoData.get(position).setMerienda(holder.merPocoRadio.getText().toString());
                            break;
                        case R.id.merNadaRadio:
                            momentoData.get(position).setMerienda(holder.merNadaRadio.getText().toString());
                            break;
                        case R.id.merTodoRadio:
                            momentoData.get(position).setMerienda(holder.merTodoRadio.getText().toString());
                            break;
                        case R.id.merMasRadio:
                            momentoData.get(position).setMerienda(holder.merMasRadio.getText().toString());
                            break;
                    }
                }
            });

            holder.switchAviso.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked){
                        momentoData.get(position).setAviso(true);
                    }else {
                        momentoData.get(position).setAviso(false);
                    }
                }
            });

            holder.switchDurmio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked){
                        momentoData.get(position).setDurmio(true);
                    }else {
                        momentoData.get(position).setDurmio(false);
                    }
                }
            });

            holder.editMili.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    momentoData.get(position).setMililitro(s.toString());
                }
            });

            holder.editComGen.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    momentoData.get(position).setComentario(s.toString());
                }
            });

            holder.editMinu.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    momentoData.get(position).setTiempo(s.toString());
                }
            });

            holder.editHora.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    momentoData.get(position).setHora(s.toString());
                }
            });

            holder.editPip.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    momentoData.get(position).setPipi(s.toString());
                }
            });

            holder.editPop.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    momentoData.get(position).setPopo(s.toString());
                }
            });

            convertView.setTag(holder);

        }else {
            holder=(ViewHolder)convertView.getTag();
        }

        obId=ParseUser.getCurrentUser().getObjectId();



        return convertView;
    }





}

