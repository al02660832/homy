package nieucrm.nieu.mx.skola.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import nieucrm.nieu.mx.skola.Adapter.PaqueteAdapter;
import nieucrm.nieu.mx.skola.DataModel.PaqueteData;
import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class PaquetesFragment extends Fragment {

    ListView listaPaquete;
    ParseQuery<ParseObject>paquete;
    PaqueteData data;
    ArrayList<PaqueteData>array;
    PaqueteAdapter adapter;
    ProgressBar progressPac;
    String escuelaId;

    Fragment fragment;
    Bundle b;
    FragmentTransaction transaction;

    public PaquetesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_paquetes, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        listaPaquete=(ListView)view.findViewById(R.id.listaPaquete);
        progressPac=(ProgressBar)view.findViewById(R.id.progressPac);

        getPaquetes();

    }

    private void getPaquetes(){
        array=new ArrayList<>();
        escuelaId=((ParseApplication)getActivity().getApplication()).getEscuelaId();
        ParseObject escuela=ParseObject.createWithoutData("Escuela",escuelaId);

        paquete=ParseQuery.getQuery("Paquetes").include("nivel").include("alimentos").orderByAscending("createdAt");
        paquete.whereEqualTo("escuela",escuela);
        paquete.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                progressPac.setVisibility(View.GONE);
                listaPaquete.setVisibility(View.VISIBLE);
                if (e==null){

                    for (ParseObject o:objects){
                        data=new PaqueteData();
                        data.setObjectId(o.getObjectId());
                        if (o.getParseObject("nivel")==null){
                            data.setNivel("");
                            data.setNivelId("");
                        }else {
                            ParseObject niv=o.getParseObject("nivel");
                            data.setNivel(niv.getString("nombre"));
                            data.setNivelId(niv.getObjectId());

                        }

                        data.setNuevo(false);

                        data.setNum(o.getString("nombre"));
                        data.setHoras(o.getString("horario"));
                        if (o.getDate("horaEntrada")!=null){
                            data.setHoraEntrada(o.getDate("horaEntrada"));

                        }else {
                            Calendar calendar=Calendar.getInstance();
                            data.setHoraEntrada(calendar.getTime());

                        }

                        if (o.getDate("horaSalida")!=null){
                            data.setHoraSalida(o.getDate("horaSalida"));

                        }else {
                            Calendar calendar=Calendar.getInstance();
                            data.setHoraSalida(calendar.getTime());

                        }

                        data.setTipo(o.getString("tipo"));
                        data.setHorasServicio(o.getInt("horasServicio"));
                        data.setPrecio(o.getString("precio"));
                        data.setTaller(o.getBoolean("tallerVespertino"));
                        data.setProntoPago(o.getString("prontoPago"));

                        if (o.getList("alimentos")==null){
                            data.setAlimento("");
                        }else {
                            List<ParseObject>list=o.getList("alimentos");
                            StringBuilder a=new StringBuilder();
                            for (int i=0;i<list.size();i++){
                                try {
                                    if (i == list.size() - 1) {
                                        a.append(list.get(i).getString("Nombre"));
                                    } else {
                                        a.append(list.get(i).getString("Nombre")).append(", ");
                                        //Error

                                    }
                                }catch (Exception e1){
                                    Log.d("Error",e1.getMessage());
                                }
                                data.setAlimento(a.toString());
                            }
                        }


                        array.add(data);
                    }
                    adapter=new PaqueteAdapter(getContext(),array);
                    listaPaquete.setAdapter(adapter);
                }else {
                    Toast.makeText(getContext(),"Error",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.crear_taller,menu);
        inflater.inflate(R.menu.crear_paquete,menu);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.crearPac:
                fragment=new NewPaqueteFragment();
                b=new Bundle();
                b.putString("num",String.valueOf(array.size()+1));
                fragment.setArguments(b);
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                break;
            case R.id.crearTall:
                fragment=new TallerFragment();
                b=new Bundle();
                //b.putString("num",String.valueOf(array.size()+1));
                //fragment.setArguments(b);
                transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                break;
        }
        return super.onOptionsItemSelected(item);

    }


}

