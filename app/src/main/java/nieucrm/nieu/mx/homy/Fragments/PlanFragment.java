package nieucrm.nieu.mx.skola.Fragments;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

import mx.openpay.android.Openpay;
import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class PlanFragment extends Fragment {
    TextView txtPlan,cargoPago,cargoPago2,planSkola,planMkt, precioMkt, precioSkola;
    ImageButton btnSkolaPlan,btnMktPlan;
    String opIdSkola,opIdMkt;
    Fragment fragment;
    ProgressBar progressPlan;
    LinearLayout layoutPlanes;
    String amountSkola,amountMkt;
    Boolean ivaSkola,ivaMkt;
    String escuelaId;
    ParseObject escuela;
    String nombreMkt, nombreSkola;

    String avenirBold = "font/avenir-next-bold.ttf", avenirMedium = "font/avenir-next-medium.ttf",avenirDemi = "font/avenir-next-demi-bold.ttf";
    List<String>planes;
    ArrayList<ParseObject>planBtn;
    /*
    Se van a mostrar dos botones: Skola App y Skola App + Marketing
    Para tener los IDs de las subscripciones OpenPay se hace query a la tabla OPSubscripcionPlan.
    Esa tabla tiene una columna "openPaySubscripcionId".
    Cada ID se asigna a un botón.
    El botón que el usuario seleccione, se tendrá que pasar a la siguiente pantalla como objeto
     */

    public PlanFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_plan, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        txtPlan=(TextView)view.findViewById(R.id.txtPlan);
        cargoPago=(TextView)view.findViewById(R.id.cargoPago);
        cargoPago2=(TextView)view.findViewById(R.id.cargoPago2);
        planSkola=(TextView)view.findViewById(R.id.planSkola);
        planMkt=(TextView)view.findViewById(R.id.planMkt);
        btnSkolaPlan=(ImageButton)view.findViewById(R.id.btnSkolaPlan);
        btnMktPlan=(ImageButton)view.findViewById(R.id.btnMktPlan);
        progressPlan=(ProgressBar)view.findViewById(R.id.progressPlan);
        layoutPlanes=(LinearLayout)view.findViewById(R.id.layoutPlanes);
        precioSkola=(TextView)view.findViewById(R.id.skolaPrecio);
        precioMkt=(TextView)view.findViewById(R.id.mktPrecio);
        setTypeface();
        getCurrentEscuela();

    }

    public void setTypeface(){
        Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), avenirBold);
        Typeface medium = Typeface.createFromAsset(getActivity().getAssets(), avenirMedium);
        Typeface demi=Typeface.createFromAsset(getActivity().getAssets(), avenirDemi);

        txtPlan.setTypeface(bold);
        cargoPago.setTypeface(medium);
        cargoPago2.setTypeface(medium);
        planSkola.setTypeface(demi);
        planMkt.setTypeface(demi);
        precioMkt.setTypeface(bold);
        precioSkola.setTypeface(bold);



    }

    public void getCurrentEscuela(){
        escuelaId=((ParseApplication)getActivity().getApplication()).getEscuelaId();
        escuela=ParseObject.createWithoutData("Escuela",escuelaId);
        ParseQuery<ParseObject>school=ParseQuery.getQuery("Escuela");
        school.whereEqualTo("objectId",escuelaId);
        school.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (object!=null){
                    planes=object.getList("planesSubscripcionActivados");
                    getSubscripcionPlan();
                }
            }
        });
    }

    public void getSubscripcionPlan(){
        /*
        Para tener los IDs de las subscripciones
        OpenPay se hace query a la tabla OPSubscripcionPlan.
        Esa tabla tiene una columna "openPaySubscripcionId".
        Cada ID se asigna a un botón.
        El botón que el usuario seleccione, se tendrá que pasar a la siguiente pantalla como objeto
         */
        planBtn=new ArrayList<>();
        ParseQuery<ParseObject> op=ParseQuery.getQuery("OPSubscripcionPlan");
        op.whereContainedIn("objectId",planes);
        op.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                progressPlan.setVisibility(View.GONE);
                layoutPlanes.setVisibility(View.VISIBLE);
                if (e==null){
                    planBtn.addAll(objects);
                    opIdSkola=planBtn.get(0).getString("openPaySubscripcionId");
                    amountSkola=planBtn.get(0).getString("amount");
                    ivaSkola=planBtn.get(0).getBoolean("IVA");
                    nombreSkola=planBtn.get(0).getString("nombre");
                    precioSkola.setText("$"+amountSkola);
                    planSkola.setText(nombreSkola);


                    opIdMkt=planBtn.get(1).getString("openPaySubscripcionId");
                    amountMkt=planBtn.get(1).getString("amount");
                    ivaMkt=planBtn.get(1).getBoolean("IVA");
                    nombreMkt=planBtn.get(1).getString("nombre");
                    precioMkt.setText("$"+amountMkt);
                    planMkt.setText(nombreMkt);

                    Log.e("Lista",String.valueOf(objects.size()));
                    setListener();

                }else {
                    Toast.makeText(getActivity(),"Error, intente de nuevo",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void setListener(){
        btnSkolaPlan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment=new RegisterFragment();

                Bundle b=new Bundle();
                b.putString("openpayId",opIdSkola);
                b.putString("amount",amountSkola);
                b.putBoolean("iva",ivaSkola);
                fragment.setArguments(b);
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
            }
        });

        btnMktPlan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment=new RegisterFragment();
                Bundle b=new Bundle();
                b.putString("openpayId",opIdMkt);
                b.putString("amount",amountMkt);
                b.putBoolean("iva",ivaMkt);
                fragment.setArguments(b);
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
            }
        });
    }


}
