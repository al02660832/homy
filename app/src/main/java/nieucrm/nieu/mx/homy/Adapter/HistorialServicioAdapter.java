package nieucrm.nieu.mx.skola.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import nieucrm.nieu.mx.skola.DataModel.ServiciosData;
import nieucrm.nieu.mx.skola.Fragments.ServicesFragment;
import nieucrm.nieu.mx.skola.Fragments.ServiciosDetailFragment;
import nieucrm.nieu.mx.skola.R;


/**
 * Created by Carlos Romero on 10/12/2016.
 */

public class HistorialServicioAdapter extends BaseAdapter {

    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf",demibold="font/avenir-next-demi-bold.ttf";
    String fecha;

    private Context context;
    LayoutInflater inflater;
    private List<ServiciosData> serviciosData=null;
    private ArrayList<ServiciosData>arrayList=null;


    public HistorialServicioAdapter(Context context, List<ServiciosData> serviciosData) {
        this.context = context;
        this.serviciosData=serviciosData;
        try {
            inflater=LayoutInflater.from(context);

        }catch (Exception e){
            Log.d("Error",e.getMessage());
        }
        this.arrayList=new ArrayList<>();
        this.arrayList.addAll(serviciosData);
    }

    public class ViewHolder{
        TextView txtServicio,txtNombreServicio,fechaServicio,fechaServ,txtAlumnoServicio,nombreAlumnoS,estadoServicio,cancelarServicio;

    }


    @Override
    public int getCount() {
        return serviciosData.size();
    }

    @Override
    public Object getItem(int position) {
        return serviciosData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Typeface bold=Typeface.createFromAsset(context.getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(context.getAssets(),avenirMedium);
        Typeface dem=Typeface.createFromAsset(context.getAssets(),demibold);

        ViewHolder holder;
       // TextView nombreEstudiante=(TextView)v.findViewById(R.id.nombreEstudiante);

        if(convertView==null){
            holder=new ViewHolder();
            convertView=inflater.inflate(R.layout.historial_servicio_item_layout,null);
            holder.txtServicio = (TextView) convertView.findViewById(R.id.txtServicio);
            holder.txtNombreServicio = (TextView) convertView.findViewById(R.id.txtNombreServicio);
            holder.fechaServicio = (TextView) convertView.findViewById(R.id.fechaServicio);
            holder.fechaServ=(TextView)convertView.findViewById(R.id.fechaServ);
            holder.txtAlumnoServicio=(TextView)convertView.findViewById(R.id.txtAlumnoServicio);
            holder.nombreAlumnoS=(TextView)convertView.findViewById(R.id.nombreAlumnoS);
            holder.estadoServicio=(TextView)convertView.findViewById(R.id.estadoServicio);
            holder.cancelarServicio=(TextView)convertView.findViewById(R.id.cancelarServicio);



            holder.txtServicio.setTypeface(dem);
            holder.txtNombreServicio.setTypeface(dem);
            holder.fechaServicio.setTypeface(dem);
            holder.txtAlumnoServicio.setTypeface(dem);

            holder.fechaServ.setTypeface(medium);
            holder.nombreAlumnoS.setTypeface(medium);
            holder.estadoServicio.setTypeface(medium);
            holder.cancelarServicio.setTypeface(medium);






            convertView.setTag(holder);
        }else {
            holder=(ViewHolder)convertView.getTag();
        }

        SimpleDateFormat format = new SimpleDateFormat("dd 'de' MMMM",new Locale("es"));
        fecha=format.format(serviciosData.get(position).getDate());
        serviciosData.get(position).setFecha(fecha);

        switch (serviciosData.get(position).getStatus()){
            case 0:
                serviciosData.get(position).setEstado("Solicitado");
                break;
            case 1:
                serviciosData.get(position).setEstado("En Progreso");
                break;
            case 2:
                serviciosData.get(position).setEstado("Completado");
                break;
            case 3:
                serviciosData.get(position).setEstado("Pagado");
                break;
            case 8:
                serviciosData.get(position).setEstado("Rechazado");
                break;
            case 9:
                serviciosData.get(position).setEstado("Cancelado");
                break;
        }

        holder.txtNombreServicio.setText(serviciosData.get(position).getNombre());
        holder.fechaServ.setText(serviciosData.get(position).getFecha());
        holder.nombreAlumnoS.setText(serviciosData.get(position).getAlumno());
        holder.estadoServicio.setText(serviciosData.get(position).getEstado());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new ServicesFragment();
                Bundle bundle=new Bundle();
                bundle.putString("nombre",serviciosData.get(position).getNombre());
                bundle.putString("precio",serviciosData.get(position).getPrecio());
                bundle.putString("descripcion",serviciosData.get(position).getDescripcion());
                bundle.putString("objectid",serviciosData.get(position).getObjectId());
                bundle.putString("estado",serviciosData.get(position).getEstado());
                bundle.putString("comentarios",serviciosData.get(position).getComentarios());
                bundle.putString("alumnoId",serviciosData.get(position).getAlumnoId());
                bundle.putString("alumno",serviciosData.get(position).getAlumno());
                bundle.putString("fecha",serviciosData.get(position).getFecha());
                bundle.putInt("status",serviciosData.get(position).getStatus());


                fragment.setArguments(bundle);
                FragmentTransaction transaction= ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
            }
        });

        return convertView;


    }

}
