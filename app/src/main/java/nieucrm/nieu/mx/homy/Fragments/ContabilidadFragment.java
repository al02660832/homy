package nieucrm.nieu.mx.skola.Fragments;


import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class ContabilidadFragment extends Fragment {

    ParseQuery<ParseObject>service;

    int suma=0,resultado=0,cobrar=0;

    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf",demibold="font/avenir-next-demi-bold.ttf";

    ImageButton cerrar;
    ArrayList<Integer>serviciosStatus=new ArrayList<>();

    TextView contabilidadTag,ingresosTag,txtIngresos,cobradoTag,txtCobrado,porCobrarTag,txtPorCobrar;

    String escuelaId;
    ParseObject escuela;

    Activity a;

    public ContabilidadFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            a=(Activity) context;
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_contabilidad, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        contabilidadTag=(TextView)view.findViewById(R.id.contabilidadTag);
        ingresosTag=(TextView)view.findViewById(R.id.ingresosTag);
        txtIngresos=(TextView)view.findViewById(R.id.txtIngresos);
        cobradoTag=(TextView)view.findViewById(R.id.cobradoTag);
        txtCobrado=(TextView)view.findViewById(R.id.txtCobrado);
        porCobrarTag=(TextView)view.findViewById(R.id.porCobrarTag);
        txtPorCobrar=(TextView)view.findViewById(R.id.txtPorCobrar);

        cerrar=(ImageButton)view.findViewById(R.id.cerrar);

        cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();

            }
        });

        bindTypeface();
        getNumbers();

    }


    public void getNumbers(){
        serviciosStatus.add(2);
        serviciosStatus.add(3);

        escuelaId=((ParseApplication)a.getApplication()).getEscuelaId();
        escuela=ParseObject.createWithoutData("Escuela",escuelaId);

        ParseQuery<ParseObject> servicios=ParseQuery.getQuery("Servicio");
        servicios.whereEqualTo("escuela",escuela);

        service=ParseQuery.getQuery("ServicioSolicitado");
        service.include("estudiante").include("servicio").include("personaAutorizada");
        service.include("personaAutorizada.escuela");
        service.whereMatchesQuery("servicio",servicios);
        service.whereContainedIn("status",serviciosStatus);
        service.orderByDescending("fechaSolicitud");

        service.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e==null){

                    double ingresos=0;
                    double cobrado=0;
                    double porCobrar=0;

                    for (ParseObject o:objects){
                        ParseObject service=o.getParseObject("servicio");
                        ParseUser user=o.getParseUser("personaAutorizada");
                        int status=o.getNumber("status").intValue();
                        double servicioPrecio=service.getNumber("precio").doubleValue();

                        if (status==2)
                        {
                          porCobrar=porCobrar+servicioPrecio;
                        }else {
                            cobrado=cobrado+servicioPrecio;
                        }



                    }
                    ingresos=cobrado+porCobrar;

                    txtIngresos.setText("$"+String.valueOf(ingresos));
                    txtCobrado.setText("$"+String.valueOf(cobrado));
                    txtPorCobrar.setText("$"+String.valueOf(porCobrar));


                }else {
                    Log.d("Error",e.getMessage());
                }
            }
        });

    }

    public void bindTypeface(){
        Typeface bold=Typeface.createFromAsset(a.getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(a.getAssets(),avenirMedium);
        Typeface dem=Typeface.createFromAsset(a.getAssets(),demibold);

        txtCobrado.setTypeface(dem);
        txtPorCobrar.setTypeface(dem);
        txtIngresos.setTypeface(dem);
        cobradoTag.setTypeface(dem);
        contabilidadTag.setTypeface(dem);
        ingresosTag.setTypeface(dem);
        porCobrarTag.setTypeface(dem);

    }

}
