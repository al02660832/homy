package nieucrm.nieu.mx.skola.Activity;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.parse.ParseUser;

import nieucrm.nieu.mx.skola.Fragments.HomeFragment;
import nieucrm.nieu.mx.skola.Fragments.TeacherFragment;
import nieucrm.nieu.mx.skola.Fragments.VisitantesFragment;
import nieucrm.nieu.mx.skola.R;


public class Guest extends AppCompatActivity {

    Fragment fragment;
    int usertype;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Full screen is set for the Window
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();

        setContentView(R.layout.activity_guest);

        if(ParseUser.getCurrentUser()==null){
            fragment=new VisitantesFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.content_guest,fragment,"fragment").commit();
        }else {

            finish();
            startActivity(new Intent(getApplicationContext(),Home.class));
        }



    }
}
