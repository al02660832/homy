package nieucrm.nieu.mx.skola.Fragments;


import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.annotation.StringDef;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.opencsv.CSVWriter;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class ReportesFragment extends Fragment {

    TextView fechaReporteI,txtFechaReporteI,fechaReporteF,txtFechaReporteF,alumnosReportes,txtAlumnosR;
    Button btnReportesAccesos;

    Date inicio,limits;
    String escuelaId;
    ParseObject escuela;

    String avenirBold = "font/avenir-next-bold.ttf", avenirMedium = "font/avenir-next-medium.ttf",avenirDemi="font/avenir-next-demi-bold.ttf";

    DatePickerDialog.OnDateSetListener date,fin;
    Calendar myCalendar = Calendar.getInstance();
    Calendar calendar=Calendar.getInstance();
    ParseQuery<ParseObject> accesosReport;
    ArrayList<ParseObject>arrayAccesos=new ArrayList<>();
    ArrayList<String []>lista=new ArrayList<>();


    String baseDir = android.os.Environment.getExternalStorageDirectory().getAbsolutePath();
    String fileName = "ReportesAccesos.csv";
    String filePath = baseDir + File.separator + fileName;
    File f = new File(filePath);

    ArrayList<ParseObject>estudiantes;
    ArrayList<String>nombres;
    ArrayList<String> mSelectedItems;
    ArrayList<String>idEstudiante=new ArrayList<>();
    String [] n;

    ProgressDialog dialog;


    String alumno,autorizada,parentesco,fecha,hora,puntualidad,horario,acceso;
    int color;

    int limit,skip;

    public ReportesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_reportes, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);

        fechaReporteI=(TextView)v.findViewById(R.id.fechaReporteI);
        txtFechaReporteI=(TextView)v.findViewById(R.id.txtFechaReporteI);
        fechaReporteF=(TextView)v.findViewById(R.id.fechaReporteF);
        txtFechaReporteF=(TextView)v.findViewById(R.id.txtFechaReporteF);
        alumnosReportes=(TextView)v.findViewById(R.id.alumnosReportes);
        txtAlumnosR=(TextView)v.findViewById(R.id.txtAlumnosR);

        btnReportesAccesos=(Button) v.findViewById(R.id.btnReportesAccesos);


        setType();
        fechaInicio();
        fechaLimite();

        btnReportesAccesos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reporteAccesos();
            }
        });

        txtAlumnosR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog=new ProgressDialog(getContext());
                dialog.setTitle("Cargando");
                dialog.setMessage("Por favor espere...");
                dialog.setCancelable(false);
                dialog.show();
                setAlumnos();
            }
        });


    }

    public void fechaInicio(){
        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();

            }

        };

        txtFechaReporteI.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();


            }
        });



    }

    public void fechaLimite(){
        fin = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLimit();

            }

        };

        txtFechaReporteF.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(getActivity(), fin, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();


            }
        });
    }

    public void updateLabel() {
        String myFormat = "dd/MMM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        txtFechaReporteI.setText(sdf.format(myCalendar.getTime()));
    }

    public void updateLimit() {
        String myFormat = "dd/MMM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        txtFechaReporteF.setText(sdf.format(calendar.getTime()));
    }


    public void reporteAccesos(){

        limit=1000;
        skip=0;
        inicio=myCalendar.getTime();
        limits=calendar.getTime();

        ParseQuery<ParseUser> q = ParseUser.getQuery();
        q.whereEqualTo("usertype",2);





        accesosReport=ParseQuery.getQuery("Acceso");
        accesosReport.setLimit(limit);
        accesosReport.setSkip(skip);
        accesosReport.include("student");
        accesosReport.include("user");
        if (mSelectedItems!=null){
            accesosReport.whereContainedIn("student",idEstudiante);
        }
        accesosReport.whereMatchesQuery("user",q);
        accesosReport.whereEqualTo("puntualidad",0);
        accesosReport.whereGreaterThanOrEqualTo("createdAt",inicio);
        accesosReport.whereLessThan("createdAt",limits);

        accesosReport.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(final List<ParseObject> objects, ParseException e) {
                if (e==null){
                    arrayAccesos.addAll(objects);
                    if (objects.size()==limit){
                        skip+=limit;
                        accesosReport.setSkip(skip);
                        accesosReport.findInBackground(new FindCallback<ParseObject>() {
                            @Override
                            public void done(List<ParseObject> objects2, ParseException e) {
                                if (e==null){
                                    arrayAccesos.addAll(objects2);
                                    if (objects2.size()==limit) {
                                        skip+=limit;
                                        accesosReport.setSkip(skip);
                                        accesosReport.findInBackground(new FindCallback<ParseObject>() {
                                            @Override
                                            public void done(List<ParseObject> objects3, ParseException e) {
                                                if (e==null){
                                                    arrayAccesos.addAll(objects3);
                                                    if (objects3.size()==limit){
                                                        skip+=limit;
                                                        accesosReport.setSkip(skip);
                                                        accesosReport.findInBackground(new FindCallback<ParseObject>() {
                                                            @Override
                                                            public void done(List<ParseObject> objects4, ParseException e) {
                                                                if (e==null){
                                                                    arrayAccesos.addAll(objects4);
                                                                    if (objects4.size()==limit){
                                                                        skip+=limit;
                                                                        accesosReport.setSkip(skip);
                                                                        accesosReport.findInBackground(new FindCallback<ParseObject>() {
                                                                            @Override
                                                                            public void done(List<ParseObject> objects5, ParseException e) {
                                                                                if (e==null){
                                                                                    arrayAccesos.addAll(objects5);
                                                                                    if (objects5.size()==limit){
                                                                                        skip+=limit;
                                                                                        accesosReport.setSkip(skip);
                                                                                        accesosReport.findInBackground(new FindCallback<ParseObject>() {
                                                                                            @Override
                                                                                            public void done(List<ParseObject> objects6, ParseException e) {
                                                                                                if (e==null){
                                                                                                    arrayAccesos.addAll(objects6);
                                                                                                    if (objects6.size()==limit){
                                                                                                        skip+=limit;
                                                                                                        accesosReport.setSkip(skip);
                                                                                                        accesosReport.findInBackground(new FindCallback<ParseObject>() {
                                                                                                            @Override
                                                                                                            public void done(List<ParseObject> objects7, ParseException e) {
                                                                                                                if (e==null){
                                                                                                                    arrayAccesos.addAll(objects7);
                                                                                                                    if (objects7.size()==limit){
                                                                                                                        skip+=limit;
                                                                                                                        accesosReport.setSkip(skip);
                                                                                                                        accesosReport.findInBackground(new FindCallback<ParseObject>() {
                                                                                                                            @Override
                                                                                                                            public void done(List<ParseObject> objects8, ParseException e) {
                                                                                                                                if (e==null){
                                                                                                                                    arrayAccesos.addAll(objects8);
                                                                                                                                    if (objects8.size()==limit){
                                                                                                                                        Log.d("Numero de accesos","Mas de 9mil");
                                                                                                                                    }else {
                                                                                                                                        //Crear documento
                                                                                                                                        createCsvDocument();
                                                                                                                                        //Menos de 8
                                                                                                                                    }
                                                                                                                                }else {
                                                                                                                                    Log.d("Error","Error query 8 reportes");

                                                                                                                                }

                                                                                                                            }
                                                                                                                        });
                                                                                                                    }else {
                                                                                                                        //Crear documento
                                                                                                                        createCsvDocument();
                                                                                                                        //Menos de 7
                                                                                                                    }
                                                                                                                }else {
                                                                                                                    Log.d("Error","Error query 7 reportes");

                                                                                                                }

                                                                                                            }
                                                                                                        });
                                                                                                    }else {
                                                                                                        //Crear documento
                                                                                                        createCsvDocument();
                                                                                                        //Menos de 6
                                                                                                    }
                                                                                                }else {
                                                                                                    Log.d("Error","Error query 6 reportes");

                                                                                                }

                                                                                            }
                                                                                        });
                                                                                    }else {
                                                                                        //Crear documento
                                                                                        createCsvDocument();
                                                                                        //Menos de 5
                                                                                    }
                                                                                }else {
                                                                                    Log.d("Error","Error query 5 reportes");

                                                                                }

                                                                            }
                                                                        });
                                                                    }else {
                                                                        //Crear documento
                                                                        createCsvDocument();
                                                                        //Menos de 4
                                                                    }
                                                                }else {
                                                                    Log.d("Error","Error query 4 reportes");
                                                                }
                                                            }
                                                        });
                                                    }else {
                                                        //Crear documento
                                                        createCsvDocument();
                                                        //Menos de 3

                                                    }
                                                }else {
                                                    Log.d("Error","Error query 3 reportes");
                                                    Toast.makeText(getContext(),"Error query 3 reportes",Toast.LENGTH_SHORT).show();
                                                }

                                            }
                                        });
                                    }else {
                                        //Crear documento

                                        createCsvDocument();
                                        //Menos de 2
                                    }
                                }else {
                                    Log.d("Error","Error query 2 reportes");
                                    Toast.makeText(getContext(),"Error query 2 reportes",Toast.LENGTH_SHORT).show();
                                }

                            }
                        });
                    }else {
                        //Crear documentos
                        createCsvDocument();
                        //Menos de 1000
                    }
                }else {
                    Toast.makeText(getContext(),"Error query reportes",Toast.LENGTH_SHORT).show();

                    Log.d("Error","Error query reportes");
                }
            }
        });
        


    }

    public void createCsvDocument(){

        CSVWriter writer=null;

        if(f.exists() && !f.isDirectory()){
            FileWriter mFileWriter = null;
            try {
                mFileWriter = new FileWriter(filePath , true);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            writer = new CSVWriter(mFileWriter);
        }
        else {
            try {
                writer = new CSVWriter(new FileWriter(filePath));
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        String data [] = new String[] {"Alumno","Persona Autorizada","Parentesco","Fecha","Hora", "Puntualidad","Horario del alumno", "Acceso","\r\n"};
        writer.writeNext(data);

        try {
            for (ParseObject o: arrayAccesos){

                ParseObject estObjects=o.getParseObject("student");
                ParseUser userObject=o.getParseUser("user");
                if(estObjects==null){
                    alumno="Alumno";
                    horario="00:00-00:00";

                }else{
                    alumno=estObjects.getString("NOMBRE")+" "+estObjects.getString("APELLIDO");

                    horario=estObjects.getString("HORARIO");
                }

                if(userObject==null){
                    autorizada="Padre";
                    parentesco="Parentesco";
                }else{
                    autorizada=userObject.getString("nombre")+" "+userObject.getString("apellidos");
                    parentesco=userObject.getString("parentesco");

                }

                Date p =o.getCreatedAt();
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                try {
                    p = sdf.parse(sdf.format(p));
                } catch (java.text.ParseException e1) {
                    e1.printStackTrace();
                }
                sdf.setTimeZone(TimeZone.getTimeZone("CST"));

                fecha=sdf.format(p);

                DateFormat df=new SimpleDateFormat("HH:mm:ss",Locale.US);
                Date d=new Date(o.getCreatedAt().getTime());
                String format=df.format(d);
                hora=format;

                color=o.getNumber("puntualidad").intValue();



                if (color==0){
                    puntualidad="A tiempo";
                }else {
                    if (color==1){
                        puntualidad="Tolerancia";
                    }else{
                        if(color==2) {
                            puntualidad="Tarde";
                        }else {
                            puntualidad="A tiempo";
                        }
                    }
                }


                acceso="Salida";

                //writeCsvData();

                String datos [] = new String[]{alumno,autorizada,parentesco,fecha,hora,puntualidad,horario,acceso,"\r\n"};

                lista.add(datos);
                writer.writeNext(datos);


            }
            writer.close();
            Toast.makeText(getContext(), "Data exported succesfully!", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Toast.makeText(getContext(), "Error exporting data!", Toast.LENGTH_SHORT).show();
        }

        sentMessage();

    }

    public void sentMessage(){
        if (f.exists() && !f.isDirectory()) {
            Uri uri = Uri.fromFile(f);
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO,null);
            emailIntent.setType("*/*");
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "EXTRA_SUBJECT");
            emailIntent.putExtra(Intent.EXTRA_STREAM, uri);
            startActivity(Intent.createChooser(emailIntent, "Send email..."));
        }
    }
/*
    public void createCsvDocument(){
        String baseDir = android.os.Environment.getExternalStorageDirectory().getAbsolutePath();
        String fileName = "ReporteAccesps.csv";
        String filePath = baseDir + File.separator + fileName;
        File f = new File(filePath );
        CSVWriter writer;
// File exist
        if(f.exists() && !f.isDirectory()){
            mFileWriter = new FileWriter(filePath , true);
            writer = new CSVWriter(mFileWriter);
        }
        else {
            writer = new CSVWriter(new FileWriter(filePath));
        }
        String[] data = {"Ship Name","Scientist Name", "...",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").formatter.format(date)});

        writer.writeNext(data);

        writer.close();
    }

*/
public void setAlumnos(){

    nombres=new ArrayList<>();
    estudiantes=new ArrayList<>();

    escuelaId = ((ParseApplication) getActivity().getApplication()).getEscuelaId();
    escuela = ParseObject.createWithoutData("Escuela", escuelaId);
    ParseQuery<ParseObject> est= new ParseQuery<>("Estudiantes")
            .include("PersonasAutorizadas").include("grupo");
    est.whereEqualTo("escuela",escuela);


    est.findInBackground(new FindCallback<ParseObject>() {

        @Override

        public void done(List<ParseObject> objects, ParseException e) {
            dialog.hide();
            if (e==null){

                for (ParseObject o:objects){

                    if (o.getString("NOMBRE")==null){

                    }else {

                        nombres.add(o.getString("NOMBRE") + " " + o.getString("ApPATERNO"));
                        estudiantes.add(o);
                    }
                }

            }else {

                Toast.makeText(getContext(),"Error",Toast.LENGTH_SHORT).show();

            }


            n=new String[nombres.size()];

            n=nombres.toArray(n);


            boolean []elementoSeleccionado=new boolean[nombres.size()];

            for(int i=0;i<elementoSeleccionado.length;i++){

                elementoSeleccionado[i]=false;

            }


            mSelectedItems = new ArrayList<>();  // Where we track the selected items

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            // Set the dialog title

            builder.setTitle("Para: ")

                    // Specify the list array, the items to be selected by default (null for none),

                    // and the listener through which to receive callbacks when items are selected

                    .setMultiChoiceItems(n, elementoSeleccionado,

                            new DialogInterface.OnMultiChoiceClickListener() {

                                @Override

                                public void onClick(DialogInterface dialog, int which,

                                                    boolean isChecked) {

                                    if (isChecked) {

                                        // If the user checked the item, add it to the selected items

                                        mSelectedItems.add(n[which]);
                                        idEstudiante.add(estudiantes.get(which).getObjectId());

                                    } else if (mSelectedItems.contains(which)) {

                                        // Else, if the item is already in the array, remove it

                                        mSelectedItems.remove(Integer.valueOf(which));
                                        idEstudiante.remove(which);

                                    }

                                }

                            })

                    // Set the action buttons

                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                        @Override

                        public void onClick(DialogInterface dialog, int id) {

                            // User clicked OK, so save the mSelectedItems results somewhere

                            // or return them to the component that opened the dialog


                            txtAlumnosR.setText("");

                            for (int j = 0; j < mSelectedItems.size(); j++){


                                if (j==mSelectedItems.size()-1){

                                    txtAlumnosR.append(mSelectedItems.get(j));


                                }else{

                                    txtAlumnosR.append(mSelectedItems.get(j) + ", ");

                                }

                            }

                        }

                    })

                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                        @Override

                        public void onClick(DialogInterface dialog, int id) {


                        }

                    })


                    .create().show();



        }

    });

}

    public void sendEmail(){
        Uri.fromFile(new File(filePath));
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "EXTRA_SUBJECT");
        emailIntent.putExtra(Intent.EXTRA_STREAM, filePath);
        startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }

    public void setType(){
        Typeface bold = Typeface.createFromAsset(getContext().getAssets(), avenirBold);
        Typeface medium = Typeface.createFromAsset(getContext().getAssets(), avenirMedium);
        Typeface demi=Typeface.createFromAsset(getContext().getAssets(),avenirDemi);

        fechaReporteI.setTypeface(medium);
        txtFechaReporteI.setTypeface(medium);
        fechaReporteF.setTypeface(medium);
        txtFechaReporteF.setTypeface(medium);
        alumnosReportes.setTypeface(medium);
        txtAlumnosR.setTypeface(medium);
        btnReportesAccesos.setTypeface(medium);


    }

}
