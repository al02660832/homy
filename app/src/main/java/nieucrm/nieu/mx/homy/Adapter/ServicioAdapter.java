package nieucrm.nieu.mx.skola.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import nieucrm.nieu.mx.skola.DataModel.PagosData;
import nieucrm.nieu.mx.skola.DataModel.ServiciosData;
import nieucrm.nieu.mx.skola.Fragments.PagosDetailFragment;
import nieucrm.nieu.mx.skola.Fragments.ServiciosDetailFragment;
import nieucrm.nieu.mx.skola.R;


/**
 * Created by Carlos Romero on 10/12/2016.
 */

public class ServicioAdapter extends BaseAdapter {

    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf",demibold="font/avenir-next-demi-bold.ttf";


    private Context context;
    LayoutInflater inflater;
    private List<ServiciosData> serviciosData=null;
    private ArrayList<ServiciosData>arrayList=null;


    public ServicioAdapter(Context context, List<ServiciosData> serviciosData) {
        this.context = context;
        this.serviciosData=serviciosData;
        try {
            inflater=LayoutInflater.from(context);

        }catch (Exception e){
            Log.d("Error",e.getMessage());
        }
        this.arrayList=new ArrayList<>();
        this.arrayList.addAll(serviciosData);
    }

    public class ViewHolder{
        TextView nombre;
        TextView precio;
    }


    @Override
    public int getCount() {
        return serviciosData.size();
    }

    @Override
    public Object getItem(int position) {
        return serviciosData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Typeface bold=Typeface.createFromAsset(context.getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(context.getAssets(),avenirMedium);
        Typeface dem=Typeface.createFromAsset(context.getAssets(),demibold);

        ViewHolder holder;
       // TextView nombreEstudiante=(TextView)v.findViewById(R.id.nombreEstudiante);

        if(convertView==null){
            holder=new ViewHolder();
            convertView=inflater.inflate(R.layout.servicio_item_layout,null);
            holder.nombre = (TextView) convertView.findViewById(R.id.nombreServicio);
            holder.precio = (TextView) convertView.findViewById(R.id.precioServicio);

            holder.nombre.setTypeface(dem);
            holder.precio.setTypeface(dem);





            convertView.setTag(holder);
        }else {
            holder=(ViewHolder)convertView.getTag();
        }

        holder.nombre.setText(serviciosData.get(position).getNombre());
        holder.precio.setText(serviciosData.get(position).getPrecio());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new ServiciosDetailFragment();
                Bundle bundle=new Bundle();
                bundle.putString("nombre",serviciosData.get(position).getNombre());
                bundle.putString("precio",serviciosData.get(position).getPrecio());
                bundle.putString("descripcion",serviciosData.get(position).getDescripcion());
                bundle.putString("objectid",serviciosData.get(position).getObjectId());
                fragment.setArguments(bundle);
                FragmentTransaction transaction= ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
            }
        });

        return convertView;


    }

}
