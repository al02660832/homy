package nieucrm.nieu.mx.skola.Helpers;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

/**
 * Created by mmac1 on 12/08/2017.
 */

public class Estado {

    Context context;

    public void changeStatus(String objectId, final boolean estado){
        ParseQuery<ParseObject> q=ParseQuery.getQuery("Servicio");
        q.whereEqualTo("objectId",objectId);
        q.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (e==null){
                    object.put("activo",estado);
                    object.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            Log.d("Status modificado",String.valueOf(estado));
                        }
                    });
                }else {
                    Log.d("Error",e.getMessage());

                }
            }
        });
    }

    public void changePresencia(String objectId, final boolean presente, final Context context){
        ParseQuery<ParseObject> q=ParseQuery.getQuery("Presencia").include("estudiante").orderByDescending("presente");
        q.whereEqualTo("objectId",objectId);
        q.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (e==null){
                    object.put("presente",presente);
                    object.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e==null){
                                Log.e("Status modificado",String.valueOf(presente));
                                Toast.makeText(context,"Presencia modificada exitosamente",Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(context,"Error, intente de nuevo",Toast.LENGTH_SHORT).show();

                            }
                        }
                    });
                }else {
                    Log.e("Error",e.getMessage());

                }
            }
        });
    }

}
