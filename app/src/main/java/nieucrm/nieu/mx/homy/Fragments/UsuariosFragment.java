package nieucrm.nieu.mx.skola.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class UsuariosFragment extends Fragment {

    ImageButton btnAdministracion,btnMaestro,btnPapa;
    Fragment fragment=null;

    public UsuariosFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_usuarios, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnAdministracion=(ImageButton)view.findViewById(R.id.btnAdministracion);
        btnMaestro=(ImageButton)view.findViewById(R.id.btnMaestro);
        //btnPapa=(ImageButton)view.findViewById(R.id.btnPapa);

        /*
        btnPapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment=new UsersFragment();
                Bundle b=new Bundle();
                b.putInt("usertype",2);
                fragment.setArguments(b);
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
            }
        });
        */

        btnAdministracion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment=new UsersFragment();
                Bundle b=new Bundle();

                b.putInt("usertype",0);
                fragment.setArguments(b);
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
            }
        });

        btnMaestro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment=new UsersFragment();
                Bundle b=new Bundle();
                b.putInt("usertype",1);
                fragment.setArguments(b);
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
            }
        });

    }
}
