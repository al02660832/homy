package nieucrm.nieu.mx.skola.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import nieucrm.nieu.mx.skola.DataModel.ServiciosData;
import nieucrm.nieu.mx.skola.Helpers.Estado;
import nieucrm.nieu.mx.skola.Fragments.NewServiciosFragment;
import nieucrm.nieu.mx.skola.R;


/**
 * Created by Carlos Romero on 10/12/2016.
 */

public class ServicioActivoAdapter extends BaseAdapter {

    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf",demibold="font/avenir-next-demi-bold.ttf";


    private Context context;
    LayoutInflater inflater;
    private List<ServiciosData> serviciosData=null;
    private ArrayList<ServiciosData>arrayList=null;


    public ServicioActivoAdapter(Context context, List<ServiciosData> serviciosData) {
        this.context = context;
        this.serviciosData=serviciosData;
        try {
            inflater=LayoutInflater.from(context);

        }catch (Exception e){
            Log.d("Error",e.getMessage());
        }
        this.arrayList=new ArrayList<>();
        this.arrayList.addAll(serviciosData);
    }

    public class ViewHolder{
        TextView nombre;
        TextView precio;
        TextView descripcion;
        Switch activo;
    }


    @Override
    public int getCount() {
        return serviciosData.size();
    }

    @Override
    public Object getItem(int position) {
        return serviciosData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Typeface bold=Typeface.createFromAsset(context.getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(context.getAssets(),avenirMedium);
        Typeface dem=Typeface.createFromAsset(context.getAssets(),demibold);

        final ViewHolder holder;
       // TextView nombreEstudiante=(TextView)v.findViewById(R.id.nombreEstudiante);

        if(convertView==null){
            holder=new ViewHolder();
            convertView=inflater.inflate(R.layout.servicio_admin_item_layout,null);
            holder.nombre = (TextView) convertView.findViewById(R.id.nomServicio);
            holder.precio = (TextView) convertView.findViewById(R.id.preServicio);
            holder.descripcion=(TextView)convertView.findViewById(R.id.descripcionServicio);
            holder.activo=(Switch)convertView.findViewById(R.id.switchActivo);

            holder.nombre.setTypeface(dem);
            holder.precio.setTypeface(dem);
            holder.descripcion.setTypeface(medium);

            holder.nombre.setText(serviciosData.get(position).getNombre());
            holder.precio.setText(serviciosData.get(position).getPrecio());
            holder.descripcion.setText(serviciosData.get(position).getDescripcion());
            holder.activo.setChecked(serviciosData.get(position).isActivo());

            convertView.setTag(holder);
        }else {
            holder=(ViewHolder)convertView.getTag();
        }

        holder.activo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (holder.activo.isChecked()){
                    Estado estado=new Estado();
                    estado.changeStatus(serviciosData.get(position).getObjectId(),true);
                }else {
                    Estado estado=new Estado();
                    estado.changeStatus(serviciosData.get(position).getObjectId(),false);
                }
            }
        });

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment fragment = new NewServiciosFragment();
                Bundle bundle=new Bundle();
                bundle.putString("nombre",serviciosData.get(position).getNombre());
                bundle.putInt("precio",serviciosData.get(position).getPrice());
                bundle.putString("descripcion",serviciosData.get(position).getDescripcion());
                bundle.putBoolean("activo",serviciosData.get(position).isActivo());
                bundle.putString("objectId",serviciosData.get(position).getObjectId());
                fragment.setArguments(bundle);
                FragmentTransaction transaction= ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();

            }
        });

        return convertView;


    }

}
