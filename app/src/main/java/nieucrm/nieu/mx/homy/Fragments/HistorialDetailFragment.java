package nieucrm.nieu.mx.skola.Fragments;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import nieucrm.nieu.mx.skola.Helpers.Constants;
import nieucrm.nieu.mx.skola.Helpers.Util;
import nieucrm.nieu.mx.skola.R;


/**
 * Created by Carlos Romero Morales
 */
public class HistorialDetailFragment extends Fragment {

    TextView fechaH, horaH, creadoH, emisorH, paraH, receptorH, asuntoHistorial, asuntoH, contenidoH, vistoHistorial,aprobadoPo,approve;
    ListView listaVisto;

    String avenirBold = "font/avenir-next-bold.ttf", avenirMedium = "font/avenir-next-medium.ttf", avenirDemi = "font/avenir-next-demi-bold.ttf";


    String hora, fecha, emisor, receptor, asunto, descripcion, id;

    ArrayList<String> nombres;
    ArrayAdapter<String> adapter;
    boolean aws,anuncioPhoto;
    String parseUri;
    ImageButton clipImg;
    LinearLayout layApp;

    TransferUtility utility;
    TransferObserver observer;
    File imageFile;
    File outputDir;

    ProgressDialog progressDialog;
    String aprobadoPor;
    Activity a;

    public HistorialDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            a=(Activity) context;
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        utility= Util.getTransferUtility(a);
        return inflater.inflate(R.layout.fragment_historial_detail, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);

        fechaH = (TextView) v.findViewById(R.id.fechaH);
        horaH = (TextView) v.findViewById(R.id.horaH);
        creadoH = (TextView) v.findViewById(R.id.creadoH);
        emisorH = (TextView) v.findViewById(R.id.emisorH);
        paraH = (TextView) v.findViewById(R.id.paraH);
        receptorH = (TextView) v.findViewById(R.id.receptorH);
        asuntoHistorial = (TextView) v.findViewById(R.id.asuntoHistorial);
        asuntoH = (TextView) v.findViewById(R.id.asuntoH);
        contenidoH = (TextView) v.findViewById(R.id.contenidoH);
        vistoHistorial = (TextView) v.findViewById(R.id.vistoHistorial);
        clipImg = (ImageButton) v.findViewById(R.id.clipImg);

        listaVisto = (ListView) v.findViewById(R.id.listaVisto);
        aprobadoPo=(TextView)v.findViewById(R.id.aprobadoPor);
        approve=(TextView)v.findViewById(R.id.approve);
        layApp=(LinearLayout)v.findViewById(R.id.layApp);



        getData();

        progressDialog = new ProgressDialog(a);

        progressDialog.setTitle("Cargando");
        progressDialog.setMessage("Por favor espere...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        clipImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imagen();
            }
        });

        setType();
        getVisto();

    }

    public void getVisto() {
        nombres = new ArrayList<>();
        ParseObject anuncio = ParseObject.createWithoutData("anuncio", id);

        ParseQuery<ParseObject> q = new ParseQuery<ParseObject>("actividad");
        q.include("anuncioID");
        q.include("userID");
        q.whereEqualTo("anuncioID", anuncio);
        q.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                progressDialog.hide();

                if (e == null) {

                    for (ParseObject o : objects) {
                        ParseUser user = o.getParseUser("userID");
                        if (user == null) {

                        } else {
                            nombres.add(user.getString("nombre") + " " + user.getString("apellidos"));

                        }
                    }

                    adapter = new ArrayAdapter<String>(a, android.R.layout.simple_list_item_1, android.R.id.text1, nombres);
                    listaVisto.setAdapter(adapter);

                } else {
                    Toast.makeText(a, "Error", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void setType() {
        Typeface bold = Typeface.createFromAsset(a.getAssets(), avenirBold);
        Typeface medium = Typeface.createFromAsset(a.getAssets(), avenirMedium);
        Typeface demi = Typeface.createFromAsset(a.getAssets(), avenirDemi);
        fechaH.setTypeface(medium);
        horaH.setTypeface(medium);
        creadoH.setTypeface(medium);
        emisorH.setTypeface(medium);
        paraH.setTypeface(medium);
        receptorH.setTypeface(medium);
        asuntoHistorial.setTypeface(medium);
        asuntoH.setTypeface(medium);
        contenidoH.setTypeface(medium);
        vistoHistorial.setTypeface(demi);
        aprobadoPo.setTypeface(medium);
        approve.setTypeface(medium);

    }

    public void getData(){

        Bundle b=this.getArguments();
        if (b!=null){
            hora=b.getString("hora");
            fecha=b.getString("fecha");
            emisor=b.getString("emisor");
            receptor=b.getString("receptor");
            asunto=b.getString("asunto");
            descripcion=b.getString("descripcion");
            id=b.getString("id");
            aprobadoPor=b.getString("aprobadoPor");
            if (b.getString("parseUri")==null){
                if (b.getBoolean("aws")){
                    clipImg.setVisibility(View.VISIBLE);
                    aws=b.getBoolean("aws");
                }else {
                    if (b.getBoolean("tablaAnuncio")){
                        anuncioPhoto=b.getBoolean("tablaAnuncio");
                        clipImg.setVisibility(View.VISIBLE);
                    }else {
                        clipImg.setVisibility(View.GONE);
                    }
                }

            }else {
                clipImg.setVisibility(View.VISIBLE);

                parseUri=b.getString("parseUri");
            }


            fechaH.setText(fecha);
            horaH.setText(hora);
            emisorH.setText(emisor);
            receptorH.setText(receptor);
            asuntoH.setText(asunto);
            contenidoH.setText(descripcion);
            if (aprobadoPor==null||aprobadoPor.equals("")){
                layApp.setVisibility(View.GONE);
            }else {
                aprobadoPo.setText(aprobadoPor);
            }

        }

    }

    public void imagen(){
        Fragment fragment = new AnuncioPhotoFragment();
        Bundle bundle=new Bundle();
        bundle.putString("objectId",id);
        if (anuncioPhoto){
            bundle.putBoolean("anuncioPhoto",true);
        }else {
            if (aws){
                bundle.putBoolean("aws",true);
            }else {
                //ParseUri
                //            Picasso.with(getContext()).load(parseUri).resize(500,800).into(showImage);

                bundle.putString("parseUri",parseUri);
            }
        }
        fragment.setArguments(bundle);
        FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
        transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
        //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();

    }


}
