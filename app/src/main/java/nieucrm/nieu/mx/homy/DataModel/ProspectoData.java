package nieucrm.nieu.mx.skola.DataModel;

import java.util.Date;

/**
 * Created by mmac1 on 26/09/2017.
 */

public class ProspectoData {

    private String captura,estudianteApellidos,fechaNacimiento,seguimiento,papaNombre,mamaApellidos,mamaNombre,papaApellidos,promocion,mamaEmail,papaTelefono,papaEmail,estudianteNombre,mamaTelefono,objectId;
    private Date estudianteFechaNacimiento;
    private String fechaCreacion, horaCreacion,nombre,edad,fechaIngreso,preguntas,telefono,mail;
    private boolean web;
    public String getCaptura() {
        return captura;
    }

    public void setCaptura(String captura) {
        this.captura = captura;
    }

    public String getEstudianteApellidos() {
        return estudianteApellidos;
    }

    public void setEstudianteApellidos(String estudianteApellidos) {
        this.estudianteApellidos = estudianteApellidos;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getSeguimiento() {
        return seguimiento;
    }

    public void setSeguimiento(String seguimiento) {
        this.seguimiento = seguimiento;
    }

    public String getPapaNombre() {
        return papaNombre;
    }

    public void setPapaNombre(String papaNombre) {
        this.papaNombre = papaNombre;
    }

    public String getMamaApellidos() {
        return mamaApellidos;
    }

    public void setMamaApellidos(String mamaApellidos) {
        this.mamaApellidos = mamaApellidos;
    }

    public String getMamaNombre() {
        return mamaNombre;
    }

    public void setMamaNombre(String mamaNombre) {
        this.mamaNombre = mamaNombre;
    }

    public String getPapaApellidos() {
        return papaApellidos;
    }

    public void setPapaApellidos(String papaApellidos) {
        this.papaApellidos = papaApellidos;
    }

    public String getPromocion() {
        return promocion;
    }

    public void setPromocion(String promocion) {
        this.promocion = promocion;
    }

    public String getMamaEmail() {
        return mamaEmail;
    }

    public void setMamaEmail(String mamaEmail) {
        this.mamaEmail = mamaEmail;
    }

    public String getPapaTelefono() {
        return papaTelefono;
    }

    public void setPapaTelefono(String papaTelefono) {
        this.papaTelefono = papaTelefono;
    }

    public String getPapaEmail() {
        return papaEmail;
    }

    public void setPapaEmail(String papaEmail) {
        this.papaEmail = papaEmail;
    }

    public String getEstudianteNombre() {
        return estudianteNombre;
    }

    public void setEstudianteNombre(String estudianteNombre) {
        this.estudianteNombre = estudianteNombre;
    }

    public String getMamaTelefono() {
        return mamaTelefono;
    }

    public void setMamaTelefono(String mamaTelefono) {
        this.mamaTelefono = mamaTelefono;
    }

    public Date getEstudianteFechaNacimiento() {
        return estudianteFechaNacimiento;
    }

    public void setEstudianteFechaNacimiento(Date estudianteFechaNacimiento) {
        this.estudianteFechaNacimiento = estudianteFechaNacimiento;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getHoraCreacion() {
        return horaCreacion;
    }

    public void setHoraCreacion(String horaCreacion) {
        this.horaCreacion = horaCreacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getPreguntas() {
        return preguntas;
    }

    public void setPreguntas(String preguntas) {
        this.preguntas = preguntas;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public boolean isWeb() {
        return web;
    }

    public void setWeb(boolean web) {
        this.web = web;
    }
}
