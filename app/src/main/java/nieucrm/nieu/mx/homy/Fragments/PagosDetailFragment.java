package nieucrm.nieu.mx.skola.Fragments;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.github.chrisbanes.photoview.PhotoView;
import com.parse.DeleteCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;

import nieucrm.nieu.mx.skola.Helpers.Constants;
import nieucrm.nieu.mx.skola.Helpers.Util;
import nieucrm.nieu.mx.skola.R;

/**
 * Created by Carlos Romero
 */

public class PagosDetailFragment extends Fragment {

    TextView nombreEstudiante,concepto,conceptoPago,confirmacion,textManual,eliminarPago,mesPago;
    PhotoView fotoPago;
    String nombre, conceptoP,fileUri;
    String avenirBold = "font/avenir-next-bold.ttf", avenirMedium = "font/avenir-next-medium.ttf",demibold="font/avenir-next-demi-bold.ttf";

    String objeto, mes;

    TransferUtility utility;
    TransferObserver observer;
    File imageFile;
    File outputDir;

    ProgressBar progress;





    public PagosDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            nombre = bundle.getString("nombreAlumno");
            conceptoP = bundle.getString("conceptoPago");
            fileUri = bundle.getString("parseImage");
            objeto=bundle.getString("oId");
            mes=bundle.getString("mes");
        }

        utility= Util.getTransferUtility(getContext());

        setHasOptionsMenu(true);



        return inflater.inflate(R.layout.fragment_pagos_detail, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        nombreEstudiante=(TextView)view.findViewById(R.id.nombreEstudiante);
        concepto=(TextView)view.findViewById(R.id.concepto);
        conceptoPago=(TextView)view.findViewById(R.id.conceptoPago);
        fotoPago=(PhotoView) view.findViewById(R.id.fotoPago);
        confirmacion=(TextView)view.findViewById(R.id.confirmacion);
        textManual=(TextView)view.findViewById(R.id.textManual);
        progress=(ProgressBar)view.findViewById(R.id.progress);
        eliminarPago=(TextView)view.findViewById(R.id.eliminarPago);
        mesPago=(TextView)view.findViewById(R.id.mesPago);

        if (ParseUser.getCurrentUser().getNumber("usertype").intValue()==2){
            eliminarPago.setVisibility(View.GONE);
        }

        if (fileUri.equals("M")){
            textManual.setVisibility(View.VISIBLE);
            progress.setVisibility(View.GONE);

        }

        if (fileUri.equals("N")){
            textManual.setVisibility(View.VISIBLE);
            textManual.setText("No existe foto registrada de este pago");
            progress.setVisibility(View.GONE);


        }else {
            Picasso.with(getContext()).load(fileUri).resize(500,800).into(fotoPago);
        }

        eliminarPago.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eliminarPago(objeto);
            }
        });

        outputDir = getContext().getCacheDir();
        try {
            imageFile= File.createTempFile("prefix", "extension", outputDir);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (fileUri.equals("A")){
            observer=utility.download(Constants.BUCKET_NAME,objeto,imageFile);
            observer.setTransferListener(new TransferListener() {
                @Override
                public void onStateChanged(int id, TransferState state) {
                    Log.d("Estatus:",state.toString());

                    if (TransferState.COMPLETED.equals(observer.getState())) {

                        Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
                        fotoPago.setImageBitmap(bitmap);
                        progress.setVisibility(View.GONE);
                        try {
                            Toast.makeText(getContext(), "Foto lista", Toast.LENGTH_SHORT).show();
                        }catch (Exception e1){
                            Log.e("Exception",e1.getMessage());
                        }
                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                    long _bytesCurrent = bytesCurrent;
                    long _bytesTotal = bytesTotal;

                    float percentage =  ((float)_bytesCurrent /(float)_bytesTotal * 100);
                    Log.d("percentage","" +percentage);


                }

                @Override
                public void onError(int id, Exception ex) {

                    Log.d("Error al descargar", ex.getMessage());
                }
            });
            if( fotoPago.getDrawable() != null){

                //Image Assigned

            }else{
                //nothing assigned

            }
        }

        nombreEstudiante.setText(nombre);
        conceptoPago.setText(conceptoP);
        mesPago.setText(mes);
        fontFace();

    }

    public void fontFace(){
        Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), avenirBold);
        Typeface medium = Typeface.createFromAsset(getActivity().getAssets(), avenirMedium);
        Typeface demi=Typeface.createFromAsset(getActivity().getAssets(),demibold);
        nombreEstudiante.setTypeface(bold);
        confirmacion.setTypeface(medium);
        concepto.setTypeface(medium);
        conceptoPago.setTypeface(bold);
        textManual.setTypeface(demi);
        eliminarPago.setTypeface(demi);

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.new_message,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id=item.getItemId();
        if(id==R.id.write_mess){
            Fragment fragment = new NewMessageFragment();
            Bundle bundle=new Bundle();
            bundle.putString("seccion","cf");
            bundle.putString("tipo","eJZIOPloQH");
            bundle.putString("idEstudiante",objeto);
            fragment.setArguments(bundle);
            FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
            transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
        }

        return super.onOptionsItemSelected(item);

    }

    public void eliminarPago(String id){
        ParseQuery<ParseObject>pagosQuery=ParseQuery.getQuery("pagos");
        pagosQuery.whereEqualTo("objectId",id);
        pagosQuery.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (e==null){
                    object.deleteInBackground(new DeleteCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e==null){
                                try {
                                    Toast.makeText(getActivity(),"Pago eliminado satisfactoriamente",Toast.LENGTH_SHORT).show();
                                    getActivity().getSupportFragmentManager().popBackStack();

                                }catch (Exception ex){
                                    Log.e("Error eliminar",ex.getMessage());
                                }
                            }else {
                                Log.e("Eliminar",e.getMessage());
                                try {
                                    Toast.makeText(getActivity(),"Error al eliminar, intente de nuevo",Toast.LENGTH_SHORT).show();
                                }catch (Exception ex){
                                    Log.e("Error eliminar",ex.getMessage());
                                }
                            }
                        }
                    });
                }else {
                    Log.d("Error",e.getMessage());
                }
            }
        });
    }

}
