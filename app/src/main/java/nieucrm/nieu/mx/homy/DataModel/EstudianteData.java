package nieucrm.nieu.mx.skola.DataModel;

/**
 * Created by Carlos Romero on 23/12/2016.
 */

public class EstudianteData {
    String grupo,nombreEstudiante,genero,idEstudiante,idGrupo,nombre;
    int noMensajes;

    public EstudianteData() {
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public String getNombreEstudiante() {
        return nombreEstudiante;
    }

    public void setNombreEstudiante(String nombreEstudiante) {
        this.nombreEstudiante = nombreEstudiante;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getIdEstudiante() {
        return idEstudiante;
    }

    public void setIdEstudiante(String idEstudiante) {
        this.idEstudiante = idEstudiante;
    }

    public int getNoMensajes() {
        return noMensajes;
    }

    public void setNoMensajes(int noMensajes) {
        this.noMensajes = noMensajes;
    }

    public String getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(String idGrupo) {
        this.idGrupo = idGrupo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
