package nieucrm.nieu.mx.skola.Fragments;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import nieucrm.nieu.mx.skola.Helpers.Constants;
import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.Helpers.Util;
import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class MessageDetailFragment extends Fragment {

    String fecha,hora,parentesco,nombreAlumno,asunto,descripcion,seleccion,id,para;
    TextView mensajeTexto,fechaM,horaM,parentescoM,nombreM,contenidoM,asuntoM,contenidoEditMensaje;
    String avenirBold = "font/avenir-next-bold.ttf", avenirMedium = "font/avenir-next-medium.ttf",demibold="font/avenir-next-demi-bold.ttf";
    Button btnArchivar,btnResponder,btnAprobar,btnRechazar;
    FrameLayout frameMensajes;
    LinearLayout backMensaje;
    String tipo,idEst;
    ImageButton imagenClip;
    boolean aws,anuncioPhoto;
    String parseUri;
    TransferUtility utility;
    TransferObserver observer;
    File imageFile;
    File outputDir;
    HashMap<String, Object>rechazo;

    ImageView showImage;

    HashMap<String,Object>params;

    String escuelaId;

    ParseObject anuncio;

    Activity a;

    public MessageDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            a=(Activity) context;
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true);

        return inflater.inflate(R.layout.fragment_message_detail, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mensajeTexto=(TextView)view.findViewById(R.id.mensajeTexto);
        fechaM=(TextView)view.findViewById(R.id.fechaM);
        horaM=(TextView)view.findViewById(R.id.horaM);
        parentescoM=(TextView)view.findViewById(R.id.parentescoM);
        nombreM=(TextView)view.findViewById(R.id.nombreM);
        contenidoM=(TextView)view.findViewById(R.id.contenidoM);
        asuntoM=(TextView)view.findViewById(R.id.asuntoM);

        frameMensajes=(FrameLayout)view.findViewById(R.id.frameMensajes);
        backMensaje=(LinearLayout)view.findViewById(R.id.backMensaje);
        contenidoEditMensaje=(TextView)view.findViewById(R.id.contenidoEditMensaje);

        btnArchivar=(Button)view.findViewById(R.id.btnArchivar);
        btnResponder=(Button)view.findViewById(R.id.btnResponder);
        btnAprobar=(Button)view.findViewById(R.id.btnAprobar);
        btnRechazar=(Button)view.findViewById(R.id.btnRechazar);

        imagenClip=(ImageButton)view.findViewById(R.id.btnClip);

        getData();
        setContenido();
        fontFace();

        vistoAnuncio();

        btnArchivar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                archivar();
            }
        });
        btnAprobar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aprobar();
            }
        });
        btnResponder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                responder();
            }
        });
        btnRechazar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rechazar();
            }
        });
        imagenClip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imagen();
            }
        });

    }

    public void imagen(){

        Fragment fragment = new AnuncioPhotoFragment();
        Bundle bundle=new Bundle();
        bundle.putString("objectId",id);
        if (anuncioPhoto){
            bundle.putBoolean("anuncioPhoto",true);
        }else {
            if (aws){
                bundle.putBoolean("aws",true);
            }else {
                //ParseUri
                //            Picasso.with(getContext()).load(parseUri).resize(500,800).into(showImage);
                bundle.putString("parseUri",parseUri);
            }
        }
        fragment.setArguments(bundle);
        FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
        transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
        //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();



    }


    public void setContenido(){
        fechaM.setText(fecha);
        horaM.setText(hora);
        parentescoM.setText(parentesco);
        nombreM.setText(para+nombreAlumno);
        contenidoEditMensaje.setText(descripcion);
        asuntoM.setText(asunto);

        if (seleccion.equals("pendiente")){
            btnResponder.setVisibility(View.GONE);
            btnArchivar.setVisibility(View.GONE);
            btnAprobar.setVisibility(View.VISIBLE);
            btnRechazar.setVisibility(View.VISIBLE);
            frameMensajes.setBackgroundColor(getResources().getColor(R.color.green_events));
            backMensaje.setBackgroundColor(getResources().getColor(R.color.green_events));

        }

    }

    public void getData(){
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            fecha=bundle.getString("fecha");
            hora=bundle.getString("hora");
            parentesco=bundle.getString("parentesco");
            nombreAlumno=bundle.getString("nombreAlumno");
            asunto=bundle.getString("asunto");
            descripcion=bundle.getString("descripcion");
            seleccion=bundle.getString("seleccion");
            idEst=bundle.getString("studentId");
            tipo="RRedHmnzoO";

            id=bundle.getString("id");
            if (bundle.getString("para")==null){
                para="";
            }else {
                para=bundle.getString("para");
            }
            if (bundle.getString("parseUri")==null){
                if (bundle.getBoolean("aws")){
                    imagenClip.setVisibility(View.VISIBLE);
                    aws=bundle.getBoolean("aws");
                }else {
                    if (bundle.getBoolean("tablaAnuncio")){
                        anuncioPhoto=bundle.getBoolean("tablaAnuncio");
                        imagenClip.setVisibility(View.VISIBLE);
                    }else {
                        imagenClip.setVisibility(View.GONE);
                    }
                }
            }else {
                imagenClip.setVisibility(View.VISIBLE);
                parseUri=bundle.getString("parseUri");
            }

        }
    }

    public void fontFace(){
        Typeface bold = Typeface.createFromAsset(a.getAssets(), avenirBold);
        Typeface medium = Typeface.createFromAsset(a.getAssets(), avenirMedium);
        Typeface demi=Typeface.createFromAsset(a.getAssets(),demibold);

        fechaM.setTypeface(medium);
        mensajeTexto.setTypeface(bold);
        horaM.setTypeface(medium);
        parentescoM.setTypeface(medium);
        nombreM.setTypeface(medium);
        contenidoEditMensaje.setTypeface(medium);
        asuntoM.setTypeface(medium);
        btnArchivar.setTypeface(demi);
        btnResponder.setTypeface(demi);
    }

    public void archivar(){
        ParseQuery<ParseObject>q=ParseQuery.getQuery("anuncio");
        q.whereEqualTo("objectId",id);
        q.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (e==null){
                    object.put("actionTaken",true);
                    object.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            Toast.makeText(a,"Archivado",Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }

    public void aprobar(){
        params=new HashMap<>();
        ParseQuery<ParseObject>q=ParseQuery.getQuery("anuncio");
        q.whereEqualTo("objectId",id);
        escuelaId=((ParseApplication)a.getApplication()).getEscuelaId();

        q.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (e==null){
                    object.put("aprobado",true);
                    object.put("aprobadoPor",ParseUser.getCurrentUser());
                    object.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e==null){
                                params.put("anuncioObjectId", id);
                                params.put("escuelaObjId",escuelaId);
                                ParseCloud.callFunctionInBackground("adminApprovedAnuncio", params, new FunctionCallback<Object>() {

                                    @Override
                                    public void done(Object object, ParseException e) {
                                        if (e == null) {
                                            Log.d("Cloudcode", "Success");

                                        } else {
                                            Log.d("Cloudcode", e.getMessage());
                                        }
                                    }
                                });
                                Toast.makeText(a,"Aprobado",Toast.LENGTH_SHORT).show();

                            }
                        }
                    });
                }
            }
        });
    }

    public void vistoAnuncio(){
        anuncio=ParseObject.createWithoutData("anuncio",id);
        ParseQuery<ParseObject> q=ParseQuery.getQuery("actividad");
        q.whereEqualTo("anuncioID",anuncio);
        q.whereEqualTo("userID", ParseUser.getCurrentUser());
        q.whereEqualTo("tipo","seen");
        q.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    if (objects.size()==0){
                        //Crear un visto
                        ParseObject seen=new ParseObject("actividad");
                        seen.put("anuncioID",anuncio);
                        seen.put("userID",ParseUser.getCurrentUser());
                        seen.put("tipo","seen");
                        seen.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {

                            }
                        });
                    }else{
                        //Mensaje visto
                        //Toast.makeText(getContext(),"Visto",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    public void savePhoto() {

        //Funciona
        showImage.buildDrawingCache();

        Bitmap bmp = showImage.getDrawingCache();

        File storageLoc = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES); //context.getExternalFilesDir(null);

        File file = new File(storageLoc, System.currentTimeMillis() + ".jpg");

        try{
            FileOutputStream fos = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.close();

            scanFile(a, Uri.fromFile(file));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void sharePhoto(){


        showImage.buildDrawingCache();

        Bitmap bmp = showImage.getDrawingCache();

        File storageLoc = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES); //context.getExternalFilesDir(null);

        File file = new File(storageLoc, System.currentTimeMillis() + ".jpg");

        try{
            FileOutputStream fos = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.close();

            scanFile(a, Uri.fromFile(file));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/*");
        share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
        startActivity(Intent.createChooser(share,"Share via"));

    }

    public void responder(){
        Fragment fragment = new NewMessageFragment();
        Bundle bundle=new Bundle();
        bundle.putString("seccion",seleccion);
        bundle.putString("tipo",tipo);
        bundle.putString("idEstudiante",idEst);
        bundle.putString("nombre",nombreAlumno);
        fragment.setArguments(bundle);
        FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
        transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
    }

    public void rechazar(){
        rechazo=new HashMap<>();
        rechazo.put("anuncioObjectId",id);
        ParseCloud.callFunctionInBackground("rechazarAnuncioMaestra", rechazo, new FunctionCallback<Object>() {
            @Override
            public void done(Object object, ParseException e) {
                if (e == null){
                    Log.d("Cloudcode-sNotificacion","Success");
                }else {
                    Log.d("Error",e.getMessage());
                }
            }
        });
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);

    }

    public void displayChooser(){
        final CharSequence[] items = {"Guardar imagen", "Compartir",
                "Cancelar"};
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(a);
        builder.setTitle("Selecciona una opción: ");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Guardar imagen")) {
                    savePhoto();
                } else if (items[item].equals("Compartir")) {

                    sharePhoto();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private static void scanFile(Context context, Uri imageUri){
        Intent scanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        scanIntent.setData(imageUri);
        context.sendBroadcast(scanIntent);

    }

}
