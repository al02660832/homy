package nieucrm.nieu.mx.skola.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import nieucrm.nieu.mx.skola.DataModel.PresenciaData;
import nieucrm.nieu.mx.skola.R;


/**
 * Created by Carlos Romero on 14/03/2017.
 */

public class PresenciaAdapter extends BaseAdapter {

    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf",avenirDemibold="font/avenir-next-demi-bold.ttf";



    private Context context;
    LayoutInflater inflater;
    private List<PresenciaData> presenciaData=null;
    private ArrayList<PresenciaData> array=null;
    LinearLayout presenciaLayout;

    public PresenciaAdapter(Context context, List<PresenciaData> presenciaData) {
        this.context = context;
        this.presenciaData=presenciaData;
        try {
            inflater=LayoutInflater.from(context);

        }catch (Exception e){
            Log.d("Error",e.getMessage());
        }
        this.array=new ArrayList<>();
        this.array.addAll(presenciaData);
    }

    public class ViewHolder{
        TextView nombrePresencia,estadoPresencia,fechaPresencia,horaPresencia;
    }


    @Override
    public int getCount() {
        return presenciaData.size();
    }

    @Override
    public Object getItem(int position) {
        return presenciaData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Typeface bold=Typeface.createFromAsset(context.getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(context.getAssets(),avenirMedium);
        Typeface demibold=Typeface.createFromAsset(context.getAssets(),avenirDemibold);

        ViewHolder holder;
        // TextView nombreEstudiante=(TextView)v.findViewById(R.id.nombreEstudiante);

        if(convertView==null){
            holder=new ViewHolder();
            convertView=inflater.inflate(R.layout.presencia_item_layout,null);

            presenciaLayout=(LinearLayout)convertView.findViewById(R.id.presenciaLayout);

            holder.nombrePresencia=(TextView)convertView.findViewById(R.id.nombrePresencia);
            holder.estadoPresencia=(TextView)convertView.findViewById(R.id.estadoPresencia);
            holder.fechaPresencia=(TextView)convertView.findViewById(R.id.fechaPresencia);
            holder.horaPresencia=(TextView)convertView.findViewById(R.id.horaPresencia);

            holder.horaPresencia.setTypeface(medium);
            holder.fechaPresencia.setTypeface(medium);
            holder.estadoPresencia.setTypeface(medium);
            holder.nombrePresencia.setTypeface(medium);
            

            convertView.setTag(holder);
        }else {
            holder=(ViewHolder)convertView.getTag();
        }



        holder.nombrePresencia.setText(presenciaData.get(position).getEstudiante());
        holder.fechaPresencia.setText(presenciaData.get(position).getFecha());
        holder.horaPresencia.setText(presenciaData.get(position).getHora());

        if (presenciaData.get(position).isPresente()){
            holder.estadoPresencia.setText("Presente");
            convertView.setBackgroundColor(Color.parseColor("#ECF5DD"));
        }else {
            if (!presenciaData.get(position).isPresente()){
                holder.estadoPresencia.setText("Fuera");
                convertView.setBackgroundColor(Color.parseColor("#FFFFFF"));
            }
        }

        return convertView;
    }





}

