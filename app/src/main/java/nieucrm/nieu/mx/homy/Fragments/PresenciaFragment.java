package nieucrm.nieu.mx.skola.Fragments;


import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import nieucrm.nieu.mx.skola.Adapter.PresenciaAdapter;
import nieucrm.nieu.mx.skola.DataModel.PresenciaData;
import nieucrm.nieu.mx.skola.Helpers.Estado;
import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class PresenciaFragment extends Fragment {

    ParseObject escuela;
    String escuelaId;
    ParseQuery<ParseObject>presence;
    TextView presenciaTag,numAlumnos;
    ListView listaPresencia;
    PresenciaData data;
    PresenciaAdapter adapter;
    ArrayList<ParseObject>presente;
    ArrayList<PresenciaData>array;
    Date updatedAt;
    Calendar calendar;
    String[] strDays = new String[]{"Domingo",
            "Lunes","Martes",
            "Miercoles","Jueves",
            "Viernes","Sabado"};
    String fecha,hora;
    DateFormat hourFormat;
    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf",demiBold="font/avenir-next-demi-bold.ttf";
    HashMap<String, Object> params;
    ParseObject acceso;


    public PresenciaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_presencia, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenciaTag=(TextView)view.findViewById(R.id.presenciaTag);
        numAlumnos=(TextView)view.findViewById(R.id.numAlumnos);
        listaPresencia=(ListView)view.findViewById(R.id.listaPresencia);

        setTypefaces();
        getPresencia();


    }

    public void getPresencia(){
        escuelaId=((ParseApplication)getActivity().getApplication()).getEscuelaId();
        escuela=ParseObject.createWithoutData("Escuela",escuelaId);
        presente=new ArrayList<>();
        array=new ArrayList<>();

        ParseQuery<ParseObject>estudiantesQuery=ParseQuery.getQuery("Estudiantes");
        estudiantesQuery.whereEqualTo("escuela",escuela);

        //SimpleDateFormat format = new SimpleDateFormat("dd 'de' MMMM ",new Locale("es"));
        hourFormat = new SimpleDateFormat("HH:mm:ss");
        calendar=Calendar.getInstance();
        presence=ParseQuery.getQuery("Presencia").include("estudiante");

        presence.whereMatchesQuery("estudiante",estudiantesQuery);

        presence.orderByDescending("presente");
        presence.setLimit(500);

        presence.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e==null){
                    for (ParseObject o:objects){
                        if (o.getBoolean("presente")){
                            presente.add(o);
                        }
                        ParseObject est=o.getParseObject("estudiante");
                        data=new PresenciaData();
                        data.setStudentId(est.getObjectId());
                        data.setId(o.getObjectId());
                        updatedAt=o.getUpdatedAt();
                        calendar.setTime(updatedAt);
                        fecha=strDays[calendar.get(Calendar.DAY_OF_WEEK)-1]+" "+calendar.get(Calendar.DATE)+"/"+calendar.get(Calendar.MONTH);
                        hora=hourFormat.format(updatedAt);
                        data.setFecha(fecha);
                        data.setHora(hora);
                        data.setPresente(o.getBoolean("presente"));

                        if(est==null){
                            data.setEstudiante("Alumno");
                        }else{
                            String nombre=est.getString("NOMBRE")+" "+est.getString("APELLIDO");
                            data.setEstudiante(nombre);
                        }

                        array.add(data);

                    }
                    numAlumnos.setText(String.valueOf(presente.size()));
                    adapter=new PresenciaAdapter(getContext(),array);
                    listaPresencia.setAdapter(adapter);

                    listaPresencia.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                            builder.setMessage("¿Cambiar presencia?")
                                    .setTitle(array.get(position).getEstudiante())
                                    .setCancelable(false)
                                    .setNeutralButton("Cancelar",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                }
                                            })
                                    .setNegativeButton("Registrar acceso y notificación", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            registrarAcceso(array.get(position).getStudentId());
                                        }
                                    })
                                    .setPositiveButton("Cambiar presencia manual",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(final DialogInterface dialog, int id) {
                                                    Estado estado=new Estado();
                                                    if(array.get(position).isPresente()){
                                                        estado.changePresencia(array.get(position).getId(),false,getContext());
                                                    }else {
                                                        estado.changePresencia(array.get(position).getId(),true,getContext());
                                                    }
                                                }
                                            });
                            AlertDialog alert = builder.create();
                            alert.show();



                        }
                    });

                }else {
                    Toast.makeText(getContext(),"Error",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void registrarAcceso(String studentId){
        params=new HashMap<>();
        acceso=new ParseObject("Acceso");
        escuelaId=((ParseApplication)getActivity().getApplication()).getEscuelaId();
        acceso.put("user", ParseUser.getCurrentUser());
        ParseObject student=ParseObject.createWithoutData("Estudiantes", studentId);
        acceso.put("student",student);
        acceso.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e==null){
                    params.put("accesoObjectId", acceso.getObjectId());
                    params.put("escuelaObjId",escuelaId);
                    ParseCloud.callFunctionInBackground("accesos", params, new FunctionCallback<Object>() {

                        @Override
                        public void done(Object object, ParseException e) {
                            if (e == null) {
                                Log.d("Cloudcode", "Success");
                            } else {
                                Log.d("Error cloud", e.getMessage());
                            }
                        }
                    });
                    Toast.makeText(getContext(),"Acceso registrado exitosamente",Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(getActivity(),"Error, intente de nuevo",Toast.LENGTH_SHORT).show();

                }
            }
        });
    }

    public void setTypefaces(){
        Typeface bold=Typeface.createFromAsset(getActivity().getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(getActivity().getAssets(),avenirMedium);
        Typeface demibold=Typeface.createFromAsset(getActivity().getAssets(),demiBold);

        presenciaTag.setTypeface(medium);
        numAlumnos.setTypeface(medium);


    }

}
