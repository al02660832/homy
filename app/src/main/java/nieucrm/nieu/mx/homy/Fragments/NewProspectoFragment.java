package nieucrm.nieu.mx.skola.Fragments;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class NewProspectoFragment extends Fragment {

    String avenirBold = "font/avenir-next-bold.ttf", avenirMedium = "font/avenir-next-medium.ttf",demibold="font/avenir-next-demi-bold.ttf";

    TextView registroTag,divisorEstudiante,nombrePros,apellidoPro,fechaPros,fechaNac,mamaTag,
            nombreMama,apellidoMama,papaTag,nombrePapa,apellidoPapa,teleProspec,
            telefPapa,correoPa,promocion,enterar,seguimiento,correoTag;
    EditText editNomPros,editApePro,editNomMama,editApeMama,editTelePros,editCoMama,editNomPapa,
            editApePapa,editTelefPapa,editCoPapa,editProm,editEnterar,editSeguim;
    Button btnGuardarProspecto;

    String estudianteNombre,estudianteApellidos,seguimientos,captura,fechaNacimiento,mamaEmail,mamaNombre,mamaApellidos,mamaTelefono,papaNombre,papaApellidos,papaEmail,papaTelefono,promociones;
            //EsutidanteFechaNacimiento
    String escuelaId;
    ParseObject escuela;
    Date estudianteFechaNacimiento;
    boolean nuevo=true;
    String objectId;
    DatePickerDialog.OnDateSetListener date;
    Calendar myCalendar = Calendar.getInstance();
    Activity a;


    public NewProspectoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            a=(Activity) context;
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Bundle b=this.getArguments();
        if (b!=null){
            nuevo=false;
            objectId=b.getString("objectId");
            estudianteNombre=b.getString("estudianteNombre");
            estudianteApellidos=b.getString("estudianteApellidos");
            seguimientos=b.getString("seguimiento");
            captura=b.getString("captura");
            fechaNacimiento=b.getString("fechaNacimiento");
            mamaEmail=b.getString("mamaEmail");
            mamaNombre=b.getString("mamaNombre");
            mamaApellidos=b.getString("mamaApellidos");
            mamaTelefono=b.getString("mamaTelefono");
            papaNombre=b.getString("papaNombre");
            papaApellidos=b.getString("papaApellidos");
            papaEmail=b.getString("papaEmail");
            papaTelefono=b.getString("papaTelefono");
            promociones=b.getString("promocion");
            estudianteFechaNacimiento=(Date)b.getSerializable("estudianteFechaNacimiento");
        }

        return inflater.inflate(R.layout.fragment_new_prospecto, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        registroTag=(TextView)view.findViewById(R.id.registroTag);
        divisorEstudiante=(TextView)view.findViewById(R.id.divisorEstudiante);
        nombrePros=(TextView)view.findViewById(R.id.nombrePros);
        apellidoPro=(TextView)view.findViewById(R.id.apellidoPro);
        fechaPros=(TextView)view.findViewById(R.id.fechaPros);
        fechaNac=(TextView)view.findViewById(R.id.fechaNac);
        mamaTag=(TextView)view.findViewById(R.id.mamaTag);
        nombreMama=(TextView)view.findViewById(R.id.nombreMama);
        apellidoMama=(TextView)view.findViewById(R.id.apellidoMama);
        papaTag=(TextView)view.findViewById(R.id.papaTag);
        nombrePapa=(TextView)view.findViewById(R.id.nombrePapa);
        apellidoPapa=(TextView)view.findViewById(R.id.apellidoPapa);
        telefPapa=(TextView)view.findViewById(R.id.telefPapa);
        correoPa=(TextView)view.findViewById(R.id.correoPa);
        promocion=(TextView)view.findViewById(R.id.promocion);
        enterar=(TextView)view.findViewById(R.id.enterar);
        seguimiento=(TextView)view.findViewById(R.id.seguimiento);
        correoTag=(TextView)view.findViewById(R.id.correoTag);

        editNomPros=(EditText)view.findViewById(R.id.editNomPro);
        editApePro=(EditText)view.findViewById(R.id.editApePro);
        editNomMama=(EditText)view.findViewById(R.id.editNomMama);
        editApeMama=(EditText)view.findViewById(R.id.editApeMama);
        teleProspec=(TextView) view.findViewById(R.id.teleProspec);
        editTelePros=(EditText)view.findViewById(R.id.editTelePros);
        editCoMama=(EditText)view.findViewById(R.id.editCoMama);
        editNomPapa=(EditText)view.findViewById(R.id.editNomPapa);
        editApePapa=(EditText)view.findViewById(R.id.editApePapa);
        editTelefPapa=(EditText)view.findViewById(R.id.editTelefPapa);
        editCoPapa=(EditText)view.findViewById(R.id.editCoPapa);
        editProm=(EditText)view.findViewById(R.id.editProm);
        editEnterar=(EditText)view.findViewById(R.id.editEnterar);
        editSeguim=(EditText)view.findViewById(R.id.editSeguim);

        btnGuardarProspecto=(Button)view.findViewById(R.id.btnGuardarProspecto);

        editNomPros.setText(estudianteNombre);
        editApePro.setText(estudianteApellidos);
        fechaNac.setText(fechaNacimiento);
        editNomMama.setText(mamaNombre);
        editApeMama.setText(mamaApellidos);
        editTelePros.setText(mamaTelefono);
        editCoMama.setText(mamaEmail);

        editNomPapa.setText(papaNombre);
        editApePapa.setText(papaApellidos);
        editTelefPapa.setText(papaTelefono);
        editCoPapa.setText(papaEmail);

        editProm.setText(promociones);
        editEnterar.setText(captura);
        editSeguim.setText(seguimientos);

        getTypeface();

        btnGuardarProspecto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (nuevo){
                    saveProspecto();
                    //Toast.makeText(getContext(),"Nuevo",Toast.LENGTH_SHORT).show();
                }else {
                    updateProspecto();
                    //Toast.makeText(getContext(),"Actualizar",Toast.LENGTH_SHORT).show();

                }
            }
        });

        fechaNac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDate();
            }
        });

    }

    public void getTypeface(){
        Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), avenirBold);
        Typeface medium = Typeface.createFromAsset(getActivity().getAssets(), avenirMedium);
        Typeface dem=Typeface.createFromAsset(getActivity().getAssets(),demibold);

        registroTag.setTypeface(dem);
        divisorEstudiante.setTypeface(dem);
        nombrePros.setTypeface(medium);
        apellidoPro.setTypeface(medium);
        fechaPros.setTypeface(medium);
        fechaNac.setTypeface(medium);
        mamaTag.setTypeface(dem);
        nombreMama.setTypeface(medium);
        apellidoMama.setTypeface(medium);
        papaTag.setTypeface(dem);
        nombrePapa.setTypeface(medium);
        apellidoPapa.setTypeface(medium);
        teleProspec.setTypeface(medium);
        telefPapa.setTypeface(medium);
        correoPa.setTypeface(medium);
        promocion.setTypeface(medium);
        enterar.setTypeface(medium);
        seguimiento.setTypeface(medium);
        editNomPros.setTypeface(medium);
        editApePro.setTypeface(medium);
        editNomMama.setTypeface(medium);
        editApeMama.setTypeface(medium);
        editTelePros.setTypeface(medium);
        editCoMama.setTypeface(medium);
        editNomPapa.setTypeface(medium);
        editApePapa.setTypeface(medium);
        editTelefPapa.setTypeface(medium);
        editCoPapa.setTypeface(medium);
        editProm.setTypeface(medium);
        editEnterar.setTypeface(medium);
        editSeguim.setTypeface(medium);
        correoTag.setTypeface(medium);

        btnGuardarProspecto.setTypeface(medium);
    }

    public void saveProspecto(){
        escuelaId=((ParseApplication)a.getApplication()).getEscuelaId();
        ParseObject escuela=ParseObject.createWithoutData("Escuela",escuelaId);

        captura=editEnterar.getText().toString();
        estudianteNombre=editNomPros.getText().toString();
        estudianteApellidos=editApePro.getText().toString();
        seguimientos=editSeguim.getText().toString();
        estudianteFechaNacimiento=myCalendar.getTime();

        mamaEmail=editCoMama.getText().toString();
        mamaNombre=editNomMama.getText().toString();
        mamaApellidos=editApeMama.getText().toString();
        mamaTelefono=editTelePros.getText().toString();
        papaNombre=editNomPapa.getText().toString();
        papaApellidos=editApePapa.getText().toString();
        papaEmail=editCoPapa.getText().toString();
        papaTelefono=editTelefPapa.getText().toString();
        promociones=editProm.getText().toString();

        ParseObject prospecto=new ParseObject("Prospecto");
        prospecto.put("captura",captura);
        prospecto.put("estudianteApellidos",estudianteApellidos);
        prospecto.put("estudianteFechaNacimiento",estudianteFechaNacimiento);
        prospecto.put("seguimiento",seguimientos);
        prospecto.put("papaNombre",papaNombre);
        prospecto.put("mamaApellidos",mamaApellidos);
        prospecto.put("mamaNombre",mamaNombre);
        prospecto.put("papaApellidos",papaApellidos);
        prospecto.put("promocion",promociones);
        prospecto.put("mamaEmail",mamaEmail);
        prospecto.put("papaTelefono",papaTelefono);
        prospecto.put("papaEmail",papaEmail);
        prospecto.put("estudianteNombre",estudianteNombre);
        prospecto.put("mamaTelefono",mamaTelefono);
        prospecto.put("escuela",escuela);
        prospecto.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e==null){
                    Toast.makeText(a,"Prospecto creado exitosamente",Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(a,"Error. Intente de nuevo",Toast.LENGTH_SHORT).show();

                }
            }
        });

    }

    public void updateProspecto(){
        captura=editEnterar.getText().toString();
        estudianteNombre=editNomPros.getText().toString();
        estudianteApellidos=editApePro.getText().toString();
        seguimientos=editSeguim.getText().toString();
        estudianteFechaNacimiento=myCalendar.getTime();

        mamaEmail=editCoMama.getText().toString();
        mamaNombre=editNomMama.getText().toString();
        mamaApellidos=editApeMama.getText().toString();
        mamaTelefono=editTelePros.getText().toString();
        papaNombre=editNomPapa.getText().toString();
        papaApellidos=editApePapa.getText().toString();
        papaEmail=editCoPapa.getText().toString();
        papaTelefono=editTelefPapa.getText().toString();
        promociones=editProm.getText().toString();
        ParseQuery<ParseObject>pQuery=ParseQuery.getQuery("Prospecto");
        pQuery.whereEqualTo("objectId",objectId);
        pQuery.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject prospecto, ParseException e) {
                if (e==null){
                    escuelaId=((ParseApplication)a.getApplication()).getEscuelaId();
                    ParseObject escuela=ParseObject.createWithoutData("Escuela",escuelaId);
                    prospecto.put("captura",captura);
                    prospecto.put("estudianteApellidos",estudianteApellidos);
                    prospecto.put("estudianteFechaNacimiento",estudianteFechaNacimiento);
                    prospecto.put("seguimiento",seguimientos);
                    prospecto.put("papaNombre",papaNombre);
                    prospecto.put("mamaApellidos",mamaApellidos);
                    prospecto.put("mamaNombre",mamaNombre);
                    prospecto.put("papaApellidos",papaApellidos);
                    prospecto.put("promociones",promociones);
                    prospecto.put("mamaEmail",mamaEmail);
                    prospecto.put("papaTelefono",papaTelefono);
                    prospecto.put("papaEmail",papaEmail);
                    prospecto.put("estudianteNombre",estudianteNombre);
                    prospecto.put("mamaTelefono",mamaTelefono);
                    prospecto.put("escuela",escuela);
                    prospecto.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e==null){
                                Toast.makeText(a,"Prospecto guardado exitosamente",Toast.LENGTH_SHORT).show();
                            }else {
                                Log.e("Error",e.getMessage());
                            }
                        }
                    });
                }else {
                    Log.e("Error",e.getMessage());
                }
            }
        });
    }

    public void getDate(){
        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "dd/MM/yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                estudianteFechaNacimiento=myCalendar.getTime();
                fechaNac.setText(sdf.format(myCalendar.getTime()));
            }

        };

        fechaNac.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(a, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });
    }

}
