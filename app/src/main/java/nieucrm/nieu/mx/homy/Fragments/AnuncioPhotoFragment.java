package nieucrm.nieu.mx.skola.Fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.chrisbanes.photoview.PhotoView;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;

import nieucrm.nieu.mx.skola.Helpers.Constants;
import nieucrm.nieu.mx.skola.Helpers.Util;
import nieucrm.nieu.mx.skola.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AnuncioPhotoFragment extends Fragment {

    PDFView pdfImagen;
    PhotoView anuncioPhoto;
    ParseObject anuncio;
    TransferUtility utility;
    File outputDir;
    File pdfFile, imageFile;
    boolean anuncioP,aws,picasso;
    String objectId;
    String parseUri;
    ProgressBar progressImagen;
    Activity a;

    public AnuncioPhotoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            a=(Activity) context;
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        Bundle b=this.getArguments();
        objectId=b.getString("objectId");
        if (b.getBoolean("anuncioPhoto")){
            anuncioP=true;
        }else {
            if (b.getBoolean("aws")){
                aws=true;
            }else {
                if (b.getString("parseUri")!=null){
                    picasso=true;
                    parseUri=b.getString("parseUri");
                }
            }
        }
        return inflater.inflate(R.layout.fragment_anuncio_photo, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        pdfImagen=(PDFView) view.findViewById(R.id.imagenPdf);
        anuncioPhoto=(PhotoView) view.findViewById(R.id.photoAnuncio);
        progressImagen=(ProgressBar)view.findViewById(R.id.progressImage);

        if (anuncioP){
            getAnuncio();
        }else {
            if (aws){
                //Download from Image from anuncio
                downloadFromS3(objectId);
            }else {
                if (picasso){
                    //Load image from Parse Server
                    Picasso.with(a).load(parseUri).resize(500,800).into(anuncioPhoto);
                }
            }
        }
    }


    public void getAnuncio(){
        anuncio=ParseObject.createWithoutData("anuncio",objectId);
        ParseQuery<ParseObject>photo=ParseQuery.getQuery("AnuncioPhoto");
        photo.include("anuncio");
        photo.whereEqualTo("anuncio",anuncio);
        photo.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (e==null){
                    if (object.getBoolean("aws")){
                        //Download from S3
                        if (object.getString("TipoArchivo")==null){
                            downloadFromS3(object.getObjectId());
                        }else {
                            if (object.getString("TipoArchivo").equals("PDF")){
                                //Descargar PDF
                                downloadPDFFromS3(object.getObjectId());
                                //downloadFile();
                                Log.i("Tipo de archivo: ","PDF");
                            }else {
                                //Descargar fotos
                                downloadFromS3(object.getObjectId());
                                Log.i("Tipo de archivo: ","Imagen");

                            }
                        }

                    }else {
                        Log.i("File", "Get File from Parse server");
                    }
                }else {
                    Log.e("Error","Error de conexión, compruebe su conexión a internet");
                }
            }
        });
    }

    public void downloadPDFFromS3(String objectId){

        try {
            utility= Util.getTransferUtility(a);
        }catch (Exception e1){
            Log.e("Error credencial",e1.getMessage());
        }

        outputDir = a.getCacheDir();
        try {
            pdfFile= File.createTempFile(objectId, ".pdf", outputDir);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            TransferObserver observe= utility.download(Constants.BUCKET_NAME,objectId,pdfFile);
            observe.setTransferListener(new TransferListener() {
                @Override
                public void onStateChanged(int id, TransferState state) {
                    if (state==TransferState.COMPLETED){
                        progressImagen.setVisibility(View.GONE);
                        pdfImagen.setVisibility(View.VISIBLE);

                        pdfImagen.fromFile(pdfFile).defaultPage(1).enableSwipe(true).onLoad(new OnLoadCompleteListener() {
                            @Override
                            public void loadComplete(int nbPages) {
                                Log.d("Number of pages",String.valueOf(nbPages));

                                Toast.makeText(a,"Download is ready",Toast.LENGTH_SHORT).show();
                            }
                        }).onPageChange(new OnPageChangeListener() {
                            @Override
                            public void onPageChanged(int page, int pageCount) {
                                Log.d("Page:",String.valueOf(page));
                            }
                        }).load();
                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                    long _bytesCurrent = bytesCurrent;
                    long _bytesTotal = bytesTotal;

                    float percentage =  ((float)_bytesCurrent /(float)_bytesTotal * 100);
                    Log.d("percentage","" +percentage);
                }

                @Override
                public void onError(int id, Exception ex) {
                    progressImagen.setVisibility(View.GONE);
                    Log.d("Error de Amazon",ex.getMessage());
                }
            });

        }catch (Exception e){
            Log.e("Error",e.getMessage());
        }
    }

    public void downloadFromS3(String objectId){

        try {
            utility= Util.getTransferUtility(a);
        }catch (Exception e1){
            Log.e("Error credencial",e1.getMessage());
        }

        outputDir = a.getCacheDir();
        try {
            imageFile= File.createTempFile("prefix", "extension", outputDir);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            TransferObserver observe= utility.download(Constants.BUCKET_NAME,objectId,imageFile);
            observe.setTransferListener(new TransferListener() {
                @Override
                public void onStateChanged(int id, TransferState state) {
                    if (state==TransferState.COMPLETED){

                        Bitmap bitmap= BitmapFactory.decodeFile(imageFile.getAbsolutePath());
                        anuncioPhoto.setImageBitmap(bitmap);
                        progressImagen.setVisibility(View.GONE);
                        anuncioPhoto.setVisibility(View.VISIBLE);

                        try {
                            Toast.makeText(a, "Foto lista", Toast.LENGTH_SHORT).show();
                        }catch (Exception e1){
                            Log.e("Exception",e1.getMessage());
                        }
                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                    long _bytesCurrent = bytesCurrent;
                    long _bytesTotal = bytesTotal;

                    float percentage =  ((float)_bytesCurrent /(float)_bytesTotal * 100);
                    Log.d("percentage","" +percentage);
                }

                @Override
                public void onError(int id, Exception ex) {

                    Log.d("Error de Amazon",ex.getMessage());
                    Toast.makeText(a,"Error, intente de nuevo",Toast.LENGTH_SHORT).show();
                }
            });

        }catch (Exception e){
            Log.e("Error",e.getMessage());
        }
    }
}
