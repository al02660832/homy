package nieucrm.nieu.mx.skola.Fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import nieucrm.nieu.mx.skola.Adapter.ActivityAdapter;
import nieucrm.nieu.mx.skola.DataModel.ActivityData;
import nieucrm.nieu.mx.skola.R;

/**
 * A simple {@link Fragment} subclass.
 */

public class ActivityFragment extends Fragment {

    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf",demibold="font/avenir-next-demi-bold.ttf";
    ProgressBar progressPlaneacion;

    ListView listaPlaneación;
    String semana,dia,mes;
    TextView txtSemana;
    ParseQuery<ParseObject>planeacion;

    List<ParseObject>objectList;

    String groupId, nombreGrupo;

    ArrayList<String>numbersOfTheWeekArray=new ArrayList<>();
    ArrayList<Date>datesOfCurrentWeek=new ArrayList<>();
    ArrayList<String>planeacionDaysArray=new ArrayList<>();

    ArrayList<ParseObject>array=new ArrayList<>();

    ArrayList<ActivityData>activityData=new ArrayList<>();

    ActivityAdapter adapter;

    int usertype;

    String[] strMonths = new String[]{
            "Enero", "Febebro", "Marzo",
            "Abril", "Mayo", "Junio",
            "Julio","Agosto", "Septiembre",
            "Octubre", "Noviembre", "Diciembre"};
    String[] strDays = new String[]{ "Lunes","Martes","Miércoles","Jueves","Viernes"};
    Activity a;

    public ActivityFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            a=(Activity) context;
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        Bundle b=this.getArguments();
        if (b!=null){
            groupId=b.getString("grupoId");
            nombreGrupo=b.getString("grupoNombre");
        }

        return inflater.inflate(R.layout.fragment_activity, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        txtSemana=(TextView)view.findViewById(R.id.textSemana);
        progressPlaneacion=(ProgressBar)view.findViewById(R.id.progressPlaneacion);

        ParseUser user=ParseUser.getCurrentUser();
        usertype=user.getNumber("usertype").intValue();

        /** end after 1 month from now */
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 1);

        /** start before 1 month from now */
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, -1);

        listaPlaneación=(ListView)view.findViewById(R.id.listActivity);

        setTypeface();

        getWeek();

    }

    public void getWeek() {

        numbersOfTheWeekArray=new ArrayList<>();
        datesOfCurrentWeek=new ArrayList<>();
        planeacionDaysArray=new ArrayList<>();

       array=new ArrayList<>();

        activityData=new ArrayList<>();

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

        Calendar friday = Calendar.getInstance();
        friday.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);

        Calendar inicio=Calendar.getInstance();
        inicio.set(Calendar.DAY_OF_WEEK,Calendar.SUNDAY);



        for (int i=0;i<5;i++){
            inicio.add(Calendar.DATE,1);
            String dia=(String)android.text.format.DateFormat.format("dd",inicio);
            datesOfCurrentWeek.add(inicio.getTime());
            numbersOfTheWeekArray.add(dia);

            //Toast.makeText(getContext(),String.valueOf(datesOfCurrentWeek.size()),Toast.LENGTH_SHORT).show();
        }

        String lun=(String)android.text.format.DateFormat.format("dd",cal);

        //String vie=(String)android.text.format.DateFormat.format("dd",friday);

        SimpleDateFormat format = new SimpleDateFormat("dd 'de' MMMM ",new Locale("es"));
        String vie=format.format(friday.getTime());


        semana="Semana del "+lun+" al "+vie;

        mes=strMonths[cal.get(Calendar.MONTH)];

        txtSemana.setText(semana);

        ParseObject grupo=ParseObject.createWithoutData("grupo",groupId);

        Calendar in=Calendar.getInstance();
        in.set(Calendar.DAY_OF_WEEK,Calendar.SUNDAY);

        Calendar fin=Calendar.getInstance();
        fin.set(Calendar.DAY_OF_WEEK,Calendar.FRIDAY);



        planeacion=ParseQuery.getQuery("Planeacion");
        planeacion.whereGreaterThanOrEqualTo("fecha",in.getTime());
        planeacion.whereLessThanOrEqualTo("fecha",fin.getTime());
        planeacion.orderByAscending("fecha");
        planeacion.whereEqualTo("grupo",grupo);
        planeacion.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                progressPlaneacion.setVisibility(View.GONE);
                listaPlaneación.setVisibility(View.VISIBLE);
                if (e==null){
                    objectList=objects;
                    for (ParseObject o:objects){
                        Date f=o.getDate("fecha");
                        String numeroDia=(String)android.text.format.DateFormat.format("dd",f);
                        planeacionDaysArray.add(numeroDia);
                        array.add(o);
                    }

                    for (int i=0;i<numbersOfTheWeekArray.size();i++){
                        ActivityData data=new ActivityData();
                        data.setNumero(numbersOfTheWeekArray.get(i));
                        if (i==strDays[i].length()){

                        }else {
                            data.setDia(strDays[i]);
                        }
                        data.setDate(datesOfCurrentWeek.get(i));
                        SimpleDateFormat format = new SimpleDateFormat("dd 'de' MMMM",new Locale("es"));
                        String f=format.format(datesOfCurrentWeek.get(i));
                        data.setFecha(f);
                        data.setNombreGrupo(nombreGrupo);
                        data.setGrupo(groupId);

                        String day=numbersOfTheWeekArray.get(i);

                        if (planeacionDaysArray.contains(day)){
                            int d=planeacionDaysArray.indexOf(day);
                            data.setTitulo(array.get(d).getString("titulo"));
                            data.setDescripcion(array.get(d).getString("descripcion"));
                            data.setNotas(array.get(d).getString("notas"));
                            data.setTema(array.get(d).getString("tema"));
                            data.setGrupo(groupId);
                            data.setVacio(false);
                            data.setValores(array.get(d).getString("valores"));
                            data.setHabitos(array.get(d).getString("habitos"));
                            data.setParseO(true);


                        }else {

                            data.setTitulo("Actividad por planear");
                            data.setParseO(false);
                            data.setVacio(true);
                        }

                        activityData.add(data);

                    }


                    adapter=new ActivityAdapter(a,activityData);
                    listaPlaneación.setAdapter(adapter);

                }else {
                    Toast.makeText(a,"Error",Toast.LENGTH_SHORT).show();

                }
            }
        });


    }

    public void setTypeface(){
        Typeface bold=Typeface.createFromAsset(a.getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(a.getAssets(),avenirMedium);
        Typeface dem=Typeface.createFromAsset(a.getAssets(),demibold);

        txtSemana.setTypeface(dem);
    }

}
