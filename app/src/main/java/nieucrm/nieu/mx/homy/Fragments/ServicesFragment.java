package nieucrm.nieu.mx.skola.Fragments;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.HashMap;

import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class ServicesFragment extends Fragment {

    TextView statusServicio,nombreS,descripS,alumS,txtS,txtFS,txtFDS,comentario,comentarioServicio,precioTag
            ,precioServicioTxt;

    Button rechazarServicio,enviarMensajePadre,mandarRecordatorio;

    String tipo;

    ImageButton btnCancelarServicio,btnAceptarServicio,btnPagarServicio,btnCompletarServicio;

    String nombre,precio,descripcion,objectId,estado,comentarios,alumnoId,fecha,nombreAlumno;
    int status,usertype;
    String avenirBold = "font/avenir-next-bold.ttf", avenirMedium = "font/avenir-next-medium.ttf",demibold="font/avenir-next-demi-bold.ttf";

    String escuelaId;

    HashMap<String, Object> params;


    public ServicesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        Bundle bundle=this.getArguments();

        nombre=bundle.getString("nombre");
        precio=bundle.getString("precio");
        descripcion=bundle.getString("descripcion");
        objectId=bundle.getString("objectid");
        estado=bundle.getString("estado");
        comentarios=bundle.getString("comentarios");
        alumnoId=bundle.getString("alumnoId");
        fecha=bundle.getString("fecha");
        status=bundle.getInt("status");
        nombreAlumno=bundle.getString("alumno");




        return inflater.inflate(R.layout.fragment_services, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        statusServicio=(TextView)view.findViewById(R.id.statusServicio);
        nombreS=(TextView)view.findViewById(R.id.nombreS);
        descripS=(TextView)view.findViewById(R.id.descripS);
        alumS=(TextView)view.findViewById(R.id.alumS);
        txtS=(TextView)view.findViewById(R.id.txtS);
        txtFS=(TextView)view.findViewById(R.id.txtFS);
        txtFDS=(TextView)view.findViewById(R.id.txtFDS);
        comentario=(TextView)view.findViewById(R.id.comentario);
        comentarioServicio=(TextView)view.findViewById(R.id.comentarioServicio);
        precioTag=(TextView)view.findViewById(R.id.precioTag);
        precioServicioTxt=(TextView)view.findViewById(R.id.precioServicioTxt);

        btnCancelarServicio=(ImageButton)view.findViewById(R.id.btnCancelarServicio);
        btnAceptarServicio=(ImageButton)view.findViewById(R.id.btnAceptarServicio);
        btnCompletarServicio=(ImageButton)view.findViewById(R.id.btnCompletarServicio);
        btnPagarServicio=(ImageButton)view.findViewById(R.id.btnPagarServicio);

        rechazarServicio=(Button)view.findViewById(R.id.rechazarServicio);
        enviarMensajePadre=(Button)view.findViewById(R.id.enviarMensajePadre);
        mandarRecordatorio=(Button)view.findViewById(R.id.mandarRecordatorio);

        usertype= ParseUser.getCurrentUser().getNumber("usertype").intValue();

        escuelaId=((ParseApplication)getActivity().getApplication()).getEscuelaId();


        setListener();
        fonts();
        defineStatus(status);
        setInfo();




    }

    public void setListener(){
        btnPagarServicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pagarServicio();
            }
        });
        btnAceptarServicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aceptarServicio();
            }
        });
        btnPagarServicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pagarServicio();
            }
        });
        btnCompletarServicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                completarServicio();
            }
        });
        btnCancelarServicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelarServicio();
            }
        });
        rechazarServicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rechazarServicio();
            }
        });

        mandarRecordatorio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendAviso();
            }
        });
        enviarMensajePadre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });
    }

    public void setInfo(){
        statusServicio.setText(estado);
        nombreS.setText(nombre);
        descripS.setText(descripcion);
        txtFDS.setText(fecha);
        comentarioServicio.setText(comentarios);
        precioServicioTxt.setText(precio);
        txtS.setText(nombreAlumno);

    }


    /*
    * Solicitado (0) - Papá
Aceptado/En Progreso (1) - Escuela
Completado (2) - Escuela
Pagado (3) - Escuela
Rechazado por la escuela (8) - Escuela
Cancelado por el Papá (9) - Papá
    * */
    public void defineStatus(int status){
        switch (status){
            case 0:
                estado="Solicitado";

                //Rechazar

                if (usertype==1||usertype==0){
                    btnAceptarServicio.setVisibility(View.VISIBLE);
                    rechazarServicio.setVisibility(View.VISIBLE);
                }else {
                    btnCancelarServicio.setVisibility(View.VISIBLE);
                }

                break;
            case 1:
                estado="Aceptado/En Progreso";

                if (usertype==1||usertype==0){
                    btnCompletarServicio.setVisibility(View.VISIBLE);
                    rechazarServicio.setVisibility(View.VISIBLE);
                }


                //Rechazar

                break;
            case 2:
                estado="Completado";
                if (usertype==1||usertype==0){
                    btnPagarServicio.setVisibility(View.VISIBLE);
                    mandarRecordatorio.setVisibility(View.VISIBLE);
                }


                //Mandar recordatorio


                break;
            case 3:
                estado="Pagado";

                break;
            case 8:
                estado="Rechazado";
                if (usertype==1||usertype==0){
                    btnAceptarServicio.setVisibility(View.VISIBLE);
                    enviarMensajePadre.setVisibility(View.VISIBLE);
                }

                //Mandar mensaje a papa

                break;
            case 9:
                estado="Cancelado";
                if (usertype==1||usertype==0){
                    btnAceptarServicio.setVisibility(View.VISIBLE);
                    enviarMensajePadre.setVisibility(View.VISIBLE);
                }


                //Mandar mensaje a papa
                break;
        }
    }

    public void rechazarServicio(){
        params=new HashMap<>();

        ParseQuery<ParseObject> q=ParseQuery.getQuery("ServicioSolicitado");
        q.whereEqualTo("objectId",objectId);
        q.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (e==null){
                    object.put("status",8);
                    object.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            Toast.makeText(getContext(),"Servicio Rechazado",Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }

    public void cancelarServicio(){
        params=new HashMap<>();

        ParseQuery<ParseObject> q=ParseQuery.getQuery("ServicioSolicitado");
        q.whereEqualTo("objectId",objectId);
        q.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (e==null){
                    object.put("status",9);
                    object.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {

                            if (e==null){
                                params.put("statusString",estado);
                                params.put("estudianteId",alumnoId);
                                params.put("escuelaObjId",escuelaId);

                                ParseCloud.callFunctionInBackground("servicioSolicitadoNotifyParentStatusChanged", params, new FunctionCallback<Object>() {

                                    @Override
                                    public void done(Object object, ParseException e) {
                                        if (e == null){
                                            Log.d("Cloudcode","Success");
                                        }else {
                                            Log.d("Error",e.getMessage());
                                        }
                                    }
                                });

                                Toast.makeText(getContext(),"Servicio Cancelado",Toast.LENGTH_SHORT).show();

                            }

                        }
                    });
                }
            }
        });
    }


    public void pagarServicio(){
        params=new HashMap<>();

        ParseQuery<ParseObject> q=ParseQuery.getQuery("ServicioSolicitado");
        q.whereEqualTo("objectId",objectId);
        q.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (e==null){
                    object.put("status",3);
                    object.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {

                            if (e==null){
                                params.put("statusString",estado);
                                params.put("estudianteId",alumnoId);
                                params.put("escuelaObjId",escuelaId);

                                ParseCloud.callFunctionInBackground("servicioSolicitadoNotifyParentStatusChanged", params, new FunctionCallback<Object>() {

                                    @Override
                                    public void done(Object object, ParseException e) {
                                        if (e == null){
                                            Log.d("Cloudcode","Success");
                                        }else {
                                            Log.d("Error",e.getMessage());
                                        }
                                    }
                                });

                                Toast.makeText(getContext(),"Servicio pagado",Toast.LENGTH_SHORT).show();


                            }

                        }
                    });
                }
            }
        });
    }

    public void completarServicio(){
        params=new HashMap<>();

        ParseQuery<ParseObject> q=ParseQuery.getQuery("ServicioSolicitado");
        q.whereEqualTo("objectId",objectId);
        q.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (e==null){
                    object.put("status",2);
                    object.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e==null){
                                params.put("statusString",estado);
                                params.put("estudianteId",alumnoId);
                                params.put("escuelaObjId",escuelaId);

                                ParseCloud.callFunctionInBackground("servicioSolicitadoNotifyParentStatusChanged", params, new FunctionCallback<Object>() {

                                    @Override
                                    public void done(Object object, ParseException e) {
                                        if (e == null){
                                            Log.d("Cloudcode","Success");
                                        }else {
                                            Log.d("Error",e.getMessage());
                                        }
                                    }
                                });

                                Toast.makeText(getContext(),"Servicio completado",Toast.LENGTH_SHORT).show();



                            }
                        }
                    });
                }
            }
        });
    }

    public void aceptarServicio(){
        params=new HashMap<>();

        ParseQuery<ParseObject> q=ParseQuery.getQuery("ServicioSolicitado");
        q.whereEqualTo("objectId",objectId);
        q.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (e==null){
                    object.put("status",1);
                    object.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {

                            if (e==null){
                                params.put("statusString",estado);
                                params.put("estudianteId",alumnoId);
                                params.put("escuelaObjId",escuelaId);

                                ParseCloud.callFunctionInBackground("servicioSolicitadoNotifyParentStatusChanged", params, new FunctionCallback<Object>() {

                                    @Override
                                    public void done(Object object, ParseException e) {
                                        if (e == null){
                                            Log.d("Cloudcode","Success");
                                        }else {
                                            Log.d("Error",e.getMessage());
                                        }
                                    }
                                });

                                Toast.makeText(getContext(),"Servicio aceptado",Toast.LENGTH_SHORT).show();

                            }



                        }
                    });
                }
            }
        });
    }

    public void sendAviso(){
        ParseQuery<ParseObject>query=ParseQuery.getQuery("tipoAnuncio");
        query.whereEqualTo("nombre","Aviso de Pago");
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                tipo=object.getObjectId();
            }
        });
        Fragment fragment = new NewMessageFragment();
        Bundle bundle=new Bundle();
        bundle.putString("tipo",tipo);
        bundle.putString("idEstudiante",alumnoId);
        fragment.setArguments(bundle);
        FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
        transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
    }

    public void sendMessage(){
        Fragment fragment = new NewMessageFragment();
        Bundle bundle=new Bundle();
        bundle.putString("idEstudiante",alumnoId);
        fragment.setArguments(bundle);
        FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
        transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
    }


    public void fonts(){
        Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), avenirBold);
        Typeface medium = Typeface.createFromAsset(getActivity().getAssets(), avenirMedium);
        Typeface demi=Typeface.createFromAsset(getActivity().getAssets(),demibold);

        precioServicioTxt.setTypeface(demi);
        statusServicio.setTypeface(demi);
        nombreS.setTypeface(demi);
        descripS.setTypeface(medium);
        txtFDS.setTypeface(medium);
        comentarioServicio.setTypeface(medium);
        txtS.setTypeface(medium);

        rechazarServicio.setTypeface(demi);
        enviarMensajePadre.setTypeface(demi);
        mandarRecordatorio.setTypeface(demi);

        alumS.setTypeface(medium);
        txtFS.setTypeface(medium);
        comentario.setTypeface(medium);
        precioTag.setTypeface(medium);

    }

}
