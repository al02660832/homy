package nieucrm.nieu.mx.skola.DataModel;

import java.util.Date;

/**
 * Created by Carlos Romero on 26/12/2016.
 */

public class AnuncioData {

    String tipo;
    String materia;
    String destinatario,emisor,autorId;
    String descripcion;
    String genero;
    String id;
    String parseUri;
    boolean awsAttachment;
    boolean tablaAnuncio;

    boolean aviso,durmio, momento;
    String pipi,popo,desayuno,comida,merienda,colacion,tiempo,leche,comentario,horario;



    String fechaMes,fechaEntrega;
    Date fecha,fechaE;

    public AnuncioData() {
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getMateria() {
        return materia;
    }

    public void setMateria(String materia) {
        this.materia = materia;
    }

    public String getDestinatario() {
        return destinatario;
    }

    public void setDestinatario(String destinatario) {
        this.destinatario = destinatario;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getFechaMes() {
        return fechaMes;
    }

    public void setFechaMes(String fechaMes) {
        this.fechaMes = fechaMes;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEmisor() {
        return emisor;
    }

    public void setEmisor(String emisor) {
        this.emisor = emisor;
    }

    public String getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(String fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public Date getFechaE() {
        return fechaE;
    }

    public void setFechaE(Date fechaE) {
        this.fechaE = fechaE;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getAutorId() {
        return autorId;
    }

    public void setAutorId(String autorId) {
        this.autorId = autorId;
    }

    public String getID() {
        return id;
    }

    public void setID(String id) {
        this.id = id;
    }

    public String getParseUri() {
        return parseUri;
    }

    public void setParseUri(String parseUri) {
        this.parseUri = parseUri;
    }

    public boolean isAwsAttachment() {
        return awsAttachment;
    }

    public void setAwsAttachment(boolean awsAttachment) {
        this.awsAttachment = awsAttachment;
    }

    public boolean isTablaAnuncio() {
        return tablaAnuncio;
    }

    public void setTablaAnuncio(boolean tablaAnuncio) {
        this.tablaAnuncio = tablaAnuncio;
    }

    public boolean isAviso() {
        return aviso;
    }

    public void setAviso(boolean aviso) {
        this.aviso = aviso;
    }

    public boolean isDurmio() {
        return durmio;
    }

    public void setDurmio(boolean durmio) {
        this.durmio = durmio;
    }

    public String getPipi() {
        return pipi;
    }

    public void setPipi(String pipi) {
        this.pipi = pipi;
    }

    public String getPopo() {
        return popo;
    }

    public void setPopo(String popo) {
        this.popo = popo;
    }

    public String getDesayuno() {
        return desayuno;
    }

    public void setDesayuno(String desayuno) {
        this.desayuno = desayuno;
    }

    public String getComida() {
        return comida;
    }

    public void setComida(String comida) {
        this.comida = comida;
    }

    public String getMerienda() {
        return merienda;
    }

    public void setMerienda(String merienda) {
        this.merienda = merienda;
    }

    public String getColacion() {
        return colacion;
    }

    public void setColacion(String colacion) {
        this.colacion = colacion;
    }

    public String getTiempo() {
        return tiempo;
    }

    public void setTiempo(String tiempo) {
        this.tiempo = tiempo;
    }

    public boolean isMomento() {
        return momento;
    }

    public void setMomento(boolean momento) {
        this.momento = momento;
    }

    public String getLeche() {
        return leche;
    }

    public void setLeche(String leche) {
        this.leche = leche;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }
}
