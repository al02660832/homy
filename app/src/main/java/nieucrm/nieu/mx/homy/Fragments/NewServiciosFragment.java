package nieucrm.nieu.mx.skola.Fragments;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewServiciosFragment extends Fragment {

    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf",demibold="font/avenir-next-demi-bold.ttf";

    TextView crearServicioTag,nombreServicioTag,precioServicioTag,descripcionServicio,servicioActivoTag,descripcionServicioActivo;
    EditText editNombreServicio,editPrecioServicio,editDescripcionServicio;
    Switch switchServicio;
    ImageButton guardarServicio;

    String nombre,descripcion,objectId;
    int precio;
    boolean estado;
    String escuelaId;
    ParseObject escuela;

    ParseObject servicio;

    public NewServiciosFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment




        return inflater.inflate(R.layout.fragment_new_servicios, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        crearServicioTag=(TextView)view.findViewById(R.id.crearServicioTag);
        nombreServicioTag=(TextView)view.findViewById(R.id.nombreServicioTag);
        precioServicioTag=(TextView)view.findViewById(R.id.precioServicioTag);
        descripcionServicio=(TextView)view.findViewById(R.id.descripcionServicio);
        servicioActivoTag=(TextView)view.findViewById(R.id.servicioActivoTag);
        descripcionServicioActivo=(TextView)view.findViewById(R.id.descripcionServicioActivo);

        guardarServicio=(ImageButton)view.findViewById(R.id.guardarServicio);

        editNombreServicio=(EditText) view.findViewById(R.id.editNombreServicio);
        editPrecioServicio=(EditText) view.findViewById(R.id.editPrecioServicio);
        editDescripcionServicio=(EditText) view.findViewById(R.id.editDescripcionServicio);

        switchServicio=(Switch)view.findViewById(R.id.switchServicio);

        Bundle b=this.getArguments();
        if (b!=null) {
            precio = b.getInt("precio");
            nombre = b.getString("nombre");
            descripcion = b.getString("descripcion");
            estado = b.getBoolean("activo");
            objectId = b.getString("objectId");

            editPrecioServicio.setText(String.valueOf(precio));
            editNombreServicio.setText(nombre);
            editDescripcionServicio.setText(descripcion);
            switchServicio.setChecked(estado);
        }


            if (objectId==null){
                guardarServicio.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        newService();
                    }
                });
            }else {
                guardarServicio.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        updateService();
                    }
                });
            }




        setTypef();

    }

    public void newService(){
        nombre=editNombreServicio.getText().toString().trim();
        precio=Integer.valueOf(editPrecioServicio.getText().toString().trim());
        descripcion=editDescripcionServicio.getText().toString().trim();
        estado=switchServicio.isChecked();

        escuelaId=((ParseApplication)getActivity().getApplication()).getEscuelaId();
        escuela=ParseObject.createWithoutData("Escuela",escuelaId);


        servicio=new ParseObject("Servicio");
        servicio.put("nombre",nombre);
        servicio.put("precio",precio);
        servicio.put("descripcion",descripcion);
        servicio.put("activo",estado);
        servicio.put("escuela",escuela);
        servicio.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e==null){
                    getActivity().getSupportFragmentManager().popBackStack();

                    Log.d("Servicio: ","Creado exitosamente");

                }else {
                    Log.d("Error",e.getMessage());
                }
            }
        });
    }

    public void updateService(){
        escuelaId=((ParseApplication)getActivity().getApplication()).getEscuelaId();
        escuela=ParseObject.createWithoutData("Escuela",escuelaId);
        ParseQuery<ParseObject> q=ParseQuery.getQuery("Servicio");
        q.whereEqualTo("objectId",objectId);
        q.whereEqualTo("escuela",escuela);

        q.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (e==null){
                    object.put("activo",estado);
                    object.put("nombre",nombre);
                    object.put("precio",precio);
                    object.put("descripcion",descripcion);
                    object.put("escuela",escuela);
                    object.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            getActivity().getSupportFragmentManager().popBackStack();
                            Log.d("Status modificado",String.valueOf(estado));
                        }
                    });
                }else {
                    Log.d("Error",e.getMessage());

                }
            }
        });
    }


    public void setTypef(){
        Typeface bold=Typeface.createFromAsset(getContext().getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(getContext().getAssets(),avenirMedium);
        Typeface dem=Typeface.createFromAsset(getContext().getAssets(),demibold);

        crearServicioTag.setTypeface(dem);
        nombreServicioTag.setTypeface(dem);
        precioServicioTag.setTypeface(dem);
        descripcionServicio.setTypeface(dem);
        servicioActivoTag.setTypeface(dem);
        descripcionServicioActivo.setTypeface(medium);

        editNombreServicio.setTypeface(medium);
        editPrecioServicio.setTypeface(medium);
        editDescripcionServicio.setTypeface(medium);


    }


}
