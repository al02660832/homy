package nieucrm.nieu.mx.skola.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parse.ParseUser;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import nieucrm.nieu.mx.skola.DataModel.ActivityData;
import nieucrm.nieu.mx.skola.DataModel.EventData;
import nieucrm.nieu.mx.skola.Fragments.EventDetailsFragment;
import nieucrm.nieu.mx.skola.Fragments.NewActivityFragment;
import nieucrm.nieu.mx.skola.R;


/**
 * Created by Carlos Romero on 10/12/2016.
 */

public class ActivityAdapter extends BaseAdapter {

    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf",demibold="font/avenir-next-demi-bold.ttf";
    String day,month,title,number;
    Long dateE;
    String fechaString;

    private Context context;
    LayoutInflater inflater;
    private List<ActivityData> activityData=null;
    private ArrayList<ActivityData>arrayList=null;
    int usertype=ParseUser.getCurrentUser().getNumber("usertype").intValue();


    public ActivityAdapter(Context context, List<ActivityData> activityData) {
        this.context = context;
        this.activityData = activityData;
        try {
            inflater=LayoutInflater.from(context);

        }catch (Exception e){
            Log.d("Error",e.getMessage());
        }
        this.arrayList=new ArrayList<>();
        this.arrayList.addAll(activityData);
    }

    public class ViewHolder{
        TextView txtNumber;
        TextView txtDia;
        TextView txtTema;
        TextView txtDescripcion;
    }


    @Override
    public int getCount() {
        return activityData.size();
    }

    @Override
    public Object getItem(int position) {
        return activityData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Typeface bold=Typeface.createFromAsset(context.getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(context.getAssets(),avenirMedium);
        Typeface dem=Typeface.createFromAsset(context.getAssets(),demibold);

        final ViewHolder holder;

        if(convertView==null){
            holder=new ViewHolder();
            convertView=inflater.inflate(R.layout.planeacion_item_layout,null);
            holder.txtNumber = (TextView) convertView.findViewById(R.id.txtNumber);
            holder.txtDia = (TextView) convertView.findViewById(R.id.txtDia);
            holder.txtDescripcion = (TextView) convertView.findViewById(R.id.txtDescripcion);
            holder.txtTema= (TextView) convertView.findViewById(R.id.txtTema);

            holder.txtNumber.setTypeface(bold);
            holder.txtDescripcion.setTypeface(medium);
            holder.txtTema.setTypeface(dem);
            holder.txtDia.setTypeface(medium);

            convertView.setTag(holder);
        }else {
            holder=(ViewHolder)convertView.getTag();
        }

        holder.txtDia.setText(activityData.get(position).getDia());
        holder.txtNumber.setText(activityData.get(position).getNumero());
        holder.txtDescripcion.setText(activityData.get(position).getDescripcion());
        holder.txtTema.setText(activityData.get(position).getTitulo());


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new NewActivityFragment();
                Bundle bundle=new Bundle();

                if (usertype==0||usertype==1){
                    if (activityData.get(position).isParseO()){
                        bundle.putString("notas",activityData.get(position).getNotas());
                        bundle.putString("tema",activityData.get(position).getTema());
                        bundle.putString("descripcion",activityData.get(position).getDescripcion());
                        bundle.putString("valores",activityData.get(position).getValores());
                        bundle.putString("habitos",activityData.get(position).getHabitos());
                        bundle.putString("titulo",activityData.get(position).getTitulo());


                    }
                    bundle.putSerializable("date",activityData.get(position).getDate());
                    bundle.putString("fecha",activityData.get(position).getFecha());
                    bundle.putString("grupo",activityData.get(position).getGrupo());
                    bundle.putString("numero",activityData.get(position).getNumero());
                    bundle.putString("dia",activityData.get(position).getDia());
                    bundle.putString("mes",activityData.get(position).getMonth());
                    bundle.putString("nombreGrupo",activityData.get(position).getNombreGrupo());
                    fragment.setArguments(bundle);
                    FragmentTransaction transaction= ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
                    transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                    transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                }else {
                    if (activityData.get(position).getVacio()){

                    }else {
                        bundle.putString("notas",activityData.get(position).getNotas());
                        bundle.putString("tema",activityData.get(position).getTema());
                        bundle.putString("descripcion",activityData.get(position).getDescripcion());
                        bundle.putString("valores",activityData.get(position).getValores());
                        bundle.putString("habitos",activityData.get(position).getHabitos());
                        bundle.putString("titulo",activityData.get(position).getTitulo());
                        bundle.putSerializable("date",activityData.get(position).getDate());
                        bundle.putString("fecha",activityData.get(position).getFecha());
                        bundle.putString("grupo",activityData.get(position).getGrupo());
                        bundle.putString("numero",activityData.get(position).getNumero());
                        bundle.putString("dia",activityData.get(position).getDia());
                        bundle.putString("mes",activityData.get(position).getMonth());
                        bundle.putString("nombreGrupo",activityData.get(position).getNombreGrupo());
                        fragment.setArguments(bundle);
                        FragmentTransaction transaction= ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
                        transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                        transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                    }
                }










            }
        });

        return convertView;
    }

}
