package nieucrm.nieu.mx.skola.DataModel;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by mmac1 on 12/02/2018.
 */

public class TallerData {
    private String nombre;
    private ArrayList weekdays;
    private Date horaEntrada,horaSalida;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ArrayList getWeekdays() {
        return weekdays;
    }

    public void setWeekdays(ArrayList weekdays) {
        this.weekdays = weekdays;
    }

    public Date getHoraEntrada() {
        return horaEntrada;
    }

    public void setHoraEntrada(Date horaEntrada) {
        this.horaEntrada = horaEntrada;
    }

    public Date getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(Date horaSalida) {
        this.horaSalida = horaSalida;
    }
}
