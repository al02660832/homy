package nieucrm.nieu.mx.skola.Fragments;


import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.List;

import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class DrapeFragment extends Fragment {

    String escuela, nombre;
    int cuentaPad,cuentaEst;
    TextView txtHola,registrados,probar,periodo,funciones,seleccion;
    ImageButton seleccionPlan;
    Fragment fragment;
    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf",demibold="font/avenir-next-demi-bold.ttf";
    Activity a;

    public DrapeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            a=(Activity) context;
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true);

        return inflater.inflate(R.layout.fragment_drape, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        txtHola=(TextView) view.findViewById(R.id.txtHola);
        registrados=(TextView)view.findViewById(R.id.registrados);
        seleccionPlan=(ImageButton)view.findViewById(R.id.seleccionPlan);
        probar=(TextView)view.findViewById(R.id.probar);
        periodo=(TextView)view.findViewById(R.id.periodo);
        funciones=(TextView)view.findViewById(R.id.funciones);
        seleccion=(TextView)view.findViewById(R.id.seleccion);
        setType();
        getParents();

        seleccionPlan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment=new SubscriptionFragment();
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
            }
        });

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id=item.getItemId();


        return super.onOptionsItemSelected(item);

    }

    public void getParents(){

        escuela=((ParseApplication)a.getApplication()).getEscuelaId();
        nombre=((ParseApplication)a.getApplication()).getEscuelaNombre();
        ParseObject school=ParseObject.createWithoutData("Escuela",escuela);
        txtHola.setText("¡Hola, "+nombre+"!");

        ParseQuery<ParseUser>padres=ParseUser.getQuery();

        padres.whereEqualTo("escuela",school);


        padres.findInBackground(new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> objects, ParseException e) {
                if (e==null){
                    cuentaPad=objects.size();
                    getStudents();
                }else {

                }
            }
        });

    }

    public void getStudents(){
        escuela=((ParseApplication)a.getApplication()).getEscuelaId();
        ParseQuery<ParseObject>estudiantes=ParseQuery.getQuery("Estudiantes");
        ParseObject school=ParseObject.createWithoutData("Escuela",escuela);
        estudiantes.whereEqualTo("escuela",school);
        estudiantes.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e==null){
                    cuentaEst=objects.size();
                    registrados.setText("¡Al día de hoy tienen registrados a "+cuentaPad +" Padres de familia y "+cuentaEst+" estudiantes!");
                }else {

                }
            }
        });
    }

    public void setType(){
        Typeface bold = Typeface.createFromAsset(getContext().getAssets(), avenirBold);
        Typeface medium = Typeface.createFromAsset(getContext().getAssets(), avenirMedium);
        Typeface dem=Typeface.createFromAsset(getActivity().getAssets(),demibold);

        txtHola.setTypeface(dem);
        registrados.setTypeface(dem);
        probar.setTypeface(dem);
        periodo.setTypeface(dem);
        funciones.setTypeface(dem);
        seleccion.setTypeface(dem);

    }

}
