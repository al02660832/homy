package nieucrm.nieu.mx.skola.Fragments;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import nieucrm.nieu.mx.skola.Adapter.RecordAdapter;
import nieucrm.nieu.mx.skola.DataModel.RecordData;
import nieucrm.nieu.mx.skola.Helpers.Constants;
import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.Helpers.Util;
import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 * Created by Carlos Romero Morales
 * 07/08/2018
 */
public class RecordFragment extends Fragment {

    ListView listaAutorizada;
    ImageView recordImage;
    TextView recordName, recordGroup, recordSchedule, recordAge;

    String nombre, grupo, horario, edad, escuelaId, studentId;
    ArrayList<RecordData> listaPersonas;
    ArrayList<ParseObject> listAlumnos;
    ParseObject student;
    int position = 0;
    ParseObject g;

    File outputDir,imageFile;
    TransferUtility utility;
    TransferObserver observer;

    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf",demibold="font/avenir-next-demi-bold.ttf";


    public RecordFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true);

        return inflater.inflate(R.layout.fragment_record, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recordName = (TextView) view.findViewById(R.id.recordName);
        recordGroup = (TextView) view.findViewById(R.id.recordGroup);
        recordSchedule = (TextView) view.findViewById(R.id.recordSchedule);
        recordAge = (TextView) view.findViewById(R.id.recordAge);

        recordImage = (ImageView) view.findViewById(R.id.recordImage);

        listaAutorizada = (ListView) view.findViewById(R.id.listaAutorizada);
        utility= Util.getTransferUtility(getActivity());

        setTypeface();

        getRecordData();

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();

        inflater.inflate(R.menu.cambiar_estudiante,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.changeChild:
                if (listAlumnos.size()>1){
                    changeKid();

                }else {

                }
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);

    }

    private void getRecordData() {
        listAlumnos = new ArrayList<>();
        escuelaId = ((ParseApplication) getActivity().getApplication()).getEscuelaId();
        ParseObject escuela = ParseObject.createWithoutData("Escuela", escuelaId);
        ParseQuery<ParseObject> est = ParseQuery.getQuery("Estudiantes")
                .include("PersonasAutorizadas").include("grupo")
                .whereEqualTo("PersonasAutorizadas", ParseUser.getCurrentUser());
        est.whereEqualTo("escuela", escuela);
        est.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    listAlumnos.addAll(objects);
                    if (listAlumnos.size() != 0) {
                        student = listAlumnos.get(position);
                        nombre = student.getString("NOMBRE") + " " + student.getString("APELLIDO");
                        g = listAlumnos.get(position).getParseObject("grupo");
                        grupo = g.getString("grupoId");
                        horario = student.getString("HORARIO");
                        edad = getAge(student.getDate("fechaNacimiento"));

                        if (grupo.equals("")){
                            recordGroup.setVisibility(View.GONE);
                        }else {
                            recordGroup.setVisibility(View.VISIBLE);
                        }

                        if (horario.equals("")){
                            recordSchedule.setVisibility(View.GONE);
                        }else {
                            recordSchedule.setVisibility(View.VISIBLE);
                        }

                        if (edad.equals("")){
                            recordAge.setVisibility(View.GONE);
                        }else {
                            recordAge.setVisibility(View.VISIBLE);
                        }
                        recordName.setText(nombre);
                        recordGroup.setText(grupo);
                        recordSchedule.setText(horario);
                        recordAge.setText(edad);
                        //downloadPhoto(student.getObjectId(),recordImage);
                        getStudentPhoto(student.getObjectId());

                    }
                    getAuthorizeData(listAlumnos.get(0));
                } else {
                    Log.e("Error",e.getMessage());
                }
            }
        });
    }

    private void getAuthorizeData(ParseObject e) {
        listaPersonas = new ArrayList<>();
        ParseRelation<ParseUser>authorizedPerson=e.getRelation("PersonasAutorizadas");
        authorizedPerson.getQuery().findInBackground(new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> objects, ParseException e) {
                if (e == null) {
                    for (ParseUser o : objects) {
                        RecordData data = new RecordData();
                        data.setObjectId(o.getObjectId());
                        data.setNombre(o.getString("nombre"));
                        data.setApellidos(o.getString("apellidos"));
                        data.setParentesco(o.getString("parentesco"));
                        data.setCorreo(o.getString("email"));
                        data.setTelCasa(o.getString("telefonocasa"));
                        data.setTelCelular(o.getString("telefonocelular"));
                        data.setTelOficina(o.getString("telefonooficina"));
                        data.setDomicilio(o.getString("domicilio"));
                        listaPersonas.add(data);
                    }
                    RecordAdapter a = new RecordAdapter(getActivity(), listaPersonas);
                    listaAutorizada.setAdapter(a);
                } else {

                }
            }
        });


    }

    private String getAge(Date dateBirth){
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.setTime(dateBirth);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)){
            age--;
        }
        String ageS;
        if (age==1){
            ageS = String.valueOf(age)+" año";
        }else {
            ageS = String.valueOf(age)+" años";
        }

        return ageS;
    }

    //Foto de los niños
    private void downloadPhoto(String objectId, final ImageView i){
        outputDir = getActivity().getCacheDir();

        try {
            imageFile= File.createTempFile(objectId, ".jpeg", outputDir);
        } catch (IOException e) {
            e.printStackTrace();
        }
        observer=utility.download(Constants.BUCKET_NAME,objectId,imageFile);
        observer.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                Log.d("Estatus:",state.toString());

                if (TransferState.COMPLETED.equals(observer.getState())) {

                    Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
                    //fotoPadre.setImageBitmap(bitmap);
                    //progress.setVisibility(View.GONE);
                    i.setImageBitmap(bitmap);
                    try {
                        Toast.makeText(getActivity(), "Foto lista", Toast.LENGTH_SHORT).show();
                    }catch (Exception e1){
                        Log.e("Exception",e1.getMessage());
                    }
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                long _bytesCurrent = bytesCurrent;
                long _bytesTotal = bytesTotal;

                float percentage =  ((float)_bytesCurrent /(float)_bytesTotal * 100);
                Log.d("percentage","" +percentage);

            }

            @Override
            public void onError(int id, Exception ex) {

                Log.d("Error al descargar", ex.getMessage());
            }
        });
    }

    public void changeKid(){
        if (position==listAlumnos.size()-1){
            position=0;
            student = listAlumnos.get(position);
            nombre = student.getString("NOMBRE") + " " + student.getString("APELLIDO");
            ParseObject g = listAlumnos.get(position).getParseObject("grupo");
            grupo = g.getString("grupoId");
            horario = student.getString("HORARIO");
            edad = getAge(student.getDate("fechaNacimiento"));
            if (grupo.equals("")){
                recordGroup.setVisibility(View.GONE);
            }else {
                recordGroup.setVisibility(View.VISIBLE);
            }

            if (horario.equals("")){
                recordSchedule.setVisibility(View.GONE);
            }else {
                recordSchedule.setVisibility(View.VISIBLE);
            }

            if (edad.equals("")){
                recordAge.setVisibility(View.GONE);
            }else {
                recordAge.setVisibility(View.VISIBLE);
            }
            recordName.setText(nombre);
            recordGroup.setText(grupo);
            recordSchedule.setText(horario);
            recordAge.setText(edad);
            getStudentPhoto(student.getObjectId());

            //downloadPhoto(student.getObjectId(),recordImage);
        }else {
            position++;
            student = listAlumnos.get(position);
            nombre = student.getString("NOMBRE") + " " + student.getString("APELLIDO");
            ParseObject g = listAlumnos.get(position).getParseObject("grupo");
            grupo = g.getString("grupoId");
            horario = student.getString("HORARIO");
            edad = getAge(student.getDate("fechaNacimiento"));
            if (grupo.equals("")){
                recordGroup.setVisibility(View.GONE);
            }else {
                recordGroup.setVisibility(View.VISIBLE);
            }

            if (horario.equals("")){
                recordSchedule.setVisibility(View.GONE);
            }else {
                recordSchedule.setVisibility(View.VISIBLE);
            }

            if (edad.equals("")){
                recordAge.setVisibility(View.GONE);
            }else {
                recordAge.setVisibility(View.VISIBLE);
            }
            recordName.setText(nombre);
            recordGroup.setText(grupo);
            recordSchedule.setText(horario);
            recordAge.setText(edad);
            getStudentPhoto(student.getObjectId());

            //downloadPhoto(student.getObjectId(),recordImage);

        }
    }

    private void getStudentPhoto(String studentId){
        ParseObject estudiante=ParseObject.createWithoutData("Estudiantes",studentId);
        ParseQuery<ParseObject>studentPhoto=ParseQuery.getQuery("studentPhoto");
        studentPhoto.whereEqualTo("estudiante",estudiante);
        studentPhoto.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (e==null){
                    downloadPhoto(object.getObjectId(),recordImage);
                }else {
                    Log.e("Error",e.getMessage());
                }
            }
        });

    }

    private void setTypeface(){

        Typeface bold=Typeface.createFromAsset(getActivity().getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(getActivity().getAssets(),avenirMedium);
        Typeface demi=Typeface.createFromAsset(getActivity().getAssets(),demibold);


        recordName.setTypeface(demi);
        recordGroup.setTypeface(medium);
        recordSchedule.setTypeface(medium);
        recordAge.setTypeface(medium);


    }

}
