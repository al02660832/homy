package nieucrm.nieu.mx.skola.Fragments;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;

import java.util.HashMap;

import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFragment extends Fragment {

    TextView regFisc,txtNom,txtDireccion,txtRfc,txtRazon,txtEmail;
    EditText editNombreFiscal,editDirecccionFiscal,editRfc,editRazonSocial,editEmail;
    Button btnNext;
    String avenirBold = "font/avenir-next-bold.ttf", avenirMedium = "font/avenir-next-medium.ttf",avenirDemi = "font/avenir-next-demi-bold.ttf";
    String nombre,direccion,rfc,razon,email,escuelaId;
    HashMap<String, Object>params,newCustomer;
    Fragment fragment;
    String openpayId,amount;
    boolean iva;
    /*
    El usuario llena los campos.
    Nombre, email y RFC son obligatorios
    Al presionar el botón de siguiente, se manda llamar un cloudCode llamado "openPayCreateCustomer"
    con los parámetros "escuelaObjId" que es el objectId de current Escuela
    y parámetro "newCustomer" que es un diccionario con la información
    que ingresó en los campos de texto.
    Sólo ir a la siguiente pantalla hasta que el cloudCode te regrese SUCCESS.
    Pasar el objeto del plan de la pantalla anterior a la siguiente pantalla
     */

    /*
    Dictionary *newCustomer =
    {
    @"name" : self.nombreTextField.text,
    @"email" : self.emailTextField.text,
    @"domicilio" : self.domicilioTextField.text,
    @"razonSocial" : self.razonSocialTextField.text,
    @"rfc" : self.rfcTextField.text
    };
     */

    public RegisterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        Bundle b=this.getArguments();
        if (b!=null){
            openpayId=b.getString("openpayId");
            iva=b.getBoolean("iva");
            amount=b.getString("amount");
        }
        return inflater.inflate(R.layout.fragment_register, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        regFisc=(TextView)view.findViewById(R.id.regFisc);
        txtNom=(TextView)view.findViewById(R.id.txtNom);
        txtEmail=(TextView)view.findViewById(R.id.txtEmail);
        txtDireccion=(TextView)view.findViewById(R.id.txtDireccion);
        txtRfc=(TextView)view.findViewById(R.id.txtRfc);
        txtRazon=(TextView)view.findViewById(R.id.txtRazon);
        editNombreFiscal=(EditText) view.findViewById(R.id.editNombreFiscal);
        editDirecccionFiscal=(EditText) view.findViewById(R.id.editDireccionFiscal);
        editRfc=(EditText) view.findViewById(R.id.editRfc);
        editEmail=(EditText) view.findViewById(R.id.editEmail);
        editRazonSocial=(EditText) view.findViewById(R.id.editRazonSocial);
        btnNext=(Button) view.findViewById(R.id.btnNext);

        SetTypeface();

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fillFields();
            }
        });

    }

    private void SetTypeface() {
        Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), avenirBold);
        Typeface medium = Typeface.createFromAsset(getActivity().getAssets(), avenirMedium);
        Typeface demi=Typeface.createFromAsset(getActivity().getAssets(), avenirDemi);

        regFisc.setTypeface(demi);
        txtNom.setTypeface(demi);
        txtDireccion.setTypeface(demi);
        txtRfc.setTypeface(demi);
        txtRazon.setTypeface(demi);
        txtEmail.setTypeface(demi);
        editNombreFiscal.setTypeface(demi);
        editEmail.setTypeface(demi);
        editDirecccionFiscal.setTypeface(demi);
        editRfc.setTypeface(demi);
        editRazonSocial.setTypeface(demi);
        btnNext.setTypeface(demi);



    }

    private void getOpenpayCode(){
        /*
        se manda llamar un cloudCode llamado "openPayCreateCustomer"
        con los parámetros "escuelaObjId" que es el objectId de current Escuela
        y parámetro "newCustomer" que es un diccionario con la información
        que ingresó en los campos de texto.
         */
        escuelaId=((ParseApplication)getActivity().getApplication()).getEscuelaId();
        params=new HashMap<>();

        params.put("escuelaObjId",escuelaId);
        params.put("newCustomer",newCustomer);

        ParseCloud.callFunctionInBackground("openPayCreateCustomer", params, new FunctionCallback<Object>() {
            @Override
            public void done(Object object, ParseException e) {
                if (e == null){
                    Log.d("openPayCreateCustomer","Success");

                    fragment=new CreditCardFragment();
                    Bundle b=new Bundle();
                    b.putString("openPay",openpayId);
                    b.putBoolean("iva",iva);
                    b.putString("amount",amount);
                    fragment.setArguments(b);
                    FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                    transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                    transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();

                }else {
                    Log.d("openPayCreate-Error",e.getMessage());
                }
            }
        });
    }

    private void fillFields(){
        newCustomer=new HashMap<>();
        nombre=editNombreFiscal.getText().toString().trim();
        direccion=editDirecccionFiscal.getText().toString().trim();
        rfc=editRfc.getText().toString().trim();
        razon=editRazonSocial.getText().toString().trim();
        email=editEmail.getText().toString().trim();

        if (TextUtils.isEmpty(nombre)||TextUtils.isEmpty(email)||TextUtils.isEmpty(rfc)){
            Toast.makeText(getActivity(),"Rellene todos los campos",Toast.LENGTH_SHORT).show();
        }else {
            /*
            @"name" : self.nombreTextField.text,
            @"email" : self.emailTextField.text,
            @"domicilio" : self.domicilioTextField.text,
            @"razonSocial" : self.razonSocialTextField.text,
            @"rfc" : self.rfcTextField.text
             */
            newCustomer.put("name",nombre);
            newCustomer.put("email",email);
            newCustomer.put("domicilio",direccion);
            newCustomer.put("razonSocial",razon);
            newCustomer.put("rfc",rfc);
            getOpenpayCode();

        }

    }

}
