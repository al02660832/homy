package nieucrm.nieu.mx.skola.Fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.HashMap;

import mx.openpay.android.Openpay;
import mx.openpay.android.OperationCallBack;
import mx.openpay.android.OperationResult;
import mx.openpay.android.exceptions.OpenpayServiceException;
import mx.openpay.android.exceptions.ServiceUnavailableException;
import mx.openpay.android.model.Card;
import mx.openpay.android.model.Token;
import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class CreditCardFragment extends Fragment {

    TextView txtRegistro, txtSeleccionado, txtAmount;
    EditText nombreTarjeta,numeroTarjeta,cvvTarjeta;
    Spinner mesTarjeta,añoTarjeta;
    ImageButton registrarTarjeta;
    Button limpiarForm;
    String avenirBold = "font/avenir-next-bold.ttf", avenirMedium = "font/avenir-next-medium.ttf",avenirDemi = "font/avenir-next-demi-bold.ttf";
    String nombre,numero,cvv,apodo;
    Integer mes,año;
    HashMap<String,Object>params;
    Fragment fragment;
    ProgressBar progressCard;
    LinearLayout layoutCard;

    ProgressDialog dialog;

    String amount,openpayplanId;
    ParseObject subscripcion;
    Token token;
    boolean iva;
    String escuelaId;
    ParseObject escuela;
    String openPayCustomerId;
    Activity a;
    /*
    Con el objeto que se pasó de la pantalla anterior,
    mostrar la columna "amount" que es lo que se va a pagar,
    y si la columna IVA es true, concatenar string " + I.V.A."
    Hacer query a tabla "Subscripcion".
    El objeto lo guardo como variable global de ese controlador
     */

    /*
    PFQuery *query = [PFQuery queryWithClassName:@"Subscripcion"];
    [query whereKey:@"objectId" equalTo:self.subscripcionObjId];
    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error){
    if (!error) {
    subscripcion = object;
    NSLog(@"getSubscripcionObject: %@", subscripcion);
    }
    }];
     */

    /*
    Todos los campos son obligatorios
    Cuando el usuario presiona el botón de hacer el pago,
    lo primero que se hace es tokenizar la tarjeta.
    Esta es una función del SDK de OpenPay
    (void)openPayTokenizeCard {
    Openpay *openpay = [[Openpay alloc] initWithMerchantId:@"me4y705wfz71dnlwtwvk"
    apyKey:@"pk_4499bfe9ab4449309baf905694623d75"
    isProductionMode:YES];

    [openpay createTokenWithCard:card
    success:^(OPToken *token) {
    NSLog(@"OpenPay Token: %@", token.id);
    [self cloudCodeCall:token.id];
    } failure:^(NSError *error) {
    [self presentFeedbackForUser:@"Algo falló al
    registrar tarjeta" :[error localizedDescription]];
    }];
    }

     */

    public CreditCardFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            a=(Activity) context;
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Bundle b=this.getArguments();
        if (b!=null){
            openpayplanId=b.getString("openPay");
            iva=b.getBoolean("iva");
            amount=b.getString("amount");
        }

        return inflater.inflate(R.layout.fragment_credit_card, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        txtRegistro = (TextView) view.findViewById(R.id.txtRegistro);
        nombreTarjeta = (EditText) view.findViewById(R.id.nombreTarjeta);
        numeroTarjeta = (EditText) view.findViewById(R.id.numeroTarjeta);
        cvvTarjeta = (EditText) view.findViewById(R.id.cvvTarjeta);
        mesTarjeta = (Spinner) view.findViewById(R.id.mesTarjeta);
        añoTarjeta = (Spinner) view.findViewById(R.id.añoTarjeta);
        registrarTarjeta = (ImageButton) view.findViewById(R.id.registrarTarjeta);
        limpiarForm = (Button) view.findViewById(R.id.limpiarForm);
        txtAmount = (TextView) view.findViewById(R.id.txtAmount);
        txtSeleccionado = (TextView) view.findViewById(R.id.txtSeleccionado);
        progressCard = (ProgressBar) view.findViewById(R.id.progressCard);
        layoutCard = (LinearLayout) view.findViewById(R.id.layoutCard);


        setTypeface();


        getCurrentPlan();

        if (iva){
            String cantidad="$"+amount+" + IVA";
            txtAmount.setText(cantidad);
        }else {
            txtAmount.setText(amount);
        }

        registrarTarjeta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registrarTarjeta.setClickable(false);
                dialog=new ProgressDialog(a);
                dialog.setTitle("Cargando");
                dialog.setMessage("Por favor espere...");
                dialog.setCancelable(false);
                dialog.show();
                fillBlanks();
            }
        });
        limpiarForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearForm();
            }
        });

    }

    private void setTypeface(){
        Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), avenirBold);
        Typeface medium = Typeface.createFromAsset(getActivity().getAssets(), avenirMedium);
        Typeface demi=Typeface.createFromAsset(getActivity().getAssets(), avenirDemi);

        txtRegistro.setTypeface(demi);
        nombreTarjeta.setTypeface(medium);
        numeroTarjeta.setTypeface(medium);
        cvvTarjeta.setTypeface(medium);
        limpiarForm.setTypeface(demi);
        txtAmount.setTypeface(medium);
        txtSeleccionado.setTypeface(medium);

    }

    private void clearForm(){
        nombreTarjeta.setText("");
        numeroTarjeta.setText("");
        cvvTarjeta.setText("");
        mesTarjeta.setSelection(0);
        añoTarjeta.setSelection(0);
    }

    private void fillBlanks(){
        nombre=nombreTarjeta.getText().toString().trim();
        numero=numeroTarjeta.getText().toString().trim();
        cvv=cvvTarjeta.getText().toString().trim();
        //Numeros!!!!
        try {
            mes=Integer.valueOf(mesTarjeta.getSelectedItem().toString());
            año=Integer.valueOf(añoTarjeta.getSelectedItem().toString());
        }catch (Exception e){
            Log.e("Error","No es numero");
        }

        if (TextUtils.isEmpty(nombre)||
                TextUtils.isEmpty(numero)||
                TextUtils.isEmpty(cvv)||
                mesTarjeta.getSelectedItem().toString().equals("Mes de expiración")||
                añoTarjeta.getSelectedItem().toString().equals("Año de expiración")){
            Toast.makeText(a,"Rellene todos los campos",Toast.LENGTH_SHORT).show();
        }else {
            createToken();
        }

    }

    public void createToken(){
        //Pruebas
        //Openpay openpay=new Openpay("mruzbn5vzcao7hu3rznn","sk_394a1262cc1f4d589a03e00cff2f59d0",false);

        Openpay openpay=new Openpay("me4y705wfz71dnlwtwvk","pk_4499bfe9ab4449309baf905694623d75",true);
        //Create Token
        Card card=new Card();
        card.holderName(nombre);
        card.cardNumber(numero);
        card.expirationMonth(mes);
        card.expirationYear(año);
        card.cvv2(cvv);

        openpay.createToken(card, new OperationCallBack<Token>() {
            @Override
            public void onError(OpenpayServiceException error) {
                Toast.makeText(a,"Algo fallo al registrar su tarjeta, intente de nuevo",Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                dialog.hide();
                registrarTarjeta.setClickable(true);

                Log.e("#",error.getDescription());
            }

            @Override
            public void onCommunicationError(ServiceUnavailableException e) {

            }

            @Override
            public void onSuccess(OperationResult<Token> operationResult) {
                operationResult.getResult().getId();
                token=operationResult.getResult();
                getCloudCode();

                /*
                Después de tokenizar la tarjeta, se llama a un cloudCode
                llamado "openPaySaveCard" con parámetros
                "openPayCustomerId", "subscriptionPlan", "cardToken"
                 */



            }
        });



    }

    private void getCloudCode(){
        params=new HashMap<>();
        //Params
        //openPayCustomerId
        params.put("openPayCustomerId",openPayCustomerId);
        //subscriptionPlan
        params.put("subscriptionPlan",openpayplanId);
        //cardToken
        //No acepta token
        params.put("cardToken",token.getId());
        ParseCloud.callFunctionInBackground("openPaySaveCard", params, new FunctionCallback<Object>() {
            @Override
            public void done(Object object, ParseException e) {
                dialog.hide();
                if (e == null){
                    Log.d("openPaySaveCard","Success");

                    fragment=new SuccessfulRegistrationFragment();
                    FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                    transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                    transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                }else {
                    Log.d("openPaySaveCard",e.getMessage());
                    Toast.makeText(a,"No se pudo completar la subscripción a Skola." +
                            " Por favor contacta al equipo de soporte técnico para resolver este tema.",Toast.LENGTH_SHORT).show();

                }
            }
        });
    }

    public void getCurrentPlan(){
        escuelaId=((ParseApplication)a.getApplication()).getEscuelaId();
        escuela=ParseObject.createWithoutData("Escuela",escuelaId);
        ParseQuery<ParseObject> subscripcion=ParseQuery.getQuery("Subscripcion");
        subscripcion.whereEqualTo("escuela",escuela);
        subscripcion.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                progressCard.setVisibility(View.GONE);
                if (e==null){
                    Log.d("Error","Error current plan");
                    layoutCard.setVisibility(View.VISIBLE);
                    openPayCustomerId=object.getString("openPayCustomerId");
                }else {
                    layoutCard.setVisibility(View.VISIBLE);
                    openPayCustomerId=object.getString("openPayCustomerId");

                }
            }
        });

    }

    /*

    public void getCurrentPlan(){
        escuelaId=((ParseApplication)getActivity().getApplication()).getEscuelaId();
        escuela=ParseObject.createWithoutData("Escuela",escuelaId);
        ParseQuery<ParseObject> subscripcion=ParseQuery.getQuery("Subscripcion");
        subscripcion.whereEqualTo("escuela",escuela);
        subscripcion.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (e==null){
                    if (object==null){
                        Toast.makeText(context,"Error, intente de nuevo",Toast.LENGTH_SHORT).show();

                    }else {
                        selectedPlanId=object.getString("selectedPlanId");
                        getSubscripcionPlan(selectedPlanId);
                    }
                }else {
                    progressSub.setVisibility(View.GONE);
                    Log.i("Subscripcion","No tiene");
                    Log.e("Error1",e.getMessage());

                    fragment=new PlanFragment();
                    FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                    transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                    transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();

                }
            }
        });

    }

    public void getSubscripcionPlan(String planId){
        ParseQuery<ParseObject>op=ParseQuery.getQuery("OPSubscripcionPlan");
        op.whereEqualTo("openPaySubscripcionId",planId);
        op.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                progressSub.setVisibility(View.GONE);
                if (e==null){
                    layoutPlan.setVisibility(View.VISIBLE);
                    openPaySubscripcionPlan=object.getString("openPaySubscripcionPlan");
                    amount="$"+object.getString("amount");
                    tuSubs.setText(openPaySubscripcionPlan);
                    txtCantidad.setText(amount);
                }else {
                    Log.e("Error",e.getMessage());
                    fragment=new PlanFragment();
                    FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                    transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                    transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                }
            }
        });
    }
     */

}
