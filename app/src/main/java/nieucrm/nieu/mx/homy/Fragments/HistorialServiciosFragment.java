package nieucrm.nieu.mx.skola.Fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import nieucrm.nieu.mx.skola.Adapter.HistorialServicioAdapter;
import nieucrm.nieu.mx.skola.DataModel.ServiciosData;
import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class HistorialServiciosFragment extends Fragment {

    ListView historialServicios;
    ParseQuery<ParseObject> servicio;
    HistorialServicioAdapter adapter;
    ArrayList<ServiciosData>array=new ArrayList<>();
    ProgressDialog dialog;

    String estado;
    Activity a;

    public HistorialServiciosFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            a=(Activity) context;
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_historial_servicios, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);

        historialServicios=(ListView)v.findViewById(R.id.listaHistorialServicios);

        dialog=new ProgressDialog(a);
        dialog.setTitle("Cargando");
        dialog.setMessage("Por favor espere...");
        dialog.setCancelable(false);
        dialog.show();
        getServicios();
    }

    public void getServicios(){
        ParseQuery<ParseObject>query1 = new ParseQuery<>("Estudiantes").include("PersonasAutorizadas").whereEqualTo("PersonasAutorizadas", ParseUser.getCurrentUser());

        servicio=ParseQuery.getQuery("ServicioSolicitado");
        servicio.include("estudiante").include("servicio").include("personaAutorizada");
        servicio.whereEqualTo("personaAutorizada",ParseUser.getCurrentUser());
        servicio.whereMatchesQuery("estudiante",query1);
        servicio.orderByDescending("fechaSolicitud");
        servicio.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                dialog.hide();
                if (e==null){
                    for (ParseObject o:objects){
                        ParseObject estObjects=o.getParseObject("estudiante");
                        ParseObject service=o.getParseObject("servicio");

                        ServiciosData data=new ServiciosData();

                        if(estObjects==null){
                            data.setAlumno("Alumno");
                        }else{
                            data.setAlumno(estObjects.getString("NOMBRE")+" "+estObjects.getString("APELLIDO"));
                        }

                        if (service==null){
                            data.setNombre("Servicio");
                        }else {
                            data.setNombre(service.getString("nombre"));
                            data.setDescripcion(service.getString("descripcion"));
                            data.setPrecio("$"+String.valueOf(service.getNumber("precio").intValue()));

                        }

                        data.setStatus(o.getNumber("status").intValue());

                        data.setObjectId(o.getObjectId());
                        data.setComentarios(o.getString("comentarios"));

                        data.setDate(o.getDate("fechaSolicitud"));

                        array.add(data);

                    }
                    adapter=new HistorialServicioAdapter(a,array);
                    historialServicios.setAdapter(adapter);
                }else {
                    Log.d("Error",e.getMessage());
                }
            }
        });
    }

    public void defineStatus(int status){

    }

}
