package nieucrm.nieu.mx.skola.Fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickResult;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import nieucrm.nieu.mx.skola.Adapter.ImageAdapter;
import nieucrm.nieu.mx.skola.DataModel.ImageData;
import nieucrm.nieu.mx.skola.DataModel.ImageItem;
import nieucrm.nieu.mx.skola.DataModel.ImageList;
import nieucrm.nieu.mx.skola.Helpers.Constants;
import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.Helpers.Util;
import nieucrm.nieu.mx.skola.R;


/**
 * Created by Carlos Romero Morales
 * A simple {@link Fragment} subclass.
 */
public class EventsGalleryFragment extends Fragment {

    GridView gridView;
    int i;

    String eventId;

    TextView nombreEs;

    String camino;



    //GridViewAdapter adapter;

    String nombreEvento;
    ArrayList<Bitmap>bitList;
    TransferObserver observe;
    ArrayList<ImageItem> imageItems;



    private final int PHOTO_CODE = 200;
    private final int SELECT_PICTURE = 300;
    private static String APP_DIRECTORY = "MyPictureApp/";
    private static String MEDIA_DIRECTORY = APP_DIRECTORY + "PictureApp";
    private String mPath;
    String escuelaId;

    HashMap<String, Object> params;

    ImageAdapter a;
    ImageData data;
    ArrayList<ImageData>array;
    String aut;

    ProgressDialog dialog,d;

    ParseFile photoFile;

    ParseObject eventoGaleria;
    String oId;
 //   http(s)://s3.amazonaws.com/<bucket>/<object>
    //https://AMAZON_S3_BUCKET.YOUR_BUCKET_REGION.amazonaws.com/myfile


    String region="https://"+Constants.BUCKET_NAME+"."+Constants.BUCKET_REGION+".amazonaws.com/";

    int usertype;

    TransferUtility utility;
    TransferObserver observer;
    File imageFile;
    File file;
    File outputDir;
    boolean aws;
    String parseUri;
    File myFile;
    Activity ac;

    ArrayList<ImageList>arrayList=new ArrayList<>();

    public EventsGalleryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            ac=(Activity) context;
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Bundle b=this.getArguments();
        eventId=b.getString("eventId");
        nombreEvento=b.getString("nombreEvento");
        setHasOptionsMenu(true);

        ParseUser user=ParseUser.getCurrentUser();
        usertype=user.getNumber("usertype").intValue();

        utility= Util.getTransferUtility(ac);

        return inflater.inflate(R.layout.fragment_events_gallery, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        gridView=(GridView)view.findViewById(R.id.gridview);
        nombreEs = (TextView) ac.findViewById(R.id.textAction);

        myFile = new File(ac.getFilesDir(), "/Images/"+nombreEvento+"/");
        myFile.mkdirs();

        nombreEs.setText(nombreEvento);
        dialog=new ProgressDialog(ac);
        dialog.setTitle("Cargando");
        dialog.setMessage("Por favor espere...");
        dialog.setCancelable(false);
        dialog.show();
        getImage();


    }

    public void getImage(){

        array=new ArrayList<>();
        i=0;
        outputDir = ac.getCacheDir();

        ParseObject evento=ParseObject.createWithoutData("evento",eventId);
        ParseQuery<ParseObject>imageParse=new ParseQuery<ParseObject>("EventoGaleria");
        imageParse.include("evento");
        imageParse.include("autor");
        imageParse.include("estudianteTag");
        imageParse.whereEqualTo("evento",evento);
        imageParse.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                dialog.hide();
                if (e==null){

                    for (ParseObject o:objects){


                        data=new ImageData();
                        ImageList map=new ImageList();
                        if (o.getParseFile("foto")!=null){
                            ParseFile ima=o.getParseFile("foto");

                            map.setImagen(ima.getUrl());
                        }else {
                            if (o.getParseUser("autor")==null){
                                data.setAutor("");
                            }else {
                                ParseUser user=o.getParseUser("autor");
                                data.setAutor(user.getString("nombre")+" "+user.getString("apellidos"));
                            }

                            data.setImagen(o.getObjectId());
                        }
                        array.add(data);

                        //arrayList.add(map);
                    }

                    a=new ImageAdapter(ac,array);
                    gridView.setAdapter(a);




                }else {
                    Log.d("Error",e.getMessage());
                }


                /*
                adapter=new GridViewAdapter(getContext(),arrayList);
                gridView.setAdapter(adapter);
                */

            }
        });
    }


    public void downloadFromS3(String objectId){

        outputDir =  getContext().getCacheDir();

        try {
            utility= Util.getTransferUtility(getContext());
        }catch (Exception e1){
            Log.e("Error credencial",e1.getMessage());
        }

        try {
            imageFile= File.createTempFile(objectId, null, outputDir);
        } catch (IOException e) {
            e.printStackTrace();
        }




        try {
            observe= utility.download(Constants.BUCKET_NAME,objectId,imageFile);
            observe.setTransferListener(new TransferListener() {
                @Override
                public void onStateChanged(int id, TransferState state) {
                    if (TransferState.COMPLETED.equals(observe.getState())){
                        Bitmap bitmap=BitmapFactory.decodeFile(imageFile.getAbsolutePath());
                        bitList.add(bitmap);
                        //photo.setImageBitmap(bitmap);

                        Log.e("Completed",imageFile.toString());





                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                    long _bytesCurrent = bytesCurrent;
                    long _bytesTotal = bytesTotal;

                    float percentage =  ((float)_bytesCurrent /(float)_bytesTotal * 100);
                }

                @Override
                public void onError(int id, Exception ex) {
                    Log.e("Error de Amazon",ex.getMessage());
                }
            });

        }catch (Exception e){
            Log.e("Error",e.getMessage());
        }
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();

            if (usertype==2){

            }else {
                inflater.inflate(R.menu.gallery_photo,menu);
            }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id=item.getItemId();
        if(id==R.id.gallery){
            showPicker();
        }

        return super.onOptionsItemSelected(item);

    }

    public static byte[] bitmapToByteArray(Bitmap bmp) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    public String getPath(Uri uri, Activity activity) {
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = activity
                .managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private Bitmap addWaterMark(Bitmap src) {
        int w = src.getWidth();
        int h = src.getHeight();
        Bitmap result = Bitmap.createBitmap(w, h, src.getConfig());
        Canvas canvas = new Canvas(result);
        canvas.drawBitmap(src, 0, 0, null);

        Bitmap waterMark = BitmapFactory.decodeResource(getContext().getResources(), R.mipmap.ic_launcher);
        canvas.drawBitmap(waterMark, 0, 0, null);

        return result;
    }

    public void savePhoto(){
        params=new HashMap<>();
        escuelaId=((ParseApplication)ac.getApplication()).getEscuelaId();

        ParseObject event=ParseObject.createWithoutData("evento",eventId);
        eventoGaleria=new ParseObject("EventoGaleria");
        eventoGaleria.put("evento",event);
        eventoGaleria.put("foto", photoFile);
        eventoGaleria.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e==null){
                    d.hide();
                    params.put("eventoNombre",nombreEvento);
                    params.put("escuelaObjId",escuelaId);

                    ParseCloud.callFunctionInBackground("eventoFotoNueva", params, new FunctionCallback<Object>() {

                        @Override
                        public void done(Object object, ParseException e) {
                            if (e == null){
                                Log.d("Cloudcode","Success");
                            }else {
                                Log.d("Error",e.getMessage());
                            }
                        }
                    });

                        TransferObserver observe= utility.upload(Constants.BUCKET_NAME,eventoGaleria.getObjectId(),new java.io.File(camino ));
                        observe.setTransferListener(new TransferListener() {
                            @Override
                            public void onStateChanged(int id, TransferState state) {
                                Toast.makeText(getContext(), "Foto guardada", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                                long _bytesCurrent = bytesCurrent;
                                long _bytesTotal = bytesTotal;

                                float percentage =  ((float)_bytesCurrent /(float)_bytesTotal * 100);
                                Log.d("percentage","" +percentage);
                            }

                            @Override
                            public void onError(int id, Exception ex) {
                                Log.d("Error de Amazon",ex.getMessage());
                            }
                        });

                    Toast.makeText(ac,"Foto agregada correctamente",Toast.LENGTH_SHORT).show();
                }else {
                    Log.d("Error",e.getMessage());
                }
            }
        });
    }



    /*
    public void imagen(String id){

        outputDir = getContext().getCacheDir();
        try {
            imageFile= File.createTempFile("prefix", "extension", outputDir);
        } catch (IOException e) {
            e.printStackTrace();
        }

        android.app.AlertDialog.Builder ImageDialog = new android.app.AlertDialog.Builder(getContext());
        ImageDialog.setTitle("Imagen adjunta");
        final ImageView showImage = new ImageView(getContext());
        if (parseUri!=null){
            Picasso.with(getContext()).load(parseUri).resize(500,800).into(showImage);
        }else {
            if (aws){

                observer=utility.download(Constants.BUCKET_NAME,id,imageFile);

                Toast.makeText(getContext(), "Cargando...", Toast.LENGTH_SHORT).show();

                observer.setTransferListener(new TransferListener() {

                    @Override
                    public void onStateChanged(int id, TransferState state) {
                        Log.d("Estatus:",state.toString());

                        if (TransferState.COMPLETED.equals(observer.getState())) {

                            Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
                            showImage.setImageBitmap(bitmap);
                            Toast.makeText(getContext(), "Foto lista", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                        long _bytesCurrent = bytesCurrent;
                        long _bytesTotal = bytesTotal;

                        float percentage =  ((float)_bytesCurrent /(float)_bytesTotal * 100);
                        Log.d("percentage","" +percentage);


                    }

                    @Override
                    public void onError(int id, Exception ex) {
                        Log.d("Error al descargar", ex.getMessage());
                    }
                });
            }
        }

        ImageDialog.setView(showImage);

        ImageDialog.setNegativeButton("ok", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface arg0, int arg1)
            {
            }
        });
        ImageDialog.show();


    }
    */
    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {}
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if(dir!= null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    public void showPicker(){
        PickImageDialog.build(new PickSetup().setCameraButtonText("Cámara").setGalleryButtonText("Galería").setTitle("Agregar foto").setCancelText("Cancelar"))
                .setOnPickResult(new IPickResult() {
                    @Override
                    public void onPickResult(PickResult r) {
                        //TODO: do what you have to...
                        if (r.getError() == null) {
                            //If you want the Uri.
                            //Mandatory to refresh image from Uri.
                            //getImageView().setImageURI(null);

                            //Setting the real returned image.
                            //getImageView().setImageURI(r.getUri());

                            //If you want the Bitmap.

                            Bitmap bitmap = r.getBitmap();
                            // TODO use bitmap

                            camino=r.getPath();

                            savePhoto();

                            //Image path
                            //r.getPath();
                        } else {
                            //Handle possible errors
                            //TODO: do what you have to do with r.getError();
                            Toast.makeText(ac, r.getError().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                }).show(getActivity().getSupportFragmentManager());
    }




}
