package nieucrm.nieu.mx.skola.Fragments;


import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.CalendarContract;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import android.graphics.Matrix;

import android.graphics.RectF;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;


import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickResult;
import com.vinaygaba.rubberstamp.RubberStamp;
import com.vinaygaba.rubberstamp.RubberStampConfig;
import com.vinaygaba.rubberstamp.RubberStampPosition;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import nieucrm.nieu.mx.skola.Helpers.Constants;
import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.Helpers.Util;
import nieucrm.nieu.mx.skola.R;


/**
 * Created by Carlos Romero
 */

public class EventDetailsFragment extends Fragment {

    String avenirBold = "font/avenir-next-bold.ttf", avenirMedium = "font/avenir-next-medium.ttf";
    TextView tituloEvento, lugarEvento, horaEvento, descripcionEvento, fechaEvento, nombreEs;
    String titulo, lugar, descripcion, fecha, hora, objectId;
    Long date;
    Date f;
    Button eliminarEvent;
    long fec;
    ImageButton btnAgregar;
    Button btnGaleria, btnFotoEvento;
    LinearLayout fotosEvento, agregarFoto;
    ProgressDialog dialog, d;
    int usertype;
    String escuelaId;

    int REQUEST_CODE_PICKER = 3, RESULT_OK = 0;

    HashMap<String, Object> params;

    ParseFile photoFile;

    ParseObject eventoGaleria;
    TransferUtility utility;
    TransferObserver observer;

    private final int PHOTO_CODE = 200;
    private final int SELECT_PICTURE = 300;
    private String mPath, camino;
    private static String APP_DIRECTORY = "Skola/";
    private static String MEDIA_DIRECTORY = APP_DIRECTORY + "PictureApp";


    //New code
    private static final String TAG = "EventDetailsFragment";
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static String filePath = null;
    public static final String IMAGE_NAME = "FinalImageWithWaterMark";
    private Button captureImageBtn;
    // Camera activity request codes
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private Uri fileUri; // file url to store image/video
    ImageView parentImageIv;

    String nombre;
    Activity a;

    public EventDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            a=(Activity) context;
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        utility = Util.getTransferUtility(a);
        return inflater.inflate(R.layout.fragment_event_details, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);

        tituloEvento = (TextView) v.findViewById(R.id.tituloEvento);
        lugarEvento = (TextView) v.findViewById(R.id.lugarEvento);
        horaEvento = (TextView) v.findViewById(R.id.horaEvento);
        descripcionEvento = (TextView) v.findViewById(R.id.descripcionEvento);
        fechaEvento = (TextView) v.findViewById(R.id.fechaEvento);
        btnAgregar = (ImageButton) v.findViewById(R.id.btnAgregar);
        btnGaleria = (Button) v.findViewById(R.id.btnGaleria);
        btnFotoEvento = (Button) v.findViewById(R.id.btnFotoEvento);
        eliminarEvent = (Button) v.findViewById(R.id.eliminarEvent);

        fotosEvento = (LinearLayout) v.findViewById(R.id.fotosEvento);
        agregarFoto = (LinearLayout) v.findViewById(R.id.agregarFoto);

        nombreEs = (TextView) a.findViewById(R.id.textAction);


        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setEvent();
            }
        });

        dialog = new ProgressDialog(a);
        dialog.setTitle("Cargando");
        dialog.setMessage("Por favor espere...");
        dialog.setCancelable(false);
        dialog.show();

        ParseUser user = ParseUser.getCurrentUser();
        usertype = user.getNumber("usertype").intValue();


        setInformacion();

        getImage();

        if (usertype != 0) {
            eliminarEvent.setVisibility(View.GONE);
        }

        eliminarEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteEvent(objectId);
            }
        });


        btnGaleria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new EventsGalleryFragment();
                Bundle b = new Bundle();
                b.putString("eventId", objectId);
                b.putString("nombreEvento", titulo);
                fragment.setArguments(b);
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                transaction.replace(R.id.content_home, fragment, "fragment").addToBackStack(null).commit();
                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
            }
        });

        btnFotoEvento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //showPicker();
                multiPicker();
            }
        });

        Typeface();
    }

    public void setInformacion() {

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            titulo = bundle.getString("nombreEvento");
            lugar = bundle.getString("lugarEvento");
            descripcion = bundle.getString("descripcionEvento");
            fecha = bundle.getString("fechaEvento");
            hora = bundle.getString("horaEvento");
            f = (Date) bundle.getSerializable("fE");
            objectId = bundle.getString("eventId");
            if (f != null) {
                fec = f.getTime();
            }
            date = bundle.getLong("dateEvento");
        }

        nombreEs.setText(titulo);
        tituloEvento.setText(titulo);
        lugarEvento.setText(lugar);
        descripcionEvento.setText(descripcion);
        fechaEvento.setText(fecha);
        horaEvento.setText(hora);
    }

    public void Typeface() {
        Typeface bold = Typeface.createFromAsset(a.getAssets(), avenirBold);
        Typeface medium = Typeface.createFromAsset(a.getAssets(), avenirMedium);

        tituloEvento.setTypeface(bold);
        descripcionEvento.setTypeface(medium);
        lugarEvento.setTypeface(medium);
        fechaEvento.setTypeface(medium);
        horaEvento.setTypeface(medium);

    }

    public void setEvent() {
        Calendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(date);
        ContentResolver cr = a.getContentResolver();
        long calID = 3;

        ContentValues values = new ContentValues();
        values.put(CalendarContract.Events.DTSTART, fec);
        values.put(CalendarContract.Events.DTEND, fec + 3600000);
        values.put(CalendarContract.Events.TITLE, titulo);
        values.put(CalendarContract.Events.DESCRIPTION, descripcion);
        values.put(CalendarContract.Events.CALENDAR_ID, calID);
        values.put(CalendarContract.Events.EVENT_TIMEZONE, "America/Mexico_City");
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);
        long eventID = Long.parseLong(uri.getLastPathSegment());
        Toast.makeText(a, "Evento agregado", Toast.LENGTH_SHORT).show();


    }

    public void getImage() {
        ParseObject evento = ParseObject.createWithoutData("evento", objectId);
        ParseQuery<ParseObject> imageParse = new ParseQuery<ParseObject>("EventoGaleria");
        imageParse.include("evento");
        imageParse.include("estudianteTag");
        imageParse.whereEqualTo("evento", evento);
        imageParse.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                dialog.hide();
                if (e == null) {
                    if (objects.size() > 0) {
                        fotosEvento.setVisibility(View.VISIBLE);
                        agregarFoto.setVisibility(View.GONE);

                    } else {

                        if (usertype == 2) {

                        } else {
                            agregarFoto.setVisibility(View.VISIBLE);
                            fotosEvento.setVisibility(View.GONE);
                        }
                    }
                } else {
                    Log.d("Error", e.getMessage());
                }
            }
        });
    }

    public static byte[] bitmapToByteArray(Bitmap bmp) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    private Bitmap addWaterMark(Bitmap src) {
        int w = src.getWidth();
        int h = src.getHeight();
        Bitmap result = Bitmap.createBitmap(w, h, src.getConfig());
        Canvas canvas = new Canvas(result);
        canvas.drawBitmap(src, 0, 0, null);

        Bitmap waterMark = BitmapFactory.decodeResource(a.getResources(), R.mipmap.ic_launcher);
        canvas.drawBitmap(waterMark, 0, 0, null);

        return result;
    }

    public void savePhoto(final String p) {
        escuelaId = ((ParseApplication) a.getApplication()).getEscuelaId();

        ParseObject event = ParseObject.createWithoutData("evento", objectId);
        eventoGaleria = new ParseObject("EventoGaleria");
        eventoGaleria.put("evento", event);
        eventoGaleria.put("autor", ParseUser.getCurrentUser());

        params = new HashMap<>();
        params.put("eventoNombre", titulo);
        params.put("escuelaObjId", escuelaId);

        eventoGaleria.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
//                d.hide();
                if (e == null) {

                    /*
                    ParseCloud.callFunctionInBackground("eventoFotoNueva", params, new FunctionCallback<Object>() {

                        @Override
                        public void done(Object object, ParseException e) {
                            if (e == null){
                                Log.d("Cloudcode","Success");
                            }else {
                                Log.d("Error",e.getMessage());
                            }
                        }
                    });
                    */


                } else {
                    Log.d("Error", e.getMessage());
                }
            }
        });
    }

    /*
    public void showPicker(){
        PickImageDialog.build(new PickSetup().setCameraButtonText("Cámara").setGalleryButtonText("Galería").setTitle("Agregar foto").setCancelText("Cancelar"))
                .setOnPickResult(new IPickResult() {
                    @Override
                    public void onPickResult(PickResult r) {
                        //TODO: do what you have to...
                        if (r.getError() == null) {
                            //If you want the Uri.
                            //Mandatory to refresh image from Uri.
                            //getImageView().setImageURI(null);

                            //Setting the real returned image.
                            //getImageView().setImageURI(r.getUri());

                            //If you want the Bitmap.

                            Bitmap bitmap = r.getBitmap();
                            // TODO use bitmap

                            Bitmap bm=addWaterMark(bitmap);
                            byte[] bytes = bitmapToByteArray(bm);
                            photoFile = new ParseFile("photo.jpg", bytes);
                            photoFile.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    //Toast.makeText(getContext(),"Imagen guardada",Toast.LENGTH_LONG).show();
                                    savePhoto();
                                }
                            });

                            //Image path
                            //r.getPath();
                        } else {
                            //Handle possible errors
                            //TODO: do what you have to do with r.getError();
                            Toast.makeText(getContext(), r.getError().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                }).show(getActivity().getSupportFragmentManager());
    }
    */



    public void multiPicker() {
        ImagePicker.create(this).toolbarImageTitle("Selecciona: ")
                .start(REQUEST_CODE_PICKER);
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        List<Image> images = ImagePicker.getImages(data);
        if (images != null && !images.isEmpty()) {
            //imageView.setImageBitmap(BitmapFactory.decodeFile(images.get(0).getPath()));
            Toast.makeText(a, String.valueOf(images.size()), Toast.LENGTH_SHORT).show();

            for (final Image i : images) {
                //Bitmap b=BitmapFactory.decodeFile(i.getPath());
                saveEventPhoto(i.getPath());
                //savePhoto();
            }

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void saveEventPhoto(final String paths){

        escuelaId = ((ParseApplication) a.getApplication()).getEscuelaId();

        ParseObject event = ParseObject.createWithoutData("evento", objectId);
        eventoGaleria = new ParseObject("EventoGaleria");
        eventoGaleria.put("evento", event);
        eventoGaleria.put("autor", ParseUser.getCurrentUser());

        params = new HashMap<>();
        params.put("eventoNombre", titulo);
        params.put("escuelaObjId", escuelaId);

        eventoGaleria.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
//                d.hide();
                if (e == null) {
                    uploadToS3(paths, eventoGaleria.getObjectId());
                            /*
                            ParseCloud.callFunctionInBackground("eventoFotoNueva", params, new FunctionCallback<Object>() {

                                @Override
                                public void done(Object object, ParseException e) {
                                    if (e == null){
                                        Log.d("Cloudcode","Success");
                                    }else {
                                        Log.d("Error",e.getMessage());
                                    }
                                }
                            });
                            */
                            Toast.makeText(a,"Foto lista",Toast.LENGTH_SHORT).show();

                } else {
                    Log.d("Error", e.getMessage());
                }
            }
        });
    }

    public void uploadToS3(String path, String objectId) {
        try {
            TransferObserver observe = utility.upload(Constants.BUCKET_NAME, objectId, new java.io.File(path));
            observe.setTransferListener(new TransferListener() {
                @Override
                public void onStateChanged(int id, TransferState state) {
                    if (state == TransferState.COMPLETED) {
                        Toast.makeText(a, "Foto guardada", Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                    long _bytesCurrent = bytesCurrent;
                    long _bytesTotal = bytesTotal;

                    float percentage = ((float) _bytesCurrent / (float) _bytesTotal * 100);
                    Log.d("percentage", "" + percentage);
                }

                @Override
                public void onError(int id, Exception ex) {
                    Log.d("Error de Amazon", ex.getMessage());
                }
            });

            Log.d("Foto", "Foto subida");
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
        }

    }

    //Nuevo
    public void deleteEvent(String id) {

        ParseQuery<ParseObject> eventQuery = ParseQuery.getQuery("evento");
        eventQuery.whereEqualTo("objectId", id);
        eventQuery.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (e == null) {
                    object.deleteInBackground(new DeleteCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                Toast.makeText(a, "Evento eliminado exitosamente", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(a, "Error al eliminar evento, intente de nuevo", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else {
                    Toast.makeText(a, "Error,verifique su conexión a internet", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    /*
    public static Bitmap addWatermark(Resources res, Bitmap source) {
        int w, h;
        Canvas c;
        Paint paint;
        Bitmap bmp, watermark;

        Matrix matrix;
        float scale;
        RectF r;

        w = source.getWidth();
        h = source.getHeight();

        // Create the new bitmap
        bmp = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);

        paint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DITHER_FLAG | Paint.FILTER_BITMAP_FLAG);

        // Copy the original bitmap into the new one
        c = new Canvas(bmp);
        c.drawBitmap(source, 0, 0, paint);

        // Load the watermark
        watermark = BitmapFactory.decodeResource(res, R.drawable.logo_skola);
        // Scale the watermark to be approximately 40% of the source image height
        scale = (float) (((float) h * 0.40) / (float) watermark.getHeight());

        // Create the matrix
        matrix = new Matrix();
        matrix.postScale(scale, scale);
        // Determine the post-scaled size of the watermark
        r = new RectF(0, 0, watermark.getWidth(), watermark.getHeight());
        matrix.mapRect(r);
        // Move the watermark to the bottom right corner
        matrix.postTranslate(w - r.width(), h - r.height());

        // Draw the watermark
        c.drawBitmap(watermark, matrix, paint);
        // Free up the bitmap memory
        watermark.recycle();

        return bmp;
    }
    */

    /*
    private void addImageWaterMark(){
        Bitmap bitmap = null;
        try {
            filePath = getFilePath();

            File f = new File(filePath);
            if (f.exists()) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                try
                {
                    bitmap = BitmapFactory.decodeStream(new FileInputStream(f), null, options);
                    Bitmap output = addWatermark(getResources(), bitmap);
                    save image to sdcard
                    saveImage(output, IMAGE_NAME);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    */

    /*
    private String getFilePath() {

        String fileNameWithFullPath = null;
        File sdCardRoot = Environment.getExternalStorageDirectory();
        File yourDir = new File(sdCardRoot, "Skola");
        for (File f : yourDir.listFiles()) {
            if (f.isFile()) {
                String name = f.getName();
                Log.i("file names", name);

                if (name.contains("yuv")) {
                    fileNameWithFullPath = "/storage/emulated/0/Skola/" + name;
                }

            }

        }

        return fileNameWithFullPath;
    }
    */

    /*
    private void saveImage(Bitmap finalBitmap, String image_name) {
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root+"/Skola/");

        filePath = root+"/Skola/"+image_name+".jpg";

        myDir.mkdirs();
        String fname = image_name+ ".jpg";
        File file = new File(myDir, fname);
        if (file.exists()) file.delete();
        Log.i("LOAD", root + fname);
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    */

    private Bitmap getLogo(){
        nombre=((ParseApplication)a.getApplication()).getEscuelaNombre();
        Bitmap logo=null;


        return logo;
    }

    public void setWatermark(Bitmap base){
        Bitmap bitmap=BitmapFactory.decodeResource(getResources(),R.drawable.logo_skola);
        RubberStampConfig config=new RubberStampConfig.RubberStampConfigBuilder()
                //Base sobre la que se pondra la marca
                .base(base)
                //Logo de escuela
                .rubberStamp(bitmap)
                .rubberStampPosition(RubberStampPosition.TOP_LEFT)
                .build();

        RubberStamp rubber=new RubberStamp(a);
        rubber.addStamp(config);
        ImageView i=new ImageView(a);

    }

}
