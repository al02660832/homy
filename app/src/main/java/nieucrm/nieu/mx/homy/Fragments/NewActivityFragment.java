package nieucrm.nieu.mx.skola.Fragments;


import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.Date;
import java.util.HashMap;

import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;



/**
 * A simple {@link Fragment} subclass.
 */
public class NewActivityFragment extends Fragment {

    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf",demibold="font/avenir-next-demi-bold.ttf";

    String objectId,numero,dia,title,notas,tema,grupo,descripcion,valores,habitos,titulo,fecha,dateTag;

    Date fec;

    ParseObject grupos;
    ParseObject planeacion;

    HashMap<String, Object> params;


    TextView fechaActividad,txtTituloActividad,txtTema,txtHabitos,txtValores,txtDescripActividad,txtNotas;
    EditText editTituloActividad,editHabitos,editValores,editDescripActividad,editNotas,editTema;
    Button crearActividad;

    String grupoId,grupoName,escuelaId;

    int usertype;

    Activity a;

    public NewActivityFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            a=(Activity) context;
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_new_activity, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);

        fechaActividad=(TextView)v.findViewById(R.id.fechaActividad);
        txtTituloActividad=(TextView)v.findViewById(R.id.txtTituloActividad);
        txtHabitos=(TextView)v.findViewById(R.id.txtHabitos);
        txtValores=(TextView)v.findViewById(R.id.txtValores);
        txtDescripActividad=(TextView)v.findViewById(R.id.txtDescripActividad);
        txtNotas=(TextView)v.findViewById(R.id.txtNotas);
        txtTema=(TextView)v.findViewById(R.id.txtTema);

        editTituloActividad=(EditText)v.findViewById(R.id.editTituloActividad);
        editHabitos=(EditText)v.findViewById(R.id.editHabitos);
        editValores=(EditText)v.findViewById(R.id.editValores);
        editDescripActividad=(EditText)v.findViewById(R.id.editDescripActividad);
        editNotas=(EditText)v.findViewById(R.id.editNotas);
        editTema=(EditText)v.findViewById(R.id.editTema);


        crearActividad=(Button)v.findViewById(R.id.crearActividad);

        usertype= ParseUser.getCurrentUser().getNumber("usertype").intValue();


        setTypeface();

        getInfo();

        if (usertype==2){
            crearActividad.setVisibility(View.GONE);
        }

        crearActividad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createPlaneacion();
            }
        });

    }

    public void getInfo(){
        Bundle b = this.getArguments();
        if (b != null) {
            dateTag=b.getString("dia")+" "+b.getString("fecha");
            descripcion=b.getString("descripcion");
            valores=b.getString("valores");
            titulo=b.getString("titulo");
            notas=b.getString("notas");
            tema=b.getString("tema");
            habitos=b.getString("habitos");
            grupo=b.getString("grupo");
            grupoName=b.getString("nombreGrupo");

            fecha=b.getString("fecha");

            fec=(Date) b.getSerializable("date");
            //fec=;

            editNotas.setText(notas);
            editTema.setText(tema);
            editHabitos.setText(habitos);
            editTituloActividad.setText(titulo);
            editValores.setText(valores);
            editDescripActividad.setText(descripcion);

            fechaActividad.setText(dateTag);
        }
    }

    public void setTypeface(){
        Typeface bold=Typeface.createFromAsset(a.getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(a.getAssets(),avenirMedium);
        Typeface dem=Typeface.createFromAsset(a.getAssets(),demibold);

        fechaActividad.setTypeface(dem);
        txtTituloActividad.setTypeface(dem);
        txtTema.setTypeface(dem);
        txtHabitos.setTypeface(dem);
        txtNotas.setTypeface(dem);
        txtDescripActividad.setTypeface(dem);
        txtValores.setTypeface(dem);

        editDescripActividad.setTypeface(medium);
        editHabitos.setTypeface(medium);
        editNotas.setTypeface(medium);
        editTema.setTypeface(medium);
        editTituloActividad.setTypeface(medium);
        editValores.setTypeface(medium);

        crearActividad.setTypeface(dem);

    }

    public void createPlaneacion(){
        params=new HashMap<>();
        escuelaId=((ParseApplication)a.getApplication()).getEscuelaId();
        notas=editNotas.getText().toString().trim();
        tema=editTema.getText().toString().trim();
        descripcion=editDescripActividad.getText().toString().trim();
        valores=editValores.getText().toString().trim();
        habitos=editHabitos.getText().toString().trim();
        titulo=editTituloActividad.getText().toString().trim();

        planeacion=new ParseObject("Planeacion");
        grupos=ParseObject.createWithoutData("grupo",grupo);
        planeacion.put("notas",notas);
        planeacion.put("tema",tema);
        planeacion.put("descripcion",descripcion);
        planeacion.put("valores",valores);
        planeacion.put("habitos",habitos);
        planeacion.put("titulo",titulo);
        planeacion.put("fecha",fec);
        planeacion.put("grupo",grupos);
        planeacion.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e==null){
                    Toast.makeText(a,"La planeación fue guardada exitosamente",Toast.LENGTH_SHORT).show();
                    grupoId=grupo;
                    params.put("grupoId",grupoId);
                    params.put("grupoName",grupoName);
                    params.put("escuelaObjId",escuelaId);
                    ParseCloud.callFunctionInBackground("planeacionParentNotification", params, new FunctionCallback<Object>() {

                        @Override
                        public void done(Object object, ParseException e) {
                            if (e == null){
                                Log.d("Cloudcode","Success");
                            }else {
                                Log.d("Error",e.getMessage());
                            }
                        }
                    });
                }else {
                    Toast.makeText(a,"Error, intente de nuevo",Toast.LENGTH_SHORT).show();
                    Log.d("Error",e.getMessage());
                }
            }
        });

    }
}
