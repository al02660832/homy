package nieucrm.nieu.mx.skola.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

import nieucrm.nieu.mx.skola.Adapter.ServicioActivoAdapter;
import nieucrm.nieu.mx.skola.DataModel.ServiciosData;
import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class ServiciosActivosFragment extends Fragment {
    ListView listaServiciosActivos;
    ServicioActivoAdapter adapter;
    ArrayList<ServiciosData>serviciosArray;
    ProgressBar progressActivo;
    String escuelaId;


    public ServiciosActivosFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_servicios_activos, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        listaServiciosActivos=(ListView)view.findViewById(R.id.listaServiciosActivos);

        progressActivo=(ProgressBar)view.findViewById(R.id.progressActivo);

        getServices();

    }

    public void getServices(){
        serviciosArray=new ArrayList<>();
        escuelaId=((ParseApplication)getActivity().getApplication()).getEscuelaId();
        ParseObject escuela=ParseObject.createWithoutData("Escuela",escuelaId);

        ParseQuery<ParseObject> servicios=ParseQuery.getQuery("Servicio").include("escuela");
        servicios.whereEqualTo("escuela",escuela);
        servicios.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                progressActivo.setVisibility(View.GONE);
                listaServiciosActivos.setVisibility(View.VISIBLE);
                if (e==null){
                    for (ParseObject o:objects){
                        ServiciosData data=new ServiciosData();
                        data.setNombre(o.getString("nombre"));
                        data.setDescripcion(o.getString("descripcion"));
                        data.setObjectId(o.getObjectId());
                        data.setActivo(o.getBoolean("activo"));
                        data.setPrecio("$"+String.valueOf(o.getNumber("precio").intValue()));
                        data.setPrice(o.getNumber("precio").intValue());
                        serviciosArray.add(data);
                    }
                    adapter=new ServicioActivoAdapter(getContext(),serviciosArray);
                    listaServiciosActivos.setAdapter(adapter);
                }else {
                    Toast.makeText(getContext(),"Error en query",Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {


        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.crear_servicio,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id=item.getItemId();
        if(id==R.id.crearServicio){

            Fragment fragment = new NewServiciosFragment();
            Bundle bundle=new Bundle();
            fragment.setArguments(bundle);
            FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
            transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();

        }


        return super.onOptionsItemSelected(item);

    }

}
