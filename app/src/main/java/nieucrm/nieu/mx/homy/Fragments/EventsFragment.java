package nieucrm.nieu.mx.skola.Fragments;


import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import nieucrm.nieu.mx.skola.Adapter.EventAdapter;
import nieucrm.nieu.mx.skola.DataModel.EventData;
import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;

/**
 * Created by Carlos Romero
 */

public class EventsFragment extends Fragment {

    ListView listView;
    ProgressBar progressEvento;
    EventAdapter adapter;
    ArrayList<EventData>listEventos;
    String descripcion,lugar,nombre,fecha;
    String escuelaId;
    Activity a;


    public EventsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            a=(Activity) context;
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        setHasOptionsMenu(true);
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_events, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle=this.getArguments();
        if (bundle!=null){
            getView().setBackgroundColor(Color.parseColor("#629EEA"));
        }

        listView=(ListView)view.findViewById(R.id.listaEvento);
        progressEvento=(ProgressBar)view.findViewById(R.id.progressEvento);
        listEventos=new ArrayList<>();

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -1);
        escuelaId=((ParseApplication)a.getApplication()).getEscuelaId();
        ParseObject escuela=ParseObject.createWithoutData("Escuela",escuelaId);

        ParseQuery<ParseObject> query=ParseQuery.getQuery("evento").whereGreaterThan("fecha",cal.getTime());
        query.whereEqualTo("escuela",escuela);
        query.orderByAscending("fecha");




        query.findInBackground(new FindCallback<ParseObject>() {

            @Override
            public void done(List<ParseObject> objectsList, ParseException e) {
                progressEvento.setVisibility(View.GONE);
                listView.setVisibility(View.VISIBLE);

                if (e==null){

                    for(ParseObject object:objectsList){
                        EventData event=new EventData();

                        event.setFecha(object.getDate("fecha"));
                        event.setNombre(object.getString("nombre"));
                        event.setObjectId(object.getObjectId());

                        nombre=object.getString("nombre");
                        descripcion=object.getString("descripcion");
                        fecha=object.getDate("fecha").toString();
                        lugar=object.getString("lugar");
                        event.setDescripcion(descripcion);
                        event.setLugar(lugar);
                        listEventos.add(event);

                    }

                    adapter=new EventAdapter(a,listEventos);
                    listView.setAdapter(adapter);

                }

            }
        });


    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        if (ParseUser.getCurrentUser().getNumber("usertype").intValue()==0) {
            inflater.inflate(R.menu.new_event, menu);
        }else {
            Log.d("Tipo de usuario","Padre o profesor");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id=item.getItemId();
        if(id==R.id.newEvent){
            Fragment fragment = new NewEventFragment();
            FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
            transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
        }

        return super.onOptionsItemSelected(item);

    }

}
