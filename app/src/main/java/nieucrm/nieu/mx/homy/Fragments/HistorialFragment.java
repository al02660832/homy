package nieucrm.nieu.mx.skola.Fragments;



import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import nieucrm.nieu.mx.skola.Adapter.HistorialAdapter;
import nieucrm.nieu.mx.skola.DataModel.MensajeData;
import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class HistorialFragment extends Fragment {

    ListView historialMensajes;
    ArrayList<MensajeData> mensajeDataArray=new ArrayList<>();
    HistorialAdapter historialAdapter;
    ProgressBar progressBar;
    ArrayList<String>anuncioPhoto;
    String escuelaId;
    ParseObject escuela;
    Activity a;

    public HistorialFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            a=(Activity) context;
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_historial, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);

        historialMensajes=(ListView)v.findViewById(R.id.historialMensaje);

        progressBar=(ProgressBar)v.findViewById(R.id.progressHistorial);

        getAnuncioPhoto();

    }

    public void getMensajes(){
        try {
            escuelaId = ((ParseApplication) a.getApplication()).getEscuelaId();

        }catch (Exception ex){
            ex.printStackTrace();
        }

        escuela = ParseObject.createWithoutData("Escuela", escuelaId);
        ParseQuery<ParseObject>q=new ParseQuery<ParseObject>("anuncio");
        q.include("autor");
        q.include("grupos");
        q.include("estudiante");
        q.include("tipo");
        q.include("aprobadoPor");

        q.orderByDescending("createdAt");

        //Obtener maestros y admins
        //Evitar padres
        ParseQuery<ParseUser> usertypeQuery=ParseUser.getQuery();
        usertypeQuery.whereEqualTo("escuela",escuela);
        usertypeQuery.whereNotEqualTo("usertype",2);

        q.whereMatchesQuery("autor",usertypeQuery);
        //Anuncios
        q.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                progressBar.setVisibility(View.GONE);
                historialMensajes.setVisibility(View.VISIBLE);
                if (e==null){

                    for (ParseObject o:objects){
                        MensajeData mensajeData=new MensajeData();

                        ParseObject est=o.getParseObject("estudiante");
                        ParseUser autor=o.getParseUser("autor");
                        ParseObject gru=o.getParseObject("grupos");
                        ParseObject od=o.getParseObject("tipo");

                        if (est==null){
                            if (o.get("grupos")==null){
                                mensajeData.setDestinatario("Todo el colegio");
                            }else {
                                if(o.getList("grupos")==null){

                                }else {
                                    List<ParseObject>g=o.getList("grupos");
                                    StringBuilder a=new StringBuilder();
                                    for (int i=0;i<g.size();i++){
                                        try{
                                            if (g.get(i).getString("grupoId")==null){
                                                Log.e("Grupo vacio","Error");
                                            }else {
                                                if(i==g.size()-1){
                                                    a.append(g.get(i).getString("grupoId"));
                                                }else {
                                                    a.append(g.get(i).getString("grupoId")).append(", ");

                                                }
                                            }
                                            mensajeData.setReceptor(a.toString());
                                        }catch (Exception e1){
                                            Log.e("Excepcion",e1.getMessage());
                                        }
                                    }
                                }

                            }
                        }else {
                            mensajeData.setStudentId(est.getObjectId());
                            mensajeData.setReceptor(est.getString("NOMBRE")+" "+est.getString("APELLIDO"));
                        }

                        if (autor==null){
                            mensajeData.setParentesco("General");
                        } else {
                            if (autor.getString("parentesco").equals("Admin")){
                                mensajeData.setEmisor(autor.getString("parentesco"));
                            }else{
                                mensajeData.setEmisor(autor.getString("parentesco"));

                            }
                        }


                        Date p =o.getCreatedAt();
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                        try {
                            p = sdf.parse(sdf.format(p));
                        } catch (java.text.ParseException e1) {
                            e1.printStackTrace();
                        }
                        sdf.setTimeZone(TimeZone.getTimeZone("CST"));

                        mensajeData.setFechaEntrega(sdf.format(p));

                        mensajeData.setID(o.getObjectId());

                        DateFormat df=new SimpleDateFormat("HH:mm:ss", Locale.US);
                        Date d=new Date(o.getCreatedAt().getTime());
                        String format=df.format(d);
                        mensajeData.setHora(format);
                        if (od==null){
                            mensajeData.setTipo("Anuncio");
                        }else {

                            mensajeData.setTipo(od.getString("nombre"));
                        }

                        if (o.getParseFile("attachment")==null){
                            if (o.getBoolean("awsAttachment")){
                                mensajeData.setAwsAttachment(true);
                            }else {
                                if (anuncioPhoto.size()==0){
                                    Log.e("Foto","Fotos en anuncio");
                                }else {
                                    if (anuncioPhoto.contains(o.getObjectId())){
                                        mensajeData.setTablaAnuncio(true);
                                        Log.e("Foto","Foto en tabla anuncio"+o.getObjectId());
                                    }
                                }
                            }
                        }else {
                            mensajeData.setParseUri(o.getParseFile("attachment").getUrl());
                        }

                        //Set hora
                        // mensajeData.setHora("Hora");
                        mensajeData.setDescripcion(o.getString("descripcion"));

                        mensajeData.setApproved(o.getBoolean("aprobado"));

                        if (o.getParseUser("aprobadoPor")==null){

                        }else {
                            ParseUser apro=o.getParseUser("aprobadoPor");
                            mensajeData.setAprobadoPor(apro.getString("nombre")+" "+apro.getString("apellidos"));
                        }

                        mensajeDataArray.add(mensajeData);
                        //
                    }
                    historialAdapter=new HistorialAdapter(a,mensajeDataArray);
                    historialMensajes.setAdapter(historialAdapter);


                }else {

                    Toast.makeText(a,"Error",Toast.LENGTH_SHORT).show();

                }
            }
        });
    }

    public void getAnuncioPhoto(){
        anuncioPhoto=new ArrayList<>();
        ParseQuery<ParseObject>anuncioPhotos=ParseQuery.getQuery("AnuncioPhoto");
        anuncioPhotos.include("anuncio");
        anuncioPhotos.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {

                if (e==null){
                    for (ParseObject o:objects){
                        if (o.getParseObject("anuncio")!=null){
                            anuncioPhoto.add(o.getParseObject("anuncio").getObjectId());

                        }
                    }
                    Log.e("AnuncioPhoto",String.valueOf(anuncioPhoto.size()));
                }
                //Llamar al primer metodo
                getMensajes();
            }
        });
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);

    }
}
