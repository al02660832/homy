package nieucrm.nieu.mx.skola.Activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.service.MarketService;
import com.onesignal.OneSignal;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.LogOutCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import in.myinnos.library.AppIconNameChanger;
import nieucrm.nieu.mx.skola.BuildConfig;
import nieucrm.nieu.mx.skola.Fragments.AccessFragment;
import nieucrm.nieu.mx.skola.Fragments.ActivityFragment;
import nieucrm.nieu.mx.skola.Fragments.AdminAccesosFragment;
import nieucrm.nieu.mx.skola.Fragments.AdministrationFragment;
import nieucrm.nieu.mx.skola.Fragments.AnnouncementFragment;
import nieucrm.nieu.mx.skola.Fragments.CredentialFragment;
import nieucrm.nieu.mx.skola.Fragments.CreditCardFragment;
import nieucrm.nieu.mx.skola.Fragments.DrapeFragment;
import nieucrm.nieu.mx.skola.Fragments.EstudiantesFragment;
import nieucrm.nieu.mx.skola.Fragments.EventsFragment;
import nieucrm.nieu.mx.skola.Fragments.GroupFragment;
import nieucrm.nieu.mx.skola.Fragments.GroupProfileFragment;
import nieucrm.nieu.mx.skola.Fragments.GruposAdminFragment;
import nieucrm.nieu.mx.skola.Fragments.HistorialServiciosFragment;
import nieucrm.nieu.mx.skola.Fragments.HomeFragment;
import nieucrm.nieu.mx.skola.Fragments.HomeworkFragment;
import nieucrm.nieu.mx.skola.Fragments.InformacionAdminFragment;
import nieucrm.nieu.mx.skola.Fragments.InformationAdminFragment;
import nieucrm.nieu.mx.skola.Fragments.InformationFragment;
import nieucrm.nieu.mx.skola.Fragments.InformationViewFragment;
import nieucrm.nieu.mx.skola.Fragments.MensajesAdminFragment;
import nieucrm.nieu.mx.skola.Fragments.MonitorAccessFragment;
import nieucrm.nieu.mx.skola.Fragments.NewGroupFragment;
import nieucrm.nieu.mx.skola.Fragments.NoticeFragment;
import nieucrm.nieu.mx.skola.Fragments.PagosAdminFragment;
import nieucrm.nieu.mx.skola.Fragments.PagosFragment;
import nieucrm.nieu.mx.skola.Fragments.PagosListFragment;
import nieucrm.nieu.mx.skola.Fragments.PaquetesFragment;
import nieucrm.nieu.mx.skola.Fragments.PerfilEstudianteFragment;
import nieucrm.nieu.mx.skola.Fragments.PinViewFragment;
import nieucrm.nieu.mx.skola.Fragments.PlanFragment;
import nieucrm.nieu.mx.skola.Fragments.ProspectoFragment;
import nieucrm.nieu.mx.skola.Fragments.RegisterFragment;
import nieucrm.nieu.mx.skola.Fragments.ReportesFragment;
import nieucrm.nieu.mx.skola.Fragments.ServiciosActivosFragment;
import nieucrm.nieu.mx.skola.Fragments.ServiciosAdminFragment;
import nieucrm.nieu.mx.skola.Fragments.SubscriptionFragment;
import nieucrm.nieu.mx.skola.Fragments.SuccessfulRegistrationFragment;
import nieucrm.nieu.mx.skola.Fragments.TeacherFragment;
import nieucrm.nieu.mx.skola.Fragments.UsuariosFragment;
import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.Helpers.Shaker;
import nieucrm.nieu.mx.skola.R;

/**
 * Created by Carlos Romero
 */

public class Home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,Shaker.Callback {

    private TextView txtUsuario,txtLogo;
    private ImageButton btnLogo;
    ImageView imagenAction,imageView;
    TextView textAction;
    ActionBarDrawerToggle toggle;


    JSONObject t;
    private Shaker shaker=null;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    Toolbar toolbar;
    int usertype;

    ArrayList<String>tag;

    String activeName;

    ParseObject escuela;
    String escuelaId,nombre;
    List<String>disableNames;

    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf";
    Fragment fragment=null;

    Collection<String> receivedTags;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        MarketService ms = new MarketService(this);
        ms.level(MarketService.MAJOR).level(MarketService.MINOR).level(MarketService.REVISION).checkVersion();


        tag=new ArrayList<>();


        setContentView(R.layout.activity_home);


        if(ParseUser.getCurrentUser()==null){
            finish();
            startActivity(new Intent(getApplicationContext(),Login.class));
        }else{

            usertype=ParseUser.getCurrentUser().getNumber("usertype").intValue();
            checkAndRequestPermissions();

            if (usertype==1||usertype==0){

                fragment=new TeacherFragment();
                getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").commit();
            }else{
                if (usertype==2) {
                    fragment = new HomeFragment();
                    getSupportFragmentManager().beginTransaction().add(R.id.content_home, fragment, "fragment").commit();
                }
            }


        }



        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");

       textAction=(TextView)findViewById(R.id.textAction);
       imagenAction=(ImageView)findViewById(R.id.imagenAction);

        //textAction.setText("");


        //imagenAction=(ImageView)findViewById(R.id.actionImage);
        changeBar();

        setSupportActionBar(toolbar);








        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View v=navigationView.getHeaderView(0);
        View view=navigationView;

        txtUsuario=(TextView)v.findViewById(R.id.txtUsuario);
        txtLogo=(TextView)v.findViewById(R.id.txtLogo);
        btnLogo=(ImageButton)view.findViewById(R.id.btnGroup);

        imageView=(ImageView)v.findViewById(R.id.imageView);
        shaker=new Shaker(this, 2d, 500, this);






        if(ParseUser.getCurrentUser()==null){
            txtUsuario.setText(" ");

        }else{

            ParseQuery<ParseUser> userQuery=ParseUser.getQuery().include("escuela");
            userQuery.whereEqualTo("objectId",ParseUser.getCurrentUser().getObjectId());
            userQuery.getFirstInBackground(new GetCallback<ParseUser>() {
                @Override
                public void done(ParseUser object, ParseException e) {
                    if(e==null) {
                        if (object.getParseObject("escuela")==null){
                            Toast.makeText(getApplicationContext(),"El usuario no tiene escuela definida",Toast.LENGTH_SHORT).show();
                        }else {
                            escuela = object.getParseObject("escuela");
                            escuelaId=escuela.getObjectId();
                            nombre=escuela.getString("nombre");

                            ((ParseApplication)getApplication()).setEscuelaId(escuelaId);
                            ((ParseApplication)getApplication()).setEscuelaNombre(nombre);


                            changeImage();

                            OneSignal.getTags(new OneSignal.GetTagsHandler() {
                                @Override
                                public void tagsAvailable(JSONObject tags) {
                                    if (tags==null){
                                        Log.d("Tags","No tags found");
                                        try {
                                            registerTags();
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }else {
                                        tag.add(tags.toString());
                                        OneSignal.deleteTag(tag.toString());
                                        try {
                                            registerTags();
                                        } catch (JSONException e1) {
                                            e1.printStackTrace();
                                        }
                                    }
                                }
                            });

                            ParseQuery<ParseObject>escuelas=ParseQuery.getQuery("Escuela");
                            escuelas.whereEqualTo("objectId",escuelaId);
                            escuelas.getFirstInBackground(new GetCallback<ParseObject>() {
                                @Override
                                public void done(ParseObject object, ParseException e) {
                                    if (e==null){
                                        if (!object.getBoolean("paid")){
                                            Log.e("Escuela","Pagada");
                                           if (usertype==1||usertype==0){
                                               fragment = new DrapeFragment();

                                               FragmentTransaction transaction= getSupportFragmentManager().beginTransaction();
                                               transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                                               transaction.replace(R.id.content_home,fragment,"fragment").commit();
                                           }
                                        }
                                    }else {
                                        Log.e("Escuela","No Pagada");
                                        Log.e("Error ",e.getMessage() );
                                    }
                                }
                            });

                            //Toast.makeText(getContext(),escuela.getObjectId(),Toast.LENGTH_SHORT).show();
                            //Toast.makeText(getApplicationContext(),escuela.getObjectId(),Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.e("Error",e.getMessage());

                    }
                }
            });



            String userName=ParseUser.getCurrentUser().getString("nombre");
            String apellidos=ParseUser.getCurrentUser().getString("apellidos");
            txtUsuario.setText("¡Hola "+userName+" "+apellidos+"!");

        }

        btnLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "http://skola.nieu.mx/";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        setTypeface();

        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        shaker.close();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        shaker.close();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                /*
                Toast.makeText(getApplicationContext(),"Funciona",Toast.LENGTH_SHORT).show();
                getSupportFragmentManager().popBackStack();*/
                getSupportFragmentManager().popBackStack();
                return true;
            case R.id.credential:
                fragment = new CredentialFragment();

                FragmentTransaction transaction= getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.settingsDrawer) {

            /*
            getPackageManager().setComponentEnabledSetting(
                    new ComponentName(".Activity.Home", ".Activity.Guest"),
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
                    */
            if (usertype==0){
                fragment=new SubscriptionFragment();
                FragmentTransaction transaction= getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
            }else {

                fragment=new PinViewFragment();
                FragmentTransaction transaction= getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();

                //Toast.makeText(getApplicationContext(),"Este módulo aún se encuentra en desarrollo",Toast.LENGTH_LONG).show();
            }


        } else if (id == R.id.logoutDrawer) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("¿Desea cerrar sesión?")
                    .setTitle("Advertencia")
                    .setCancelable(false)
                    .setNegativeButton("Cancelar",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            })
                    .setPositiveButton("Continuar",
                            new DialogInterface.OnClickListener() {
                                public void onClick(final DialogInterface dialog, int id) {

                                    ParseUser.logOutInBackground(new LogOutCallback() {
                                        @Override
                                        public void done(ParseException e) {
                                           if(e==null){

                                               removeTags();



                                               finish();
                                               startActivity(new Intent(getApplicationContext(),Login.class));
                                           }else {
                                               Toast.makeText(getApplicationContext(),"Error",Toast.LENGTH_LONG).show();
                                           }
                                        }
                                    });

                                }
                            });
            AlertDialog alert = builder.create();
            alert.show();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void shakingStarted() {

        if (fragment instanceof CredentialFragment){
            usertype=ParseUser.getCurrentUser().getNumber("usertype").intValue();
            getSupportFragmentManager().popBackStack();

        }else {
            fragment=new CredentialFragment();
            FragmentTransaction transaction= getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
            transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
        }


    }

    @Override
    public void shakingStopped() {

    }

    public void registerTags() throws JSONException {

        if (ParseUser.getCurrentUser()==null){
            Toast.makeText(this,"Inicie sesión de nuevo",Toast.LENGTH_SHORT).show();
        }else{
            if (usertype==0){
                t=new JSONObject();

                t.put("escuela",escuelaId);
                t.put("admin","admin");

                OneSignal.sendTags(t);
                Log.d("Tags",t.toString());

            }else{
                if (usertype==1){
                    t=new JSONObject();

                    ParseQuery<ParseObject>gruposMaestro=ParseQuery.getQuery("grupo");
                    gruposMaestro.whereEqualTo("Maestros",ParseUser.getCurrentUser());

                    ParseQuery<ParseObject>estudiantes=ParseQuery.getQuery("Estudiantes");
                    estudiantes.whereMatchesQuery("grupo",gruposMaestro);
                    estudiantes.findInBackground(new FindCallback<ParseObject>() {
                        @Override
                        public void done(List<ParseObject> objects, ParseException e) {
                            for (ParseObject o:objects){
                                String channel="tuser_"+o.getObjectId();
                                try {
                                    t.put(channel,channel);
                                    t.put("escuela",escuelaId);
                                    Log.d("Tag",t.toString());

                                } catch (JSONException e1) {
                                    Log.e("Error tag",e1.getMessage());
                                }
                                OneSignal.sendTags(t);
                            }
                        }
                    });

                }
            }if (usertype==2){
                t=new JSONObject();

                ParseQuery<ParseObject>q=ParseQuery.getQuery("Estudiantes");
                q.whereEqualTo("PersonasAutorizadas",ParseUser.getCurrentUser());
                q.findInBackground(new FindCallback<ParseObject>() {
                    @Override
                    public void done(List<ParseObject> objects, ParseException e) {
                        if(e==null){
                            for (ParseObject ob:objects){
                                String channel="user_"+ob.getObjectId();
                                try {
                                    t.put(channel,channel);
                                    t.put("escuela",escuelaId);
                                    Log.d("Tag",t.toString());
                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                }
                            }
                            try{
                                OneSignal.sendTags(t);
                            }catch (Exception e2){
                                e2.printStackTrace();
                            }
                        }else{
                            Log.e("Error parse",e.getMessage());
                        }

                    }
                });
            }
        }
    }

    private  boolean checkAndRequestPermissions() {
        int camera = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);
        int storage = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int read = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int writeC = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR);
        int readC = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR);
        List<String> listPermissionsNeeded = new ArrayList<>();

        if (camera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.CAMERA);
        }
        if (storage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (read != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (writeC != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_CALENDAR);
        }
        if (readC != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_CALENDAR);
        }

        if (!listPermissionsNeeded.isEmpty())
        {
            ActivityCompat.requestPermissions(this,listPermissionsNeeded.toArray
                    (new String[listPermissionsNeeded.size()]),REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    public void changeBar(){
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                Fragment currentBackstackFragment=getSupportFragmentManager().findFragmentByTag("fragment");
                /*
                int stackHeight = getSupportFragmentManager().getBackStackEntryCount();
                if (stackHeight > 0) { // if we have something on the stack (doesn't include the current shown fragment)
                    getSupportActionBar().setHomeButtonEnabled(true);
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                } else {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                    getSupportActionBar().setHomeButtonEnabled(false);
                    toggle.setDrawerIndicatorEnabled(true);
                }
                */
                getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
                    @Override
                    public void onBackStackChanged() {
                        if(getSupportFragmentManager().getBackStackEntryCount() > 0){
                            toggle.setDrawerIndicatorEnabled(false);
                            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                            toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    getSupportFragmentManager().popBackStack();
                                }
                            });
                        }
                        else {
                            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                            toggle.setDrawerIndicatorEnabled(true);
                        }
                    }
                });

                if(currentBackstackFragment instanceof TeacherFragment){

                    imagenAction.setVisibility(View.VISIBLE);
                    toolbar.setBackground(getResources().getDrawable(R.drawable.header));
                    textAction.setVisibility(View.GONE);
                    //nombre=((ParseApplication)getApplication()).getEscuelaNombre();
                    //textAction.setText(nombre);

                }

                if(currentBackstackFragment instanceof HomeFragment){
                    imagenAction.setVisibility(View.VISIBLE);
                    toolbar.setBackground(getResources().getDrawable(R.drawable.home_header));
                    textAction.setVisibility(View.GONE);
                    //nombre=((ParseApplication)getApplication()).getEscuelaNombre();
                    //textAction.setText(nombre);
                }

                if (currentBackstackFragment instanceof DrapeFragment){
                    textAction.setText("Tu Plan");
                    textAction.setVisibility(View.VISIBLE);
                    toolbar.setBackground(getResources().getDrawable(R.drawable.home_header));
                    imagenAction.setVisibility(View.GONE);
                }

                if (currentBackstackFragment instanceof SuccessfulRegistrationFragment){
                    textAction.setText("Confirmación");
                    toolbar.setBackground(getResources().getDrawable(R.drawable.grupos_header));
                    imagenAction.setVisibility(View.GONE);
                }

                if (currentBackstackFragment instanceof PlanFragment){
                    textAction.setText("Skola");
                    toolbar.setBackground(getResources().getDrawable(R.drawable.grupos_header));
                    imagenAction.setVisibility(View.GONE);
                }

                if (currentBackstackFragment instanceof SubscriptionFragment){
                    textAction.setText("Tu subscripción");
                    toolbar.setBackground(getResources().getDrawable(R.drawable.grupos_header));
                    imagenAction.setVisibility(View.GONE);
                }



                if (currentBackstackFragment instanceof RegisterFragment || currentBackstackFragment instanceof CreditCardFragment){
                    textAction.setText("Registro");
                    toolbar.setBackground(getResources().getDrawable(R.drawable.grupos_header));
                    imagenAction.setVisibility(View.GONE);
                }



                if(currentBackstackFragment instanceof AccessFragment||currentBackstackFragment instanceof AdminAccesosFragment){
                    textAction.setText("Accesos");
                    toolbar.setBackground(getResources().getDrawable(R.drawable.accesos_header));
                    //imagenAction.setImageDrawable(getResources().getDrawable(R.drawable.accesos_header));
                    imagenAction.setVisibility(View.GONE);
                    textAction.setVisibility(View.VISIBLE);



                }

                if(currentBackstackFragment instanceof AdministrationFragment){
                    textAction.setText("Administración");
                    if (usertype==1||usertype==0){
                        toolbar.setBackground(getResources().getDrawable(R.drawable.admin_admin_header));
                    }else {
                        toolbar.setBackground(getResources().getDrawable(R.drawable.admin_header));
                    }

                    imagenAction.setVisibility(View.GONE);
                    textAction.setVisibility(View.VISIBLE);


                }

                if (currentBackstackFragment instanceof UsuariosFragment){
                    textAction.setText("Usuarios");
                    textAction.setVisibility(View.VISIBLE);

                    toolbar.setBackground(getResources().getDrawable(R.drawable.admin_admin_header));
                }

                if(currentBackstackFragment instanceof EventsFragment){
                    textAction.setText("Eventos");
                    textAction.setVisibility(View.VISIBLE);

                    if (usertype==1||usertype==0){
                        toolbar.setBackground(getResources().getDrawable(R.drawable.admin_eventos_header));
                        imagenAction.setVisibility(View.GONE);

                    }else {
                        toolbar.setBackground(getResources().getDrawable(R.drawable.eventos_header));
                        imagenAction.setVisibility(View.GONE);
                    }


                }
                if(currentBackstackFragment instanceof InformationFragment){
                    textAction.setText("Información");
                    textAction.setVisibility(View.VISIBLE);

                    toolbar.setBackground(getResources().getDrawable(R.drawable.info_header));
                    imagenAction.setVisibility(View.GONE);

                }
                if(currentBackstackFragment instanceof InformationViewFragment || currentBackstackFragment instanceof NoticeFragment){
                    textAction.setText("Información");
                    textAction.setVisibility(View.VISIBLE);

                    toolbar.setBackground(getResources().getDrawable(R.drawable.info_header));
                    imagenAction.setVisibility(View.GONE);

                }
                if(currentBackstackFragment instanceof PagosFragment){
                    textAction.setText("Pagos");
                    textAction.setVisibility(View.VISIBLE);

                    toolbar.setBackground(getResources().getDrawable(R.drawable.pago_colegiatura_header));
                    imagenAction.setVisibility(View.GONE);

                }
                if (currentBackstackFragment instanceof PagosListFragment){
                    textAction.setText("Pagos");
                    textAction.setVisibility(View.VISIBLE);

                    toolbar.setBackground(getResources().getDrawable(R.drawable.pago_colegiatura_header));
                    imagenAction.setVisibility(View.GONE);

                }
                if (currentBackstackFragment instanceof EstudiantesFragment){
                    textAction.setText("Estudiantes");
                    textAction.setVisibility(View.VISIBLE);

                    toolbar.setBackground(getResources().getDrawable(R.drawable.estudiantes_header));
                    imagenAction.setVisibility(View.GONE);

                }
                if (currentBackstackFragment instanceof CredentialFragment){
                    textAction.setVisibility(View.VISIBLE);

                    toolbar.setBackground(getResources().getDrawable(R.drawable.accesos_header));
                    textAction.setText("Credencial Digital");
                    imagenAction.setVisibility(View.GONE);

                }
                if (currentBackstackFragment instanceof PerfilEstudianteFragment){
                    textAction.setVisibility(View.VISIBLE);

                    imagenAction.setVisibility(View.GONE);
                }

                if (currentBackstackFragment instanceof GroupFragment||currentBackstackFragment instanceof NewGroupFragment){
                    textAction.setVisibility(View.VISIBLE);

                    textAction.setText("Grupos");
                    toolbar.setBackground(getResources().getDrawable(R.drawable.accesos_header));
                    imagenAction.setVisibility(View.GONE);
                }

                if (currentBackstackFragment instanceof PaquetesFragment){
                    textAction.setVisibility(View.VISIBLE);

                    textAction.setText("Paquetes");
                    toolbar.setBackground(getResources().getDrawable(R.drawable.admin_eventos_header));
                    imagenAction.setVisibility(View.GONE);

                }

                if (currentBackstackFragment instanceof InformacionAdminFragment){
                    textAction.setText("Información");
                    textAction.setVisibility(View.VISIBLE);

                    toolbar.setBackground(getResources().getDrawable(R.drawable.info_header));
                    imagenAction.setVisibility(View.GONE);

                }

                if (currentBackstackFragment instanceof InformationAdminFragment){
                    textAction.setVisibility(View.VISIBLE);

                    toolbar.setBackground(getResources().getDrawable(R.drawable.info_header));
                    imagenAction.setVisibility(View.GONE);

                }

                if (currentBackstackFragment instanceof ProspectoFragment){
                    textAction.setText("Prospectos");
                    textAction.setVisibility(View.VISIBLE);

                    toolbar.setBackground(getResources().getDrawable(R.drawable.eventos_header));
                    imagenAction.setVisibility(View.GONE);

                }

                if (currentBackstackFragment instanceof GruposAdminFragment){
                    textAction.setText("Grupos");
                    textAction.setVisibility(View.VISIBLE);

                    toolbar.setBackground(getResources().getDrawable(R.drawable.estudiantes_header));
                    imagenAction.setVisibility(View.GONE);

                }

                if (currentBackstackFragment instanceof GroupProfileFragment){
                    textAction.setVisibility(View.VISIBLE);

                    toolbar.setBackground(getResources().getDrawable(R.drawable.accesos_header));
                    imagenAction.setVisibility(View.GONE);
                }


                if (currentBackstackFragment instanceof HomeworkFragment){
                    textAction.setText("Tareas");
                    textAction.setVisibility(View.VISIBLE);

                    toolbar.setBackground(getResources().getDrawable(R.drawable.tarea_header));
                    imagenAction.setVisibility(View.GONE);
                }

                if (currentBackstackFragment instanceof AnnouncementFragment){
                    textAction.setText("Anuncios");
                    textAction.setVisibility(View.VISIBLE);

                    toolbar.setBackground(getResources().getDrawable(R.drawable.tarea_anuncio_header));
                    imagenAction.setVisibility(View.GONE);
                }

                if (currentBackstackFragment instanceof MonitorAccessFragment){
                    textAction.setText("Monitor");
                    textAction.setVisibility(View.VISIBLE);

                    toolbar.setBackground(getResources().getDrawable(R.drawable.header_azul));
                    imagenAction.setVisibility(View.GONE);
                }

                if (currentBackstackFragment instanceof MensajesAdminFragment){
                    textAction.setText("Comunicación");
                    textAction.setVisibility(View.VISIBLE);

                    toolbar.setBackground(getResources().getDrawable(R.drawable.accesos_header));
                    imagenAction.setVisibility(View.GONE);
                }

                if (currentBackstackFragment instanceof ServiciosAdminFragment){
                    textAction.setText("Servicios");
                    textAction.setVisibility(View.VISIBLE);

                    toolbar.setBackground(getResources().getDrawable(R.drawable.libreta_anuncio_header));
                }

                if (currentBackstackFragment instanceof ServiciosActivosFragment){
                    textAction.setText("Servicios");
                    textAction.setVisibility(View.VISIBLE);

                    toolbar.setBackground(getResources().getDrawable(R.drawable.libreta_anuncio_header));
                }
                if (currentBackstackFragment instanceof HistorialServiciosFragment){
                    textAction.setText("Historial");
                    textAction.setVisibility(View.VISIBLE);

                }

                if (currentBackstackFragment instanceof PagosAdminFragment){
                    textAction.setText("Pagos");
                    textAction.setVisibility(View.VISIBLE);

                }
                if (currentBackstackFragment instanceof ReportesFragment){
                    textAction.setText("Reportes");
                    textAction.setVisibility(View.VISIBLE);

                }
                if (currentBackstackFragment instanceof ActivityFragment){
                    textAction.setText("Planeación");
                    textAction.setVisibility(View.VISIBLE);

                }
            }

        });
    }

    public void setTypeface(){
        Typeface bold=Typeface.createFromAsset(getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(getAssets(),avenirMedium);
        txtUsuario.setTypeface(medium);
        txtLogo.setTypeface(medium);
        textAction.setTypeface(medium);
    }

    private void changeImage(){
        disableNames=new ArrayList<>();

        if (nombre.equals("Skola México")){
            imagenAction.setImageResource(R.drawable.sidemenu_logo);
            //textAction.setText(nombre);
            imageView.setImageResource(R.drawable.sidemenu_logo);

            activeName="nieucrm.nieu.mx.skola.Home.SkolaMexico";

            activeName="nieucrm.nieu.mx.skola.Home.SkolaMexico";

            disableNames.add("nieucrm.nieu.mx.skola.Home.Azul");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsMetepec");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsToluca");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pashamama");
            disableNames.add("nieucrm.nieu.mx.skola.Home.LittleFeet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Homy");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Chibolines");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCap");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCol");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBMet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.CDA");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Cipres");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Educ");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Summer");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Naoli");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Chick");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Nat");
            disableNames.add("nieucrm.nieu.mx.skola.Home.PI");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Antara");

            new AppIconNameChanger.Builder(Home.this).activeName(activeName)
                    .disableNames(disableNames).packageName(BuildConfig.APPLICATION_ID).build().setNow();

        }

        if (nombre.equals("Baúl Azúl Cancún")){
            imagenAction.setImageResource(R.drawable.sidemenu_baul);
            //textAction.setText(nombre);
            imageView.setImageResource(R.drawable.sidemenu_baul);

            Log.e("Error icono",getComponentName().toString());

            activeName="nieucrm.nieu.mx.skola.Home.Azul";

            disableNames.add("nieucrm.nieu.mx.skola.Home.SkolaMexico");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsMetepec");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsToluca");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pashamama");
            disableNames.add("nieucrm.nieu.mx.skola.Home.LittleFeet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Homy");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Chibolines");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCap");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCol");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBMet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.CDA");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Cipres");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Educ");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Summer");



            new AppIconNameChanger.Builder(Home.this).activeName(activeName)
                    .disableNames(disableNames).packageName(BuildConfig.APPLICATION_ID).build().setNow();


        }


        if (nombre.equals("BabyBoomers Metepec")){
            imagenAction.setImageResource(R.drawable.sidemenu_bb);
            //textAction.setText(nombre);

            imageView.setImageResource(R.drawable.sidemenu_bb);

            activeName="nieucrm.nieu.mx.skola.Home.BBMet";

            disableNames.add("nieucrm.nieu.mx.skola.Home.Azul");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsMetepec");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsToluca");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pashamama");
            disableNames.add("nieucrm.nieu.mx.skola.Home.LittleFeet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Homy");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Chibolines");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCap");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCol");
            disableNames.add("nieucrm.nieu.mx.skola.Home.SkolaMexico");
            disableNames.add("nieucrm.nieu.mx.skola.Home.CDA");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Cipres");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Educ");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Summer");


            new AppIconNameChanger.Builder(Home.this).activeName(activeName)
                    .disableNames(disableNames).packageName(BuildConfig.APPLICATION_ID).build().setNow();


        }

        if (nombre.equals("BabyBoomers Capultitlán")){
            imagenAction.setImageResource(R.drawable.sidemenu_bb);
            //textAction.setText(nombre);

            imageView.setImageResource(R.drawable.sidemenu_bb);

            activeName="nieucrm.nieu.mx.skola.Home.BBCap";

            disableNames.add("nieucrm.nieu.mx.skola.Home.Azul");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsMetepec");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsToluca");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pashamama");
            disableNames.add("nieucrm.nieu.mx.skola.Home.LittleFeet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Homy");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Chibolines");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCol");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBMet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.SkolaMexico");
            disableNames.add("nieucrm.nieu.mx.skola.Home.CDA");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Cipres");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Educ");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Summer");

            new AppIconNameChanger.Builder(Home.this).activeName(activeName)
                    .disableNames(disableNames).packageName(BuildConfig.APPLICATION_ID).build().setNow();


        }

        if (nombre.equals("BabyBoomers Colón")){
            imagenAction.setImageResource(R.drawable.sidemenu_bb);
            //textAction.setText(nombre);

            imageView.setImageResource(R.drawable.sidemenu_bb);

            activeName="nieucrm.nieu.mx.skola.Home.BBCol";

            disableNames.add("nieucrm.nieu.mx.skola.Home.Azul");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsMetepec");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsToluca");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pashamama");
            disableNames.add("nieucrm.nieu.mx.skola.Home.LittleFeet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Homy");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Chibolines");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCap");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBMet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.SkolaMexico");
            disableNames.add("nieucrm.nieu.mx.skola.Home.CDA");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Cipres");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Educ");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Summer");


            new AppIconNameChanger.Builder(Home.this).activeName(activeName)
                    .disableNames(disableNames).packageName(BuildConfig.APPLICATION_ID).build().setNow();


        }

        if (nombre.equals("Moms and Tots Metepec")){
            imagenAction.setImageResource(R.drawable.sidemenu_mtm);
            //textAction.setText(nombre);

            imageView.setImageResource(R.drawable.sidemenu_mtm);

            activeName="nieucrm.nieu.mx.skola.Home.MomsMetepec";

            disableNames.add("nieucrm.nieu.mx.skola.Home.Azul");
            disableNames.add("nieucrm.nieu.mx.skola.Home.SkolaMexico");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsToluca");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pashamama");
            disableNames.add("nieucrm.nieu.mx.skola.Home.LittleFeet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Homy");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Chibolines");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCap");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCol");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBMet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.CDA");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Cipres");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Educ");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Summer");


            new AppIconNameChanger.Builder(Home.this).activeName(activeName)
                    .disableNames(disableNames).packageName(BuildConfig.APPLICATION_ID).build().setNow();
        }

        if (nombre.equals("Moms and Tots Toluca")){
            imagenAction.setImageResource(R.drawable.sidemenu_mtm);
            //textAction.setText(nombre);

            imageView.setImageResource(R.drawable.sidemenu_mtm);

            activeName="nieucrm.nieu.mx.skola.Home.MomsToluca";

            disableNames.add("nieucrm.nieu.mx.skola.Home.Azul");
            disableNames.add("nieucrm.nieu.mx.skola.Home.SkolaMexico");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsMetepec");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pashamama");
            disableNames.add("nieucrm.nieu.mx.skola.Home.LittleFeet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Homy");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Chibolines");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCap");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCol");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBMet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.CDA");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Cipres");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Educ");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Summer");


            new AppIconNameChanger.Builder(Home.this).activeName(activeName)
                    .disableNames(disableNames).packageName(BuildConfig.APPLICATION_ID).build().setNow();
        }


        if (nombre.equals("Chibolines")){
            imagenAction.setImageResource(R.drawable.sidemenu_chib);
            //textAction.setText(nombre);

            imageView.setImageResource(R.drawable.sidemenu_chib);

            activeName="nieucrm.nieu.mx.skola.Home.Chibolines";

            disableNames.add("nieucrm.nieu.mx.skola.Home.Azul");
            disableNames.add("nieucrm.nieu.mx.skola.Home.SkolaMexico");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsMetepec");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsToluca");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pashamama");
            disableNames.add("nieucrm.nieu.mx.skola.Home.LittleFeet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Homy");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCap");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCol");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBMet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.CDA");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Cipres");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Educ");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Summer");



            new AppIconNameChanger.Builder(Home.this).activeName(activeName)
                    .disableNames(disableNames).packageName(BuildConfig.APPLICATION_ID).build().setNow();

        }

        if (nombre.equals("Little Feet")){
            imagenAction.setImageResource(R.drawable.sidemenu_lf);
            //textAction.setText(nombre);

            imageView.setImageResource(R.drawable.sidemenu_lf);

            activeName="nieucrm.nieu.mx.skola.Home.LittleFeet";

            disableNames.add("nieucrm.nieu.mx.skola.Home.Azul");
            disableNames.add("nieucrm.nieu.mx.skola.Home.SkolaMexico");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsMetepec");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsToluca");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pashamama");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Homy");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Chibolines");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCap");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCol");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBMet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.CDA");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Cipres");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Educ");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Summer");


            new AppIconNameChanger.Builder(Home.this).activeName(activeName)
                    .disableNames(disableNames).packageName(BuildConfig.APPLICATION_ID).build().setNow();

        }

        if (nombre.equals("Homy")){
            imagenAction.setImageResource(R.drawable.sidemenu_homy);
            //textAction.setText(nombre);

            imageView.setImageResource(R.drawable.sidemenu_homy);

            activeName="nieucrm.nieu.mx.skola.Home.Homy";

            disableNames.add("nieucrm.nieu.mx.skola.Home.Azul");
            disableNames.add("nieucrm.nieu.mx.skola.Home.SkolaMexico");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsMetepec");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsToluca");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pashamama");
            disableNames.add("nieucrm.nieu.mx.skola.Home.LittleFeet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Chibolines");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCap");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCol");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBMet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.CDA");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Cipres");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Educ");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Summer");




            new AppIconNameChanger.Builder(Home.this).activeName(activeName)
                    .disableNames(disableNames).packageName(BuildConfig.APPLICATION_ID).build().setNow();

        }

        if (nombre.equals("Colegio del Ángel")){
            imagenAction.setImageResource(R.drawable.sidemenu_cda);
            //textAction.setText(nombre);

            imageView.setImageResource(R.drawable.sidemenu_cda);

            activeName="nieucrm.nieu.mx.skola.Home.CDA";

            disableNames.add("nieucrm.nieu.mx.skola.Home.Azul");
            disableNames.add("nieucrm.nieu.mx.skola.Home.SkolaMexico");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsMetepec");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsToluca");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pashamama");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Homy");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Chibolines");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCap");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCol");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBMet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.LittleFeet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Cipres");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Educ");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Summer");



            new AppIconNameChanger.Builder(Home.this).activeName(activeName)
                    .disableNames(disableNames).packageName(BuildConfig.APPLICATION_ID).build().setNow();
        }

        if (nombre.equals("Pashamama")){
            imagenAction.setImageResource(R.drawable.sidemenu_pasha);
            //textAction.setText(nombre);

            imageView.setImageResource(R.drawable.sidemenu_pasha);

            activeName="nieucrm.nieu.mx.skola.Home.Pashamama";


            disableNames.add("nieucrm.nieu.mx.skola.Home.Azul");
            disableNames.add("nieucrm.nieu.mx.skola.Home.SkolaMexico");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsMetepec");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsToluca");
            disableNames.add("nieucrm.nieu.mx.skola.Home.LittleFeet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Homy");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Chibolines");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCap");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCol");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBMet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Cipres");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Educ");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Summer");

            disableNames.add("nieucrm.nieu.mx.skola.Home.CDA");

            new AppIconNameChanger.Builder(Home.this).activeName(activeName)
                    .disableNames(disableNames).packageName(BuildConfig.APPLICATION_ID).build().setNow();

        }

        if (nombre.equals("Estancia Pandín")){
            imagenAction.setImageResource(R.drawable.sidemenu_pandin);
            //textAction.setText("Estancia Pandín");

            imageView.setImageResource(R.drawable.sidemenu_pandin);

            activeName="nieucrm.nieu.mx.skola.Home.Pandin";

            disableNames.add("nieucrm.nieu.mx.skola.Home.Azul");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Cipres");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Educ");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Summer");

            disableNames.add("nieucrm.nieu.mx.skola.Home.SkolaMexico");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsMetepec");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsToluca");
            disableNames.add("nieucrm.nieu.mx.skola.Home.LittleFeet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Homy");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Chibolines");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCap");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCol");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBMet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.CDA");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pashamama");


            new AppIconNameChanger.Builder(Home.this).activeName(activeName)
                    .disableNames(disableNames).packageName(BuildConfig.APPLICATION_ID).build().setNow();


        }

        if (nombre.equals("Summerville Polanco")){
            imagenAction.setImageResource(R.drawable.sidemenu_summer);
            //textAction.setText("Summerville Polanco");
            imageView.setImageResource(R.drawable.sidemenu_summer);

            activeName="nieucrm.nieu.mx.skola.Home.Summer";

            disableNames.add("nieucrm.nieu.mx.skola.Home.Pandin");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Azul");
            disableNames.add("nieucrm.nieu.mx.skola.Home.SkolaMexico");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsMetepec");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsToluca");
            disableNames.add("nieucrm.nieu.mx.skola.Home.LittleFeet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Homy");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Chibolines");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCap");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCol");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBMet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.CDA");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pashamama");

            disableNames.add("nieucrm.nieu.mx.skola.Home.Cipres");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Educ");



            new AppIconNameChanger.Builder(Home.this).activeName(activeName)
                    .disableNames(disableNames).packageName(BuildConfig.APPLICATION_ID).build().setNow();


        }

        if (nombre.equals("Educación Especial MAS")){
            imagenAction.setImageResource(R.drawable.sidemenu_edu);
            //textAction.setText("Educación Especial MAS");
            imageView.setImageResource(R.drawable.sidemenu_edu);

            activeName="nieucrm.nieu.mx.skola.Home.Educ";


            disableNames.add("nieucrm.nieu.mx.skola.Home.SkolaMexico");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsMetepec");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsToluca");
            disableNames.add("nieucrm.nieu.mx.skola.Home.LittleFeet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Homy");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Chibolines");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCap");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCol");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBMet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.CDA");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pashamama");

            disableNames.add("nieucrm.nieu.mx.skola.Home.Summer");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pandin");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Azul");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Cipres");


            new AppIconNameChanger.Builder(Home.this).activeName(activeName)
                    .disableNames(disableNames).packageName(BuildConfig.APPLICATION_ID).build().setNow();




        }

        if (nombre.equals("Colegio Ciprés Metepec")){
            imagenAction.setImageResource(R.drawable.home_cipres);
            //textAction.setText("Colegio Ciprés Metepec");
            imageView.setImageResource(R.drawable.sidemenu_cipres);

            activeName="nieucrm.nieu.mx.skola.Home.Cipres";


            disableNames.add("nieucrm.nieu.mx.skola.Home.SkolaMexico");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsMetepec");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsToluca");
            disableNames.add("nieucrm.nieu.mx.skola.Home.LittleFeet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Homy");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Chibolines");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCap");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCol");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBMet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.CDA");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pashamama");

            disableNames.add("nieucrm.nieu.mx.skola.Home.Summer");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pandin");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Azul");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Educ");


            new AppIconNameChanger.Builder(Home.this).activeName(activeName)
                    .disableNames(disableNames).packageName(BuildConfig.APPLICATION_ID).build().setNow();


        }

        if (nombre.equals("Glenn Doman")){
            imagenAction.setImageResource(R.drawable.sidemenu_glenn);
            //textAction.setText("Colegio Ciprés Metepec");
            imageView.setImageResource(R.drawable.sidemenu_glenn);

            activeName="nieucrm.nieu.mx.skola.Home.Glenn";


            disableNames.add("nieucrm.nieu.mx.skola.Home.SkolaMexico");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsMetepec");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsToluca");
            disableNames.add("nieucrm.nieu.mx.skola.Home.LittleFeet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Homy");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Chibolines");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCap");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCol");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBMet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.CDA");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pashamama");

            disableNames.add("nieucrm.nieu.mx.skola.Home.Summer");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pandin");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Azul");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Educ");


            new AppIconNameChanger.Builder(Home.this).activeName(activeName)
                    .disableNames(disableNames).packageName(BuildConfig.APPLICATION_ID).build().setNow();


        }
        if (nombre.equals("Colegio Torreón")){
            imagenAction.setImageResource(R.drawable.sidemenu_torreon);
            //textAction.setText("Colegio Ciprés Metepec");
            imageView.setImageResource(R.drawable.sidemenu_torreon);

            activeName="nieucrm.nieu.mx.skola.Home.Torreon";


            disableNames.add("nieucrm.nieu.mx.skola.Home.SkolaMexico");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsMetepec");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsToluca");
            disableNames.add("nieucrm.nieu.mx.skola.Home.LittleFeet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Homy");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Chibolines");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCap");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCol");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBMet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.CDA");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pashamama");

            disableNames.add("nieucrm.nieu.mx.skola.Home.Summer");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pandin");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Azul");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Educ");


            new AppIconNameChanger.Builder(Home.this).activeName(activeName)
                    .disableNames(disableNames).packageName(BuildConfig.APPLICATION_ID).build().setNow();


        }

        if (nombre.equals("Kids Bee Happy")){
            imagenAction.setImageResource(R.drawable.sidemenu_kids);
            //textAction.setText("Colegio Ciprés Metepec");
            imageView.setImageResource(R.drawable.sidemenu_kids);

            activeName="nieucrm.nieu.mx.skola.Home.Kids";


            disableNames.add("nieucrm.nieu.mx.skola.Home.SkolaMexico");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsMetepec");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsToluca");
            disableNames.add("nieucrm.nieu.mx.skola.Home.LittleFeet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Homy");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Chibolines");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCap");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCol");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBMet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.CDA");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pashamama");

            disableNames.add("nieucrm.nieu.mx.skola.Home.Summer");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pandin");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Azul");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Educ");


            new AppIconNameChanger.Builder(Home.this).activeName(activeName)
                    .disableNames(disableNames).packageName(BuildConfig.APPLICATION_ID).build().setNow();


        }

        if (nombre.equals("Balance Cube Metepec")){
            imagenAction.setImageResource(R.drawable.sidemenu_cube);
            //textAction.setText("Colegio Ciprés Metepec");
            imageView.setImageResource(R.drawable.sidemenu_cube);

            activeName="nieucrm.nieu.mx.skola.Home.Balance";


            disableNames.add("nieucrm.nieu.mx.skola.Home.SkolaMexico");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsMetepec");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsToluca");
            disableNames.add("nieucrm.nieu.mx.skola.Home.LittleFeet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Homy");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Chibolines");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCap");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCol");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBMet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.CDA");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pashamama");

            disableNames.add("nieucrm.nieu.mx.skola.Home.Summer");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pandin");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Azul");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Educ");


            new AppIconNameChanger.Builder(Home.this).activeName(activeName)
                    .disableNames(disableNames).packageName(BuildConfig.APPLICATION_ID).build().setNow();


        }

        if (nombre.equals("Mi Mundo Mágico")){
            imagenAction.setImageResource(R.drawable.sidemenu_mundo);
            //textAction.setText("Colegio Ciprés Metepec");
            imageView.setImageResource(R.drawable.sidemenu_mundo);

            activeName="nieucrm.nieu.mx.skola.Home.Mundo";


            disableNames.add("nieucrm.nieu.mx.skola.Home.SkolaMexico");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsMetepec");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsToluca");
            disableNames.add("nieucrm.nieu.mx.skola.Home.LittleFeet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Homy");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Chibolines");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCap");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCol");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBMet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.CDA");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pashamama");

            disableNames.add("nieucrm.nieu.mx.skola.Home.Summer");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pandin");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Azul");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Educ");


            new AppIconNameChanger.Builder(Home.this).activeName(activeName)
                    .disableNames(disableNames).packageName(BuildConfig.APPLICATION_ID).build().setNow();

        }

        if (nombre.equals("Colegio Antara")){
            imagenAction.setImageResource(R.drawable.sidemenu_ant);
            //textAction.setText("Colegio Ciprés Metepec");
            imageView.setImageResource(R.drawable.sidemenu_ant);

            activeName="nieucrm.nieu.mx.skola.Home.Antara";


            disableNames.add("nieucrm.nieu.mx.skola.Home.SkolaMexico");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsMetepec");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsToluca");
            disableNames.add("nieucrm.nieu.mx.skola.Home.LittleFeet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Homy");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Chibolines");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCap");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCol");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBMet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.CDA");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pashamama");

            disableNames.add("nieucrm.nieu.mx.skola.Home.Summer");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pandin");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Azul");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Educ");


            new AppIconNameChanger.Builder(Home.this).activeName(activeName)
                    .disableNames(disableNames).packageName(BuildConfig.APPLICATION_ID).build().setNow();

        }

        if (nombre.equals("Kids & Babies Toluca")){
            imagenAction.setImageResource(R.drawable.sidemenu_kb);
            //textAction.setText("Colegio Ciprés Metepec");
            imageView.setImageResource(R.drawable.sidemenu_kb);

            activeName="nieucrm.nieu.mx.skola.Home.KB";


            disableNames.add("nieucrm.nieu.mx.skola.Home.SkolaMexico");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Antara");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsMetepec");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsToluca");
            disableNames.add("nieucrm.nieu.mx.skola.Home.LittleFeet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Homy");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Chibolines");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCap");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCol");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBMet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.CDA");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pashamama");

            disableNames.add("nieucrm.nieu.mx.skola.Home.Summer");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pandin");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Azul");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Educ");


            new AppIconNameChanger.Builder(Home.this).activeName(activeName)
                    .disableNames(disableNames).packageName(BuildConfig.APPLICATION_ID).build().setNow();

        }

        if (nombre.equals("Napanee Centro")){
            imagenAction.setImageResource(R.drawable.sidemenu_napa);
            //textAction.setText("Colegio Ciprés Metepec");
            imageView.setImageResource(R.drawable.sidemenu_napa);

            activeName="nieucrm.nieu.mx.skola.Home.NapCentro";


            disableNames.add("nieucrm.nieu.mx.skola.Home.SkolaMexico");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Antara");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsMetepec");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsToluca");
            disableNames.add("nieucrm.nieu.mx.skola.Home.LittleFeet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Homy");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Chibolines");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCap");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCol");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBMet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.CDA");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pashamama");

            disableNames.add("nieucrm.nieu.mx.skola.Home.Summer");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pandin");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Azul");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Educ");


            new AppIconNameChanger.Builder(Home.this).activeName(activeName)
                    .disableNames(disableNames).packageName(BuildConfig.APPLICATION_ID).build().setNow();

        }

        if (nombre.equals("Napanee Bambini")){
            imagenAction.setImageResource(R.drawable.sidemenu_napa);
            //textAction.setText("Colegio Ciprés Metepec");
            imageView.setImageResource(R.drawable.sidemenu_napa);

            activeName="nieucrm.nieu.mx.skola.Home.NapBambi";


            disableNames.add("nieucrm.nieu.mx.skola.Home.SkolaMexico");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Antara");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsMetepec");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsToluca");
            disableNames.add("nieucrm.nieu.mx.skola.Home.LittleFeet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Homy");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Chibolines");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCap");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCol");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBMet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.CDA");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pashamama");

            disableNames.add("nieucrm.nieu.mx.skola.Home.Summer");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pandin");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Azul");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Educ");


            new AppIconNameChanger.Builder(Home.this).activeName(activeName)
                    .disableNames(disableNames).packageName(BuildConfig.APPLICATION_ID).build().setNow();

        }

        if (nombre.equals("Napanee Baby")){
            imagenAction.setImageResource(R.drawable.sidemenu_napa);
            //textAction.setText("Colegio Ciprés Metepec");
            imageView.setImageResource(R.drawable.sidemenu_napa);

            activeName="nieucrm.nieu.mx.skola.Home.NapBaby";

            disableNames.add("nieucrm.nieu.mx.skola.Home.SkolaMexico");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Antara");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsMetepec");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsToluca");
            disableNames.add("nieucrm.nieu.mx.skola.Home.LittleFeet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Homy");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Chibolines");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCap");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCol");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBMet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.CDA");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pashamama");

            disableNames.add("nieucrm.nieu.mx.skola.Home.Summer");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pandin");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Azul");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Educ");


            new AppIconNameChanger.Builder(Home.this).activeName(activeName)
                    .disableNames(disableNames).packageName(BuildConfig.APPLICATION_ID).build().setNow();

        }

        if (nombre.equals("Bimbi")){
            imagenAction.setImageResource(R.drawable.sidemenu_bimbi);
            //textAction.setText("Colegio Ciprés Metepec");
            imageView.setImageResource(R.drawable.sidemenu_bimbi);

            activeName="nieucrm.nieu.mx.skola.Home.Bimbi";


            disableNames.add("nieucrm.nieu.mx.skola.Home.SkolaMexico");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Antara");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsMetepec");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsToluca");
            disableNames.add("nieucrm.nieu.mx.skola.Home.LittleFeet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Homy");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Chibolines");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCap");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCol");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBMet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.CDA");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pashamama");

            disableNames.add("nieucrm.nieu.mx.skola.Home.Summer");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pandin");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Azul");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Educ");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Merlin");


            new AppIconNameChanger.Builder(Home.this).activeName(activeName)
                    .disableNames(disableNames).packageName(BuildConfig.APPLICATION_ID).build().setNow();

        }

        if (nombre.equals("Magia de Merlín")){
            imagenAction.setImageResource(R.drawable.sidemenu_merlin);
            //textAction.setText("Colegio Ciprés Metepec");
            imageView.setImageResource(R.drawable.sidemenu_merlin);

            activeName="nieucrm.nieu.mx.skola.Home.Merlin";

            disableNames.add("nieucrm.nieu.mx.skola.Home.SkolaMexico");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Bimbi");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Antara");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsMetepec");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsToluca");
            disableNames.add("nieucrm.nieu.mx.skola.Home.LittleFeet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Homy");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Chibolines");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCap");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCol");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBMet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.CDA");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pashamama");

            disableNames.add("nieucrm.nieu.mx.skola.Home.Summer");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pandin");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Azul");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Educ");


            new AppIconNameChanger.Builder(Home.this).activeName(activeName)
                    .disableNames(disableNames).packageName(BuildConfig.APPLICATION_ID).build().setNow();

        }

        if (nombre.equals("Chick Peas")){
            imagenAction.setImageResource(R.drawable.sidemenu_chick);
            //textAction.setText("Colegio Ciprés Metepec");
            imageView.setImageResource(R.drawable.sidemenu_chick);

            activeName="nieucrm.nieu.mx.skola.Home.Chick";

            disableNames.add("nieucrm.nieu.mx.skola.Home.SkolaMexico");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Bimbi");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Antara");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsMetepec");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsToluca");
            disableNames.add("nieucrm.nieu.mx.skola.Home.LittleFeet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Homy");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Chibolines");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCap");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCol");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBMet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.CDA");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pashamama");

            disableNames.add("nieucrm.nieu.mx.skola.Home.Summer");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pandin");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Azul");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Educ");


            new AppIconNameChanger.Builder(Home.this).activeName(activeName)
                    .disableNames(disableNames).packageName(BuildConfig.APPLICATION_ID).build().setNow();

        }

        if (nombre.equals("Naoli")){
            imagenAction.setImageResource(R.drawable.sidemenu_naoli);
            //textAction.setText("Colegio Ciprés Metepec");
            imageView.setImageResource(R.drawable.sidemenu_naoli);

            activeName="nieucrm.nieu.mx.skola.Home.Naoli";

            disableNames.add("nieucrm.nieu.mx.skola.Home.SkolaMexico");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Bimbi");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Antara");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsMetepec");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsToluca");
            disableNames.add("nieucrm.nieu.mx.skola.Home.LittleFeet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Homy");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Chibolines");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCap");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCol");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBMet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.CDA");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pashamama");

            disableNames.add("nieucrm.nieu.mx.skola.Home.Summer");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pandin");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Azul");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Educ");


            new AppIconNameChanger.Builder(Home.this).activeName(activeName)
                    .disableNames(disableNames).packageName(BuildConfig.APPLICATION_ID).build().setNow();

        }

        if (nombre.equals("Natural Kids")){
            imagenAction.setImageResource(R.drawable.sidemenu_nat);
            //textAction.setText("Colegio Ciprés Metepec");
            imageView.setImageResource(R.drawable.sidemenu_nat);

            activeName="nieucrm.nieu.mx.skola.Home.Nat";

            disableNames.add("nieucrm.nieu.mx.skola.Home.SkolaMexico");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Bimbi");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Antara");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsMetepec");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsToluca");
            disableNames.add("nieucrm.nieu.mx.skola.Home.LittleFeet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Homy");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Chibolines");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCap");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCol");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBMet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.CDA");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pashamama");

            disableNames.add("nieucrm.nieu.mx.skola.Home.Summer");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pandin");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Azul");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Educ");


            new AppIconNameChanger.Builder(Home.this).activeName(activeName)
                    .disableNames(disableNames).packageName(BuildConfig.APPLICATION_ID).build().setNow();

        }

        if (nombre.equals("Peques Inn")){
            imagenAction.setImageResource(R.drawable.sidemenu_pi);
            //textAction.setText("Colegio Ciprés Metepec");
            imageView.setImageResource(R.drawable.sidemenu_pi);

            activeName="nieucrm.nieu.mx.skola.Home.PI";
            disableNames.add("nieucrm.nieu.mx.skola.Home.SkolaMexico");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Bimbi");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Antara");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsMetepec");
            disableNames.add("nieucrm.nieu.mx.skola.Home.MomsToluca");
            disableNames.add("nieucrm.nieu.mx.skola.Home.LittleFeet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Homy");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Chibolines");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCap");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBCol");
            disableNames.add("nieucrm.nieu.mx.skola.Home.BBMet");
            disableNames.add("nieucrm.nieu.mx.skola.Home.CDA");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pashamama");

            disableNames.add("nieucrm.nieu.mx.skola.Home.Summer");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Pandin");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Azul");
            disableNames.add("nieucrm.nieu.mx.skola.Home.Educ");

            new AppIconNameChanger.Builder(Home.this).activeName(activeName)
                    .disableNames(disableNames).packageName(BuildConfig.APPLICATION_ID).build().setNow();

        }

    }

    public void removeTags(){
        OneSignal.getTags(new OneSignal.GetTagsHandler() {
            @Override
            public void tagsAvailable(final JSONObject tags) {
//                receivedTags.add(String.valueOf(tags.names()));
                Log.d("Tag name",tags.toString());
                if (tags.names()!=null){
                    OneSignal.deleteTags(tags.names().toString());

                }


            }
        });
    }

}
