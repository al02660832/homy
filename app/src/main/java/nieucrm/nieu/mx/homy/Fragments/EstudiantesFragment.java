package nieucrm.nieu.mx.skola.Fragments;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import nieucrm.nieu.mx.skola.Adapter.EstudiantesAdapter;
import nieucrm.nieu.mx.skola.DataModel.EstudianteData;
import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;

/**
 * Created by Carlos Romero
 */

public class EstudiantesFragment extends Fragment {


    ListView listEstudiantes;
    ArrayList<EstudianteData>student;
    ProgressBar progressEstud;
    EstudiantesAdapter adapter;
    String eId;
    ParseQuery<ParseObject> query,numMensajes;
    EstudianteData estudiante;
    ArrayList<String> listaId;
    List<ParseObject>canalesArray;
    String grupo;
    String id;

    String escuelaId;
    ParseObject escuela;
    Activity a;

    public EstudiantesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            a=(Activity) context;
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_estudiantes, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);


        listEstudiantes=(ListView)v.findViewById(R.id.listEstudiantes);
        progressEstud=(ProgressBar)v.findViewById(R.id.progressEstud);
        student=new ArrayList<>();

        setEstudiantes();



    }

    public void setEstudiantes(){
        escuelaId=((ParseApplication)a.getApplication()).getEscuelaId();
        escuela=ParseObject.createWithoutData("Escuela",escuelaId);
        query = new ParseQuery<>("Estudiantes").include("PersonasAutorizadas").include("grupo").whereEqualTo("PersonasAutorizadas", ParseUser.getCurrentUser());
        query.whereEqualTo("escuela",escuela);
        listaId=new ArrayList<>();

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                progressEstud.setVisibility(View.GONE);
                listEstudiantes.setVisibility(View.VISIBLE);
                if (e==null){
                    for (ParseObject o:objects) {
                        estudiante = new EstudianteData();
                        estudiante.setIdEstudiante(o.getObjectId());
                        eId = o.getObjectId();
                        estudiante.setGenero(o.getString("GENERO"));
                        ParseObject gObject = o.getParseObject("grupo");
                        if (gObject==null){
                            estudiante.setGrupo("Grupo");
                        }else {
                            estudiante.setGrupo(gObject.getString("grupoId"));
                            estudiante.setIdGrupo(gObject.getObjectId());
                        }

                        //listaId.add(eId);

                        /*
                        numMensajes.whereEqualTo("canal",eId);
                        numMensajes.getFirstInBackground(new GetCallback<ParseObject>() {
                            @Override
                            public void done(ParseObject object, ParseException e) {
                                estudiante.setNoMensajes(object.getNumber("count").intValue());
                            }
                        });
                         */





                        estudiante.setNombreEstudiante(o.getString("NOMBRE")+" "+o.getString("ApPATERNO"));
                        estudiante.setNombre(o.getString("NOMBRE"));
                        estudiante.setGenero(o.getString("GENERO"));

                        student.add(estudiante);
                    }
                    adapter=new EstudiantesAdapter(a,student);
                    listEstudiantes.setAdapter(adapter);
                }

            }
        });
    }

    public void setCount(String ids){



        /*
        try {
            canalesArray=numMensajes.find();
        } catch (ParseException e) {
            e.printStackTrace();
        }

         */

        numMensajes=new ParseQuery<>("comunicacion");
        numMensajes.include("user");
        numMensajes.whereEqualTo("user",ParseUser.getCurrentUser());
        numMensajes.whereEqualTo("canal",eId);

        numMensajes.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (e==null){
                    if(object==null){

                    }else{
                        String i=String.valueOf(object.getNumber("count").intValue());
                        //Toast.makeText(getContext(),String.valueOf(estudiante.getNoMensajes()),Toast.LENGTH_SHORT).show();
                        //Toast.makeText(getContext(),object.getString("canal"),Toast.LENGTH_SHORT).show();

                        estudiante.setNoMensajes(object.getNumber("count").intValue());
                    }
                }else {
                    Log.d("No mensajes","Sin mensajes");
                }

            }
        });


    }

}
