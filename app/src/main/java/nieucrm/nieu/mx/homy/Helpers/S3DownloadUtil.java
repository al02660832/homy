package nieucrm.nieu.mx.skola.Helpers;

import android.util.Log;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3Client;

import java.io.File;

/**
 * Created by mmac1 on 09/12/2017.
 */

public class S3DownloadUtil {
    private AmazonS3Client mS3Client;
    private TransferUtility transferUtility;

    public S3DownloadUtil() {

    }

    //download method
    public void download(final String FILE_KEY, File downloadFile) {
        final TransferObserver observer = transferUtility.download(
                Constants.BUCKET_NAME,     /* The bucket to download from */
                FILE_KEY,    /* The key for the object to download */
                downloadFile        /* The file to download the object to */
        );

        observer.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                switch (state) {
                    case COMPLETED: {
                        transferUtility.deleteTransferRecord(id);
                        String resourceUrl = mS3Client.getResourceUrl(observer.getBucket(), FILE_KEY);  // get resourceUrl
                        Log.d("Estado", "onStateChanged: " + state);
                        break;
                    }

                    case CANCELED: {
                        transferUtility.deleteTransferRecord(id);
                        String resourceUrl = mS3Client.getResourceUrl(observer.getBucket(), FILE_KEY);  // get resourceUrl
                        Log.d("Estado", "onStateChanged: " + state);
                        break;
                    }

                    case FAILED: {
                        transferUtility.deleteTransferRecord(id);
                        String resourceUrl = mS3Client.getResourceUrl(observer.getBucket(), FILE_KEY);  // get resourceUrl
                        Log.d("Estado", "onStateChanged: " + state);
                        break;
                    }

                    case PAUSED: {
                        break;
                    }

                    case WAITING_FOR_NETWORK: {
                        transferUtility.deleteTransferRecord(id);
                        String resourceUrl = mS3Client.getResourceUrl(observer.getBucket(), FILE_KEY);  // get resourceUrl
                        Log.d("Estado", "onStateChanged: " + state);
                        break;
                    }
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {

            }

            @Override
            public void onError(int id, Exception ex) {
                ex.printStackTrace();
            }
        });
    }
}
