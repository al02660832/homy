package nieucrm.nieu.mx.skola.Fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.goodiebag.pinview.Pinview;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 * Created by Carlos Romero
 * 10/08/2018
 */
public class PinViewFragment extends Fragment {

    Context context;
    TextView txtHello,txtWelcome,txtEdit,txtCreate,txtRemember,txtPin,ingresaPin;
    Pinview pinView;
    Button btnForgot;

    Fragment fragment;

    String pin;
    boolean tienePin;

    public PinViewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pin_view, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        txtHello=(TextView)view.findViewById(R.id.txtHello);
        txtWelcome=(TextView)view.findViewById(R.id.txtWelcome);
        txtEdit=(TextView)view.findViewById(R.id.txtEdit);
        txtCreate=(TextView)view.findViewById(R.id.txtCreate);
        txtRemember=(TextView)view.findViewById(R.id.txtRemember);
        txtPin=(TextView)view.findViewById(R.id.txtPin);
        ingresaPin=(TextView)view.findViewById(R.id.ingresaPin);

        pinView=(Pinview)view.findViewById(R.id.pinView);

        btnForgot=(Button)view.findViewById(R.id.btnForgot);

        getPin();


    }

    public void getPin(){
        tienePin=false;
        ParseQuery<ParseUser>query=ParseUser.getQuery();
        query.whereEqualTo("objectId",ParseUser.getCurrentUser().getObjectId());
        query.getFirstInBackground(new GetCallback<ParseUser>() {
            @Override
            public void done(ParseUser object, ParseException e) {
                if (e==null){
                    pin=object.getString("pin");
                    if (pin==null||pin.equals("")){
                        tienePin=false;
                        Log.e("PIN","No tiene");

                        pinView.setPinViewEventListener(new Pinview.PinViewEventListener() {
                            @Override
                            public void onDataEntered(Pinview pinview, boolean b) {
                                //Set PIN
                                setPin(pinview.getValue());

                            }
                        });
                    }else {
                        ingresaPin.setVisibility(View.VISIBLE);
                        txtHello.setVisibility(View.GONE);
                        txtWelcome.setVisibility(View.GONE);
                        txtEdit.setVisibility(View.GONE);
                        txtCreate.setVisibility(View.GONE);
                        txtRemember.setVisibility(View.GONE);
                        tienePin=true;
                        pin=object.getString("pin");
                        Log.e("PIN","Tiene");
                        pinView.setPinViewEventListener(new Pinview.PinViewEventListener() {
                            @Override
                            public void onDataEntered(Pinview pinview, boolean b) {
                                accessWithPin(pinview.getValue());
                            }
                        });
                    }
                }else {
                    Log.e("Error",e.getMessage());
                }
            }
        });
    }

    public void setPin(final String pinValue){
        ParseQuery<ParseUser>query=ParseUser.getQuery();
        query.whereEqualTo("objectId",ParseUser.getCurrentUser().getObjectId());
        query.getFirstInBackground(new GetCallback<ParseUser>() {
            @Override
            public void done(ParseUser object, ParseException e) {
                if (e==null){
                    object.put("pin",pinValue);
                    object.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e==null){
                                Log.e("Exito","Pin creado");
                            }else {
                                Log.e("Error",e.getMessage());
                            }
                        }
                    });
                }else {
                    Log.e("Error",e.getMessage());
                }
            }
        });
    }

    public void accessWithPin(final String pinValues){
        ParseQuery<ParseUser>query=ParseUser.getQuery();
        query.whereEqualTo("objectId",ParseUser.getCurrentUser().getObjectId());
        query.getFirstInBackground(new GetCallback<ParseUser>() {
            @Override
            public void done(ParseUser object, ParseException e) {
                if (e==null){
                    if (pinValues.equals(object.getString("pin"))){
                        fragment=new RecordFragment();
                        FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                        transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                        transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                    }else{
                        Log.e("Error","Error");
                    }
                }else {
                    Log.e("Error",e.getMessage());
                }
            }
        });
    }

    public void setAlert(){

    }

}
