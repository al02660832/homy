package nieucrm.nieu.mx.skola.Fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import nieucrm.nieu.mx.skola.BuildConfig;
import nieucrm.nieu.mx.skola.Helpers.Constants;
import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.Helpers.Util;
import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class InformationAdminFragment extends Fragment {

    //EditText notasInformacion;
    WebView webView;
    TextView nombreEs;
    LinearLayout layoutInformacion;
    //Button btnModificar;

    HashMap<String, Object> params;
    String infoTypeString;
    ProgressBar progressNotas;
    String escuelaId;
    ParseObject escuela;


    TransferUtility utility;
    File outputDir;
    File pdfFile;

    PDFView pdfView;
    ProgressDialog progressDialog;



    int t;
    ParseQuery<ParseObject>c=ParseQuery.getQuery("Informacion");

    String pdfUrl;


    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf",demibold = "font/avenir-next-demi-bold.ttf";

    Activity a;

    public InformationAdminFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            a=(Activity) context;
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Bundle b=this.getArguments();
        t=b.getInt("tipo");


        return inflater.inflate(R.layout.fragment_information_admin, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);


        //notasInformacion=(EditText)v.findViewById(R.id.notasInformacion);
        webView=(WebView)v.findViewById(R.id.viewPdf);
        nombreEs = (TextView) a.findViewById(R.id.textAction);
        layoutInformacion=(LinearLayout)v.findViewById(R.id.layoutInfo);
        //btnModificar=(Button)v.findViewById(R.id.modificarInfo);
        progressNotas=(ProgressBar)v.findViewById(R.id.progressNotas);
        pdfView=(PDFView)v.findViewById(R.id.pdfAdmin);


        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setVerticalScrollBarEnabled(true);
        webView.setHorizontalScrollBarEnabled(true);

        /*
        if (BuildConfig.FLAVOR.equals("momsTotsTol")||BuildConfig.FLAVOR.equals("momsTotsMet")) {
            notasInformacion.setVisibility(View.GONE);
            btnModificar.setVisibility(View.GONE);
        }
        */

        setType();
        tipoInfo();

        /*
        btnModificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateInfo();
            }
        });
        */
    }

    /*
    public void updateInfo(){
        params = new HashMap<>();

        o.put("contenido",notasInformacion.getText().toString().trim());
        o.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e==null){

                    params.put("infoType",infoTypeString);
                    params.put("escuelaObjId",escuelaId);

                    ParseCloud.callFunctionInBackground("informationParentNotification", params, new FunctionCallback<Object>() {

                        @Override
                        public void done(Object object, ParseException e) {
                            if (e == null) {
                                Log.d("Cloudcode", "Success");

                            } else {
                                Log.e("Cloudcode", e.getMessage());
                            }
                        }
                    });
                    Toast.makeText(getContext(),"Información actualizada exitosamente",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    */
    public void tipoInfo(){

        switch (t){
            case 1:
                setView("Reglamento");
                layoutInformacion.setBackgroundColor(getResources().getColor(R.color.red_rules));

                break;

            case 2:
                //

                setView("Directorio");

                layoutInformacion.setBackgroundColor(getResources().getColor(R.color.blue_directory));
                break;

            case 3:
                //nombres.setText("Menú Semanal");
                setView("Menú Semanal");
                layoutInformacion.setBackgroundColor(getResources().getColor(R.color.green_menu));

                break;

            case 4:
                setView("Programa Semanal");
                layoutInformacion.setBackgroundColor(getResources().getColor(R.color.groups));
                break;

            case 5:
                setView("Actividad Especial");

                layoutInformacion.setBackgroundColor(getResources().getColor(R.color.blue_payment));
                break;
            case 6:
                setView("Avisos");

                layoutInformacion.setBackgroundColor(getResources().getColor(R.color.yellow_payments));
                break;

        }
    }


    public void setView(final String titulo){

        webView.setVisibility(View.VISIBLE);
        //notasInformacion.setVisibility(View.VISIBLE);

        escuelaId=((ParseApplication)a.getApplication()).getEscuelaId();
        escuela=ParseObject.createWithoutData("Escuela",escuelaId);

        c.whereEqualTo("escuela",escuela);

        c.whereEqualTo("tipo",titulo);


        c.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                progressNotas.setVisibility(View.GONE);

                /*
                if (BuildConfig.FLAVOR.equals("momsTotsTol")||BuildConfig.FLAVOR.equals("momsTotsMet")) {
                    notasInformacion.setVisibility(View.GONE);
                    btnModificar.setVisibility(View.GONE);
                }
                */
                if (e==null){
                    if (object==null){


                    }else {


                        if(object.getString("contenido")==null){
                           // notasInformacion.setVisibility(View.VISIBLE);
                        }else {
                            if (object.getString("contenido").equals("")||object.getString("contenido")==null){

                            }else{
                                String c=object.getString("contenido");

                                /*
                                notasInformacion.setText(c);
                                notasInformacion.setVisibility(View.VISIBLE);
                                */
                            }
                        }

                            if (object.getBoolean("aws")){
                                webView.setVisibility(View.GONE);
                                downloadFromS3(object.getObjectId());
                                pdfView.setVisibility(View.VISIBLE);

                                Log.e("Visibilidad",String.valueOf(pdfView.getVisibility()));

                                Log.e("Archivo ","S3");

                            }else {
                                Log.e("Archivo","Parse");
                                ParseFile file=object.getParseFile("pdf");
                                if (file!=null){
                                    pdfUrl = object.getParseFile("pdf").getUrl();

                                    webView.loadData(pdfUrl,"text/html","UTF-8");
                                    webView.loadUrl("http://docs.google.com/gview?embedded=true&url="+pdfUrl);
                                    webView.setWebViewClient(new WebViewClient() {
                                        @Override
                                        public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                            view.loadUrl(url);
                                            return true;
                                        }
                                    });
                                    webView.setVisibility(View.VISIBLE);

                                }
                            }


                    }
                }else{
                    Toast.makeText(a,"Por el momento no hay información",Toast.LENGTH_SHORT).show();
                }

            }
        });



    }


    public void setType(){
        Typeface bold=Typeface.createFromAsset(a.getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(a.getAssets(),avenirMedium);
        Typeface dem=Typeface.createFromAsset(a.getAssets(),demibold);

       // notasInformacion.setTypeface(medium);
        //btnModificar.setTypeface(dem);

    }

    public void downloadFromS3(String objectId){

        progressDialog = new ProgressDialog(a);

        progressDialog.setTitle("Cargando");
        progressDialog.setMessage("Por favor espere");
        progressDialog.setCancelable(false);
        progressDialog.show();

        try {
            utility= Util.getTransferUtility(a);
        }catch (Exception e1){
            Log.e("Error credencial",e1.getMessage());
        }

        outputDir = a.getCacheDir();
        //pdfFile= File.createTempFile(objectId+"_admin", ".pdf", outputDir);
        pdfFile=new File(outputDir.toString()+"/"+objectId+".jpeg");
        if (!pdfFile.exists()){
            try {
             pdfFile.createNewFile();
            }catch (IOException e1){
                Log.e("Nuevo",e1.getMessage());
            }
        }

        try {
            TransferObserver observe= utility.download(Constants.BUCKET_NAME,objectId,pdfFile);
            observe.setTransferListener(new TransferListener() {
                @Override
                public void onStateChanged(int id, TransferState state) {
                    if (state==TransferState.COMPLETED){
                        pdfView.fromFile(pdfFile).defaultPage(1).enableSwipe(true).onLoad(new OnLoadCompleteListener() {
                            @Override
                            public void loadComplete(int nbPages) {
                                Log.d("Number of pages",String.valueOf(nbPages));
                                progressDialog.hide();

                            }
                        }).onPageChange(new OnPageChangeListener() {
                            @Override
                            public void onPageChanged(int page, int pageCount) {
                                Log.d("Page:",String.valueOf(page));
                            }
                        }).load();
                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                    long _bytesCurrent = bytesCurrent;
                    long _bytesTotal = bytesTotal;

                    float percentage =  ((float)_bytesCurrent /(float)_bytesTotal * 100);
                    Log.d("percentage","" +percentage);
                }

                @Override
                public void onError(int id, Exception ex) {
                    Log.d("Error de Amazon",ex.getMessage());
                }
            });

        }catch (Exception e){
            Log.e("Error",e.getMessage());
        }
    }

}
