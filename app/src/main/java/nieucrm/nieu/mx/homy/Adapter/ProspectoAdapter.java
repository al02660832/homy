package nieucrm.nieu.mx.skola.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import nieucrm.nieu.mx.skola.DataModel.ProspectoData;
import nieucrm.nieu.mx.skola.Fragments.NewProspectoFragment;
import nieucrm.nieu.mx.skola.Fragments.ProspectoWebFragment;
import nieucrm.nieu.mx.skola.R;


/**
 * Created by Carlos Romero on 14/03/2017.
 */

public class ProspectoAdapter extends BaseAdapter {

    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf",avenirDemibold="font/avenir-next-demi-bold.ttf";



    private Context context;
    LayoutInflater inflater;
    private List<ProspectoData> prospectoData=null;
    private ArrayList<ProspectoData> array=null;
    Fragment fragment;
    boolean web;

    public ProspectoAdapter(Context context, List<ProspectoData> prospectoData) {
        this.context = context;
        this.prospectoData=prospectoData;
        try {
            inflater=LayoutInflater.from(context);

        }catch (Exception e){
            Log.d("Error",e.getMessage());
        }
        this.array=new ArrayList<>();
        this.array.addAll(prospectoData);
    }

    public class ViewHolder{
        TextView nombreProspecto,seguimientoProspecto,tipoProspecto;
    }


    @Override
    public int getCount() {
        return prospectoData.size();
    }

    @Override
    public Object getItem(int position) {
        return prospectoData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Typeface bold=Typeface.createFromAsset(context.getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(context.getAssets(),avenirMedium);
        Typeface demibold=Typeface.createFromAsset(context.getAssets(),avenirDemibold);

        ViewHolder holder;
        // TextView nombreEstudiante=(TextView)v.findViewById(R.id.nombreEstudiante);

        if(convertView==null){
            holder=new ViewHolder();
            convertView=inflater.inflate(R.layout.prospecto_item_layout,null);

            holder.nombreProspecto=(TextView)convertView.findViewById(R.id.nombreProspecto);
            holder.seguimientoProspecto=(TextView)convertView.findViewById(R.id.seguimientoProspecto);
            holder.tipoProspecto=(TextView)convertView.findViewById(R.id.tipoProspecto);

            holder.nombreProspecto.setTypeface(medium);
            holder.seguimientoProspecto.setTypeface(medium);
            holder.tipoProspecto.setTypeface(medium);

            convertView.setTag(holder);
        }else {
            holder=(ViewHolder)convertView.getTag();
        }


        if (prospectoData.get(position).isWeb()){
            holder.tipoProspecto.setText("Web -");
            holder.nombreProspecto.setText(prospectoData.get(position).getNombre());
            holder.seguimientoProspecto.setVisibility(View.GONE);
        }
        else {
            holder.tipoProspecto.setText("App -");
            holder.nombreProspecto.setText(prospectoData.get(position).getEstudianteNombre()+" "+prospectoData.get(position).getEstudianteApellidos());
            holder.seguimientoProspecto.setText(prospectoData.get(position).getSeguimiento());
        }


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle b = new Bundle();
                b.putString("objectId",prospectoData.get(position).getObjectId());


                if (prospectoData.get(position).isWeb()){
                    fragment=new ProspectoWebFragment();

                    b.putString("fechaCreacion",prospectoData.get(position).getFechaCreacion());
                    b.putString("horaCreacion",prospectoData.get(position).getHoraCreacion());
                    b.putString("nombre",prospectoData.get(position).getNombre());
                    b.putString("edad",prospectoData.get(position).getEdad());
                    b.putString("ingreso",prospectoData.get(position).getFechaIngreso());
                    b.putString("pregunta",prospectoData.get(position).getPreguntas());
                    b.putString("telefono",prospectoData.get(position).getTelefono());
                    b.putString("mail",prospectoData.get(position).getMail());
                    Log.e("Fragment","Web");
                }else {

                    fragment=new NewProspectoFragment();

                        b.putString("estudianteNombre", prospectoData.get(position).getEstudianteNombre());
                        b.putString("estudianteApellidos",prospectoData.get(position).getEstudianteApellidos());
                        b.putString("seguimiento",prospectoData.get(position).getSeguimiento());
                        b.putString("captura",prospectoData.get(position).getCaptura());
                        b.putSerializable("estudianteFechaNacimiento",prospectoData.get(position).getEstudianteFechaNacimiento());
                        b.putString("fechaNacimiento",prospectoData.get(position).getFechaNacimiento());
                        b.putString("mamaEmail",prospectoData.get(position).getMamaEmail());
                        b.putString("mamaNombre",prospectoData.get(position).getMamaNombre());
                        b.putString("mamaApellidos",prospectoData.get(position).getMamaApellidos());
                        b.putString("mamaTelefono",prospectoData.get(position).getMamaTelefono());
                        b.putString("papaNombre",prospectoData.get(position).getPapaNombre());
                        b.putString("papaApellidos",prospectoData.get(position).getPapaApellidos());
                        b.putString("papaEmail",prospectoData.get(position).getPapaEmail());
                        b.putString("papaTelefono",prospectoData.get(position).getPapaTelefono());
                        b.putString("promocion",prospectoData.get(position).getPromocion());
                    Log.e("Fragment","App");

                }

                fragment.setArguments(b);
                FragmentTransaction transaction= ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
            }
        });

        return convertView;
    }





}

