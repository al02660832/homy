package nieucrm.nieu.mx.skola.Fragments;


import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;

/**
 * Created by Carlos Romero
 */

public class GroupProfileFragment extends Fragment {

    String groupId;

    ProgressBar progressBar;
    ArrayList<String>estud;
    List<ParseObject>oEstudiante;
    ListView listaEstudiantes;
    ArrayAdapter<String> adapter;
    TextView nombreEs;
    Fragment fragment;
    Toolbar toolbar;
    String nombre;
    ImageButton imagenAnuncio,imagenTarea,imagenActividad,imagenMomentos;
    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf",demibold="font/avenir-next-demi-bold.ttf";

    String escuelaId;
    ParseObject escuela;

    Activity a;

    public GroupProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            a=(Activity) context;
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        Bundle b=this.getArguments();
        groupId=b.getString("grupoId");
        nombre=b.getString("name");

        return inflater.inflate(R.layout.fragment_group_profile, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listaEstudiantes=(ListView)view.findViewById(R.id.listaAlumnos);



        imagenTarea=(ImageButton)view.findViewById(R.id.imagenTarea);
        imagenAnuncio=(ImageButton)view.findViewById(R.id.imagenAnuncio);
        imagenActividad=(ImageButton)view.findViewById(R.id.imagenActividad);
        imagenMomentos=(ImageButton)view.findViewById(R.id.imagenMomentos);

        progressBar=(ProgressBar)view.findViewById(R.id.progressGP);

        nombreEs = (TextView) a.findViewById(R.id.textAction);
        toolbar=(Toolbar)a.findViewById(R.id.toolbar);
        nombreEs.setText("");
        nombreEs.setText(nombre);



        setTypeface();

        getEstudiantes();


        imagenMomentos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment=new MomentosFragment();
                Bundle b=new Bundle();
                b.putString("grupoId",groupId);
                b.putString("grupoNombre",nombre);
                fragment.setArguments(b);
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.slide_up, R.anim.slide_out_down,R.anim.slide_down, R.anim.slide_out_up);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
            }
        });

        imagenTarea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment=new HomeworkFragment();
                Bundle bundle=new Bundle();
                bundle.putString("grupoId",groupId);
                bundle.putString("grupoNombre",nombre);
                fragment.setArguments(bundle);
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.slide_up, R.anim.slide_out_down,R.anim.slide_down, R.anim.slide_out_up);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();

            }
        });

        imagenAnuncio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment=new AnnouncementFragment();
                Bundle bundle=new Bundle();
                bundle.putString("grupoId",groupId);
                bundle.putString("grupoNombre",nombre);
                fragment.setArguments(bundle);
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.slide_up, R.anim.slide_out_down,R.anim.slide_down, R.anim.slide_out_up);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();

            }
        });

        imagenActividad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment=new
                        ActivityFragment();
                Bundle bundle=new Bundle();
                bundle.putString("grupoNombre",nombre);
                bundle.putString("grupoId",groupId);
                fragment.setArguments(bundle);
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.slide_up, R.anim.slide_out_down,R.anim.slide_down, R.anim.slide_out_up);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
            }
        });
    }

    public void setTypeface(){
        Typeface bold=Typeface.createFromAsset(a.getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(a.getAssets(),avenirMedium);
        Typeface dem=Typeface.createFromAsset(a.getAssets(),demibold);



    }

    private void getEstudiantes(){
        estud=new ArrayList<>();
        escuelaId=((ParseApplication)a.getApplication()).getEscuelaId();
        escuela=ParseObject.createWithoutData("Escuela",escuelaId);
        ParseObject o=ParseObject.createWithoutData("grupo",groupId);
        ParseQuery<ParseObject>query=ParseQuery.getQuery("Estudiantes").whereEqualTo("grupo",o);
        query.whereEqualTo("status",0);
        query.whereEqualTo("escuela",escuela);

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                oEstudiante=objects;
                progressBar.setVisibility(View.GONE);
                listaEstudiantes.setVisibility(View.VISIBLE);
                for (ParseObject es:objects){
                    estud.add(es.getString("NOMBRE")+" "+es.getString("APELLIDO"));
                }
                adapter=new ArrayAdapter<>(a,android.R.layout.simple_list_item_1,android.R.id.text1,estud);
                listaEstudiantes.setAdapter(adapter);
            }
        });

        listaEstudiantes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle= new Bundle();
                bundle.putString("generoEstudiante",oEstudiante.get(position).getString("GENERO"));
                bundle.putString("nombre",oEstudiante.get(position).getString("NOMBRE"));
                bundle.putString("estudianteId",oEstudiante.get(position).getObjectId());
                bundle.putString("grupoId",groupId);
                fragment=new PerfilEstudianteFragment();
                fragment.setArguments(bundle);
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
            }
        });
    }

}
