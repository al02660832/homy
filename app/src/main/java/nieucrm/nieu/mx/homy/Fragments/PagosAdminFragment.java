package nieucrm.nieu.mx.skola.Fragments;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import nieucrm.nieu.mx.skola.Adapter.PagoAdminAdapter;
import nieucrm.nieu.mx.skola.DataModel.PagosData;
import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;

/**
 * Created by Carlos Romero
 */
public class PagosAdminFragment extends Fragment {

    TextView txt3, adelantadoTag, txt4;
    ParseQuery<ParseObject> query, queryPendiente, queryAdelantado;
    ListView listaPagado, listaPendiente, listaAdelantado;
    ArrayAdapter<String> g, p;
    List<ParseObject> pen;
    Calendar i, f;
    ArrayList<String> pago, pendiente, adelanto;
    Date inicio, fin,adelan;
    ProgressDialog dialog;
    ArrayList<String> alreadyPaid;
    Bundle b;
    ArrayList<ParseObject> pagado, pend, adelantado;
    ParseObject estudiante;
    String studentId, fechaCloud;
    HashMap<String, Object> pe, id;

    ArrayList<ParseObject> estudiantes;
    ParseObject mensaje;
    String pagoId;

    String avenirBold = "font/avenir-next-bold.ttf", avenirMedium = "font/avenir-next-medium.ttf", demibold = "font/avenir-next-demi-bold.ttf";

    String mesPago;
    Date fechaPa;


    ParseObject pagos;

    String escuelaId;

    CharSequence[] items = {"Mandar mensaje", "Confirmar pago de recibido", "Pago adelantado",
            "Cancelar"};

    int []in={1,2,3,4,5,6};
    String []num={"1","2","3","4","5","6"};
    ArrayList<String>valor;

    int mes;

    Fragment fragment;
    String nombre, apellido, concepto, fileUri, nombreCompleto, ids;

    PagosData data;
    ArrayList<PagosData>listPago;

    public PagosAdminFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        setHasOptionsMenu(true);

        return inflater.inflate(R.layout.fragment_pagos_admin, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        listaPagado = (ListView) view.findViewById(R.id.listaPagado);
        listaPendiente = (ListView) view.findViewById(R.id.listaPendiente);
        listaAdelantado = (ListView) view.findViewById(R.id.listaAdelantado);

        txt3 = (TextView) view.findViewById(R.id.textView3);
        txt4 = (TextView) view.findViewById(R.id.textView4);
        adelantadoTag = (TextView) view.findViewById(R.id.adelantadoTag);

        pago = new ArrayList<>();
        pendiente = new ArrayList<>();
        alreadyPaid = new ArrayList<>();
        pagado = new ArrayList<>();



        //Obtener fecha
        //Inicio de mes
        i = Calendar.getInstance();
        i.set(Calendar.DAY_OF_MONTH, 1);
        inicio = i.getTime();
        //Fin de mes
        f = Calendar.getInstance();
        f.set(Calendar.DAY_OF_MONTH, f.getActualMaximum(Calendar.DAY_OF_MONTH));
        fin = f.getTime();

        bindTypeface();

        dialog = new ProgressDialog(getContext());
        dialog.setTitle("Cargando");
        dialog.setMessage("Por favor espere...");
        dialog.setCancelable(false);
        dialog.show();

        //setAdelantado();
        getData();

    }

    public void getData() {
        listPago=new ArrayList<>();
        query = ParseQuery.getQuery("pagos");

        query.whereGreaterThanOrEqualTo("createdAt", inicio);
        query.whereLessThan("createdAt", fin);
        query.include("student");
        query.include("user");
        query.setLimit(250);


        query.include("student.grupo");
        query.include("user.escuela");

        escuelaId=((ParseApplication)getActivity().getApplication()).getEscuelaId();


        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                dialog.hide();
                if (e == null) {

                    for (ParseObject o : objects) {
                        data=new PagosData();
                        ParseObject userObject=o.getParseObject("user");
                        estudiante = o.getParseObject("student");
                        ParseObject gru=estudiante.getParseObject("grupo");


                        if (userObject==null){

                        }else {
                            if (userObject.getParseObject("escuela").getObjectId().equals(escuelaId)){
                                if (estudiante == null) {

                                } else {
                                    data.setAlumno(estudiante.getString("NOMBRE") + " " + estudiante.getString("ApPATERNO"));
                                    data.setGrupo(gru.getString("grupoId"));
                                    data.setConcepto(o.getString("concepto"));
                                    data.setObjectId(o.getObjectId());
                                    if (o.getParseFile("photo")==null){
                                        if (o.getBoolean("manual")){
                                            data.setFileUri("M");
                                        }else {
                                            if (o.getBoolean("aws")){
                                                data.setFileUri("A");
                                            }else {
                                                data.setFileUri("N");
                                            }
                                        }
                                    }else {
                                        data.setFileUri(o.getParseFile("photo").getUrl());
                                    }

                                    if (o.getDate("fechaAdelanto")==null){
                                        data.setMesPago(o.getCreatedAt());
                                    }else {
                                        data.setMesPago(o.getDate("fechaAdelanto"));
                                    }

                                    DateFormat f = new SimpleDateFormat("dd/MMM", new Locale("es"));

                                    data.setFecha(f.format(o.getCreatedAt()));

                                    DateFormat df=new SimpleDateFormat("HH:mm",new Locale("es"));
                                    df.setTimeZone(TimeZone.getTimeZone("PNT"));
                                    data.setHora(df.format(o.getCreatedAt()));
                                    //pago.add(estudiante.getString("NOMBRE") + " " + estudiante.getString("ApPATERNO"));
                                    nombre = estudiante.getString("NOMBRE");
                                    listPago.add(data);
                                    //alreadyPaid.add(estudiante.getObjectId());
                                    //pagado.add(o);
                                }
                            }
                        }



                    }
                    PagoAdminAdapter a=new PagoAdminAdapter(getActivity(),listPago);
                    listaPagado.setAdapter(a);
                    //g = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, android.R.id.text1, pago);
                    //listaPagado.setAdapter(g);
                    //setAdelantado();
                    setPendiente();
                } else {
                    Toast.makeText(getContext(), "Error " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        /*
        listaPagado.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                fragment = new PagosDetailFragment();

                estudiante = pagado.get(position).getParseObject("student");
                nombre = estudiante.getString("NOMBRE") + " ";
                apellido = estudiante.getString("ApPATERNO");
                nombreCompleto = nombre + apellido;
                ids = pagado.get(position).getObjectId();
                concepto = pagado.get(position).getString("concepto");
                if (pagado.get(position).getParseFile("photo") == null) {

                    if (pagado.get(position).getBoolean("manual")) {
                        fileUri = "M";
                    } else {
                        if (pagado.get(position).getBoolean("aws")){
                            fileUri="A";
                        }else {
                            fileUri="N";
                        }
                    }

                } else {
                    fileUri = pagado.get(position).getParseFile("photo").getUrl();
                }

                if (pagado.get(position).getDate("fechaAdelanto")==null){
                    fechaPa=pagado.get(position).getCreatedAt();
                }else {
                    fechaPa=pagado.get(position).getDate("fechaAdelanto");
                }

                SimpleDateFormat format = new SimpleDateFormat("MMMM",new Locale("es"));
                mesPago=format.format(fechaPa);


                b = new Bundle();
                b.putString("nombreAlumno", nombreCompleto);
                b.putString("conceptoPago", concepto);
                b.putString("parseImage", fileUri);
                b.putString("oId", ids);
                b.putString("mes",mesPago);
                fragment.setArguments(b);

                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home, fragment, "fragment").addToBackStack(null).commit();

            }
        });
        */

    }

    /*
    public void setAdelantado() {

        Calendar d=Calendar.getInstance();
        d.add(Calendar.MONTH,6);
        adelan=d.getTime();
        adelantado = new ArrayList<>();
        adelanto = new ArrayList<>();
        queryAdelantado = ParseQuery.getQuery("pagos");
        queryAdelantado.whereLessThanOrEqualTo("fechaAdelanto", adelan);
        //query.whereLessThan("createdAt", fin);
        queryAdelantado.include("student");
        queryAdelantado.include("user");
        queryAdelantado.include("user.escuela");

        escuelaId=((ParseApplication)getActivity().getApplication()).getEscuelaId();


        queryAdelantado.include("student.grupo");
        queryAdelantado.setLimit(250);
        queryAdelantado.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    for (ParseObject o : objects) {

                        ParseObject padreObject=o.getParseObject("user");

                        if (padreObject==null){
                            Log.e("Error","Sin padre");
                        }else {
                          if (padreObject.getParseObject("escuela").getObjectId().equals(escuelaId)){
                              estudiante = o.getParseObject("student");
                              if (estudiante == null) {

                              } else {
                                  pago.add(estudiante.getString("NOMBRE") + " " + estudiante.getString("ApPATERNO"));
                                  nombre = estudiante.getString("NOMBRE");
                                  alreadyPaid.add(estudiante.getObjectId());
                                  pagado.add(o);
                              }
                          } else {
                              Log.e("Escuela","No pertenece");
                          }


                        }



                    }
                    getData();
                } else {
                    getData();
                }
            }
        });

        listaAdelantado.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                fragment = new PagosDetailFragment();

                estudiante = adelantado.get(position).getParseObject("student");
                nombre = estudiante.getString("NOMBRE") + " ";
                apellido = estudiante.getString("ApPATERNO");
                nombreCompleto = nombre + apellido;
                ids = adelantado.get(position).getObjectId();
                concepto = adelantado.get(position).getString("concepto");
                if (adelantado.get(position).getParseFile("photo") == null) {

                    if (adelantado.get(position).getBoolean("manual")) {
                        fileUri = "M";
                    } else {
                        fileUri = "N";
                    }

                } else {
                    fileUri = adelantado.get(position).getParseFile("photo").getUrl();
                }

                if (adelantado.get(position).getDate("fechaAdelanto")==null){
                    fechaPa=adelantado.get(position).getCreatedAt();
                }else {
                    fechaPa=adelantado.get(position).getDate("fechaAdelanto");
                }

                SimpleDateFormat format = new SimpleDateFormat("MMMM",new Locale("es"));
                mesPago=format.format(fechaPa);

                b = new Bundle();
                b.putString("nombreAlumno", nombreCompleto);
                b.putString("conceptoPago", concepto);
                b.putString("parseImage", fileUri);
                b.putString("oId", ids);
                b.putString("mes",mesPago);
                fragment.setArguments(b);
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home, fragment, "fragment").addToBackStack(null).commit();

            }
        });
    }
*/
    public void setPendiente() {
        query = ParseQuery.getQuery("pagos");
        estudiantes = new ArrayList<>();
        query.whereGreaterThanOrEqualTo("createdAt", inicio);
        query.whereLessThan("createdAt", fin);
        query.include("student");
        query.include("user");
        query.setLimit(250);
        //Incluye grupo del include student
        query.include("student.grupo");

        query.include("user.escuela");

        escuelaId=((ParseApplication)getActivity().getApplication()).getEscuelaId();
        ParseObject escuela=ParseObject.createWithoutData("Escuela",escuelaId);


        queryPendiente = ParseQuery.getQuery("Estudiantes").include("PersonasAutorizadas");
        queryPendiente.whereEqualTo("status", 0);
        queryPendiente.whereEqualTo("escuela",escuela);
        queryPendiente.setLimit(250);
        queryPendiente.whereNotContainedIn("objectId", alreadyPaid);

        queryPendiente.setLimit(250);
        pend = new ArrayList<>();

        queryPendiente.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                dialog.hide();

                if (e == null) {
                    for (ParseObject b : objects) {
                        pendiente.add(b.getString("NOMBRE") + " " + b.getString("ApPATERNO"));
                        pen = objects;
                        estudiantes.add(b);
                    }
                    p = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, android.R.id.text1, pendiente);
                    listaPendiente.setAdapter(p);
                } else {
                    Toast.makeText(getContext(), "Error ", Toast.LENGTH_SHORT).show();

                }
            }
        });

        listaPendiente.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                //Toast.makeText(getContext(),pen.get(position).getString("NOMBRE"),Toast.LENGTH_SHORT).show();

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Pago pendiente");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (items[item].equals("Mandar mensaje")) {
                            studentId = pen.get(position).getObjectId();
                            nombre = pen.get(position).getString("NOMBRE");
                            messagePagos();
                        } else if (items[item].equals("Confirmar pago de recibido")) {
                            //Pago manual
                            studentId = pen.get(position).getObjectId();
                            cloudCall();

                        } else if (items[item].equals("Pago adelantado")) {
                            studentId = pen.get(position).getObjectId();

                            dialog.dismiss();
                            setMeses();

                        } else if (items[item].equals("Cancelar")) {
                            dialog.dismiss();
                        }
                    }
                });

                builder.show();


            }
        });


    }

    public void cloudCall() {
        escuelaId=((ParseApplication)getActivity().getApplication()).getEscuelaId();

        pe = new HashMap<>();
        id = new HashMap<>();
        //Obtener el mes en español
        SimpleDateFormat formateador = new SimpleDateFormat(
                "MMMM yyyy", new Locale("ES"));

        Calendar cal = Calendar.getInstance();

        cal.setTimeZone(TimeZone.getDefault());

        fechaCloud = formateador.format(new Date());
        //
        pe.put("monthYear", fechaCloud);
        pe.put("escuelaObjId",escuelaId);


        pagos = new ParseObject("pagos");
        ParseObject studentObject = ParseObject.createWithoutData("Estudiantes", studentId);
        pagos.put("manual", true);
        pagos.put("student", studentObject);
        pagos.put("concepto", "Colegiatura");
        pagos.put("user", ParseUser.getCurrentUser());
        pagos.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    pagoId = pagos.getObjectId();
                    ParseCloud.callFunctionInBackground("updatePagosCount", pe, new FunctionCallback<Object>() {
                        @Override
                        public void done(Object object, ParseException e) {
                            if (e == null) {
                                Log.d("CloudcodePagos", "Success");
                            } else {
                                Log.d("Error", e.getMessage());
                            }
                        }
                    });

                    id.put("pagoId", pagoId);
                    id.put("escuelaObjId",escuelaId);

                    ParseCloud.callFunctionInBackground("pagoManualNotification", id, new FunctionCallback<Object>() {
                        @Override
                        public void done(Object object, ParseException e) {
                            if (e == null) {
                                Log.d("CloudcodePagos", "Success");
                            } else {
                                Log.d("Error", e.getMessage());
                            }
                        }
                    });
                }
            }
        });
    }

    public void setMeses(){

        // Set the dialog title

        valor=new ArrayList<>();

        for (String c:num){
            valor.add(c);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // Set the dialog title

        builder.setTitle("Meses pagados: ")

                // Specify the list array, the items to be selected by default (null for none),

                // and the listener through which to receive callbacks when items are selected

                .setItems(num, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mes=Integer.valueOf(valor.get(which));
                        setAdelante(mes);
                    }

                    // Set the action buttons
                }).create().show();

    }

    public void setAdelante(int m){

        Calendar despues=Calendar.getInstance();
        despues.add(Calendar.MONTH,m);
        Date adelanto=despues.getTime();

        pagos = new ParseObject("pagos");
        ParseObject studentObject = ParseObject.createWithoutData("Estudiantes", studentId);
        pagos.put("manual", true);
        pagos.put("student", studentObject);
        pagos.put("concepto", "Colegiatura");
        pagos.put("fechaAdelanto",adelanto);
        pagos.put("user", ParseUser.getCurrentUser());
        pagos.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                   Toast.makeText(getContext(),"Pago confirmado",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void messagePagos(){
        Fragment fragment = new NewMessageFragment();
        Bundle bundle=new Bundle();
        bundle.putString("seccion","ap");
        bundle.putString("tipo","eJZIOPloQH");
        bundle.putString("idEstudiante",studentId);
        bundle.putString("nombre",nombre);
        fragment.setArguments(bundle);
        FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
        transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
    }

    public void mensajePendientes() {

        mensaje=new ParseObject("anuncio");
        Fragment fragment = new NewMessageFragment();
        Bundle bundle=new Bundle();
        bundle.putString("seccion","mp");
        bundle.putString("tipo","eJZIOPloQH");
        bundle.putStringArrayList("arrayEstudiantes",alreadyPaid);
        bundle.putString("nombre",nombre);
        fragment.setArguments(bundle);
        FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
        transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.new_message,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id=item.getItemId();
        if(id==R.id.write_mess){
            mensajePendientes();
        }


        return super.onOptionsItemSelected(item);

    }

    public void bindTypeface(){
        Typeface bold=Typeface.createFromAsset(getContext().getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(getContext().getAssets(),avenirMedium);
        Typeface dem=Typeface.createFromAsset(getContext().getAssets(),demibold);

        txt4.setTypeface(dem);
        txt3.setTypeface(dem);
        adelantadoTag.setTypeface(dem);

    }

}
