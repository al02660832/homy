package nieucrm.nieu.mx.skola.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class TallerFragment extends Fragment {

    ListView listaTaller;
    ArrayList<String>taller;
    ArrayAdapter<String>a;
    List<ParseObject>list;
    String escuela;
    Fragment fragment;

    Bundle b;
    FragmentTransaction transaction;

    public TallerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_taller, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        listaTaller=(ListView)view.findViewById(R.id.listaTaller);
        taller=new ArrayList<>();
        escuela=((ParseApplication)getActivity().getApplication()).getEscuelaId();

        ParseObject e=ParseObject.createWithoutData("Escuela",escuela);
        ParseQuery<ParseObject> q=ParseQuery.getQuery("Taller").include("escuela");
        q.whereEqualTo("escuela",e);
        q.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e==null){
                    for (ParseObject o:objects){
                        taller.add(o.getString("nombre"));
                    }

                    try {
                        a=new ArrayAdapter<String>(getContext(),android.R.layout.simple_list_item_1,android.R.id.text1,taller);
                    }catch (Exception ex){
                        ex.printStackTrace();
                    }

                    listaTaller.setAdapter(a);
                }else {
                    Log.e("Error","No se encontraron talleres");
                }
            }
        });

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.crear_taller,menu);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.crearTall:
                fragment=new NewTallerFragment();
                b=new Bundle();
                //b.putString("num",String.valueOf(array.size()+1));
                //fragment.setArguments(b);
                transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                break;
        }
        return super.onOptionsItemSelected(item);

    }
}
