package nieucrm.nieu.mx.skola.Fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import nieucrm.nieu.mx.skola.Activity.Home;
import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class VisitantesQRFragment extends Fragment {

    String avenirBold = "font/avenir-next-bold.ttf", avenirMedium = "font/avenir-next-medium.ttf",avenirDemi="font/avenir-next-demi-bold.ttf",nanum="font/NanumPenScript-Regular.ttf";

    String userName;
    String parentesco;
    int usertype;
    ImageButton escanearVisitante,solicitar;
    TextView escaneoTag,solicitarTag,txtNo,txtPrueba,skolaQr,link;

    public VisitantesQRFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_visitantes_qr, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        solicitar=(ImageButton)view.findViewById(R.id.solicitar);
        escanearVisitante=(ImageButton)view.findViewById(R.id.escanearVisitante);

        escaneoTag=(TextView)view.findViewById(R.id.escaneoTag);
        solicitarTag=(TextView)view.findViewById(R.id.solicitarTag);
        txtNo=(TextView)view.findViewById(R.id.txtNo);
        txtPrueba=(TextView)view.findViewById(R.id.txtPrueba);
        skolaQr=(TextView)view.findViewById(R.id.skolaQr);

        link=(TextView)view.findViewById(R.id.link);

        link.setText("http://skola.nieu.mx/");

        setType();

        link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url ="http://skola.nieu.mx/" ;
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        solicitar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sentMail();
            }
        });

        escanearVisitante.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scanCredencial();
            }
        });


    }

    public void setType(){
        Typeface bold = Typeface.createFromAsset(getContext().getAssets(), avenirBold);
        Typeface medium = Typeface.createFromAsset(getContext().getAssets(), avenirMedium);
        Typeface demi=Typeface.createFromAsset(getContext().getAssets(),avenirDemi);
        Typeface nan=Typeface.createFromAsset(getContext().getAssets(),nanum);

        escaneoTag.setTypeface(demi);
        solicitarTag.setTypeface(demi);
        txtNo.setTypeface(medium);
        txtPrueba.setTypeface(nan);
        skolaQr.setTypeface(medium);
        link.setTypeface(medium);


    }

    public void sentMail(){

        Intent i = new Intent(Intent.ACTION_SENDTO);
        i.setType("message/rfc822");
        i.setData(Uri.parse("mailto:"));
        i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"rafagarnica@nieu.com"});
        i.putExtra(Intent.EXTRA_SUBJECT, "Solicitud de código QR");
        i.putExtra(Intent.EXTRA_TEXT   , "Hola, ¡me interesa esta app!");
        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getContext(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }

    }


    public void scanCredencial(){

        IntentIntegrator scanIntegrator=IntentIntegrator.forSupportFragment(VisitantesQRFragment.this);
        scanIntegrator.setPrompt("Escanee su creedencial");

        scanIntegrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
        scanIntegrator.setCameraId(0);
        scanIntegrator.setBeepEnabled(true);
        scanIntegrator.setBarcodeImageEnabled(true);
        scanIntegrator.initiateScan();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        IntentResult result=IntentIntegrator.parseActivityResult(requestCode,resultCode,data);
        if(result!=null){
            String scannedCode=result.getContents();
            if(scannedCode!=null && scannedCode.length()>=10){

                beginLoginWithQR(scannedCode);

            }
            else {
                Toast.makeText(getContext(),"Error al escanear QR",Toast.LENGTH_SHORT).show();

            }
        }
        else  {
            super.onActivityResult(requestCode, resultCode, data);
        }



    }

    private void beginLoginWithQR(String qr){

        final ProgressDialog progressDialog=new ProgressDialog(getContext());

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressDialog.setTitle("Iniciando sesión");
                progressDialog.setMessage("Por favor espere");
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
        });


        String id=qr.substring(0,10);

        ParseQuery<ParseUser> userQuery=ParseUser.getQuery();
        userQuery.whereEqualTo("objectId",id);
        userQuery.getFirstInBackground(new GetCallback<ParseUser>() {
            @Override
            public void done(ParseUser object, ParseException e) {
                progressDialog.hide();
                if(e==null) {
                    parentesco = object.getString("parentesco");
                    if (parentesco.equals("Mamá") || parentesco.equals("Papá")) {
                        userName = object.getUsername();
                    }else {
                        userName = "";

                    }


                    ParseUser.logInInBackground(userName,"Angel550",new LogInCallback(){
                        @Override
                        public void done(ParseUser user, ParseException e) {

                            if(user!=null){

                                usertype=ParseUser.getCurrentUser().getNumber("usertype").intValue();
                                getActivity().finish();
                                Intent intent=new Intent(getContext(),Home.class);
                                startActivity(intent);
                                Toast.makeText(getContext(),"Registro exitoso",Toast.LENGTH_LONG).show();

                            }else{

                                Toast.makeText(getContext(),"Error al registrarse",Toast.LENGTH_LONG).show();

                            }
                        }
                    });

                }else{
                    Toast.makeText(getContext(),"Error",Toast.LENGTH_SHORT).show();

                }
            }
        });
    }
}
