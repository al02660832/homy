package nieucrm.nieu.mx.skola.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import nieucrm.nieu.mx.skola.DataModel.AnuncioData;
import nieucrm.nieu.mx.skola.Fragments.AnuncioDetailFragment;
import nieucrm.nieu.mx.skola.R;


/**
 * Created by Carlos Romero on 26/12/2016.
 */

public class AnuncioAdapter extends BaseAdapter {

    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf",avenirDemibold="font/avenir-next-demi-bold.ttf";



    private Context context;
    LayoutInflater inflater;
    String obId;
    private List<AnuncioData> anuncioData=null;
    private ArrayList<AnuncioData> array=null;

    public AnuncioAdapter(Context context, List<AnuncioData> anuncioData) {
        this.context = context;
        this.anuncioData=anuncioData;
        try {
            inflater=LayoutInflater.from(context);

        }catch (Exception e){
            Log.d("Error",e.getMessage());
        }
        this.array=new ArrayList<>();
        this.array.addAll(anuncioData);
    }

    public class ViewHolder{
        TextView tipo;
        TextView fecha;
        TextView materia;
        TextView destinatario;
        TextView para;
        ImageView imagenClip;
        ImageView imagenNuevo;

        //TextView txtDesayuno,desayunoTag,txtColac,colacTag,txtComida,comidaTag,txtMerienda,meriendaTag,txtDescanso,txtDurmio,durmioTag,txtTiempo,tiempoTag,
        //funcionesTag,txtAviso,avisoTag,txtPipi,pipiTag,txtPopo,popoTag;

        //LinearLayout momentsLayout,layoutDesa,layoutColac,layoutComi,layoutMeri,layoutDurm,layoutTime,layoutAviso,layoutPip,layoutPop,layoutMensaje;
    }


    @Override
    public int getCount() {
        return anuncioData.size();
    }

    @Override
    public Object getItem(int position) {
        return anuncioData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    ////////////////////////////////////////////////
    ///Evita que las celdas sean recicladas
    /*
    @Override
    public int getViewTypeCount() {

        int count;
        if (anuncioData.size()>0){
            count=getCount();
        }else {
            count=1;
        }
        return count;
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }
    */

    ///////////////////////////////////////////////

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Typeface bold=Typeface.createFromAsset(context.getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(context.getAssets(),avenirMedium);
        Typeface demibold=Typeface.createFromAsset(context.getAssets(),avenirDemibold);

        ViewHolder holder;
        // TextView nombreEstudiante=(TextView)v.findViewById(R.id.nombreEstudiante);

        if(convertView==null){
            holder=new ViewHolder();
            convertView=inflater.inflate(R.layout.libreta_item_layout,null);
            holder.tipo=(TextView)convertView.findViewById(R.id.txtTipo);
            holder.fecha=(TextView)convertView.findViewById(R.id.fechaAnuncio);
            holder.destinatario=(TextView)convertView.findViewById(R.id.nombreRemitente);
            holder.materia=(TextView)convertView.findViewById(R.id.txtMateria);
            holder.para=(TextView)convertView.findViewById(R.id.txtRemitente);

            /*
            holder.txtDesayuno=(TextView)convertView.findViewById(R.id.txtDesayuno);
            holder.desayunoTag=(TextView)convertView.findViewById(R.id.desayunoTag);
            holder.txtColac=(TextView)convertView.findViewById(R.id.txtColac);
            holder.colacTag=(TextView)convertView.findViewById(R.id.colacTag);
            holder.txtComida=(TextView)convertView.findViewById(R.id.txtComida);
            holder.comidaTag=(TextView)convertView.findViewById(R.id.comidaTag);
            holder.txtMerienda=(TextView)convertView.findViewById(R.id.txtMerienda);
            holder.meriendaTag=(TextView)convertView.findViewById(R.id.meriendaTag);
            holder.txtDescanso=(TextView)convertView.findViewById(R.id.txtDescanso);
            holder.txtDurmio=(TextView)convertView.findViewById(R.id.txtDurmio);
            holder.durmioTag=(TextView)convertView.findViewById(R.id.durmioTag);
            holder.txtTiempo=(TextView)convertView.findViewById(R.id.txtTiempo);
            holder.tiempoTag=(TextView)convertView.findViewById(R.id.tiempoTag);
            holder.funcionesTag=(TextView)convertView.findViewById(R.id.funcionesTag);
            holder.txtAviso=(TextView)convertView.findViewById(R.id.txtAviso);
            holder.avisoTag=(TextView)convertView.findViewById(R.id.avisoTag);
            holder.txtPipi=(TextView)convertView.findViewById(R.id.txtPipi);
            holder.pipiTag=(TextView)convertView.findViewById(R.id.pipiTag);
            holder.txtPopo=(TextView)convertView.findViewById(R.id.txtPopo);
            holder.popoTag=(TextView)convertView.findViewById(R.id.popoTag);

            holder.momentsLayout=(LinearLayout)convertView.findViewById(R.id.momentsLayout);
            holder.layoutDesa=(LinearLayout)convertView.findViewById(R.id.layoutDesa);
            holder.layoutColac=(LinearLayout)convertView.findViewById(R.id.layoutColac);
            holder.layoutComi=(LinearLayout)convertView.findViewById(R.id.layoutComi);
            holder.layoutMeri=(LinearLayout)convertView.findViewById(R.id.layoutMeri);
            holder.layoutDurm=(LinearLayout)convertView.findViewById(R.id.layoutDurm);
            holder.layoutTime=(LinearLayout)convertView.findViewById(R.id.layoutTime);
            holder.layoutAviso=(LinearLayout)convertView.findViewById(R.id.layoutAviso);
            holder.layoutPip=(LinearLayout)convertView.findViewById(R.id.layoutPip);
            holder.layoutPop=(LinearLayout)convertView.findViewById(R.id.layoutPop);
            holder.layoutMensaje=(LinearLayout)convertView.findViewById(R.id.layoutMensaje);
            */

            holder.imagenClip=(ImageView)convertView.findViewById(R.id.imagenClip);
            //holder.imagenNuevo=(ImageView)convertView.findViewById(R.id.imagenNuevo);


            holder.para.setTypeface(medium);
            holder.tipo.setTypeface(demibold);
            holder.fecha.setTypeface(medium);
            holder.materia.setTypeface(medium);
            holder.destinatario.setTypeface(medium);
            convertView.setTag(holder);
        }else {
            holder=(ViewHolder)convertView.getTag();
        }

        String dia=(String)DateFormat.format("dd", anuncioData.get(position).getFechaE());
        String mese = (String) DateFormat.format("MMM", anuncioData.get(position).getFechaE());

        anuncioData.get(position).setFechaEntrega(dia+" "+mese);

        String day = (String) DateFormat.format("dd", anuncioData.get(position).getFecha());
        String month = (String) DateFormat.format("MMM", anuncioData.get(position).getFecha());

        anuncioData.get(position).setFechaMes(day+" "+month);
        obId=ParseUser.getCurrentUser().getObjectId();
        if (anuncioData.get(position).getAutorId().equals(obId)){
            holder.fecha.setTextColor(Color.parseColor("#FFFFFF"));
            holder.destinatario.setTextColor(Color.parseColor("#FFFFFF"));
            holder.materia.setTextColor(Color.parseColor("#FFFFFF"));
            holder.para.setTextColor(Color.parseColor("#FFFFFF"));

            /*
            holder.txtDesayuno.setTextColor(Color.parseColor("#FFFFFF"));
            holder.desayunoTag.setTextColor(Color.parseColor("#FFFFFF"));
            holder.txtColac.setTextColor(Color.parseColor("#FFFFFF"));
            holder.colacTag.setTextColor(Color.parseColor("#FFFFFF"));
            holder.txtComida.setTextColor(Color.parseColor("#FFFFFF"));
            holder.comidaTag.setTextColor(Color.parseColor("#FFFFFF"));
            holder.txtMerienda.setTextColor(Color.parseColor("#FFFFFF"));
            holder.meriendaTag.setTextColor(Color.parseColor("#FFFFFF"));
            holder.txtDescanso.setTextColor(Color.parseColor("#FFFFFF"));
            holder.txtDurmio.setTextColor(Color.parseColor("#FFFFFF"));
            holder.durmioTag.setTextColor(Color.parseColor("#FFFFFF"));
            holder.txtTiempo.setTextColor(Color.parseColor("#FFFFFF"));
            holder.tiempoTag.setTextColor(Color.parseColor("#FFFFFF"));
            holder.funcionesTag.setTextColor(Color.parseColor("#FFFFFF"));
            holder.txtAviso.setTextColor(Color.parseColor("#FFFFFF"));
            holder.avisoTag.setTextColor(Color.parseColor("#FFFFFF"));
            holder.txtPipi.setTextColor(Color.parseColor("#FFFFFF"));
            holder.pipiTag.setTextColor(Color.parseColor("#FFFFFF"));
            holder.txtPopo.setTextColor(Color.parseColor("#FFFFFF"));
            holder.popoTag.setTextColor(Color.parseColor("#FFFFFF"));
            */

            convertView.setBackground(context.getResources().getDrawable(R.drawable.layout_current_user));
        }else {
            convertView.setBackground(context.getResources().getDrawable(R.drawable.layout_bg));
            holder.fecha.setTextColor(Color.parseColor("#000000"));
            holder.destinatario.setTextColor(Color.parseColor("#000000"));
            holder.materia.setTextColor(Color.parseColor("#000000"));
            holder.para.setTextColor(Color.parseColor("#FB553B"));

        }

        if (anuncioData.get(position).getParseUri()!=null||anuncioData.get(position).isAwsAttachment()||anuncioData.get(position).isTablaAnuncio()){
            holder.imagenClip.setVisibility(View.VISIBLE);
        }else {
            holder.imagenClip.setVisibility(View.INVISIBLE);
        }


        /*
        if (anuncioData.get(position).getDesayuno()==null||anuncioData.get(position).getDesayuno().equals("")){
            holder.layoutDesa.setVisibility(View.GONE);
        }else {
            holder.desayunoTag.setText(anuncioData.get(position).getDesayuno());
        }

        if (anuncioData.get(position).getMerienda()==null||anuncioData.get(position).getMerienda().equals("")){
            holder.layoutMeri.setVisibility(View.GONE);

        }else {
            holder.meriendaTag.setText(anuncioData.get(position).getMerienda());
        }

        if (anuncioData.get(position).getComida()==null||anuncioData.get(position).getComida().equals("")){
            holder.layoutComi.setVisibility(View.GONE);
        }else {
            holder.comidaTag.setText(anuncioData.get(position).getComida());
        }

        if (anuncioData.get(position).getColacion()!=null){
            holder.colacTag.setText(anuncioData.get(position).getColacion());
        }else {
            holder.layoutColac.setVisibility(View.GONE);
        }

        if (anuncioData.get(position).isDurmio()){
            holder.durmioTag.setText("Sí");
        }else {
            holder.durmioTag.setText("No");
        }

        if (anuncioData.get(position).getTiempo()!=null){
            holder.durmioTag.setText(anuncioData.get(position).getColacion()+" minutos");
        }else {
            holder.layoutDurm.setVisibility(View.GONE);
        }

        if (anuncioData.get(position).isAviso()){
            holder.avisoTag.setText("Sí");
        }else {
            holder.avisoTag.setText("No");
        }

        if (anuncioData.get(position).getPipi()==null||anuncioData.get(position).getPipi().equals("")){
            holder.pipiTag.setText(anuncioData.get(position).getPipi());
        }else {
            holder.layoutPip.setVisibility(View.GONE);
        }

        if (anuncioData.get(position).getPopo()!=null){
            holder.popoTag.setText(anuncioData.get(position).getPopo());
        }else {
            holder.layoutPop.setVisibility(View.GONE);
        }


        if (anuncioData.get(position).isMomento()){
            holder.tipo.setTextColor(Color.parseColor("#FB553B"));
            holder.layoutMensaje.setVisibility(View.GONE);
            holder.momentsLayout.setVisibility(View.VISIBLE);

            convertView.setEnabled(false);

        }else {
            holder.momentsLayout.setVisibility(View.GONE);
            holder.layoutMensaje.setVisibility(View.VISIBLE);

        }
        */

        if (anuncioData.get(position).getTipo().equals("Anuncio")){
            holder.tipo.setTextColor(Color.parseColor("#4FC1E9"));
        }else {
            if (anuncioData.get(position).getTipo().equals("Tarea")){
                holder.tipo.setTextColor(Color.parseColor("#A0D468"));
            }else {
                if (anuncioData.get(position).getTipo().equals("Enfermería")){
                    holder.tipo.setTextColor(Color.parseColor("#ED5565"));
                }else {
                    if (anuncioData.get(position).isMomento()){
                        holder.tipo.setTextColor(Color.parseColor("#FB553B"));
                    }else {
                        holder.tipo.setTextColor(Color.parseColor("#48CFAD"));
                    }

                }
            }
        }










        holder.tipo.setText(anuncioData.get(position).getTipo());
        holder.fecha.setText(anuncioData.get(position).getFechaMes());
        holder.materia.setText(anuncioData.get(position).getMateria());
        holder.destinatario.setText(anuncioData.get(position).getDestinatario());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new AnuncioDetailFragment();
                Bundle bundle=new Bundle();
                bundle.putString("objectId",anuncioData.get(position).getID());
                bundle.putString("generoAnuncio",anuncioData.get(position).getGenero());
                bundle.putString("tipoAnuncio",anuncioData.get(position).getTipo());
                bundle.putString("fechaEntrega",anuncioData.get(position).getFechaEntrega());
                bundle.putString("fechaCreacion",anuncioData.get(position).getFechaMes());
                bundle.putString("materiaAnuncio",anuncioData.get(position).getMateria());
                bundle.putString("emisorAnuncio",anuncioData.get(position).getEmisor());
                bundle.putString("receptorAnuncio",anuncioData.get(position).getDestinatario());
                bundle.putString("descripcionAnuncio",anuncioData.get(position).getDescripcion());
                if (anuncioData.get(position).getParseUri()!=null){
                    bundle.putString("parseUri",anuncioData.get(position).getParseUri());
                }else {
                    if (anuncioData.get(position).isAwsAttachment()){
                        bundle.putBoolean("aws",true);
                    }else {
                        if (anuncioData.get(position).isTablaAnuncio()){
                            bundle.putBoolean("tablaAnuncio",true);
                        }
                    }
                }

                bundle.putBoolean("momento",anuncioData.get(position).isMomento());
                bundle.putString("desayuno",anuncioData.get(position).getDesayuno());
                bundle.putString("comida",anuncioData.get(position).getComida());
                bundle.putString("colacion",anuncioData.get(position).getColacion());
                bundle.putString("merienda",anuncioData.get(position).getMerienda());
                bundle.putString("leche",anuncioData.get(position).getLeche());

                bundle.putBoolean("durmio",anuncioData.get(position).isDurmio());
                bundle.putString("tiempo",anuncioData.get(position).getTiempo());

                bundle.putBoolean("aviso",anuncioData.get(position).isAviso());
                bundle.putString("popo",anuncioData.get(position).getPopo());
                bundle.putString("pipi",anuncioData.get(position).getPipi());

                bundle.putString("comentarios",anuncioData.get(position).getComentario());
                bundle.putString("horario",anuncioData.get(position).getHorario());



                fragment.setArguments(bundle);
                FragmentTransaction transaction= ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
            }
        });

        return convertView;
    }





}

