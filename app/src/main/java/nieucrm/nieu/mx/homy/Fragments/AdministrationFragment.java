package nieucrm.nieu.mx.skola.Fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import nieucrm.nieu.mx.skola.Adapter.ServicioAdapter;
import nieucrm.nieu.mx.skola.DataModel.ServiciosData;
import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;

public class AdministrationFragment extends Fragment {

    ImageButton btnPagos,btnAccesos,btnUsuarios,btnReportes,btnHistorial,
            btnHistorialAdmin,btnGrupoAdmin,btnInfoAdmin,btnPresenciaAdmin,btnPaqueteAdmin,
            btnProspectosAdmin,btnEstudiantesAdmin,btnAccesosAdmin,btnPagosAdmin;
    Fragment fragment=null;
    TextView textPagos,textAccesos,historialServicios,txtSolicitarServicio,historialServiAdmin;
    int usertype;
    ListView listaServicios;
    LinearLayout layoutServicio;
    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf",demiBold="font/avenir-next-demi-bold.ttf";



    ArrayList<ServiciosData>serviciosArray;
    ServicioAdapter adapter;

    ProgressDialog dialog;

    String  escuela="";

    LinearLayout layoutAdmin,layoutPadre,layoutTeacher;
    Activity a;

    public AdministrationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            a=(Activity) context;
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Bundle bundle=this.getArguments();
        //escuela= bundle.getString("escuela");

        return inflater.inflate(R.layout.fragment_administration, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        escuela=((ParseApplication)a.getApplication()).getEscuelaId();

        btnAccesos = (ImageButton) view.findViewById(R.id.btnAccesos);
        btnPagos = (ImageButton) view.findViewById(R.id.btnPagos);
        btnHistorial=(ImageButton)view.findViewById(R.id.btnHistorial);

        layoutServicio=(LinearLayout)view.findViewById(R.id.layoutServicio);

        btnAccesosAdmin=(ImageButton)view.findViewById(R.id.btnAccesosAdmin);
        btnPagosAdmin=(ImageButton)view.findViewById(R.id.btnPagosAdmin);
        btnUsuarios=(ImageButton)view.findViewById(R.id.btnUsuarios);
        btnReportes=(ImageButton)view.findViewById(R.id.btnReportes);
        btnHistorialAdmin=(ImageButton)view.findViewById(R.id.btnHistorialAdmin);
        btnGrupoAdmin=(ImageButton)view.findViewById(R.id.btnGrupoAdmin);
        btnInfoAdmin=(ImageButton)view.findViewById(R.id.btnInfoAdmin);
        btnPresenciaAdmin=(ImageButton)view.findViewById(R.id.btnPresenciaAdmin);
        btnPaqueteAdmin=(ImageButton)view.findViewById(R.id.btnPaquetesAdmin);
        btnProspectosAdmin=(ImageButton)view.findViewById(R.id.btnProspectosAdmin);
        btnEstudiantesAdmin=(ImageButton)view.findViewById(R.id.btnEstudiantesAdmin);


        layoutAdmin=(LinearLayout)view.findViewById(R.id.layoutAdmin);
        layoutPadre=(LinearLayout)view.findViewById(R.id.layoutPadre);
        layoutTeacher=(LinearLayout)view.findViewById(R.id.layoutTeacher);



        textAccesos=(TextView)view.findViewById(R.id.textAccesos);
        textPagos=(TextView)view.findViewById(R.id.textPagos);
        historialServicios=(TextView)view.findViewById(R.id.historialServicios);
        txtSolicitarServicio=(TextView)view.findViewById(R.id.txtSolicitarServicio);


        listaServicios=(ListView)view.findViewById(R.id.listaServicios);

        setTypefaces();


        usertype=ParseUser.getCurrentUser().getNumber("usertype").intValue();

        setViews(usertype);


        setListeners();

    }

    public void setViews(int user){
        if (user==0||user==1){
            layoutPadre.setBackgroundColor(Color.parseColor("#EC87C0"));
            layoutAdmin.setVisibility(View.VISIBLE);
            layoutTeacher.setVisibility(View.GONE);
            layoutServicio.setVisibility(View.GONE);
            if (user==1){
                //Maestra

                btnHistorialAdmin.setVisibility(View.VISIBLE);
                btnAccesosAdmin.setVisibility(View.VISIBLE);
                btnPresenciaAdmin.setVisibility(View.VISIBLE);
                btnInfoAdmin.setVisibility(View.VISIBLE);


                btnEstudiantesAdmin.setVisibility(View.GONE);
                btnPaqueteAdmin.setVisibility(View.GONE);
                btnPagosAdmin.setVisibility(View.GONE);
                btnReportes.setVisibility(View.GONE);
                btnGrupoAdmin.setVisibility(View.GONE);
                btnInfoAdmin.setVisibility(View.GONE);
                btnPresenciaAdmin.setVisibility(View.GONE);
                btnProspectosAdmin.setVisibility(View.GONE);

            }
        }else {
            layoutAdmin.setVisibility(View.GONE);
            layoutServicio.setVisibility(View.VISIBLE);
            getServices();

        }
    }

    public void getServices(){
        serviciosArray=new ArrayList<>();
        ParseObject escuelas=ParseObject.createWithoutData("Escuela",escuela);
        ParseQuery<ParseObject> servicios=ParseQuery.getQuery("Servicio");
        servicios.whereEqualTo("activo",true);
        servicios.whereEqualTo("escuela",escuelas);
        servicios.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e==null){
                    for (ParseObject o:objects){
                        ServiciosData data=new ServiciosData();
                        data.setNombre(o.getString("nombre"));
                        data.setDescripcion(o.getString("descripcion"));
                        data.setObjectId(o.getObjectId());
                        data.setPrecio("$"+String.valueOf(o.getNumber("precio").intValue()));
                        serviciosArray.add(data);
                    }
                    adapter=new ServicioAdapter(a,serviciosArray);
                    listaServicios.setAdapter(adapter);
                }else {
                        Toast.makeText(a,"Por el momento no hay servicios activos",Toast.LENGTH_LONG).show();

                }
            }
        });
    }

    public void setListeners(){
        btnPagos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment=new PagosListFragment();
                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
            }
        });

        btnAccesos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment=new AccessFragment();
                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
            }
        });

        btnHistorial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment=new HistorialServiciosFragment();
                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();

            }
        });

        btnAccesosAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment= new AdminAccesosFragment();
                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();

            }
        });

        btnPagosAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment=new PagosAdminFragment();
                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
            }
        });

        btnReportes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment=new ReportesFragment();
                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();

            }
        });

        btnHistorialAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment=new ServiciosAdminFragment();
                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();

            }
        });

        btnGrupoAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment=new GruposAdminFragment();
                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();

            }
        });
        btnInfoAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment=new NoticeFragment();
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
            }
        });
        btnPresenciaAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment=new PresenciaFragment();
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();

            }
        });
        btnPaqueteAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment=new PaquetesFragment();
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();

            }
        });

        btnProspectosAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment=new ProspectoFragment();
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
            }
        });

        btnEstudiantesAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment=new EstudiantesAdminFragment();
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
            }
        });

        btnUsuarios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment=new UsuariosFragment();
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
            }
        });

    }

    public void setTypefaces(){
        Typeface bold=Typeface.createFromAsset(a.getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(a.getAssets(),avenirMedium);
        Typeface demibold=Typeface.createFromAsset(a.getAssets(),demiBold);

        textPagos.setTypeface(demibold);
        textAccesos.setTypeface(demibold);
        historialServicios.setTypeface(demibold);
        txtSolicitarServicio.setTypeface(demibold);

    }

}
