package nieucrm.nieu.mx.skola.Fragments;


import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import nieucrm.nieu.mx.skola.Helpers.Constants;
import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.Helpers.Util;
import nieucrm.nieu.mx.skola.R;

import static android.content.Context.WINDOW_SERVICE;

/**
 * Created by Carlos Romero
 */

public class CredentialFragment extends Fragment {

    TextView nombreCredencial,nombreAlumnoC,textCambio;
    ParseImageView fotoPadre;
    ImageView imagenQR;
    String codigo;
    ImageButton cambioAlumno;
    ArrayList<ParseObject>alumnoCredencial=new ArrayList<>();
    List<ParseObject>fotos=new ArrayList<>();
    ParseObject estudianteC;
    String nombreAlumno,alumnoId,userId;
    int position=0;
    BitMatrix bitMatrix;
    MultiFormatWriter multi;
    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf",demibold="font/avenir-next-demi-bold.ttf";
    int usertype;
    RelativeLayout btnCambio;
    TransferUtility utility;
    TransferObserver observer;
    File imageFile;
    File outputDir;
    String nombreStaff;

    int smallerDimension;

    String escuelaId;
    ParseObject escuela;

    String nombre,usuarioId;

    Activity a;

    public CredentialFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            a=(Activity) context;
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();

        super.onPrepareOptionsMenu(menu);
    }

//Codigo qr con idUsuario-idAlumno (21 caracteres)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        utility= Util.getTransferUtility(a);

        setHasOptionsMenu(true);

        return inflater.inflate(R.layout.fragment_credential, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);

        nombreCredencial=(TextView)v.findViewById(R.id.nombreCredencial);
        nombreAlumnoC=(TextView)v.findViewById(R.id.nombreAlumnoC);
        textCambio=(TextView)v.findViewById(R.id.textCambio);

        fotoPadre=(ParseImageView)v.findViewById(R.id.fotoPadre);
        imagenQR=(ImageView)v.findViewById(R.id.imagenQR);
        cambioAlumno=(ImageButton)v.findViewById(R.id.cambioAlumno);

        btnCambio=(RelativeLayout)v.findViewById(R.id.btnCambio);

        setTypeface();

        try {
            if (ParseUser.getCurrentUser().getNumber("usertype")==null){
                Log.d("Error","Usuario sin usertype");
            }else {
                usertype=ParseUser.getCurrentUser().getNumber("usertype").intValue();
            }

        }catch (Exception e){
            Log.e("Credencial",e.getMessage());
        }


        if (usertype==1||usertype==0){
            android.content.SharedPreferences ajustes=android.preference.PreferenceManager.getDefaultSharedPreferences(a);
            if (ajustes!=null){
                getFromPhoneAdmin();
            }else {
                teacherCredential();
                setFoto();

            }

        }else{
            setFoto();
            estudiantes();

        }


    }

    public void estudiantes(){

        //Find screen size
        WindowManager manager = (WindowManager) a.getSystemService(WINDOW_SERVICE);
        Display display = null;
        if (manager != null) {
            display = manager.getDefaultDisplay();
        }
        Point point = new Point();
        if (display != null) {
            display.getSize(point);
        }
        int width = point.x;
        int height = point.y;
        smallerDimension = width < height ? width : height;
        smallerDimension = smallerDimension * 3/4;

        escuelaId=((ParseApplication)a.getApplication()).getEscuelaId();
        ParseObject escuela=ParseObject.createWithoutData("Escuela",escuelaId);
        ParseQuery<ParseObject> est = ParseQuery.getQuery("Estudiantes")
                .include("PersonasAutorizadas")
                .whereEqualTo("PersonasAutorizadas", ParseUser.getCurrentUser());
        est.whereEqualTo("escuela",escuela);
        est.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e==null){
                    multi=new MultiFormatWriter();
                    alumnoCredencial.addAll(objects);

                    nombreCredencial.setText(ParseUser.getCurrentUser().getString("nombre")+" "+ParseUser.getCurrentUser().getString("apellidos"));


                    if (alumnoCredencial.size()!=0){
                        estudianteC=alumnoCredencial.get(position);
                        nombreAlumno=estudianteC.get("NOMBRE") + " " + estudianteC.getString("APELLIDO");
                        alumnoId=estudianteC.getObjectId();

                        userId= ParseUser.getCurrentUser().getObjectId();
                        codigo=userId+"-"+alumnoId;

                        if (alumnoCredencial.size()>1){
                            btnCambio.setVisibility(View.VISIBLE);
                            cambioAlumno.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (position==alumnoCredencial.size()-1){
                                        position = 0;
                                        estudianteC=alumnoCredencial.get(position);
                                        nombreAlumno=estudianteC.get("NOMBRE") + " " + estudianteC.getString("APELLIDO");
                                        alumnoId=estudianteC.getObjectId();
                                        userId= ParseUser.getCurrentUser().getObjectId();
                                        codigo=userId+"-"+alumnoId;
                                        nombreAlumnoC.setText(nombreAlumno);
                                        generateQR(codigo);
                                    }else{
                                        position++;
                                        estudianteC=alumnoCredencial.get(position);
                                        nombreAlumno=estudianteC.get("NOMBRE") + " " + estudianteC.getString("APELLIDO");
                                        alumnoId=estudianteC.getObjectId();
                                        userId= ParseUser.getCurrentUser().getObjectId();
                                        codigo=userId+"-"+alumnoId;

                                        generateQR(codigo);
                                    }
                                }
                            });
                        }
                        generateQR(codigo);
                    }else {
                        Log.d("Error","Error");
                    }


                }else {
                    try {
                        getFromPhone();
                    }catch (Exception e1){
                        Toast.makeText(a,"Error, favor de contactar a la administración",Toast.LENGTH_SHORT).show();
                    }

                }

            }
        });
    }

    public void  setFoto(){
        try {
            ParseQuery<ParseObject> q=ParseQuery.getQuery("UserPhoto").whereEqualTo("user",ParseUser.getCurrentUser());
            fotos=q.find();
            q.getFirstInBackground(new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject object, ParseException e) {
                    if (e==null){
                        if (object!=null){

                            if (object.getParseFile("userPhoto") != null) {
                                ParseFile foto=object.getParseFile("userPhoto");
                                fotoPadre.setParseFile(foto);
                                fotoPadre.loadInBackground(new GetDataCallback() {
                                    @Override
                                    public void done(byte[] data, ParseException e) {
                                    }
                                });
                            }else {
                                //AWS
                                downloadPhoto(object.getObjectId());
                            }
                        }
                    }else {
                        Log.e("Error",e.getMessage());
                    }
                }
            });

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void setTypeface(){

        Typeface bold=Typeface.createFromAsset(a.getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(a.getAssets(),avenirMedium);
        Typeface demi=Typeface.createFromAsset(a.getAssets(),demibold);


        nombreCredencial.setTypeface(bold);
        textCambio.setTypeface(demi);
        nombreAlumnoC.setTypeface(medium);


    }

    public void teacherCredential(){

        WindowManager manager = (WindowManager) a.getSystemService(WINDOW_SERVICE);
        Display display = null;
        if (manager != null) {
            display = manager.getDefaultDisplay();
        }
        Point point = new Point();
        if (display != null) {
            display.getSize(point);
        }
        int width = point.x;
        int height = point.y;
        smallerDimension = width < height ? width : height;
        smallerDimension = smallerDimension * 3/4;

        if (ParseUser.getCurrentUser().getString("nombre")==null||ParseUser.getCurrentUser().getString("apellidos")==null){
            try{
                getFromPhoneAdmin();
            }catch (Exception e1){
                nombreCredencial.setText("Usuario no identificado");
            }
        }else {
            nombreStaff=ParseUser.getCurrentUser().getString("nombre")+" "+ParseUser.getCurrentUser().getString("apellidos");
        }
        userId= ParseUser.getCurrentUser().getObjectId();


        codigo=userId+"_2";
        saveInPhoneAdmin(codigo,nombreStaff);
        generateQRAdmin(codigo,nombreStaff);

    }

    public void downloadPhoto(String objectId){
        outputDir = a.getCacheDir();
        try {
            imageFile= File.createTempFile("prefix", "extension", outputDir);
        } catch (IOException e) {
            e.printStackTrace();
        }
        observer=utility.download(Constants.BUCKET_NAME,objectId,imageFile);
        observer.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                Log.d("Estatus:",state.toString());

                if (TransferState.COMPLETED.equals(observer.getState())) {

                    Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
                    fotoPadre.setImageBitmap(bitmap);
                    //progress.setVisibility(View.GONE);
                    try {
                        Toast.makeText(a, "Foto lista", Toast.LENGTH_SHORT).show();
                    }catch (Exception e1){
                        Log.e("Exception",e1.getMessage());
                    }
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                long _bytesCurrent = bytesCurrent;
                long _bytesTotal = bytesTotal;

                float percentage =  ((float)_bytesCurrent /(float)_bytesTotal * 100);
                Log.d("percentage","" +percentage);


            }

            @Override
            public void onError(int id, Exception ex) {

                Log.d("Error al descargar", ex.getMessage());
            }
        });
    }

    public void generateQR(String code){
        multi=new MultiFormatWriter();
        nombreAlumnoC.setText(nombreAlumno);

        try {
            bitMatrix=multi.encode(code, BarcodeFormat.QR_CODE,smallerDimension,smallerDimension);
            BarcodeEncoder bar=new BarcodeEncoder();
            Bitmap bitmap=bar.createBitmap(bitMatrix);
            imagenQR.setImageBitmap(bitmap);
        }catch (WriterException et){
            et.printStackTrace();
        }
    }

    public void generateQRAdmin(String code, String nombre){
        WindowManager manager = (WindowManager) a.getSystemService(WINDOW_SERVICE);
        Display display = null;
        if (manager != null) {
            display = manager.getDefaultDisplay();
        }
        Point point = new Point();
        if (display != null) {
            display.getSize(point);
        }
        int width = point.x;
        int height = point.y;
        smallerDimension = width < height ? width : height;
        smallerDimension = smallerDimension * 3/4;
        multi=new MultiFormatWriter();
        nombreCredencial.setText(nombre);
        nombreAlumnoC.setVisibility(View.INVISIBLE);

        try {
            bitMatrix=multi.encode(code, BarcodeFormat.QR_CODE,smallerDimension,smallerDimension);
            BarcodeEncoder bar=new BarcodeEncoder();
            Bitmap bitmap=bar.createBitmap(bitMatrix);
            imagenQR.setImageBitmap(bitmap);
        }catch (WriterException et){
            et.printStackTrace();
        }
    }

    public void saveInPhoneAdmin(String idUsuario, String nombreUsuario){

        //Guardar en sharedpreferences
        android.content.SharedPreferences mSettings=android.preference.PreferenceManager.getDefaultSharedPreferences(a);
        android.content.SharedPreferences.Editor editor = mSettings.edit();
        editor.putString("userId",idUsuario);
        editor.putString("nombre",nombreUsuario);

        //Guardar datos
        editor.apply();

    }

    public void saveInPhone(String idUsuario, String nombreUsuario,ArrayList<String>listaAlumnos){

        //Guardar en sharedpreferences
        android.content.SharedPreferences mSettings=android.preference.PreferenceManager.getDefaultSharedPreferences(a);
        android.content.SharedPreferences.Editor editor = mSettings.edit();
        editor.putString("userId",idUsuario);
        editor.putString("nombre",nombreUsuario);

        Gson gson=new Gson();
        String ninos=gson.toJson(listaAlumnos);

        editor.putString("alumnos",ninos);

        //Guardar datos
        editor.apply();

    }

    public void getFromPhone(){
        //Obtener datos de SharedPreferences
        List<String>listaAlumno;
        android.content.SharedPreferences ajustes=android.preference.PreferenceManager.getDefaultSharedPreferences(a);
        String nombreGuar=ajustes.getString("nombre","No se encontro nombre");
        String idGuardado=ajustes.getString("userId","No se encontro dato");
        String ninos=ajustes.getString("alumnos","No se encontraron datos");
        Gson gson=new Gson();
        String [] lista=gson.fromJson(ninos,String[].class);
        listaAlumno= Arrays.asList(lista);


    }

    public void getFromPhoneAdmin(){
        //Obtener datos de SharedPreferences
        android.content.SharedPreferences ajustes=android.preference.PreferenceManager.getDefaultSharedPreferences(a);
        String nombreGuar=ajustes.getString("nombre","No se encontro nombre");
        String idGuardado=ajustes.getString("userId","No se encontro dato");
        generateQRAdmin(idGuardado,nombreGuar);
    }

}
