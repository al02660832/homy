package nieucrm.nieu.mx.skola.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import nieucrm.nieu.mx.skola.DataModel.AccessData;
import nieucrm.nieu.mx.skola.DataModel.RecordData;
import nieucrm.nieu.mx.skola.Helpers.Constants;
import nieucrm.nieu.mx.skola.Helpers.Util;
import nieucrm.nieu.mx.skola.R;


/**
 * Created by Carlos Romero on 10/12/2016.
 */

public class RecordAdapter extends BaseAdapter {

    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf";
    TextView estudiante;
    File outputDir,imageFile;
    TransferUtility utility;
    TransferObserver observer;


    private Context context;
    LayoutInflater inflater;
    private List<RecordData> recordData=null;
    private ArrayList<RecordData>arrayList=null;

    public RecordAdapter(Context context, List<RecordData> recordData) {
        this.context = context;
        this.recordData=recordData;
        try {
            inflater=LayoutInflater.from(context);

        }catch (Exception e){
            Log.d("Error",e.getMessage());
        }
        this.arrayList=new ArrayList<>();
        this.arrayList.addAll(recordData);
    }

    public class ViewHolder{
        TextView recordParentesco, recordNombre,recordApellidos,recordEmail,recordTelCas,recordTelCel,recordTelOfi,recordDomi;
        ImageView imagenRecord;
    }


    @Override
    public int getCount() {
        return recordData.size();
    }

    @Override
    public Object getItem(int position) {
        return recordData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {

        return getCount();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Typeface bold=Typeface.createFromAsset(context.getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(context.getAssets(),avenirMedium);

        ViewHolder holder;
       // TextView nombreEstudiante=(TextView)v.findViewById(R.id.nombreEstudiante);

        if(convertView==null){
            holder=new ViewHolder();
            convertView=inflater.inflate(R.layout.record_item_layout,null);
            holder.recordParentesco = (TextView) convertView.findViewById(R.id.recordParentesco);
            holder.recordNombre = (TextView) convertView.findViewById(R.id.recordNombre);
            holder.recordApellidos = (TextView) convertView.findViewById(R.id.recordApellidos);
            holder.recordEmail = (TextView) convertView.findViewById(R.id.recordEmail);
            holder.recordTelCas = (TextView) convertView.findViewById(R.id.recordTelCas);
            holder.recordTelOfi = (TextView) convertView.findViewById(R.id.recordTelOfi);
            holder.recordTelCel = (TextView) convertView.findViewById(R.id.recordTelCel);
            holder.recordDomi = (TextView) convertView.findViewById(R.id.recordDomi);

            holder.imagenRecord=(ImageView)convertView.findViewById(R.id.imagenRecord);

            holder.recordParentesco.setTypeface(bold);
            holder.recordNombre.setTypeface(medium);
            holder.recordApellidos.setTypeface(medium);
            holder.recordEmail.setTypeface(medium);
            holder.recordTelOfi.setTypeface(medium);
            holder.recordTelCel.setTypeface(medium);
            holder.recordTelCas.setTypeface(medium);
            holder.recordDomi.setTypeface(medium);

            convertView.setTag(holder);
        }else {
            holder=(ViewHolder)convertView.getTag();
        }

        holder.recordNombre.setText(recordData.get(position).getNombre());
        holder.recordApellidos.setText(recordData.get(position).getApellidos());
        holder.recordParentesco.setText(recordData.get(position).getParentesco());
        holder.recordTelOfi.setText(recordData.get(position).getTelOficina());
        holder.recordTelCas.setText(recordData.get(position).getTelCasa());
        holder.recordTelCel.setText(recordData.get(position).getTelCelular());
        holder.recordEmail.setText(recordData.get(position).getCorreo());
        holder.recordDomi.setText(recordData.get(position).getDomicilio());
        setFoto(recordData.get(position).getObjectId(),holder.imagenRecord);

        return convertView;


    }

    private void  setFoto(String objectId, final ImageView v){
            ParseQuery<ParseObject> q=ParseQuery.getQuery("UserPhoto");
            ParseUser a=ParseObject.createWithoutData(ParseUser.class,objectId);
            q.whereEqualTo("user",a);
            q.getFirstInBackground(new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject object, ParseException e) {
                    if (e==null){
                        if (object!=null){

                            if (object.getParseFile("userPhoto") != null) {

                            }else {
                                //AWS
                                downloadPhoto(object.getObjectId(),v);
                            }
                        }
                    }else {
                        Log.e("Error",e.getMessage());
                    }
                }
            });


    }

    private void downloadPhoto(String objectId, final ImageView i){
        outputDir = context.getCacheDir();
        utility= Util.getTransferUtility(context);

        try {
            imageFile= File.createTempFile("prefix", "extension", outputDir);
        } catch (IOException e) {
            e.printStackTrace();
        }
        observer=utility.download(Constants.BUCKET_NAME,objectId,imageFile);
        observer.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                Log.d("Estatus:",state.toString());

                if (TransferState.COMPLETED.equals(observer.getState())) {

                    Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
                    //fotoPadre.setImageBitmap(bitmap);
                    //progress.setVisibility(View.GONE);
                    i.setImageBitmap(bitmap);
                    try {
                        Log.d("Status foto","Lista");
                        //Toast.makeText(context, "Foto lista", Toast.LENGTH_SHORT).show();
                    }catch (Exception e1){
                        Log.e("Exception",e1.getMessage());
                    }
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                long _bytesCurrent = bytesCurrent;
                long _bytesTotal = bytesTotal;

                float percentage =  ((float)_bytesCurrent /(float)_bytesTotal * 100);
                Log.d("percentage","" +percentage);

            }

            @Override
            public void onError(int id, Exception ex) {

                Log.d("Error al descargar", ex.getMessage());
            }
        });
    }

}
