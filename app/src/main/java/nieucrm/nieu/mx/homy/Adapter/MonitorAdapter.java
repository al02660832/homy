package nieucrm.nieu.mx.skola.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import nieucrm.nieu.mx.skola.DataModel.AccessData;
import nieucrm.nieu.mx.skola.R;


/**
 * Created by Carlos Romero on 10/12/2016.
 */

public class MonitorAdapter extends BaseAdapter {

    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf";
    TextView estudiante;


    private Context context;
    LayoutInflater inflater;
    private List<AccessData> accessData=null;
    private ArrayList<AccessData>arrayList=null;

    public MonitorAdapter(Context context, List<AccessData> accessData) {
        this.context = context;
        this.accessData = accessData;
        try {
            inflater=LayoutInflater.from(context);

        }catch (Exception e){
            Log.d("Error",e.getMessage());
        }
        this.arrayList=new ArrayList<>();
        this.arrayList.addAll(accessData);
    }

    public class ViewHolder{
        TextView nombrePadre;
        TextView txtParentesco;
        TextView nombreAlumno;
        TextView txtGrupo;
        TextView txtHora;
    }


    @Override
    public int getCount() {
        return accessData.size();
    }

    @Override
    public Object getItem(int position) {
        return accessData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Typeface bold=Typeface.createFromAsset(context.getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(context.getAssets(),avenirMedium);

        ViewHolder holder;
       // TextView nombreEstudiante=(TextView)v.findViewById(R.id.nombreEstudiante);

        if(convertView==null){
            holder=new ViewHolder();
            convertView=inflater.inflate(R.layout.monitor_acceso_item_layout,null);
            holder.nombrePadre = (TextView) convertView.findViewById(R.id.monitorPadre);
            holder.txtParentesco = (TextView) convertView.findViewById(R.id.monitorParentesco);
            holder.nombreAlumno=(TextView)convertView.findViewById(R.id.monitorEstudiante);
            holder.txtHora=(TextView)convertView.findViewById(R.id.monitorHora);
            holder.txtGrupo=(TextView)convertView.findViewById(R.id.monitorGrupo);

            holder.txtGrupo.setTypeface(medium);
            holder.nombreAlumno.setTypeface(bold);
            holder.nombrePadre.setTypeface(medium);
            holder.txtParentesco.setTypeface(bold);
            holder.txtHora.setTypeface(bold);

            convertView.setTag(holder);
        }else {
            holder=(ViewHolder)convertView.getTag();
        }
       if ( accessData.get(position).getColor()==0){
           convertView.setBackground(context.getResources().getDrawable(R.drawable.layout_verde));

       }else {
           if (accessData.get(position).getColor()==1){
               convertView.setBackground(context.getResources().getDrawable(R.drawable.layout_amarillo));
           }else {
               if (accessData.get(position).getColor()==2){
                   convertView.setBackground(context.getResources().getDrawable(R.drawable.layout_rojo));
               }   else {
                   convertView.setBackground(context.getResources().getDrawable(R.drawable.layout_verde));
               }
           }
       }

        holder.nombrePadre.setText(accessData.get(position).getNombrePadre());
        holder.txtParentesco.setText(accessData.get(position).getParentesco());
        holder.nombreAlumno.setText(accessData.get(position).getNombreNino());
        holder.txtGrupo.setText(accessData.get(position).getGrupo());
        holder.txtHora.setText(accessData.get(position).getHora());


        return convertView;


    }

}
