package nieucrm.nieu.mx.skola.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import nieucrm.nieu.mx.skola.DataModel.AnnouncementData;
import nieucrm.nieu.mx.skola.Fragments.AnuncioDetailFragment;
import nieucrm.nieu.mx.skola.R;


/**
 * Created by Carlos Romero on 10/12/2016.
 */

public class TareasAdapter extends BaseAdapter {

    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf";

    private Context context;
    LayoutInflater inflater;
    private List<AnnouncementData> announcementData=null;
    private ArrayList<AnnouncementData>arrayList=null;


    public TareasAdapter(Context context, List<AnnouncementData> announcementData) {
        this.context = context;
        this.announcementData=announcementData;
        try {
            inflater=LayoutInflater.from(context);

        }catch (Exception e){
            Log.d("Error",e.getMessage());
        }
        this.arrayList=new ArrayList<>();
        this.arrayList.addAll(announcementData);
    }

    public class ViewHolder{
        TextView announcementDate;
        TextView announcementDescription;
        ImageView approvedCircle,rejectedCircle;
    }


    @Override
    public int getCount() {
        return announcementData.size();
    }

    @Override
    public Object getItem(int position) {
        return announcementData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Typeface bold=Typeface.createFromAsset(context.getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(context.getAssets(),avenirMedium);

        ViewHolder holder;
       // TextView nombreEstudiante=(TextView)v.findViewById(R.id.nombreEstudiante);

        if(convertView==null){
            holder=new ViewHolder();
            convertView=inflater.inflate(R.layout.anuncios_item_layout,null);
            holder.announcementDate=(TextView) convertView.findViewById(R.id.announcementDate);
            holder.announcementDescription=(TextView) convertView.findViewById(R.id.announcementDescription);
            holder.approvedCircle=(ImageView)convertView.findViewById(R.id.approvedCircle);
            holder.rejectedCircle=(ImageView)convertView.findViewById(R.id.rejectedCircle);
            holder.announcementDate.setTypeface(medium);
            holder.announcementDate.setTypeface(medium);

            convertView.setTag(holder);
        }else {
            holder=(ViewHolder)convertView.getTag();
        }

        String dia=(String) DateFormat.format("dd", announcementData.get(position).getFechaEntrega());
        String mese = (String) DateFormat.format("MMM", announcementData.get(position).getFechaEntrega());

        announcementData.get(position).setF(dia+"/"+mese);
                //=;

        String day=(String) DateFormat.format("dd", announcementData.get(position).getFecha());
        String month = (String) DateFormat.format("MMM", announcementData.get(position).getFecha());

        announcementData.get(position).setM(day+"/"+month);

        if (announcementData.get(position).isAprobado()){
            holder.rejectedCircle.setVisibility(View.GONE);
            holder.approvedCircle.setVisibility(View.VISIBLE);
        }else {
            holder.approvedCircle.setVisibility(View.GONE);
            holder.rejectedCircle.setVisibility(View.VISIBLE);
        }

        holder.announcementDate.setText(announcementData.get(position).getF());
        holder.announcementDescription.setText(announcementData.get(position).getDescripcion());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment fragment = new AnuncioDetailFragment();
                Bundle bundle=new Bundle();
                bundle.putString("generoAnuncio",announcementData.get(position).getGenero());
                bundle.putString("tipoAnuncio",announcementData.get(position).getTipo());
                bundle.putString("fechaEntrega",announcementData.get(position).getM());
                bundle.putString("fechaCreacion",announcementData.get(position).getF());
                bundle.putString("materiaAnuncio",announcementData.get(position).getMateria());
                bundle.putString("emisorAnuncio",announcementData.get(position).getEmisor());
                bundle.putString("receptorAnuncio",announcementData.get(position).getDestinatario());
                bundle.putString("descripcionAnuncio",announcementData.get(position).getDescripcion());
                bundle.putString("objectId",announcementData.get(position).getObjectId());
                fragment.setArguments(bundle);
                FragmentTransaction transaction= ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();

            }
        });

        return convertView;


    }

}
