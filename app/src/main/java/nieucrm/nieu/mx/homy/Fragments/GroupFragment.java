package nieucrm.nieu.mx.skola.Fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;

/**
 * Created by Carlos Romero
 */

public class GroupFragment extends Fragment {

    ListView listView;
    ArrayList<String>group;
    ArrayAdapter<String>g;
    List<ParseObject> ob;
    ProgressDialog progressDialog;
    ProgressBar progressGrupo;
    String escuela;
    Fragment fragment;
    Activity a;

    public GroupFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            a=(Activity) context;
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_group, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listView=(ListView)view.findViewById(R.id.listaGrupo);
        group=new ArrayList<>();
        progressGrupo=(ProgressBar)view.findViewById(R.id.progressGrupo);

        ParseQuery<ParseObject>q=ParseQuery.getQuery("grupo").include("Maestros").whereEqualTo("Maestros", ParseUser.getCurrentUser());
        escuela=((ParseApplication)a.getApplication()).getEscuelaId();

        ParseObject e=ParseObject.createWithoutData("Escuela",escuela);
        q.whereEqualTo("escuela",e);
        q.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                progressGrupo.setVisibility(View.GONE);
                listView.setVisibility(View.VISIBLE);
                if (e==null){

                    for (ParseObject o:objects){
                        group.add(o.getString("grupoId"));
                    }
                    ob=objects;
                    try {
                        g=new ArrayAdapter<String>(a,android.R.layout.simple_list_item_1,android.R.id.text1,group);

                    }catch (Exception ex){
                        ex.printStackTrace();
                    }

                    listView.setAdapter(g);
                }
                else{
                    Toast.makeText(a,e.getMessage(),Toast.LENGTH_SHORT).show();
                }
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle= new Bundle();
                bundle.putString("grupoId",ob.get(position).getObjectId());
                bundle.putString("name",ob.get(position).getString("name"));
                fragment=new GroupProfileFragment();
                fragment.setArguments(bundle);
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
            }
        });



    }
}
