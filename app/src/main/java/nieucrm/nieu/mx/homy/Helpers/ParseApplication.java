package nieucrm.nieu.mx.skola.Helpers;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.crashlytics.android.Crashlytics;
import com.onesignal.OneSignal;
import com.parse.Parse;
import com.parse.ParseInstallation;

import io.fabric.sdk.android.Fabric;
import nieucrm.nieu.mx.skola.BuildConfig;

/**
 * Created by Carlos Romero on 24/11/2016.
 */

public class ParseApplication extends Application {

    public static ParseApplication mInstance;

    private String escuelaId,escuelaNombre;

    public String getEscuelaId() {
        return escuelaId;
    }

    public void setEscuelaId(String escuelaId) {
        this.escuelaId = escuelaId;
    }

    public String getEscuelaNombre() {
        return escuelaNombre;
    }

    public void setEscuelaNombre(String escuelaNombre) {
        this.escuelaNombre = escuelaNombre;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        
        Fabric.with(this, new Crashlytics());

        OneSignal.startInit(this).autoPromptLocation(true).init();

        CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                getApplicationContext(),    /* get the context for the application */
                "us-east-2:f899b7b8-5bed-4c9c-bd04-c447b5df2ef4",    /* Identity Pool ID */
                Regions.US_EAST_1      /* Region for your identity pool--US_EAST_1 or EU_WEST_1*/
        );

        
        
        AmazonS3 s3 = new AmazonS3Client(credentialsProvider);
        
        
        //TransferUtility transferUtility = new TransferUtility(s3, getApplicationContext());

        //Actividad que extienda aplication
        Parse.enableLocalDatastore(this);
        //Conexion a Parse
        Parse.initialize(new Parse.Configuration.Builder(getApplicationContext())
                .applicationId(BuildConfig.appId).clientKey(BuildConfig.masterKey)
                .server(BuildConfig.urlServer).build());
        ParseInstallation.getCurrentInstallation().saveInBackground();

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}