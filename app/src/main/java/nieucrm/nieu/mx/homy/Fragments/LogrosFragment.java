package nieucrm.nieu.mx.skola.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

import nieucrm.nieu.mx.skola.Adapter.LogroAdapter;
import nieucrm.nieu.mx.skola.DataModel.LogroData;
import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class LogrosFragment extends Fragment {

    ArrayList<LogroData>a=new ArrayList<>();
    GridView gridLogros;
    LogroAdapter adapter;

    public LogrosFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_logros, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        gridLogros=(GridView)view.findViewById(R.id.gridLogros);
        getLogros();

    }

    public void getLogros(){
        ParseQuery<ParseObject> imageParse=new ParseQuery<ParseObject>("Logro");
        imageParse.include("imagen");
        imageParse.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e==null){
                    for (ParseObject o:objects){
                        ParseFile ima=o.getParseFile("imagen");
                        LogroData data=new LogroData();
                        data.setImagenLogro(ima.getUrl());
                        data.setNombre(o.getString("nombre"));
                        a.add(data);
                    }
                }else {
                    Log.d("Error",e.getMessage());
                }

                adapter=new LogroAdapter(getContext(),a);
                gridLogros.setAdapter(adapter);

            }
        });
    }


}
