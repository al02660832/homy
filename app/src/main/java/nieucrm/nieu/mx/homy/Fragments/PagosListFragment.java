package nieucrm.nieu.mx.skola.Fragments;


import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import nieucrm.nieu.mx.skola.Adapter.PagosAdapter;
import nieucrm.nieu.mx.skola.DataModel.PagosData;
import nieucrm.nieu.mx.skola.R;

/**
 * Created by Carlos Romero
 */

public class PagosListFragment extends Fragment {

    ListView listView;
    PagosAdapter adapter;
    ArrayList<PagosData>listPagos;
    ImageButton btnPagos;
    Fragment fragment=null;
    TextView txtAdeudo, txtTotalAdeudo;
    ArrayList<Double>adeudo;
    ParseQuery<ParseObject>students,pagoRecurrente,queryPagos,subQuery;
    Calendar i,f;
    Date inicio,fin;
    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf",demibold="font/avenir-next-demi-bold.ttf";



    public PagosListFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pagos_list, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);

        listView=(ListView)v.findViewById(R.id.listaPagos);
        btnPagos=(ImageButton)v.findViewById(R.id.botonPagos);
        txtAdeudo=(TextView)v.findViewById(R.id.txtAdeudo);
        txtTotalAdeudo=(TextView)v.findViewById(R.id.txtTotalAdeudo);

        setTypeface();
        getPagos();


        btnPagos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment=new PagosFragment();
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
            }
        });

    }

    public void getPagos() {
        listPagos = new ArrayList<>();
        adeudo = new ArrayList<>();


        students = ParseQuery.getQuery("Estudiantes");
        students.whereEqualTo("PersonasAutorizadas", ParseUser.getCurrentUser());

        //Inicio de mes
        i = Calendar.getInstance();
        i.set(Calendar.DAY_OF_MONTH, 1);
        inicio = i.getTime();
        //Fin de mes
        f = Calendar.getInstance();
        f.set(Calendar.DAY_OF_MONTH, f.getActualMaximum(Calendar.DAY_OF_MONTH));
        fin = f.getTime();

        pagoRecurrente = ParseQuery.getQuery("pagos");
        pagoRecurrente.whereMatchesQuery("student", students);
        pagoRecurrente.whereExists("fechaCobro");
        pagoRecurrente.whereGreaterThan("fechaCobro", inicio);
        pagoRecurrente.whereLessThan("fechaCobro", fin);

        queryPagos = ParseQuery.getQuery("pagos");
        queryPagos.whereMatchesQuery("student", students);
        queryPagos.whereDoesNotExist("fechaCobro");

        List<ParseQuery<ParseObject>> queries = new ArrayList<>();
        queries.add(pagoRecurrente);
        queries.add(queryPagos);
        subQuery = ParseQuery.or(queries);
        subQuery.include("user").include("student").orderByDescending("createdAt");
        final ProgressDialog progressDialog = new ProgressDialog(getContext());

        progressDialog.setTitle("Cargando");
        progressDialog.setMessage("Por favor espere...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        subQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                progressDialog.hide();
                if (e == null) {

                    for (ParseObject object : objects) {
                        PagosData pagos = new PagosData();

                        ParseObject est = object.getParseObject("student");
                        pagos.setAlumno(est.getString("NOMBRE") + " " + est.getString("APELLIDO"));
                        pagos.setPrimerNombre(est.getString("NOMBRE"));
                        pagos.setConcepto(object.getString("concepto"));
                        pagos.setObjectId(object.getObjectId());
                        if (object.getParseFile("photo") == null) {
                            if (object.getBoolean("aws")) {
                                pagos.setFileUri("A");
                            } else {
                                pagos.setFileUri("N");
                            }
                        } else {
                            pagos.setFileUri(object.getParseFile("photo").getUrl());
                        }

                        if (object.getDate("fechaAdelanto") == null) {
                            pagos.setMesPago(object.getCreatedAt());
                        } else {
                            pagos.setMesPago(object.getDate("fechaAdelanto"));
                        }

                        DateFormat f = new SimpleDateFormat("dd/MM/yy", new Locale("es"));

                        pagos.setFecha(f.format(object.getCreatedAt()));

                        if (object.getNumber("total") == null) {
                            if (object.getNumber("cantidad") == null) {
                                pagos.setCantidad("$0.0");

                            } else {
                                pagos.setCantidad("$" + String.valueOf(object.getNumber("cantidad")));
                            }
                        } else {
                            pagos.setCantidad("$" + String.valueOf(object.getNumber("total")));
                        }


                        if (object.getBoolean("pagado")) {
                            pagos.setStatus("Pagado");

                        } else {
                            pagos.setStatus("Pendiente");

                            if (object.getNumber("total") != null) {
                                adeudo.add(object.getNumber("total").doubleValue());
                            } else {
                                if (object.getNumber("cantidad") != null) {
                                    adeudo.add(object.getNumber("cantidad").doubleValue());
                                }

                            }

                        }

                        listPagos.add(pagos);
                    }

                    double res = 0;
                    for (int i = 0; i < adeudo.size(); i++) {
                        res = res + adeudo.get(i);
                    }

                    txtTotalAdeudo.setText("$" + String.valueOf(round(res,2)));
                    adapter = new PagosAdapter(getActivity(), listPagos);
                    adapter.notifyDataSetChanged();

                    listView.setAdapter(adapter);

                }


            }
        });

    }
        //////////////////////////////////////////77
        //Antiguo

        /*
        ParseQuery<ParseObject>query=ParseQuery.getQuery("pagos").include("user").include("student").orderByDescending("createdAt").whereEqualTo("user",ParseUser.getCurrentUser());
        final ProgressDialog progressDialog=new ProgressDialog(getContext());

        progressDialog.setTitle("Cargando");
        progressDialog.setMessage("Por favor espere...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                progressDialog.hide();
                if (e==null){

                    for(ParseObject object:objects){
                        PagosData pagos=new PagosData();

                        ParseObject est=object.getParseObject("student");
                        pagos.setAlumno(est.getString("NOMBRE")+" "+est.getString("APELLIDO"));
                        pagos.setPrimerNombre(est.getString("NOMBRE"));
                        pagos.setConcepto(object.getString("concepto"));
                        pagos.setObjectId(object.getObjectId());
                        if (object.getParseFile("photo")==null){
                            if (object.getBoolean("aws")){
                                pagos.setFileUri("A");
                            }else {
                                pagos.setFileUri("N");
                            }
                        }else {
                            pagos.setFileUri(object.getParseFile("photo").getUrl());
                        }

                        if (object.getDate("fechaAdelanto")==null){
                            pagos.setMesPago(object.getCreatedAt());
                        }else {
                            pagos.setMesPago(object.getDate("fechaAdelanto"));
                        }

                        DateFormat f=new SimpleDateFormat("dd/MM/yy",new Locale("es"));

                        pagos.setFecha(f.format(object.getCreatedAt()));
                        if (object.getNumber("cantidad")==null){
                            if (object.getNumber("total")==null){
                                pagos.setCantidad("$0.0");
                            }else {
                                pagos.setCantidad("$"+String.valueOf(object.getNumber("total")));
                            }
                        }else {
                            pagos.setCantidad("$"+String.valueOf(object.getNumber("cantidad")));

                        }

                        if(object.getBoolean("pagado")){
                            pagos.setStatus("Pagado");

                        }else {
                            pagos.setStatus("Pendiente");

                                if (object.getNumber("total")!=null){
                                    adeudo.add(object.getNumber("total").intValue());
                                }

                        }

                        listPagos.add(pagos);
                    }

                    double res=0;
                    for (int i=0;i<adeudo.size();i++){
                        res=res+adeudo.get(i);
                    }
                    txtTotalAdeudo.setText(String.valueOf(res));
                    adapter=new PagosAdapter(getActivity(),listPagos);
                    adapter.notifyDataSetChanged();

                    listView.setAdapter(adapter);

                }

            }
        });
        */
    private void setTypeface(){
        Typeface bold=Typeface.createFromAsset(getActivity().getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(getActivity().getAssets(),avenirMedium);
        Typeface dem=Typeface.createFromAsset(getActivity().getAssets(),demibold);

        txtTotalAdeudo.setTypeface(dem);
        txtAdeudo.setTypeface(dem);

    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
