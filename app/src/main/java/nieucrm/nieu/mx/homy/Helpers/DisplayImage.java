package nieucrm.nieu.mx.skola.Helpers;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import nieucrm.nieu.mx.skola.DataModel.ImageModel;
import nieucrm.nieu.mx.skola.R;

import static nieucrm.nieu.mx.skola.Helpers.Constants.BUCKET_NAME;
import static nieucrm.nieu.mx.skola.Helpers.Constants.PHOTO_TEMP_PATH;

/**
 * Created by mmac1 on 03/10/2017.
 */

public class DisplayImage {

    private static DisplayImage displayImage = new DisplayImage();

    private DisplayImage() {}

    public static DisplayImage getInstance () { return displayImage;}

    public void displayImageForUser (Context context, ImageView imageView, String uniqueId) {

        ImageModel imageModel = new ImageModel();
        imageModel.setId(uniqueId);
        imageModel.setLocalPath(PHOTO_TEMP_PATH);
        imageModel.setBucketName(BUCKET_NAME);

        Glide.with(context)
                .setDefaultRequestOptions(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                        .placeholder(R.mipmap.ic_launcher).error(R.drawable.red_back)
                        .fitCenter())
                .load(imageModel)
                .into(imageView);
    }

}