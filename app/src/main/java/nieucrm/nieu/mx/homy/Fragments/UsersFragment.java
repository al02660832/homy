package nieucrm.nieu.mx.skola.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import nieucrm.nieu.mx.skola.Adapter.UserAdapter;
import nieucrm.nieu.mx.skola.DataModel.UserData;
import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class UsersFragment extends Fragment {

    ListView listaUsers;
    ProgressBar progressUser;
    UserAdapter a;
    UserData data;
    ParseQuery<ParseUser> papa;
    ArrayList<UserData> arrayList;
    Bundle b;
    int usertype;
    TextView nombreEs;
    Toolbar toolbar;
    String escuelaId;
    ParseObject escuela;

    public UsersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        b=this.getArguments();
        usertype=b.getInt("usertype");

        setHasOptionsMenu(true);

        return inflater.inflate(R.layout.fragment_users, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        listaUsers=(ListView)view.findViewById(R.id.listaUsers);
        progressUser=(ProgressBar)view.findViewById(R.id.progressUser);

        nombreEs = (TextView) getActivity().findViewById(R.id.textAction);
        toolbar=(Toolbar)getActivity().findViewById(R.id.toolbar);

        getListas(usertype);



    }

    public void getListas(int i){
        escuelaId=((ParseApplication)getActivity().getApplication()).getEscuelaId();
        escuela=ParseObject.createWithoutData("Escuela",escuelaId);
        switch (i){
            case 0:
                nombreEs.setText("Administración");
                toolbar.setBackground(getResources().getDrawable(R.drawable.accesos_header));
                arrayList=new ArrayList<>();
                papa=ParseUser.getQuery();
                papa.whereEqualTo("escuela",escuela);
                papa.whereEqualTo("usertype",0);
                papa.whereEqualTo("status",0);

                papa.findInBackground(new FindCallback<ParseUser>() {
                    @Override
                    public void done(List<ParseUser> objects, ParseException e) {

                        listaUsers.setVisibility(View.VISIBLE);
                        progressUser.setVisibility(View.GONE);
                        if (e==null){
                            for (ParseUser user:objects){
                                data=new UserData();
                                data.setObjectId(user.getObjectId());
                                data.setExiste(true);
                                data.setName(user.getString("nombre")+" "+user.getString("apellidos"));
                                data.setNombre(user.getString("nombre"));
                                data.setApellido(user.getString("apellidos"));
                                data.setUsername(user.getString("username"));
                                arrayList.add(data);
                            }

                            a=new UserAdapter(getContext(),arrayList);
                            listaUsers.setAdapter(a);

                        }else {
                            Log.d("Error",e.getMessage());
                        }
                    }
                });
                break;
            case 1:
                nombreEs.setText("Maestros");
                toolbar.setBackground(getResources().getDrawable(R.drawable.grupos_header));
                arrayList=new ArrayList<>();
                papa=ParseUser.getQuery();
                papa.whereEqualTo("escuela",escuela);
                papa.whereEqualTo("usertype",1);
                papa.whereEqualTo("status",0);
                papa.findInBackground(new FindCallback<ParseUser>() {
                    @Override
                    public void done(List<ParseUser> objects, ParseException e) {

                        listaUsers.setVisibility(View.VISIBLE);
                        progressUser.setVisibility(View.GONE);
                        if (e==null){
                            for (ParseUser user:objects){
                                data=new UserData();
                                data.setObjectId(user.getObjectId());
                                data.setExiste(true);
                                data.setName(user.getString("nombre")+" "+user.getString("apellidos"));
                                data.setNombre(user.getString("nombre"));
                                data.setApellido(user.getString("apellidos"));
                                data.setUsername(user.getString("username"));
                                arrayList.add(data);
                            }

                            a=new UserAdapter(getContext(),arrayList);
                            listaUsers.setAdapter(a);

                        }else {
                            Log.d("Error",e.getMessage());
                        }
                    }
                });
                break;
            default:
                break;

        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.crear_usuario,menu);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()){
            case R.id.crearUser:
                Fragment fragment=new NewUserFragment();
                Bundle b=new Bundle();

                b.putInt("usertype",usertype);
                fragment.setArguments(b);
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();

                break;
        }


        return super.onOptionsItemSelected(item);

    }

}
