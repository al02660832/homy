package nieucrm.nieu.mx.skola.DataModel;

/**
 * Created by mmac1 on 19/07/2017.
 */

public class ImageList {

    private String imagen,autor,path;

    public String getImagen(){
        return imagen;
    }

    public void setImagen(String imagen){
        this.imagen=imagen;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
