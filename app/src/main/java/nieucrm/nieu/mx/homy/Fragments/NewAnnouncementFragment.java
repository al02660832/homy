package nieucrm.nieu.mx.skola.Fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickResult;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import nieucrm.nieu.mx.skola.Helpers.Constants;
import nieucrm.nieu.mx.skola.Helpers.ImagePicker;
import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.Helpers.Util;
import nieucrm.nieu.mx.skola.R;

/**
 * Created by Carlos Romero
 */

public class NewAnnouncementFragment extends Fragment {

    TextView para,anuncioNuevo,alumnoAnuncio,anuncioMensaje,crearAnuncio;
    EditText mensaje;
    String contenido;

    ParseObject defaultGroup;
    String grupoNombre,groupId;

    int usertype;
    String announcementId;
    HashMap<String, Object> params;
    ParseObject announcement;
    ProgressDialog dialog;
    ParseObject tAnuncio;

    ArrayList<ParseObject>estudiantes;
    ArrayList<String>nombres;
    ArrayList<String> mSelectedItems;
    ArrayList<ParseObject>grupo;
    String [] n;

    TransferUtility utility;
    TransferObserver observe;
    String camino,pagoId;
    private static final int PICK_IMAGE_ID = 234;
    private String mPath;
    ImageButton adjuntarAnuncio;
    String escuelaId;

    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf",demibold="font/avenir-next-demi-bold.ttf";

    boolean hasPhoto;
    Activity a;


    public NewAnnouncementFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            a=(Activity) context;
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        Bundle b=this.getArguments();
        if (b!=null){
            groupId=b.getString("grupoId");
            grupoNombre=b.getString("grupoNombre");
            defaultGroup=ParseObject.createWithoutData("grupo",groupId);

        }

        utility= Util.getTransferUtility(a);

        return inflater.inflate(R.layout.fragment_new_announcement, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        anuncioNuevo=(TextView)view.findViewById(R.id.textAnuncioNuevo);
        anuncioMensaje=(TextView)view.findViewById(R.id.textAnuncioMensaje);
        crearAnuncio=(Button) view.findViewById(R.id.textCrearAnuncio);

        alumnoAnuncio=(TextView)view.findViewById(R.id.anuncioAlumno);
        para=(TextView)view.findViewById(R.id.textAnuncioPara);
        mensaje=(EditText)view.findViewById(R.id.contenidoAnuncio);

        adjuntarAnuncio=(ImageButton)view.findViewById(R.id.adjuntarAnuncio);

        adjuntarAnuncio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPicker();
            }
        });

        usertype=ParseUser.getCurrentUser().getNumber("usertype").intValue();

        if (grupoNombre==null){
            Log.e("Nombre nulo","Nulo");
        }else {
            alumnoAnuncio.setText(grupoNombre);

        }

        setTypeface();


        alumnoAnuncio.setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View v) {
                dialog=new ProgressDialog(a);
                dialog.setTitle("Cargando");
                dialog.setMessage("Por favor espere...");
                dialog.setCancelable(false);
                dialog.show();

                grupos();

            }

        });



        crearAnuncio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getTipo();

            }
        });

    }

    public void getTipo(){
        ParseQuery<ParseObject>tipos=ParseQuery.getQuery("tipoAnuncio");
        tipos.whereEqualTo("nombre","Anuncio");
        tipos.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (e==null){
                    tAnuncio=object;
                    saveAnnouncement();

                }else {
                    Toast.makeText(a,"Error, intente de nuevo", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void saveAnnouncement(){
        escuelaId=((ParseApplication)a.getApplication()).getEscuelaId();
        defaultGroup=ParseObject.createWithoutData("grupo",groupId);

        params=new HashMap<>();

        if(grupo==null){
            grupo=new ArrayList<>();
            grupo.add(defaultGroup);
        }else {
            if (grupo.size()==0){
                grupo.add(defaultGroup);
            }
        }

        contenido=mensaje.getText().toString().trim();
        announcement=new ParseObject("anuncio");
        announcement.put("autor", ParseUser.getCurrentUser());
        announcement.put("grupos",grupo);
        announcement.put("materia","General");
        announcement.put("tipo",tAnuncio);

        if (ParseUser.getCurrentUser().getNumber("usertype").intValue()!=1){
            announcement.put("aprobado",true);

        }else {
            announcement.put("aprobado",false);

        }

        announcement.put("descripcion",contenido);

        announcement.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e==null){
                    pagoId=announcement.getObjectId();

                    if (hasPhoto){
                        saveToAnuncioPhoto(announcement);
                    }

                    switch (usertype){
                        case 0:
                            //Admin
                            announcementId=announcement.getObjectId();
                            params.put("anuncioObjectId",announcementId);
                            params.put("escuelaObjId",escuelaId);
                            ParseCloud.callFunctionInBackground("adminApprovedAnuncio", params, new FunctionCallback<Object>() {

                                @Override
                                public void done(Object object, ParseException e) {
                                    if (e == null){
                                        Log.d("Cloudcode","Success");
                                    }else {
                                        Log.d("Error",e.getMessage());
                                    }
                                }
                            });
                            break;
                        case 1:
                            //Teacher
                            announcementId=announcement.getObjectId();
                            params.put("anuncioObjectId",announcementId);
                            params.put("escuelaObjId",escuelaId);
                            ParseCloud.callFunctionInBackground("teacherAnuncioToBeApproved", params, new FunctionCallback<Object>() {

                                @Override
                                public void done(Object object, ParseException e) {
                                    if (e == null){
                                        Log.d("Cloudcode","Success");
                                    }else {
                                        Log.d("Error",e.getMessage());
                                    }
                                }
                            });

                            break;
                        case 2:
                            //Parent
                            announcementId=announcement.getObjectId();

                            params.put("anuncioObjectId",announcementId);
                            params.put("escuelaObjId",escuelaId);
                            ParseCloud.callFunctionInBackground("parentAnuncioCreated", params, new FunctionCallback<Object>() {

                                @Override
                                public void done(Object object, ParseException e) {
                                    if (e == null){
                                        Log.d("Cloudcode","Success");
                                    }else {
                                        Log.d("Error",e.getMessage());
                                    }
                                }
                            });
                            break;

                        default:
                            break;
                    }
                    Toast.makeText(a,"Anuncio creado exitosamente",Toast.LENGTH_SHORT).show();
                }else {
                    Log.d("ERROR",e.getMessage());
                }
            }
        });
    }

    public void grupos(){

        nombres=new ArrayList<>();
        estudiantes=new ArrayList<>();
        grupo=new ArrayList<>();
        ParseObject g=ParseObject.createWithoutData("grupo",groupId);

        escuelaId=((ParseApplication)a.getApplication()).getEscuelaId();
        ParseObject escuela=ParseObject.createWithoutData("Escuela",escuelaId);


        ParseQuery<ParseObject>q=ParseQuery.getQuery("grupo").include("Maestros").whereEqualTo("Maestros", ParseUser.getCurrentUser());

        ParseQuery<ParseObject> est= ParseQuery.getQuery("grupo").include("Maestros").include("escuela").whereEqualTo("Maestros",ParseUser.getCurrentUser());
        est.whereEqualTo("escuela",escuela);

        est.findInBackground(new FindCallback<ParseObject>() {

            @Override

            public void done(List<ParseObject> objects, ParseException e) {
                dialog.hide();
                if (e==null){

                    for (ParseObject o:objects){

                        nombres.add(o.getString("name"));
                        estudiantes.add(o);
                    }

                }else {
                    Log.e("Error grupo",e.getMessage());
                    Toast.makeText(a,"Error",Toast.LENGTH_SHORT).show();

                }


                n=new String[nombres.size()];

                n=nombres.toArray(n);


                boolean []elementoSeleccionado=new boolean[nombres.size()];

                for(int i=0;i<elementoSeleccionado.length;i++){

                    elementoSeleccionado[i]=false;

                }


                mSelectedItems = new ArrayList<>();  // Where we track the selected items

                AlertDialog.Builder builder = new AlertDialog.Builder(a);

                // Set the dialog title

                builder.setTitle("Para: ")

                        // Specify the list array, the items to be selected by default (null for none),

                        // and the listener through which to receive callbacks when items are selected

                        .setMultiChoiceItems(n, elementoSeleccionado,

                                new DialogInterface.OnMultiChoiceClickListener() {

                                    @Override

                                    public void onClick(DialogInterface dialog, int which,

                                                        boolean isChecked) {

                                        if (isChecked) {

                                            // If the user checked the item, add it to the selected items

                                            mSelectedItems.add(n[which]);
                                            grupo.add(estudiantes.get(which));

                                        } else if (mSelectedItems.contains(n[which])) {

                                            // Else, if the item is already in the array, remove it

                                            mSelectedItems.remove(n[which]);
                                            grupo.remove(estudiantes.get(which));

                                        }

                                    }

                                })

                        // Set the action buttons

                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                            @Override

                            public void onClick(DialogInterface dialog, int id) {

                                // User clicked OK, so save the mSelectedItems results somewhere

                                // or return them to the component that opened the dialog

                                //Toast.makeText(getContext(),String.valueOf( grupo.size()),Toast.LENGTH_SHORT).show();

                                alumnoAnuncio.setText("");

                                for (int j = 0; j < mSelectedItems.size(); j++){


                                    if (j==mSelectedItems.size()-1){

                                        alumnoAnuncio.append(mSelectedItems.get(j));
                                    }else{
                                        alumnoAnuncio.append(mSelectedItems.get(j) + ", ");
                                    }
                                }
                            }

                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        })
                        .create().show();
            }

        });
    }

    public void showPicker(){
        PickImageDialog.build(new PickSetup().setCameraButtonText("Cámara").setGalleryButtonText("Galería").setTitle("Agregar foto").setCancelText("Cancelar"))
                .setOnPickResult(new IPickResult() {
                    @Override
                    public void onPickResult(PickResult r) {
                        //TODO: do what you have to...
                        if (r.getError() == null) {
                            //If you want the Uri.
                            //Mandatory to refresh image from Uri.
                            //getImageView().setImageURI(null);

                            //Setting the real returned image.
                            //getImageView().setImageURI(r.getUri());

                            //If you want the Bitmap.

                            Bitmap bitmap = r.getBitmap();
                            // TODO use bitmap

                            camino=r.getPath();
                            //imagenMensaje.setImageBitmap(bitmap);
                            hasPhoto=true;

                            //Image path
                            //r.getPath();
                        } else {
                            //Handle possible errors
                            //TODO: do what you have to do with r.getError();
                            Toast.makeText(getContext(), r.getError().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                }).show(getActivity().getSupportFragmentManager());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case PICK_IMAGE_ID:
                Bitmap bitmap = ImagePicker.getImageFromResult(a, resultCode, data);
                // TODO use bitmap



                camino=mPath;
                //imagenMensaje.setImageBitmap(bitmap);
                hasPhoto=true;
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    public void setTypeface(){
        Typeface bold=Typeface.createFromAsset(a.getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(a.getAssets(),avenirMedium);
        Typeface dem=Typeface.createFromAsset(a.getAssets(),demibold);

        para.setTypeface(medium);
        anuncioNuevo.setTypeface(medium);
        crearAnuncio.setTypeface(dem);
        alumnoAnuncio.setTypeface(medium);
        anuncioMensaje.setTypeface(medium);
    }

    public void saveToAnuncioPhoto(ParseObject anuncio){
        final ParseObject anuncioPhoto=new ParseObject("AnuncioPhoto");
        anuncioPhoto.put("anuncio",anuncio);
        anuncioPhoto.put("aws",true);
        anuncioPhoto.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e==null){
                    Log.d("AnuncioPhoto","Foto guardada");
                    attachMessage(anuncioPhoto.getObjectId());
                }else {
                    Toast.makeText(a, "Verifique su conexión a internet e intente de nuevo",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void attachMessage(String anuncioId){
        observe= utility.upload(Constants.BUCKET_NAME,anuncioId,new java.io.File(camino ));
        observe.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (TransferState.COMPLETED.equals(observe.getState())) {

                    //progressMessage.setVisibility(View.GONE);
                    getActivity().getSupportFragmentManager().popBackStack();
                    Toast.makeText(a, "Foto guardada", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                long _bytesCurrent = bytesCurrent;
                long _bytesTotal = bytesTotal;

                float percentage =  ((float)_bytesCurrent /(float)_bytesTotal * 100);
                Log.d("percentage","" +percentage);
                //progressMessage.setProgress((int) percentage);
            }

            @Override
            public void onError(int id, Exception ex) {
                Log.d("Error de Amazon",ex.getMessage());
                Toast.makeText(a,"Error al subir la foto, intente de nuevo",Toast.LENGTH_SHORT).show();
            }
        });
    }
}
