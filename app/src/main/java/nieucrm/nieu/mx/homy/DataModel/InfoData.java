package nieucrm.nieu.mx.skola.DataModel;

public class InfoData {

    String objectId, tipo, contenido;
    boolean aws;

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public boolean isAws() {
        return aws;
    }

    public void setAws(boolean aws) {
        this.aws = aws;
    }

}
