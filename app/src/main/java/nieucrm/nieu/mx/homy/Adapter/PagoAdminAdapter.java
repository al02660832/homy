package nieucrm.nieu.mx.skola.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import nieucrm.nieu.mx.skola.DataModel.PagosData;
import nieucrm.nieu.mx.skola.Fragments.PagosDetailFragment;
import nieucrm.nieu.mx.skola.R;


/**
 * Created by Carlos Romero on 10/12/2016.
 */

public class PagoAdminAdapter extends BaseAdapter {

    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf";

    String fileUri;
    String mes;

    private Context context;
    LayoutInflater inflater;
    private List<PagosData> pagosData=null;
    private ArrayList<PagosData>arrayList=null;


    public PagoAdminAdapter(Context context, List<PagosData> pagosData) {
        this.context = context;
        this.pagosData = pagosData;
        try {
            inflater=LayoutInflater.from(context);

        }catch (Exception e){
            Log.d("Error",e.getMessage());
        }
        this.arrayList=new ArrayList<>();
        this.arrayList.addAll(pagosData);
    }

    public class ViewHolder{
        TextView txtEstudiantePago,txtGrupoPago,pagoConceptoAdmin,txtFechaAdmin,txtHoraAdmin;
    }


    @Override
    public int getCount() {
        return pagosData.size();
    }

    @Override
    public Object getItem(int position) {
        return pagosData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Typeface bold=Typeface.createFromAsset(context.getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(context.getAssets(),avenirMedium);

        ViewHolder holder;
       // TextView nombreEstudiante=(TextView)v.findViewById(R.id.nombreEstudiante);

        if(convertView==null){
            holder=new ViewHolder();
            convertView=inflater.inflate(R.layout.pago_admin_item_layout,null);
            holder.txtEstudiantePago = (TextView) convertView.findViewById(R.id.txtEstudiantePago);
            holder.txtGrupoPago = (TextView) convertView.findViewById(R.id.txtGrupoPago);
            holder.pagoConceptoAdmin = (TextView) convertView.findViewById(R.id.pagoConceptoAdmin);
            holder.txtFechaAdmin= (TextView) convertView.findViewById(R.id.txtFechaAdmin);
            holder.txtHoraAdmin= (TextView) convertView.findViewById(R.id.txtHoraAdmin);

            holder.txtEstudiantePago.setTypeface(bold);
            holder.txtGrupoPago.setTypeface(medium);
            holder.pagoConceptoAdmin.setTypeface(medium);
            holder.txtFechaAdmin.setTypeface(medium);
            holder.txtHoraAdmin.setTypeface(medium);

            convertView.setTag(holder);
        }else {
            holder=(ViewHolder)convertView.getTag();
        }

        fileUri=pagosData.get(position).getFileUri();

        holder.txtEstudiantePago.setText(pagosData.get(position).getAlumno());
        holder.txtGrupoPago.setText(pagosData.get(position).getGrupo());
        holder.pagoConceptoAdmin.setText(pagosData.get(position).getConcepto());
        holder.txtFechaAdmin.setText(pagosData.get(position).getFecha());
        holder.txtHoraAdmin.setText(pagosData.get(position).getHora());


        SimpleDateFormat format = new SimpleDateFormat("MMMM",new Locale("es"));
        mes=format.format(pagosData.get(position).getMesPago());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*
                estudiante = pagado.get(position).getParseObject("student");
                nombre = estudiante.getString("NOMBRE") + " ";
                apellido = estudiante.getString("ApPATERNO");
                nombreCompleto = nombre + apellido;
                ids = pagado.get(position).getObjectId();
                concepto = pagado.get(position).getString("concepto");
                if (pagado.get(position).getParseFile("photo") == null) {

                    if (pagado.get(position).getBoolean("manual")) {
                        fileUri = "M";
                    } else {
                        if (pagado.get(position).getBoolean("aws")){
                            fileUri="A";
                        }else {
                            fileUri="N";
                        }
                    }

                } else {
                    fileUri = pagado.get(position).getParseFile("photo").getUrl();
                }

                if (pagado.get(position).getDate("fechaAdelanto")==null){
                    fechaPa=pagado.get(position).getCreatedAt();
                }else {
                    fechaPa=pagado.get(position).getDate("fechaAdelanto");
                }

                SimpleDateFormat format = new SimpleDateFormat("MMMM",new Locale("es"));
                mesPago=format.format(fechaPa);


                b = new Bundle();
                b.putString("nombreAlumno", nombreCompleto);
                b.putString("conceptoPago", concepto);
                b.putString("parseImage", fileUri);
                b.putString("oId", ids);
                b.putString("mes",mesPago);
                fragment.setArguments(b);
                 */

                Fragment fragment = new PagosDetailFragment();
                Bundle bundle=new Bundle();
                bundle.putString("parseImage",pagosData.get(position).getFileUri());
                bundle.putString("nombreAlumno",pagosData.get(position).getAlumno());
                bundle.putString("conceptoPago",pagosData.get(position).getConcepto());
                bundle.putString("oId",pagosData.get(position).getObjectId());
                bundle.putString("mes",mes);
                fragment.setArguments(bundle);
                FragmentTransaction transaction= ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
            }
        });

        return convertView;


    }

}
