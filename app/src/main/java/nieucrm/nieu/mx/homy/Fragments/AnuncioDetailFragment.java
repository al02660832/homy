package nieucrm.nieu.mx.skola.Fragments;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.List;

import nieucrm.nieu.mx.skola.Helpers.Constants;
import nieucrm.nieu.mx.skola.Helpers.Util;
import nieucrm.nieu.mx.skola.R;


/**
 * Created by Carlos Romero
 */

public class AnuncioDetailFragment extends Fragment {

    String avenirBold = "font/avenir-next-bold.ttf", avenirMedium = "font/avenir-next-medium.ttf";
    String tipo,materia,emisor,receptor,fechaCreacion,fechaEntrega,descripcion,genero,objectId;
    ImageView barraTipo;
    ImageButton imagenClip;
    ProgressDialog progressDialog;
    ProgressBar progress;

    boolean aws,anuncioPhoto;


    TextView textoMateria,txtDe,txtPara,emisorAnuncio,receptorAnuncio,txtEntregado,txtCreado,txtCreacion,txtEntrega,descripAnuncio;

    ParseObject anuncio;
    String parseUri;
    TransferUtility utility;
    TransferObserver observer;
    File imageFile;
    File outputDir;

    //Momentos
    TextView txtDesayuno,desayunoTag,txtColac,colacTag,txtComida,comidaTag,txtMerienda,meriendaTag,txtDescanso,txtDurmio,durmioTag,txtTiempo,tiempoTag,funcionesTag,
    txtAviso,avisoTag,txtPipi,pipiTag,txtPopo,popoTag,txtLeche,lecheTextView,txtComent,comentarioGeneral,txtHoraMome,hourTag;

    LinearLayout momentsLayout,layoutDesa,layoutColac,layoutComi,layoutMeri,layoutDurm,layoutTime,layoutAviso,layoutPip,layoutPop,layoutComent,layoutLeche,layoutHoraMomen;
    boolean momento,durmio,aviso;
    String desayuno="",comida="",colacion="",merienda="",tiempo="",popo="",pipi="",leche="",comentario="",horario="";

    Activity a;

    public AnuncioDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            a=(Activity) context;
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        utility= Util.getTransferUtility(a);

        return inflater.inflate(R.layout.fragment_anuncio_detail, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);

        barraTipo = (ImageView) v.findViewById(R.id.barraTipo);
        textoMateria = (TextView) v.findViewById(R.id.textoMateria);
        txtDe = (TextView) v.findViewById(R.id.txtDe);
        txtPara = (TextView) v.findViewById(R.id.txtPara);
        emisorAnuncio = (TextView) v.findViewById(R.id.emisorAnuncio);
        receptorAnuncio = (TextView) v.findViewById(R.id.receptorAnuncio);
        txtCreado = (TextView) v.findViewById(R.id.txtCreado);
        txtEntregado = (TextView) v.findViewById(R.id.txtEntregado);
        txtCreacion = (TextView) v.findViewById(R.id.txtCreacion);
        txtEntrega = (TextView) v.findViewById(R.id.txtEntrega);
        descripAnuncio = (TextView) v.findViewById(R.id.descripAnuncio);
        imagenClip=(ImageButton)v.findViewById(R.id.imagenC);

        txtDesayuno=(TextView)v.findViewById(R.id.txtDesayuno);
        desayunoTag=(TextView)v.findViewById(R.id.desayunoTag);
        txtColac=(TextView)v.findViewById(R.id.txtColac);
        colacTag=(TextView)v.findViewById(R.id.colacTag);
        txtComida=(TextView)v.findViewById(R.id.txtComida);
        comidaTag=(TextView)v.findViewById(R.id.comidaTag);
        txtMerienda=(TextView)v.findViewById(R.id.txtMerienda);
        meriendaTag=(TextView)v.findViewById(R.id.meriendaTag);
        txtDescanso=(TextView)v.findViewById(R.id.txtDescanso);
        txtDurmio=(TextView)v.findViewById(R.id.txtDurmio);
        durmioTag=(TextView)v.findViewById(R.id.durmioTag);
        txtTiempo=(TextView)v.findViewById(R.id.txtTiempo);
        tiempoTag=(TextView)v.findViewById(R.id.tiempoTag);
        funcionesTag=(TextView)v.findViewById(R.id.funcionesTag);
        txtAviso=(TextView)v.findViewById(R.id.txtAviso);
        avisoTag=(TextView)v.findViewById(R.id.avisoTag);
        txtPipi=(TextView)v.findViewById(R.id.txtPipi);
        pipiTag=(TextView)v.findViewById(R.id.pipiTag);
        txtPopo=(TextView)v.findViewById(R.id.txtPopo);
        popoTag=(TextView)v.findViewById(R.id.popoTag);
        txtLeche=(TextView)v.findViewById(R.id.txtLeche);
        lecheTextView=(TextView)v.findViewById(R.id.lecheTextView);
        txtComent=(TextView)v.findViewById(R.id.txtComent);
        comentarioGeneral=(TextView)v.findViewById(R.id.comentarioGeneral);
        txtHoraMome=(TextView)v.findViewById(R.id.txtHoraMome);
        hourTag=(TextView)v.findViewById(R.id.hourTag);

        momentsLayout=(LinearLayout)v.findViewById(R.id.momentsLayout);
        layoutDesa=(LinearLayout)v.findViewById(R.id.layoutDesa);
        layoutColac=(LinearLayout)v.findViewById(R.id.layoutColac);
        layoutComi=(LinearLayout)v.findViewById(R.id.layoutComi);
        layoutMeri=(LinearLayout)v.findViewById(R.id.layoutMeri);
        layoutDurm=(LinearLayout)v.findViewById(R.id.layoutDurm);
        layoutTime=(LinearLayout)v.findViewById(R.id.layoutTime);
        layoutAviso=(LinearLayout)v.findViewById(R.id.layoutAviso);
        layoutPip=(LinearLayout)v.findViewById(R.id.layoutPip);
        layoutPop=(LinearLayout)v.findViewById(R.id.layoutPop);
        layoutComent=(LinearLayout)v.findViewById(R.id.layoutComent);
        layoutLeche=(LinearLayout)v.findViewById(R.id.layoutLeche);
        layoutHoraMomen=(LinearLayout)v.findViewById(R.id.layoutHoraMomen);



        getData();
        setType();
        vistoAnuncio();


    }

    public void getData(){
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            genero=bundle.getString("generoAnuncio");
            tipo=bundle.getString("tipoAnuncio");
            materia=bundle.getString("materiaAnuncio");
            emisor=bundle.getString("emisorAnuncio");
            receptor=bundle.getString("receptorAnuncio");
            fechaCreacion=bundle.getString("fechaCreacion");
            fechaEntrega=bundle.getString("fechaEntrega");
            descripcion=bundle.getString("descripcionAnuncio");
            objectId=bundle.getString("objectId");

            momento=bundle.getBoolean("momento");
            durmio=bundle.getBoolean("durmio");
            aviso=bundle.getBoolean("aviso");
            desayuno=bundle.getString("desayuno");
            comida=bundle.getString("comida");
            colacion=bundle.getString("colacion");
            merienda=bundle.getString("merienda");
            tiempo=bundle.getString("tiempo");
            popo=bundle.getString("popo");
            pipi=bundle.getString("pipi");
            leche=bundle.getString("leche");
            comentario=bundle.getString("comentarios");
            horario=bundle.getString("horario");

            if (bundle.getString("parseUri")==null){
                if (bundle.getBoolean("aws")){
                    imagenClip.setVisibility(View.VISIBLE);
                    aws=bundle.getBoolean("aws");
                }else {
                    if (bundle.getBoolean("tablaAnuncio")){
                        anuncioPhoto=bundle.getBoolean("tablaAnuncio");
                        imagenClip.setVisibility(View.VISIBLE);
                    }else {
                        imagenClip.setVisibility(View.GONE);
                    }
                }

            }else {
                imagenClip.setVisibility(View.VISIBLE);

                parseUri=bundle.getString("parseUri");
            }

            if (momento){
                descripAnuncio.setVisibility(View.GONE);
                momentsLayout.setVisibility(View.VISIBLE);

                if (desayuno==null||desayuno.equals("")){
                    layoutDesa.setVisibility(View.GONE);
                }else {
                    desayunoTag.setText(desayuno);
                }

                if (comida==null||comida.equals("")){
                    layoutComi.setVisibility(View.GONE);
                }else {
                    comidaTag.setText(comida);
                }

                if (colacion==null||colacion.equals("")){
                    layoutColac.setVisibility(View.GONE);
                }else {
                    colacTag.setText(colacion);
                }

                if (merienda==null||merienda.equals("")){
                    layoutMeri.setVisibility(View.GONE);
                }else {
                    meriendaTag.setText(merienda);
                }

                if (durmio){
                    durmioTag.setText("Sí");
                }else {
                    durmioTag.setText("No");
                }

                if (tiempo==null||tiempo.equals("")){
                    layoutTime.setVisibility(View.GONE);
                }else {
                    tiempoTag.setText(tiempo+" minutos");
                }

                if (aviso){
                    avisoTag.setText("Sí");
                }else {
                    avisoTag.setText("No");
                }
                if (pipi==null||pipi.equals("")){
                    layoutPip.setVisibility(View.GONE);
                }else {
                    pipiTag.setText(pipi);
                }

                if (popo==null||popo.equals("")){
                    layoutPop.setVisibility(View.GONE);
                }else {
                    popoTag.setText(popo);

                }

                if (leche==null||leche.equals("")){
                    layoutLeche.setVisibility(View.GONE);
                }else {
                    lecheTextView.setText(leche+" ml");
                    Log.e("Valor leche",leche);
                }

                if (comentario==null||comentario.equals("")){
                    layoutComent.setVisibility(View.GONE);
                }else {
                    comentarioGeneral.setText(comentario);
                    Log.e("Valor comen",comentario);
                }

                if (horario==null||horario.equals("")){
                    layoutHoraMomen.setVisibility(View.GONE);
                }else {
                    hourTag.setText(horario);
                    Log.e("Valor hora",horario);
                }

            }else {
                descripAnuncio.setVisibility(View.VISIBLE);
                momentsLayout.setVisibility(View.GONE);
            }

            if (tipo.equals("Anuncio")){
                barraTipo.setImageDrawable(a.getResources().getDrawable(R.drawable.barra_azul_claro));
            }else {
                if (tipo.equals("Enfermería")){
                    barraTipo.setImageDrawable(a.getResources().getDrawable(R.drawable.barra_roja));

                }else {
                    if (tipo.equals("Tarea")){

                        barraTipo.setImageDrawable(a.getResources().getDrawable(R.drawable.barra_verde));
                    }
                }
            }

            if (genero.equals("M")){

                getView().setBackgroundColor(Color.parseColor("#5D9CEC"));

            }else if (genero.equals("G")){

                getView().setBackgroundColor(Color.parseColor("#50CEAE"));

            }else{
                getView().setBackgroundColor(Color.parseColor("#EC87C0"));
            }

            textoMateria.setText(materia);
            emisorAnuncio.setText(emisor);
            receptorAnuncio.setText(receptor);
            txtCreacion.setText(fechaCreacion);
            txtEntrega.setText(fechaEntrega);
            descripAnuncio.setText(descripcion);

            imagenClip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imagen();
                }
            });

        }
    }

    public void setType(){
        Typeface bold = Typeface.createFromAsset(a.getAssets(), avenirBold);
        Typeface medium = Typeface.createFromAsset(a.getAssets(), avenirMedium);
        textoMateria.setTypeface(bold);
        txtDe.setTypeface(medium);
        txtPara.setTypeface(medium);
        emisorAnuncio.setTypeface(bold);
        receptorAnuncio.setTypeface(bold);
        txtCreado.setTypeface(medium);
        txtEntregado.setTypeface(medium);
        txtCreacion.setTypeface(bold);
        txtEntrega.setTypeface(bold);
        descripAnuncio.setTypeface(medium);
    }

    public void vistoAnuncio(){
        anuncio=ParseObject.createWithoutData("anuncio",objectId);
        ParseQuery<ParseObject> q=ParseQuery.getQuery("actividad");
        q.whereEqualTo("anuncioID",anuncio);
        q.whereEqualTo("userID", ParseUser.getCurrentUser());
        q.whereEqualTo("tipo","seen");
        q.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    if (objects.size()==0){
                        //Crear un visto
                        ParseObject seen=new ParseObject("actividad");
                        seen.put("anuncioID",anuncio);
                        seen.put("userID",ParseUser.getCurrentUser());
                        seen.put("tipo","seen");
                        seen.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {

                            }
                        });
                    }else{
                        //Mensaje visto
                        //Toast.makeText(getContext(),"Visto",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    public void imagen(){

        Fragment fragment = new AnuncioPhotoFragment();
        Bundle bundle=new Bundle();
        bundle.putString("objectId",objectId);
        if (anuncioPhoto){
            bundle.putBoolean("anuncioPhoto",true);
        }else {
            if (aws){
                bundle.putBoolean("aws",true);
            }else {
                //ParseUri
                //            Picasso.with(getContext()).load(parseUri).resize(500,800).into(showImage);

                bundle.putString("parseUri",parseUri);
            }
        }
        fragment.setArguments(bundle);
        FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
        transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
        //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();



    }

}
