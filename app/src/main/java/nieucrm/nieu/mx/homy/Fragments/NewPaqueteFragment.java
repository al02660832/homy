package nieucrm.nieu.mx.skola.Fragments;


import android.app.TimePickerDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;
import com.travijuu.numberpicker.library.NumberPicker;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import info.hoang8f.android.segmented.SegmentedGroup;
import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class NewPaqueteFragment extends Fragment {
    ParseObject niv;
    int i=0;
    Bundle b;
    ArrayList<ParseObject> niveles;
    String h1,h2,horario;
    String tipo="";
    String nivel,num,horas,precio,alimentos,pronto,horaEntrada,horaSalida,nivelId,objectId;
    boolean taller,nuevo;
    RadioButton radioTiempo,radioHorario;
    SegmentedGroup.LayoutParams size = new SegmentedGroup.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

    int horasServicio;

    ParseObject escuela;

    Date dateEntrada,dateSalida;

    ParseObject pack;

    String nombrePaquete;

    Button btnGuardarPaquete;
    //Switch switchTaller;
    TextView paqueteTag,entradaTag,txtHoraEntrada,salidaTag,txtHoraSalida,
            precioTagP,prontoTag,nivelPaqueteTag;

    TextView nombrePaqueteTag, tipoTag,horaServicio;
    EditText editNombrePaquete;
    LinearLayout layoutHorario,layoutTiempo;
    NumberPicker numberHoras;

    //tallerTag;
    SegmentedGroup segmentedPaquete, segmentedTipo;
    EditText precioPaquete,prontoPago;

    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf",avenirDemibold="font/avenir-next-demi-bold.ttf";

    ArrayList<ParseObject>alime;
    ArrayList<String>nombreAlim;
    ArrayList<String> mSelectedItems;
    ArrayList<ParseObject>grupo;
    String [] n;

    String escuelaId;

    public NewPaqueteFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_new_paquete, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnGuardarPaquete = (Button) view.findViewById(R.id.btnGuardarPaquete);
       // switchTaller = (Switch) view.findViewById(R.id.switchTaller);
        paqueteTag=(TextView)view.findViewById(R.id.paqueteTag);
        entradaTag=(TextView)view.findViewById(R.id.entradaTag);
        txtHoraEntrada=(TextView)view.findViewById(R.id.txtHoraEntrada);
        txtHoraSalida=(TextView)view.findViewById(R.id.txtHoraSalida);
        salidaTag=(TextView)view.findViewById(R.id.salidaTag);
        precioTagP=(TextView)view.findViewById(R.id.precioTagP);
        precioPaquete=(EditText) view.findViewById(R.id.precioPaquete);
        prontoTag=(TextView)view.findViewById(R.id.prontoTag);
        prontoPago=(EditText) view.findViewById(R.id.prontoPago);

        nivelPaqueteTag=(TextView)view.findViewById(R.id.nivelPaqueteTag);
        //tallerTag=(TextView)view.findViewById(R.id.tallerTag);
        segmentedPaquete=(SegmentedGroup)view.findViewById(R.id.segmentedPaquete);

        segmentedTipo=(SegmentedGroup)view.findViewById(R.id.segmentedTipo);
        nombrePaqueteTag=(TextView)view.findViewById(R.id.nombrePaqueteTag);
        tipoTag=(TextView)view.findViewById(R.id.tipoTag);
        horaServicio=(TextView)view.findViewById(R.id.horaServicio);
        editNombrePaquete=(EditText)view.findViewById(R.id.editNombrePaquete);
        layoutHorario=(LinearLayout)view.findViewById(R.id.layoutHorario);
        layoutTiempo=(LinearLayout)view.findViewById(R.id.layoutTiempo);
        numberHoras=(NumberPicker)view.findViewById(R.id.numberHoras);

        radioHorario=(RadioButton)view.findViewById(R.id.radioHorario);
        radioTiempo=(RadioButton)view.findViewById(R.id.radioTiempo);


        getData();
        createRadio();

        getTypeFace();

        txtHoraEntrada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                horaEntrada();
            }
        });

        txtHoraSalida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                horaSalida();
            }
        });

        /*
        txtAlimentos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getAlimento();
            }
        });
        */

        btnGuardarPaquete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (objectId!=null){
                    //Update
                    updatePack();
                    //Toast.makeText(getContext(),"Update",Toast.LENGTH_SHORT).show();
                }else {
                    //Nuevo
                    //Toast.makeText(getContext(),"Nuevo",Toast.LENGTH_SHORT).show();
                    savePackage();
                }
            }
        });

        segmentedTipo.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.radioTiempo:
                        layoutHorario.setVisibility(View.GONE);
                        layoutTiempo.setVisibility(View.VISIBLE);
                        tipo="Tiempo";
                        break;
                    case R.id.radioHorario:
                        layoutHorario.setVisibility(View.VISIBLE);
                        layoutTiempo.setVisibility(View.GONE);
                        tipo="Horario";
                        break;
                }
            }
        });

    }

    public void getData(){
        b=this.getArguments();
        if (b!=null){

            tipo=b.getString("tipo");

            if (tipo==null){

            }else {
                if (tipo.equals("Tiempo")){
                    //Tiempo
                    horasServicio=b.getInt("horasServicio");
                }else {
                    if (tipo.equals("Horario")){
                        //Horario
                        horaEntrada=b.getString("horaEntrada");
                        horaSalida=b.getString("horaSalida");
                        horas=b.getString("horas");
                    }
                }
            }



            taller=b.getBoolean("taller");
            nivel=b.getString("nivel");
            num=b.getString("num");
            precio=b.getString("precio");
            alimentos=b.getString("alimentos");
            pronto=b.getString("pronto");
            nivelId=b.getString("nivelId");
            nuevo=b.getBoolean("nuevo");
            objectId=b.getString("objectId");


        }


        paqueteTag.setText(num);
        txtHoraEntrada.setText(horaEntrada);
        txtHoraSalida.setText(horaSalida);
        precioPaquete.setText(precio);
        prontoPago.setText(pronto);
        numberHoras.setValue(horasServicio);
        if (tipo==null){
            Log.e("Error","Sin tipo");
        }else {
            if (tipo.equals("Tiempo")){
                //Tiempo
                radioTiempo.setChecked(true);
            }else {
                if (tipo.equals("Horario")){
                    radioHorario.setChecked(true);
                }
            }
        }
        //txtAlimentos.setText(alimentos);
        //falta seleccionar el nivel;
       // switchTaller.setChecked(taller);


    }

    public void getTypeFace(){
        Typeface bold=Typeface.createFromAsset(getContext().getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(getContext().getAssets(),avenirMedium);
        Typeface demi=Typeface.createFromAsset(getContext().getAssets(),avenirDemibold);
        paqueteTag.setTypeface(demi);
        entradaTag.setTypeface(medium);
        txtHoraEntrada.setTypeface(medium);
        salidaTag.setTypeface(medium);
        txtHoraSalida.setTypeface(medium);
        precioTagP.setTypeface(medium);
        precioPaquete.setTypeface(medium);
        prontoTag.setTypeface(medium);
        prontoPago.setTypeface(medium);
        //alimentosTag.setTypeface(medium);
        //txtAlimentos.setTypeface(medium);
        nivelPaqueteTag.setTypeface(medium);
       // tallerTag.setTypeface(medium);
        btnGuardarPaquete.setTypeface(demi);
        nombrePaqueteTag.setTypeface(demi);
        tipoTag.setTypeface(demi);
        horaServicio.setTypeface(demi);
        editNombrePaquete.setTypeface(medium);

    }

    public void createRadio(){
        escuelaId=((ParseApplication)getActivity().getApplication()).getEscuelaId();
        size.weight=1f;
        niveles=new ArrayList<>();
        final Typeface medium = Typeface.createFromAsset(getActivity().getAssets(), avenirMedium);
        escuela=ParseObject.createWithoutData("Escuela",escuelaId);
        ParseQuery<ParseObject> nivel=ParseQuery.getQuery("Nivel");
        nivel.whereEqualTo("escuela",escuela);
        nivel.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e==null){
                    segmentedPaquete.setWeightSum(objects.size());
                    for (ParseObject o:objects){
                        niveles.add(o);
                        RadioButton radioButton = (RadioButton)getActivity().getLayoutInflater().inflate(R.layout.radio_button_item,null);
                        if (o==null){

                        }else {
                            if (o.getObjectId().equals(nivelId)){
                                radioButton.setChecked(true);
                                niv=o;
                            }else {
                                radioButton.setChecked(false);
                            }
                        }
                        radioButton.setText(o.getString("nombre"));
                        radioButton.setLayoutParams(size);
                        radioButton.setTextColor(getResources().getColor(R.color.white));
                        radioButton.setGravity(Gravity.CENTER);
                        radioButton.setTypeface(medium);
                        radioButton.setId(i);

                        i++;
                        segmentedPaquete.addView(radioButton);
                        segmentedPaquete.updateBackground();
                    }

                    segmentedPaquete.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                            int checkedRadioButtonId=segmentedPaquete.getCheckedRadioButtonId();
                            niv=niveles.get(checkedRadioButtonId);
                        }
                    });
                }
            }
        });

    }

    public void savePackage(){
        precio=precioPaquete.getText().toString().trim();
        pronto=prontoPago.getText().toString().trim();
        horario=h1+" - "+h2;
        horasServicio=numberHoras.getValue();
        nombrePaquete=editNombrePaquete.getText().toString().trim();
        escuelaId=((ParseApplication)getActivity().getApplication()).getEscuelaId();
        escuela=ParseObject.createWithoutData("Escuela",escuelaId);

        pack=new ParseObject("Paquetes");

        if (tipo.equals("Tiempo")){
            pack.put("horasServicio",horasServicio);
        }else {
            pack.put("horaEntrada",dateEntrada);
            pack.put("horario",horario);
            pack.put("horaSalida",dateSalida);
        }

        pack.put("tipo",tipo);
        pack.put("nivel",niv);
        //pack.put("alimentos",grupo);
        pack.put("precio",precio);
        pack.put("tallerVespertino",false);

        if (nombrePaquete.equals("")){
            pack.put("nombre",num);
        }else {
            pack.put("nombre",nombrePaquete);
        }
        pack.put("prontoPago",pronto);
        pack.put("escuela",escuela);

        pack.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e==null){
                    Log.d("Save","Success");
                }else {
                    Log.e("Error",e.getMessage());
                }
            }
        });

    }

    public void updatePack(){
        ParseQuery<ParseObject>p=ParseQuery.getQuery("Paquetes").include("alimentos").include("nivel");
        p.whereEqualTo("objectId",objectId);
        escuelaId=((ParseApplication)getActivity().getApplication()).getEscuelaId();
        escuela=ParseObject.createWithoutData("Escuela",escuelaId);

        p.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {

            if (tipo.equals("Tiempo")){
                object.put("horasServicio",horasServicio);
            }else {
                object.put("horaEntrada",dateEntrada);
                object.put("horario",horas);
                object.put("horaSalida",dateSalida);
            }

            if (nombrePaquete.equals("")){
                object.put("nombre",num);
            }else {
                object.put("nombre",nombrePaquete);
            }

            object.put("tipo",tipo);
            //object.put("alimentos",grupo);
            object.put("nivel",niv);
            object.put("precio",precio);
            object.put("tallerVespertino",false);
            object.put("nombre",num);
            object.put("prontoPago",pronto);
            object.put("escuela",escuela);

            object.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                if (e==null){
                    Log.d("Save","Success");
                }else {
                    Log.e("Error",e.getMessage());
                }
                }
            });

            }
        });
    }

    public void horaEntrada(){
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(getContext(),new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                txtHoraEntrada.setText( String.format(new Locale("es"),"%02d", selectedHour) + ":" + String.format(new Locale("es"),"%02d", selectedMinute));
                h1=String.format(new Locale("es"),"%02d", selectedHour) + ":" + String.format(new Locale("es"),"%02d", selectedMinute);
                dateEntrada= new Date();
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(dateEntrada);
                calendar.set(Calendar.HOUR_OF_DAY, selectedHour);// for hour
                calendar.set(Calendar.MINUTE, selectedMinute);// for  min
            }
        }, hour, minute,false);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    public void horaSalida(){
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(getContext(),new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                txtHoraSalida.setText( selectedHour + ":" + String.format(new Locale("es"),"%02d", selectedMinute));
                h2=String.format(new Locale("es"),"%02d", selectedHour) + ":" + String.format(new Locale("es"),"%02d", selectedMinute);

                dateSalida = new Date();
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(dateSalida);
                calendar.set(Calendar.HOUR_OF_DAY, selectedHour);// for hour
                calendar.set(Calendar.MINUTE, selectedMinute);// for  min
                //Toast.makeText(getContext(),calendar.getTime().toString(),Toast.LENGTH_SHORT).show();
            }
        }, hour, minute,false);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    /*
    public void getAlimento(){

        nombreAlim=new ArrayList<>();
        alime=new ArrayList<>();
        grupo=new ArrayList<>();
        escuelaId=((ParseApplication)getActivity().getApplication()).getEscuelaId();
        escuela=ParseObject.createWithoutData("Escuela",escuelaId);

        ParseQuery<ParseObject> est= ParseQuery.getQuery("Alimentos").whereEqualTo("escuela",escuela);

        est.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e==null){
                    for (ParseObject o:objects){
                        nombreAlim.add(o.getString("name"));
                        alime.add(o);
                    }
                }else {
                    Toast.makeText(getContext(),"Error",Toast.LENGTH_SHORT).show();
                }
                n=new String[nombreAlim.size()];
                n=nombreAlim.toArray(n);

                boolean []elementoSeleccionado=new boolean[nombreAlim.size()];
                for(int i=0;i<elementoSeleccionado.length;i++){
                    elementoSeleccionado[i]=false;
                }
                mSelectedItems = new ArrayList<>();  // Where we track the selected items
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                // Set the dialog title
                builder.setTitle("Alimentos: ")
                        // Specify the list array, the items to be selected by default (null for none),
                        // and the listener through which to receive callbacks when items are selected
                        .setMultiChoiceItems(n, elementoSeleccionado,
                                new DialogInterface.OnMultiChoiceClickListener() {

                                    @Override

                                    public void onClick(DialogInterface dialog, int which,

                                                        boolean isChecked) {
                                        if (isChecked) {
                                            // If the user checked the item, add it to the selected items
                                            mSelectedItems.add(n[which]);
                                            grupo.add(alime.get(which));
                                        } else if (mSelectedItems.contains(n[which])) {
                                            // Else, if the item is already in the array, remove it
                                            mSelectedItems.remove(n[which]);
                                            grupo.remove(alime.get(which));
                                        }
                                    }

                                })

                        // Set the action buttons

                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                            @Override

                            public void onClick(DialogInterface dialog, int id) {
                                Toast.makeText(getContext(),String.valueOf(mSelectedItems.size()),Toast.LENGTH_SHORT).show();
                                // User clicked OK, so save the mSelectedItems results somewhere
                                // or return them to the component that opened the dialo
                                txtAlimentos.setText("");
                                for (int j = 0; j < mSelectedItems.size(); j++){

                                    if (j==mSelectedItems.size()-1){

                                        txtAlimentos.append(mSelectedItems.get(j));

                                    }else{

                                        txtAlimentos.append(mSelectedItems.get(j) + ", ");

                                    }
                                }
                            }

                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                            @Override

                            public void onClick(DialogInterface dialog, int id) {
                            }

                        })

                        .create().show();

            }

        });

    }
    */

}
