package nieucrm.nieu.mx.skola.Fragments;


import android.content.Context;
import android.graphics.Typeface;
import android.media.Image;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

import java.util.List;

import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class SubscriptionFragment extends Fragment {

    ParseObject escuela;
    TextView tuSubs,txtCantidad;
    String escuelaId;
    Context context;
    Fragment fragment;
    LinearLayout layoutPlan;
    ProgressBar progressSub;
    String avenirBold = "font/avenir-next-bold.ttf", avenirMedium = "font/avenir-next-medium.ttf",avenirDemi = "font/avenir-next-demi-bold.ttf";
    Button cancelarBtn;
    ImageButton historialBtn,cambiarBtn;
    ImageView planBack;
    String selectedPlanId, openPaySubscripcionPlan,amount;


    public SubscriptionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        context=container.getContext();
        return inflater.inflate(R.layout.fragment_subscription, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        layoutPlan=(LinearLayout)view.findViewById(R.id.layoutPlan);
        tuSubs=(TextView)view.findViewById(R.id.txtTuSubs);
        txtCantidad=(TextView)view.findViewById(R.id.txtCantidad);
        progressSub=(ProgressBar)view.findViewById(R.id.progressSubs);
        cancelarBtn=(Button)view.findViewById(R.id.cancelBtn);
        historialBtn=(ImageButton)view.findViewById(R.id.historialBtn);
        cambiarBtn=(ImageButton)view.findViewById(R.id.cambioBtn);
        planBack=(ImageView)view.findViewById(R.id.planBack);

        setTypeface();
        getCurrentPlan();
    }

    public void getCurrentPlan(){
        escuelaId=((ParseApplication)getActivity().getApplication()).getEscuelaId();
        escuela=ParseObject.createWithoutData("Escuela",escuelaId);
        ParseQuery<ParseObject> subscripcion=ParseQuery.getQuery("Subscripcion");
        subscripcion.whereEqualTo("escuela",escuela);
        subscripcion.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (e==null){
                    if (object==null){
                        Toast.makeText(context,"Error, intente de nuevo",Toast.LENGTH_SHORT).show();

                    }else {
                        selectedPlanId=object.getString("selectedPlanId");
                        getSubscripcionPlan(selectedPlanId);
                    }
                }else {
                    progressSub.setVisibility(View.GONE);
                    Log.i("Subscripcion","No tiene");
                    Log.e("Error1",e.getMessage());

                    fragment=new PlanFragment();
                    FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                    transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                    transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();

                }
            }
        });

    }

    public void getSubscripcionPlan(String planId){
        ParseQuery<ParseObject>op=ParseQuery.getQuery("OPSubscripcionPlan");
        op.whereEqualTo("openPaySubscripcionId",planId);
        op.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                progressSub.setVisibility(View.GONE);
                if (e==null){
                    layoutPlan.setVisibility(View.VISIBLE);
                    openPaySubscripcionPlan=object.getString("nombre");
                    amount="$"+object.getString("amount");
                    tuSubs.setText(openPaySubscripcionPlan);
                    txtCantidad.setText(amount);
                }else {
                    Log.e("Error",e.getMessage());
                    fragment=new PlanFragment();
                    FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                    transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                    transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                }
            }
        });
    }

    public void setTypeface(){
        Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), avenirBold);
        Typeface medium = Typeface.createFromAsset(getActivity().getAssets(), avenirMedium);
        Typeface demi=Typeface.createFromAsset(getActivity().getAssets(), avenirDemi);

        tuSubs.setTypeface(demi);
        txtCantidad.setTypeface(bold);
        cancelarBtn.setTypeface(demi);


    }


}
