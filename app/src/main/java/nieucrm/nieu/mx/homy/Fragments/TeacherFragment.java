package nieucrm.nieu.mx.skola.Fragments;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import nieucrm.nieu.mx.skola.R;

/**
 * Created by Carlos Romero
 */

public class TeacherFragment extends Fragment {

    TextView textGrupo,textEvento,textAdminT,textComunT;
    RelativeLayout btnGrupo,btnAdmin,btnComun,btnEvent;
    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf";
    Fragment fragment=null;
    ParseObject escuela;
    String escuelaId="",nombre;
    TextView nombreEs;
    ImageView imagenAction,imageView;
    int usertype;


    //Change Icon
    String activeName;
    List<String> disableNames=new ArrayList<>();


    public TeacherFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_teacher, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);
        usertype= ParseUser.getCurrentUser().getNumber("usertype").intValue();

        textGrupo=(TextView)v.findViewById(R.id.textGroup);
        textEvento=(TextView)v.findViewById(R.id.textCalendar);
        textAdminT=(TextView)v.findViewById(R.id.textAdministracion);
        textComunT=(TextView)v.findViewById(R.id.textComunicacion);
        nombreEs = (TextView) getActivity().findViewById(R.id.textAction);
        imagenAction=(ImageView)getActivity().findViewById(R.id.imagenAction);


        NavigationView navigationView = (NavigationView) getActivity().findViewById(R.id.nav_view);
        View vi=navigationView.getHeaderView(0);


        imageView=(ImageView)vi.findViewById(R.id.imageView);


        btnGrupo=(RelativeLayout)v.findViewById(R.id.btnGrupos);
        btnAdmin=(RelativeLayout)v.findViewById(R.id.btnAdminT);
        btnComun=(RelativeLayout)v.findViewById(R.id.btnComunicacion);
        btnEvent=(RelativeLayout)v.findViewById(R.id.btnEventosT);


        ParseQuery<ParseUser> userQuery=ParseUser.getQuery().include("escuela");
        userQuery.whereEqualTo("objectId",ParseUser.getCurrentUser().getObjectId());
        userQuery.getFirstInBackground(new GetCallback<ParseUser>() {
            @Override
            public void done(ParseUser object, ParseException e) {
                if(e==null) {

                    if (object.getParseObject("escuela")==null){

                    }else {
                        escuela = object.getParseObject("escuela");
                        escuelaId=escuela.getObjectId();
                        nombre=escuela.getString("nombre");

                        //changeImage();
                    }

                    //Toast.makeText(getContext(),escuela.getObjectId(),Toast.LENGTH_SHORT).show();
                    //Toast.makeText(getApplicationContext(),escuela.getObjectId(),Toast.LENGTH_SHORT).show();

                } else {
                    Log.e("Error",e.getMessage());

                }
            }
        });





        setHasOptionsMenu(false);
        setClickListener();
    }

    public void setClickListener(){
        btnAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment=new AdministrationFragment();
                Bundle b=new Bundle();
                b.putString("escuela",escuelaId);
                fragment.setArguments(b);
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                getActivity().getSupportFragmentManager().executePendingTransactions();

                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
            }
        });

        btnEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment=new EventsFragment();
                Bundle bundle=new Bundle();
                bundle.putString("color","azul");
                bundle.putString("escuela",escuelaId);
                fragment.setArguments(bundle);
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                getActivity().getSupportFragmentManager().executePendingTransactions();

                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
            }
        });

        btnGrupo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment=new GroupFragment();
                Bundle b=new Bundle();
                b.putString("escuela",escuelaId);
                fragment.setArguments(b);
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                getActivity().getSupportFragmentManager().executePendingTransactions();

                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();

            }
        });

        btnComun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment=new MensajesAdminFragment();
                Bundle b=new Bundle();
                b.putString("escuela",escuelaId);
                fragment.setArguments(b);
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                getActivity().getSupportFragmentManager().executePendingTransactions();


                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();

            }
        });

    }

    public void setTypeface(){
        Typeface bold=Typeface.createFromAsset(getActivity().getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(getActivity().getAssets(),avenirMedium);
        textComunT.setTypeface(medium);
        textAdminT.setTypeface(medium);
        textEvento.setTypeface(medium);
        textGrupo.setTypeface(medium);
    }

    /*
    private void changeImage(){
        if (nombre.equals("Baúl Azúl Cancún")){
            nombreEs.setText(nombre);


        }

        if (nombre.equals("Skola México")){

            nombreEs.setText(nombre);

        }
        if (nombre.equals("BabyBoomers Metepec")){
            nombreEs.setText(nombre);
        }

        if (nombre.equals("BabyBoomers Capultitlán")){
            nombreEs.setText(nombre);
        }

        if (nombre.equals("BabyBoomers Colón")){
            nombreEs.setText(nombre);
        }

        if (nombre.equals("Moms and Tots Metepec")){
            nombreEs.setText(nombre);
        }

        if (nombre.equals("Moms and Tots Toluca")){
            nombreEs.setText(nombre);
        }

        if (nombre.equals("Chibolines")){
            nombreEs.setText(nombre);
        }

        if (nombre.equals("Little Feet")){
            nombreEs.setText(nombre);
        }

        if (nombre.equals("Homy")){
            nombreEs.setText(nombre);
        }

        if (nombre.equals("Colegio del Ángel")){
            nombreEs.setText(nombre);
        }

















    }
    */

}
