package nieucrm.nieu.mx.skola.Fragments;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class NewEventFragment extends Fragment {

    DatePickerDialog.OnDateSetListener dates;
    Calendar myCalendar = Calendar.getInstance();
    String escuelaId,eventoId;

    HashMap<String, Object> params;

    Calendar date;


    ParseObject evento;

    String lugar,nombre,descripcion;
    Date fecha;

    Button crearEvento;
    TextView txtEventoNuevo,txtNombreEvento,txtFecha,eventoFecha,txtLugar,txtDescrip;
    EditText editNombreEven,editLugar,contenidoEvento;

    ProgressDialog dialog;

    String avenirBold = "font/avenir-next-bold.ttf", avenirMedium = "font/avenir-next-medium.ttf",avenirDemi="font/avenir-next-demi-bold.ttf";

    Activity a;

    public NewEventFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            a=(Activity) context;
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_new_event, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);

        txtEventoNuevo=(TextView)v.findViewById(R.id.txtEventoNuevo);
        txtNombreEvento=(TextView)v.findViewById(R.id.txtNombreEvento);
        txtFecha=(TextView)v.findViewById(R.id.txtFecha);
        eventoFecha=(TextView)v.findViewById(R.id.eventoFecha);
        txtLugar=(TextView)v.findViewById(R.id.txtLugar);
        txtDescrip=(TextView)v.findViewById(R.id.txtDescrip);

        editNombreEven=(EditText)v.findViewById(R.id.editNombreEven);
        editLugar=(EditText)v.findViewById(R.id.editLugar);
        contenidoEvento=(EditText)v.findViewById(R.id.contenidoEvento);

        crearEvento=(Button)v.findViewById(R.id.crearEvento);

        eventoFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFecha();
            }
        });

        crearEvento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveEvento();
            }
        });

    }

    public void setFecha(){

        final Calendar currentDate=Calendar.getInstance();
        date=Calendar.getInstance();
        new DatePickerDialog(a, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                date.set(year,month,dayOfMonth);
                new TimePickerDialog(a, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        date.set(Calendar.HOUR_OF_DAY,hourOfDay);
                        date.set(Calendar.MINUTE,minute);
                        Log.v("Date", "The choosen one " + date.getTime());
                        String myFormat = "dd/MMMM/yyyy"; //In which you need put here
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, new Locale("es"));
                        eventoFecha.setText(sdf.format(date.getTime()));

                    }
                },currentDate.get(Calendar.HOUR_OF_DAY),currentDate.get(Calendar.MINUTE),false).show();

            }
        },currentDate.get(Calendar.YEAR),currentDate.get(Calendar.MONTH),currentDate.get(Calendar.DATE)).show();

        /*
        dates = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        eventoFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(getActivity(), dates, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        */

    }

    public void saveEvento(){

        params=new HashMap<>();

        dialog = new ProgressDialog(a);
        dialog.setTitle("Cargando");
        dialog.setMessage("Por favor espere");
        dialog.setCancelable(false);
        dialog.show();



        evento=new ParseObject("evento");
        escuelaId=((ParseApplication)a.getApplication()).getEscuelaId();
        ParseObject escuela=ParseObject.createWithoutData("Escuela",escuelaId);


        nombre=editNombreEven.getText().toString().trim();
        lugar=editLugar.getText().toString().trim();
        descripcion=contenidoEvento.getText().toString().trim();
        fecha=date.getTime();


        evento.put("nombre",nombre);
        evento.put("lugar",lugar);
        evento.put("fecha",fecha);
        evento.put("descripcion",descripcion);
        evento.put("escuela",escuela);

        evento.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                dialog.hide();
                if (e==null){
                    Toast.makeText(a,"Evento creado exitosamente",Toast.LENGTH_SHORT).show();
                    eventoId=evento.getObjectId();
                    params.put("eventoNombre",nombre);
                    params.put("escuelaObjId",escuelaId);
                    ParseCloud.callFunctionInBackground("eventoParentNotification", params, new FunctionCallback<Object>() {
                        @Override
                        public void done(Object object, ParseException e) {
                            if (e == null){
                                Log.d("Cloudcode","Success");
                            }else {
                                Log.d("Error",e.getMessage());
                            }
                        }
                    });
                    getActivity().getSupportFragmentManager().popBackStack();

                    Log.d("Evento","Evento creado");
                }else{
                    Log.d("Evento","Error");
                }
            }
        });

    }

    public void setTypeface(){
        Typeface bold = Typeface.createFromAsset(a.getAssets(), avenirBold);
        Typeface medium = Typeface.createFromAsset(a.getAssets(), avenirMedium);
        Typeface demi=Typeface.createFromAsset(a.getAssets(),avenirDemi);

        txtEventoNuevo.setTypeface(medium);
        txtNombreEvento.setTypeface(medium);
        txtFecha.setTypeface(medium);
        eventoFecha.setTypeface(medium);
        txtLugar.setTypeface(medium);
        txtDescrip.setTypeface(medium);
        editNombreEven.setTypeface(medium);
        editLugar.setTypeface(medium);
        contenidoEvento.setTypeface(medium);

        crearEvento.setTypeface(demi);


    }

}
