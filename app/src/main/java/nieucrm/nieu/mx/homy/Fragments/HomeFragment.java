package nieucrm.nieu.mx.skola.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;

/**
 * Created by Carlos Romero
 */

public class HomeFragment extends Fragment {

    Fragment fragment;
    private ImageButton btnEstudiantes, btnEventos,btnAdministracion,btnInformacion;
    ParseObject escuela;
    String escuelaId,nombre;
    TextView nombreEs;
    ImageView imagenAction,imageView;

    Activity a;



    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            a=(Activity) context;
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //nombre=((ParseApplication)getActivity().getApplication()).getEscuelaNombre();

        return inflater.inflate(R.layout.fragment_home, container, false);

    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);


        btnEstudiantes=(ImageButton)v.findViewById(R.id.btnEstudiantes);
        btnEventos=(ImageButton)v.findViewById(R.id.btnEventos);
        btnAdministracion=(ImageButton)v.findViewById(R.id.btnAdmin);
        btnInformacion=(ImageButton)v.findViewById(R.id.btnInfo);
        //nombreEs = (TextView) getActivity().findViewById(R.id.textAction);
        imagenAction=(ImageView)a.findViewById(R.id.imagenAction);

        NavigationView navigationView = (NavigationView) a.findViewById(R.id.nav_view);
        View vi=navigationView.getHeaderView(0);

        imageView=(ImageView)vi.findViewById(R.id.imageView);


        //nombreEs.setText(nombre);


        if (ParseUser.getCurrentUser()==null){
            Log.d("Error de usuario","Usuario nulo");
        }else {
            ParseQuery<ParseUser> userQuery=ParseUser.getQuery().include("escuela");
            userQuery.whereEqualTo("objectId",ParseUser.getCurrentUser().getObjectId());
            userQuery.getFirstInBackground(new GetCallback<ParseUser>() {
                @Override
                public void done(ParseUser object, ParseException e) {
                    if(e==null) {

                        if (object.getParseObject("escuela")!=null){
                            escuela = object.getParseObject("escuela");
                            escuelaId=escuela.getObjectId();
                        }else {
                            Toast.makeText(a,"El usuario carece de escuela establecida",Toast.LENGTH_SHORT).show();
                        }

                        //nombre=escuela.getString("nombre");



                        // changeImage();

                        //Toast.makeText(getContext(),escuela.getObjectId(),Toast.LENGTH_SHORT).show();
                        //Toast.makeText(getApplicationContext(),escuela.getObjectId(),Toast.LENGTH_SHORT).show();

                    } else {
                        Log.e("Error",e.getMessage());

                    }
                }
            });
        }


        setActions();



    }

    private void setActions(){

        btnEstudiantes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment=new EstudiantesFragment();
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();

                //getActivity().getSupportFragmentManager().beginTransaction();
                getActivity().getSupportFragmentManager().executePendingTransactions();

            }
        });

        btnEventos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragment=new EventsFragment();
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                getActivity().getSupportFragmentManager().executePendingTransactions();

            }
        });

        btnAdministracion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragment=new AdministrationFragment();
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                getActivity().getSupportFragmentManager().executePendingTransactions();

            }
        });

        btnInformacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                fragment=new NoticeFragment();
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                getActivity().getSupportFragmentManager().executePendingTransactions();

            }
        });
    }

    private void changeTitle(){
       // nombreEs.setText(nombre);
    }

    /*
    private void changeImage(){
        if (nombre.equals("Baúl Azúl Cancún")){
            nombreEs.setText(nombre);


        }

        if (nombre.equals("Skola México")){

            nombreEs.setText(nombre);


        }
        if (nombre.equals("BabyBoomers Metepec")){
            nombreEs.setText(nombre);
        }

        if (nombre.equals("BabyBoomers Capultitlán")){
            nombreEs.setText(nombre);
        }

        if (nombre.equals("BabyBoomers Colón")){
            nombreEs.setText(nombre);
        }

        if (nombre.equals("Moms and Tots Metepec")){
            nombreEs.setText(nombre);
        }

        if (nombre.equals("Moms and Tots Toluca")){
            nombreEs.setText(nombre);
        }


        if (nombre.equals("Chibolines")){
            nombreEs.setText(nombre);
        }

        if (nombre.equals("Little Feet")){
            nombreEs.setText(nombre);
        }

        if (nombre.equals("Homy")){
            nombreEs.setText(nombre);
        }

        if (nombre.equals("Colegio del Ángel")){
            nombreEs.setText(nombre);
        }

    }
    */


}
