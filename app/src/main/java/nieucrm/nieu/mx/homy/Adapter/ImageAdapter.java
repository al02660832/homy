package nieucrm.nieu.mx.skola.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import nieucrm.nieu.mx.skola.DataModel.ImageData;
import nieucrm.nieu.mx.skola.Fragments.SingleItemFragment;
import nieucrm.nieu.mx.skola.Helpers.Constants;
import nieucrm.nieu.mx.skola.Helpers.Util;
import nieucrm.nieu.mx.skola.R;

/**
 * Created by mmac1 on 04/07/2017.
 */

public class ImageAdapter extends BaseAdapter {

    Context context;
    File file;

    TransferUtility utility;

    File outputDir;
    File imageFile;

    TransferObserver observer;
    Bitmap bitmap;

    LayoutInflater inflater;
    private List<ImageData> imageArrayList = null;
    private ArrayList<ImageData> arrayList;


    public ImageAdapter(Context context, List<ImageData> imageArrayList) {
        this.context = context;
        this.imageArrayList = imageArrayList;

        //S3Object//InputStream content=Util.getS3Client(context).getObject(Constants.BUCKET_NAME)
        try {
            inflater = LayoutInflater.from(context);
            //utility= Util.getTransferUtility(context);


        } catch (Exception e) {
            Log.d("Error", e.getMessage());
        }



        this.arrayList = new ArrayList<>();
        this.arrayList.addAll(imageArrayList);

        //imageLoader = new ImageLoader(context);
    }

    public class ViewHolder {

        TextView autor;
        ImageView photo;


    }

    /**
     * How many items are in the data set represented by this Adapter.
     *
     * @return Count of items.
     */
    @Override
    public int getCount() {
        return imageArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return imageArrayList.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {

        return getCount();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.grid_item_layout, null);
            holder.photo = (ImageView) convertView.findViewById(R.id.gridImage);
            holder.autor = (TextView) convertView.findViewById(R.id.autorImagen);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        downloadPhoto(imageArrayList.get(position).getImagen(),holder.photo);





        //new S3DownloadUtil().download(imageArrayList.get(position).getImagen(), new File(imageFile.toString()));




        holder.autor.setText(imageArrayList.get(position).getAutor());


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new SingleItemFragment();
                Bundle b = new Bundle();
                b.putString("image", imageArrayList.get(position).getImagen());
                fragment.setArguments(b);
                /*
                try {
                    deleteCache(context);
                } catch (Exception e) {
                    Log.d("Exception", e.getMessage());
                }
                */
                FragmentTransaction transaction = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                transaction.replace(R.id.content_home, fragment, "fragment").addToBackStack(null).commit();


            }
        });


        return convertView;
    }


    private void downloadPhoto(String objectId, final ImageView i){
        outputDir = context.getCacheDir();
        utility= Util.getTransferUtility(context);

        try {
            imageFile= File.createTempFile(objectId, ".jpeg", outputDir);
        } catch (IOException e) {
            e.printStackTrace();
        }
        observer=utility.download(Constants.BUCKET_NAME,objectId,imageFile);
        observer.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                Log.d("Estatus:",state.toString());

                if (TransferState.COMPLETED.equals(observer.getState())) {

                    Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
                    //fotoPadre.setImageBitmap(bitmap);
                    //progress.setVisibility(View.GONE);
                    i.setImageBitmap(bitmap);
                    notifyDataSetChanged();
                    try {
                        Log.d("Status foto","Lista");
                        //Toast.makeText(context, "Foto lista", Toast.LENGTH_SHORT).show();
                    }catch (Exception e1){
                        Log.e("Exception",e1.getMessage());
                    }
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                long _bytesCurrent = bytesCurrent;
                long _bytesTotal = bytesTotal;

                float percentage =  ((float)_bytesCurrent /(float)_bytesTotal * 100);
                Log.d("percentage","" +percentage);

            }

            @Override
            public void onError(int id, Exception ex) {

                Log.d("Error al descargar", ex.getMessage());
            }
        });
    }


}
