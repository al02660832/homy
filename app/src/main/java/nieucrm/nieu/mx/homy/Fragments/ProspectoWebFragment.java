package nieucrm.nieu.mx.skola.Fragments;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.api.client.util.StringUtils;

import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProspectoWebFragment extends Fragment {

    TextView tituloPros, textFechaP, fechaProspecto,horaProspecto, txtHoraP,txtNombreP,nombreProsTxt,txtEdadP,
            edadProspecto,fechaProsTxt,txtPreguntas,preguntasProspecto,txtSeguimiento,txtMailBtn,txtNumeroBtn ;
    ImageButton btnNumero,btnMail;
    Button btnGuardarPros;
    EditText editSeguimiento;
    String fechaCreacion,horaCreacion, nombre,edad,ingreso,pregunta,telefono,mail;

    String avenirBold = "font/avenir-next-bold.ttf", avenirMedium = "font/avenir-next-medium.ttf",demibold="font/avenir-next-demi-bold.ttf";



    public ProspectoWebFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_prospecto_web, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tituloPros=(TextView)view.findViewById(R.id.tituloPros);
        textFechaP=(TextView)view.findViewById(R.id.textFechaP);
        fechaProspecto=(TextView)view.findViewById(R.id.fechaProspecto);
        horaProspecto=(TextView)view.findViewById(R.id.horaProspecto);
        txtHoraP=(TextView)view.findViewById(R.id.txtHoraP);
        txtNombreP=(TextView)view.findViewById(R.id.txtNombreP);
        nombreProsTxt=(TextView)view.findViewById(R.id.nombreProsTxt);
        txtEdadP=(TextView)view.findViewById(R.id.txtEdadP);
        edadProspecto=(TextView)view.findViewById(R.id.edadProspecto);
        fechaProsTxt=(TextView)view.findViewById(R.id.fechaProsTxt);
        txtPreguntas=(TextView)view.findViewById(R.id.txtPreguntas);
        preguntasProspecto=(TextView)view.findViewById(R.id.preguntasProspecto);
        txtSeguimiento=(TextView)view.findViewById(R.id.txtSeguimiento);
        txtMailBtn=(TextView)view.findViewById(R.id.txtMailBtn);
        txtNumeroBtn=(TextView)view.findViewById(R.id.txtNumeroBtn);

        btnNumero=(ImageButton) view.findViewById(R.id.btnNumero);
        btnMail=(ImageButton) view.findViewById(R.id.btnMail);

        btnGuardarPros=(Button) view.findViewById(R.id.btnGuardarPros);

        editSeguimiento=(EditText) view.findViewById(R.id.editSeguimiento);

        setTypeface();

        Bundle b=this.getArguments();
        fechaCreacion=b.getString("fechaCreacion");
        horaCreacion=b.getString("horaCreacion");
        nombre= b.getString("nombre");
        edad=b.getString("edad");
        ingreso=b.getString("ingreso");
        pregunta=b.getString("pregunta");
        telefono=b.getString("telefono");
        mail=b.getString("mail");

        fechaProspecto.setText(org.apache.commons.lang3.StringUtils.capitalize(fechaCreacion));
        horaProspecto.setText(horaCreacion);
        nombreProsTxt.setText(nombre);
        edadProspecto.setText(edad);
        fechaProsTxt.setText(ingreso);
        preguntasProspecto.setText(pregunta);
        txtMailBtn.setText(mail);
        txtNumeroBtn.setText(telefono);


    }

    private void setTypeface(){
        Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), avenirBold);
        Typeface medium = Typeface.createFromAsset(getActivity().getAssets(), avenirMedium);
        Typeface dem=Typeface.createFromAsset(getActivity().getAssets(),demibold);

        tituloPros.setTypeface(bold);
        textFechaP.setTypeface(medium);
        fechaProspecto.setTypeface(medium);
        horaProspecto.setTypeface(medium);
        txtHoraP.setTypeface(medium);
        txtNombreP.setTypeface(medium);
        nombreProsTxt.setTypeface(medium);
        txtEdadP.setTypeface(medium);
        edadProspecto.setTypeface(medium);
        fechaProsTxt.setTypeface(medium);
        txtPreguntas.setTypeface(medium);
        preguntasProspecto.setTypeface(medium);
        txtSeguimiento.setTypeface(dem);
        txtMailBtn.setTypeface(dem);
        txtNumeroBtn.setTypeface(dem);
        btnGuardarPros.setTypeface(dem);
    }


}
