package nieucrm.nieu.mx.skola.Fragments;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

import nieucrm.nieu.mx.skola.Adapter.EstudianteAdapter;
import nieucrm.nieu.mx.skola.DataModel.EstudianteData;
import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class EstudiantesAdminFragment extends Fragment {

    ListView listaEstudiante;
    ProgressBar progressEstudiante;
    EstudianteAdapter adapter;
    ParseQuery<ParseObject>est;
    ArrayList<EstudianteData>arrayList;
    EstudianteData estudiante;
    String escuelaId;
    ParseObject escuela;
    Activity a;


    public EstudiantesAdminFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            a=(Activity) context;
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_estudiantes_admin, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        listaEstudiante=(ListView)view.findViewById(R.id.listaEstudiante);
        progressEstudiante=(ProgressBar)view.findViewById(R.id.progressEstudiante);

        getEstudiantes();



    }

    public void getEstudiantes(){
        escuelaId=((ParseApplication)a.getApplication()).getEscuelaId();
        escuela=ParseObject.createWithoutData("Escuela",escuelaId);
        arrayList=new ArrayList<>();
        est=ParseQuery.getQuery("Estudiantes").include("PersonasAutorizadas").include("grupo").whereEqualTo("status",0);
        est.whereEqualTo("escuela",escuela);
        est.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                progressEstudiante.setVisibility(View.GONE);
                listaEstudiante.setVisibility(View.VISIBLE);
                if (e==null){
                    for (ParseObject o:objects) {
                        estudiante = new EstudianteData();
                        estudiante.setIdEstudiante(o.getObjectId());
                        estudiante.setGenero(o.getString("GENERO"));
                        ParseObject gObject = o.getParseObject("grupo");
                        if (gObject==null){
                            estudiante.setGrupo("Grupo");
                        }else {
                            estudiante.setGrupo(gObject.getString("name"));
                        }

                        //listaId.add(eId);

                        /*
                        numMensajes.whereEqualTo("canal",eId);
                        numMensajes.getFirstInBackground(new GetCallback<ParseObject>() {
                            @Override
                            public void done(ParseObject object, ParseException e) {
                                estudiante.setNoMensajes(object.getNumber("count").intValue());
                            }
                        });
                         */





                        estudiante.setNombreEstudiante(o.getString("NOMBRE")+" "+o.getString("ApPATERNO"));

                        arrayList.add(estudiante);
                    }
                    adapter=new EstudianteAdapter(a,arrayList);
                    listaEstudiante.setAdapter(adapter);
                }else {
                    Log.d("Error",e.getMessage());
                }
            }
        });
    }
}
