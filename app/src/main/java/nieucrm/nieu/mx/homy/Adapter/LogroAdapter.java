package nieucrm.nieu.mx.skola.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import nieucrm.nieu.mx.skola.DataModel.LogroData;
import nieucrm.nieu.mx.skola.Fragments.SingleItemFragment;
import nieucrm.nieu.mx.skola.R;

/**
 * Created by mmac1 on 04/07/2017.
 */

public class LogroAdapter extends BaseAdapter{

    Context context;
    LayoutInflater inflater;
    private List<LogroData>logroList=null;
    private ArrayList<LogroData>arrayList;

    public LogroAdapter(Context context, List<LogroData>logroList){
        this.context=context;
        this.logroList=logroList;
        try {
            inflater=LayoutInflater.from(context);

        }catch (Exception e){
            Log.d("Error",e.getMessage());
        }
        this.arrayList=new ArrayList<LogroData>();
        this.arrayList.addAll(logroList);
        //imageLoader=new ImageLoader(context);
    }

    public class ViewHolder{
        ImageView photo;
        TextView nombre;
    }

    /**
     * How many items are in the data set represented by this Adapter.
     *
     * @return Count of items.
     */
    @Override
    public int getCount() {
        return logroList.size();
    }

    /**
     * Get the data item associated with the specified position in the data set.
     *
     * @param position Position of the item whose data we want within the adapter's
     *                 data set.
     * @return The data at the specified position.
     */
    @Override
    public Object getItem(int position) {
        return logroList.get(position);
    }

    /**
     * Get the row id associated with the specified position in the list.
     *
     * @param position The position of the item within the adapter's data set whose row id we want.
     * @return The id of the item at the specified position.
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * Get a View that displays the data at the specified position in the data set. You can either
     * create a View manually or inflate it from an XML layout file. When the View is inflated, the
     * parent View (GridView, ListView...) will apply default layout parameters unless you use
     * {@link LayoutInflater#inflate(int, ViewGroup, boolean)}
     * to specify a root view and to prevent attachment to the root.
     *
     * @param position    The position of the item within the adapter's data set of the item whose view
     *                    we want.
     * @param convertView The old view to reuse, if possible. Note: You should check that this view
     *                    is non-null and of an appropriate type before using. If it is not possible to convert
     *                    this view to display the correct data, this method can create a new view.
     *                    Heterogeneous lists can specify their number of view types, so that this View is
     *                    always of the right type (see {@link #getViewTypeCount()} and
     *                    {@link #getItemViewType(int)}).
     * @param parent      The parent that this view will eventually be attached to
     * @return A View corresponding to the data at the specified position.
     */
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if( convertView==null){
            holder=new ViewHolder();
            convertView=inflater.inflate(R.layout.logro_item_layout,null);
            holder.photo=(ImageView)convertView.findViewById(R.id.logroImagen);
            holder.nombre=(TextView)convertView.findViewById(R.id.logroNombre);
            convertView.setTag(holder);
        }else {
            holder=(ViewHolder)convertView.getTag();
        }

        //imageLoader.DisplayImage(logroList.get(position).getImagenLogro(),holder.photo);
        holder.nombre.setText(logroList.get(position).getNombre());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new SingleItemFragment();
                Bundle b=new Bundle();
                b.putString("image",logroList.get(position).getImagenLogro());
                b.putString("name",logroList.get(position).getNombre());
                fragment.setArguments(b);
                FragmentTransaction transaction= ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();


            }
        });

        return convertView;
    }







}
