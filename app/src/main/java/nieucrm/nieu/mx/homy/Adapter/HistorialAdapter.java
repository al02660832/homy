package nieucrm.nieu.mx.skola.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;



import java.util.ArrayList;
import java.util.List;

import nieucrm.nieu.mx.skola.DataModel.MensajeData;
import nieucrm.nieu.mx.skola.Fragments.HistorialDetailFragment;

import nieucrm.nieu.mx.skola.R;


/**
 * Created by Carlos Romero on 14/03/2017.
 */

public class HistorialAdapter extends BaseAdapter {

    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf",avenirDemibold="font/avenir-next-demi-bold.ttf";



    private Context context;
    LayoutInflater inflater;
    String obId;
    private List<MensajeData> mensajeData=null;
    private ArrayList<MensajeData> array=null;

    public HistorialAdapter(Context context, List<MensajeData> mensajeData) {
        this.context = context;
        this.mensajeData=mensajeData;
        try {
            inflater=LayoutInflater.from(context);

        }catch (Exception e){
            Log.d("Error",e.getMessage());
        }
        this.array=new ArrayList<>();
        this.array.addAll(mensajeData);
    }

    public class ViewHolder{
        TextView horaHistorial;
        TextView fechaHistorial;
        TextView creadoHistorial;
        TextView emisorHistorial;
        TextView paraHistorial;
        TextView receptorHistorial;
        TextView contenidoHistorial;
        ImageView clipBtn;
        ImageView noAprobado,aprobado;
    }


    @Override
    public int getCount() {
        return mensajeData.size();
    }

    @Override
    public Object getItem(int position) {
        return mensajeData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Typeface bold=Typeface.createFromAsset(context.getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(context.getAssets(),avenirMedium);
        Typeface demibold=Typeface.createFromAsset(context.getAssets(),avenirDemibold);

        ViewHolder holder;
        // TextView nombreEstudiante=(TextView)v.findViewById(R.id.nombreEstudiante);

        if(convertView==null){
            holder=new ViewHolder();
            convertView=inflater.inflate(R.layout.historial_item_layout,null);

            holder.horaHistorial=(TextView)convertView.findViewById(R.id.horaHistorial);
            holder.fechaHistorial=(TextView)convertView.findViewById(R.id.fechaHistorial);
            holder.creadoHistorial=(TextView)convertView.findViewById(R.id.creadoHistorial);
            holder.emisorHistorial=(TextView)convertView.findViewById(R.id.emisorHistorial);
            holder.contenidoHistorial=(TextView)convertView.findViewById(R.id.contenidoHistorial);
            holder.paraHistorial=(TextView)convertView.findViewById(R.id.paraHistorial);
            holder.receptorHistorial=(TextView)convertView.findViewById(R.id.receptorHistorial);
            holder.clipBtn=(ImageView)convertView.findViewById(R.id.clipBtn);
            holder.aprobado=(ImageView)convertView.findViewById(R.id.aprobado);
            holder.noAprobado=(ImageView)convertView.findViewById(R.id.noAprobado);

            holder.horaHistorial.setTypeface(demibold);
            holder.fechaHistorial.setTypeface(medium);
            holder.creadoHistorial.setTypeface(medium);
            holder.emisorHistorial.setTypeface(medium);
            holder.contenidoHistorial.setTypeface(medium);
            holder.paraHistorial.setTypeface(demibold);
            holder.receptorHistorial.setTypeface(demibold);
            convertView.setTag(holder);
        }else {
            holder=(ViewHolder)convertView.getTag();
        }



        /*
        if (mensajeData.get(position).getTipo().equals("Anuncio")){
            holder.tipo.setTextColor(Color.parseColor("#4FC1E9"));
        }else {
            if (anuncioData.get(position).getTipo().equals("Tarea")){
                holder.tipo.setTextColor(Color.parseColor("#A0D468"));
            }else {
                if (anuncioData.get(position).getTipo().equals("Enfermería")){
                    holder.tipo.setTextColor(Color.parseColor("#ED5565"));
                }else {
                    holder.tipo.setTextColor(Color.parseColor("#48CFAD"));
                }
            }
        }
        */

        if (mensajeData.get(position).getParseUri()!=null||mensajeData.get(position).isAwsAttachment()||mensajeData.get(position).isTablaAnuncio()){
            holder.clipBtn.setVisibility(View.VISIBLE);
        }else {
            holder.clipBtn.setVisibility(View.GONE);
        }

        if (mensajeData.get(position).isApproved()){
            holder.aprobado.setVisibility(View.VISIBLE);
        }else {
            holder.noAprobado.setVisibility(View.GONE);
        }


        holder.horaHistorial.setText(mensajeData.get(position).getHora());
        holder.fechaHistorial.setText(mensajeData.get(position).getFechaEntrega());
        holder.contenidoHistorial.setText(mensajeData.get(position).getDescripcion());
        holder.receptorHistorial.setText(mensajeData.get(position).getReceptor());
        holder.emisorHistorial.setText(mensajeData.get(position).getEmisor());


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment=new HistorialDetailFragment();
                Bundle b=new Bundle();
                b.putString("parentesco",mensajeData.get(position).getParentesco());
                if (mensajeData.get(position).getPara()==null){
                    b.putString("para","");
                }else {
                    b.putString("para"," Para: ");

                }
                b.putString("hora",mensajeData.get(position).getHora());
                b.putString("studentId",mensajeData.get(position).getStudentId());
                b.putString("fecha",mensajeData.get(position).getFechaEntrega());
                b.putString("emisor",mensajeData.get(position).getEmisor());
                b.putString("receptor",mensajeData.get(position).getReceptor());
                b.putString("asunto",mensajeData.get(position).getTipo());
                b.putString("descripcion",mensajeData.get(position).getDescripcion());
                b.putString("id",mensajeData.get(position).getID());
                b.putString("aprobadoPor",mensajeData.get(position).getAprobadoPor());
                if (mensajeData.get(position).getParseUri()!=null){
                    b.putString("parseUri",mensajeData.get(position).getParseUri());
                }else {
                    if (mensajeData.get(position).isAwsAttachment()){
                        b.putBoolean("aws",true);
                    }else {
                        if (mensajeData.get(position).isTablaAnuncio()){
                            b.putBoolean("tablaAnuncio",true);
                        }
                    }
                }
                fragment.setArguments(b);
                FragmentTransaction transaction= ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
            }
        });

        return convertView;
    }





}

