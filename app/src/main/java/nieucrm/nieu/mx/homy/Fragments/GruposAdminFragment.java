package nieucrm.nieu.mx.skola.Fragments;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class GruposAdminFragment extends Fragment {

    ListView listaGrupos;
    ArrayList<String> group;
    ArrayAdapter<String> g;
    List<ParseObject> ob;
    ProgressBar progressGrupo;
    String escuela;
    Activity a;

    public GruposAdminFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            a=(Activity) context;
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_grupos_admin, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressGrupo=(ProgressBar)view.findViewById(R.id.progressGrupoAdmin);
        listaGrupos=(ListView)view.findViewById(R.id.listaGrupos);

        getGrupos();

        listaGrupos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AlertDialog.Builder builder = new AlertDialog.Builder(a);
                builder.setMessage("Esta lista solo despliega los grupos creados. Si deseas entrar a la lista de grupo entra desde la pantalla principal")
                        .setTitle("Lista de grupos")
                        .setCancelable(false)
                        .setNegativeButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

    }

    public void getGrupos(){
        group=new ArrayList<>();

        escuela=((ParseApplication)a.getApplication()).getEscuelaId();

        ParseObject e=ParseObject.createWithoutData("Escuela",escuela);

        ParseQuery<ParseObject> q=ParseQuery.getQuery("grupo").include("Maestros").include("escuela").orderByAscending("name");
        q.whereEqualTo("escuela",e);

        q.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                try {
                    progressGrupo.setVisibility(View.GONE);
                    listaGrupos.setVisibility(View.VISIBLE);
                }catch (Exception e1){
                    Log.d("Error",e1.getMessage());
                }

                if (e==null){

                    for (ParseObject o:objects){
                        group.add(o.getString("name"));
                    }
                    ob=objects;
                    try {
                        g=new ArrayAdapter<String>(a,android.R.layout.simple_list_item_1,android.R.id.text1,group);
                    }catch (Exception e1){
                        Log.e("Error",e1.getMessage());
                    }

                    listaGrupos.setAdapter(g);
                }
                else{
                    Toast.makeText(a,e.getMessage(),Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.crear_grupo,menu);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.crearGrupo:
                Fragment fragment=new NewGroupFragment();
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                break;
        }
        return super.onOptionsItemSelected(item);

    }


}
