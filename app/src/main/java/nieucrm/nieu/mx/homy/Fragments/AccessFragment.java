package nieucrm.nieu.mx.skola.Fragments;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import nieucrm.nieu.mx.skola.Adapter.AccessAdapter;
import nieucrm.nieu.mx.skola.DataModel.AccessData;
import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;

/**
 * Created by Carlos Romero
 */

public class AccessFragment extends Fragment {

    ListView listView;
    ProgressBar progressAcceso;
    AccessAdapter adapter;
    ArrayList<AccessData>listAccesos;
    Fragment fragment;
    int usertype;
    String escuelaId;
    ParseObject escuela;
    Activity a;

    public AccessFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            a=(Activity) context;
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_access, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listView=(ListView)view.findViewById(R.id.listAccesos);
        progressAcceso=(ProgressBar)view.findViewById(R.id.progressAcceso);

        listAccesos=new ArrayList<>();

        getAccess();

    }

    public void getAccess(){
        escuelaId=((ParseApplication)a.getApplication()).getEscuelaId();
        escuela=ParseObject.createWithoutData("Escuela",escuelaId);

        ParseQuery<ParseObject>query1 = new ParseQuery<>("Estudiantes")
                .include("PersonasAutorizadas")
                .whereEqualTo("PersonasAutorizadas", ParseUser.getCurrentUser());
        query1.whereEqualTo("escuela",escuela);
        ParseQuery<ParseObject>query=ParseQuery.getQuery("Acceso").include("student").include("user").orderByDescending("createdAt");

        usertype=ParseUser.getCurrentUser().getNumber("usertype").intValue();

        if (usertype==1||usertype==0){
            query.whereEqualTo("user",ParseUser.getCurrentUser());
        }else {
            query.whereMatchesQuery("student",query1);
        }


        query.setLimit(40);



        query.findInBackground(new FindCallback<ParseObject>() {

            @Override
            public void done(List<ParseObject> objectList, ParseException e) {
                progressAcceso.setVisibility(View.GONE);
                listView.setVisibility(View.VISIBLE);
                if(e==null){
                    for(ParseObject objects:objectList){
                        AccessData data=new AccessData();

                        ParseObject estObjects=objects.getParseObject("student");
                        ParseUser userObject=objects.getParseUser("user");
                        if(estObjects==null){
                            data.setNombreNino("Alumno");
                        }else{
                            String nombre=estObjects.getString("NOMBRE")+" "+estObjects.getString("APELLIDO");
                            data.setNombreNino(nombre);
                        }

                        if(userObject==null){
                            data.setNombrePadre("Padre");
                            data.setParentesco("Parentesco");
                        }else{
                            String nombreP=userObject.getString("nombre")+" "+userObject.getString("apellidos");
                            String parentesco=userObject.getString("parentesco");
                            data.setNombrePadre(nombreP);
                            data.setParentesco(parentesco);
                        }

                        //data.setFecha(objects.getUpdatedAt().getDate()+"/"+objects.getUpdatedAt().getMonth()+"/"+objects.getUpdatedAt().getYear());

                        Date p =objects.getCreatedAt();
                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy",new Locale("es"));
                        try {
                            p = sdf.parse(sdf.format(p));
                        } catch (java.text.ParseException e1) {
                            e1.printStackTrace();
                        }
                        sdf.setTimeZone(TimeZone.getTimeZone("CST"));


                        data.setFecha(sdf.format(p));

                        DateFormat df=new SimpleDateFormat("HH:mm",Locale.US);
                        //df.setTimeZone(TimeZone.getTimeZone("PNT"));
                        Date d=new Date(objects.getCreatedAt().getTime());
                        String format=df.format(d);
                        data.setHora(format);

                        listAccesos.add(data);

                    }
                    adapter=new AccessAdapter(a,listAccesos);
                    listView.setAdapter(adapter);
                }
            }
        });
    }

}

