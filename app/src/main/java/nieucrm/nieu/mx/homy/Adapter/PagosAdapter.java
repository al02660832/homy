package nieucrm.nieu.mx.skola.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import nieucrm.nieu.mx.skola.DataModel.PagosData;
import nieucrm.nieu.mx.skola.Fragments.PagosDetailFragment;
import nieucrm.nieu.mx.skola.R;


/**
 * Created by Carlos Romero on 10/12/2016.
 */

public class PagosAdapter extends BaseAdapter {

    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf";

    String fileUri;
    String mes;

    private Context context;
    LayoutInflater inflater;
    private List<PagosData> pagosData=null;
    private ArrayList<PagosData>arrayList=null;


    public PagosAdapter(Context context, List<PagosData> pagosData) {
        this.context = context;
        this.pagosData = pagosData;
        try {
            inflater=LayoutInflater.from(context);

        }catch (Exception e){
            Log.d("Error",e.getMessage());
        }
        this.arrayList=new ArrayList<>();
        this.arrayList.addAll(pagosData);
    }

    public class ViewHolder{
        TextView concepto,txtPagoEstudiante,txtFechaPago,pago,txtStatus;
    }


    @Override
    public int getCount() {
        return pagosData.size();
    }

    @Override
    public Object getItem(int position) {
        return pagosData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Typeface bold=Typeface.createFromAsset(context.getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(context.getAssets(),avenirMedium);

        ViewHolder holder;
       // TextView nombreEstudiante=(TextView)v.findViewById(R.id.nombreEstudiante);

        if(convertView==null){
            holder=new ViewHolder();
            convertView=inflater.inflate(R.layout.pagos_item_layout,null);
            holder.concepto = (TextView) convertView.findViewById(R.id.pagoConcepto);
            holder.pago = (TextView) convertView.findViewById(R.id.txtPago);
            holder.txtPagoEstudiante = (TextView) convertView.findViewById(R.id.txtPagoEstudiante);
            holder.txtFechaPago= (TextView) convertView.findViewById(R.id.txtFechaPago);
            holder.txtStatus= (TextView) convertView.findViewById(R.id.txtStatus);

            holder.concepto.setTypeface(bold);
            holder.pago.setTypeface(medium);
            holder.txtFechaPago.setTypeface(medium);
            holder.txtPagoEstudiante.setTypeface(medium);
            holder.txtStatus.setTypeface(medium);

            convertView.setTag(holder);
        }else {
            holder=(ViewHolder)convertView.getTag();
        }

        fileUri=pagosData.get(position).getFileUri();

        holder.concepto.setText(pagosData.get(position).getConcepto());
        holder.pago.setText(pagosData.get(position).getCantidad());
        holder.txtStatus.setText(pagosData.get(position).getStatus());
        holder.txtPagoEstudiante.setText(pagosData.get(position).getPrimerNombre());
        holder.txtFechaPago.setText(pagosData.get(position).getFecha());

        if (pagosData.get(position).getStatus().equals("Pendiente")){
            holder.txtStatus.setTextColor(context.getResources().getColor(R.color.red_rules));
        }else {
            holder.txtStatus.setTextColor(context.getResources().getColor(R.color.green_events));

        }

        SimpleDateFormat format = new SimpleDateFormat("MMMM",new Locale("es"));
        mes=format.format(pagosData.get(position).getMesPago());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new PagosDetailFragment();
                Bundle bundle=new Bundle();
                bundle.putString("parseImage",pagosData.get(position).getFileUri());
                bundle.putString("nombreAlumno",pagosData.get(position).getAlumno());
                bundle.putString("conceptoPago",pagosData.get(position).getConcepto());
                bundle.putString("oId",pagosData.get(position).getObjectId());
                bundle.putString("mes",mes);
                fragment.setArguments(bundle);
                FragmentTransaction transaction= ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
            }
        });

        return convertView;


    }

}
