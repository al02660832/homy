package nieucrm.nieu.mx.skola.Fragments;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class SuccessfulRegistrationFragment extends Fragment {

    TextView success,welcome,dontWorry;
    String avenirBold = "font/avenir-next-bold.ttf", avenirMedium = "font/avenir-next-medium.ttf",avenirDemi = "font/avenir-next-demi-bold.ttf";


    public SuccessfulRegistrationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_successful_registration, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        success=(TextView)view.findViewById(R.id.success);
        welcome=(TextView)view.findViewById(R.id.welcome);
        dontWorry=(TextView)view.findViewById(R.id.dontWorry);

        setTypeFace();

    }

    private void setTypeFace(){
        Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), avenirBold);
        Typeface medium = Typeface.createFromAsset(getActivity().getAssets(), avenirMedium);
        Typeface demi=Typeface.createFromAsset(getActivity().getAssets(), avenirDemi);

        success.setTypeface(demi);
        welcome.setTypeface(bold);
        dontWorry.setTypeface(medium);

    }

}
