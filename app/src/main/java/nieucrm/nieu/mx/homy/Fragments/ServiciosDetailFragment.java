package nieucrm.nieu.mx.skola.Fragments;


import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class ServiciosDetailFragment extends Fragment {

    TextView nombreSer,descripServicio,alumServi,txtAlumnoS,txtFechaServi,txtFechaDesignada,txtComentarios,txtPrecio,txtPrecioServicio;
    Button btnCambioAlumno,btnCambioFecha;
    EditText editComentarios;
    ImageButton btnSolicitar;

    Date fechaSolicitud;

    HashMap<String, Object> params;

    DatePickerDialog.OnDateSetListener date;
    Calendar myCalendar = Calendar.getInstance();

    String servicioId;

    int position=0;

    int status;
    ParseObject servicios;

    ParseObject estudiante;



    ArrayList<ParseObject>alumnos=new ArrayList<>();

    String nombre,precio,descripcion,objectId,comentario,nombreAlumno,alumnoId;

    int usertype;

    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf",demibold="font/avenir-next-demi-bold.ttf";


    String pushMessage;
    String escuelaId;

    public ServiciosDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_servicios_detail, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);

        nombreSer = (TextView) v.findViewById(R.id.nombreSer);
        descripServicio = (TextView) v.findViewById(R.id.descripServicio);
        alumServi = (TextView) v.findViewById(R.id.alumServi);
        txtAlumnoS = (TextView) v.findViewById(R.id.txtAlumnoS);
        txtFechaServi = (TextView) v.findViewById(R.id.txtFechaServi);
        txtFechaDesignada = (TextView) v.findViewById(R.id.txtFechaDesignada);
        txtComentarios = (TextView) v.findViewById(R.id.txtComentarios);
        txtPrecio = (TextView) v.findViewById(R.id.txtPrecio);
        txtPrecioServicio = (TextView) v.findViewById(R.id.txtPrecioServicio);

        editComentarios = (EditText) v.findViewById(R.id.editComentarios);

        btnCambioAlumno = (Button) v.findViewById(R.id.btnCambioAlumno);
        btnCambioFecha = (Button) v.findViewById(R.id.btnCambioFecha);
        btnSolicitar = (ImageButton) v.findViewById(R.id.btnSolicitar);

        usertype=ParseUser.getCurrentUser().getNumber("usertype").intValue();

        setTypeface();
        getInfo();
        setLabel();
        setFecha();
        setEstudiantes();

        btnSolicitar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                solicitarServicio();
            }
        });
    }

    public void setLabel(){
        Calendar cal=Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("dd 'de' MMMM 'de' yyyy",new Locale("es"));
        String f=format.format(cal.getTime());
        txtFechaDesignada.setText(f);
        fechaSolicitud=cal.getTime();

    }

    public void getInfo(){
        Bundle b = this.getArguments();
        if (b != null) {

            nombre=b.getString("nombre");
            precio=b.getString("precio");
            descripcion=b.getString("descripcion");
            objectId=b.getString("objectid");
            txtPrecioServicio.setText(precio);
            nombreSer.setText(nombre);
            descripServicio.setText(descripcion);
        }
    }

    public void solicitarServicio(){
        escuelaId=((ParseApplication)getActivity().getApplication()).getEscuelaId();

        params=new HashMap<>();
        comentario=editComentarios.getText().toString().trim();
        fechaSolicitud=myCalendar.getTime();
        servicios=new ParseObject("ServicioSolicitado");
        estudiante=ParseObject.createWithoutData("Estudiantes",alumnoId);
        ParseObject servicio=ParseObject.createWithoutData("Servicio",objectId);
        servicios.put("personaAutorizada", ParseUser.getCurrentUser());
        servicios.put("status",status);
        servicios.put("comentarios",comentario);
        servicios.put("fechaSolicitud",fechaSolicitud);
        servicios.put("estudiante",estudiante);
        servicios.put("servicio",servicio);
        servicios.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e==null){
                    solicitar();

                    pushMessage="Confirmar Servicio solicitado: "+nombre+" Para: "+nombreAlumno;
                    servicioId=servicios.getObjectId();
                    params.put("pushMessage",pushMessage);
                    params.put("escuelaObjId",escuelaId);
                    ParseCloud.callFunctionInBackground("servicioSolicitadoAdminNotification", params, new FunctionCallback<Object>() {

                        @Override
                        public void done(Object object, ParseException e) {
                            if (e == null){
                                Log.d("Cloudcode","Success");
                            }else {
                                Log.d("Error",e.getMessage());
                            }
                        }
                    });
                }else {
                    Log.d("Error",e.getMessage());
                }
            }
        });

    }

    public void setEstudiantes(){
        ParseQuery<ParseObject> est = ParseQuery.getQuery("Estudiantes").include("PersonasAutorizadas").whereEqualTo("PersonasAutorizadas", ParseUser.getCurrentUser());
        est.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e==null){
                    alumnos.addAll(objects);

                    if (alumnos.size()!=0){
                        estudiante=alumnos.get(position);
                        nombreAlumno=estudiante.get("NOMBRE") + " " + estudiante.getString("APELLIDO");
                        alumnoId=estudiante.getObjectId();

                        if (alumnos.size()>1){
                            btnCambioAlumno.setVisibility(View.VISIBLE);
                            btnCambioAlumno.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (position==alumnos.size()-1){
                                        position = 0;
                                        estudiante=alumnos.get(position);
                                        nombreAlumno=estudiante.get("NOMBRE") + " " + estudiante.getString("APELLIDO");
                                        alumnoId=estudiante.getObjectId();
                                        txtAlumnoS.setText(nombreAlumno);

                                    }else{
                                        position++;
                                        estudiante=alumnos.get(position);
                                        nombreAlumno=estudiante.get("NOMBRE") + " " + estudiante.getString("APELLIDO");
                                        alumnoId=estudiante.getObjectId();

                                        txtAlumnoS.setText(nombreAlumno);
                                    }
                                }
                            });
                        }
                        txtAlumnoS.setText(nombreAlumno);

                    }else {
                        Log.d("Error","Error");
                    }


                }else {
                    Toast.makeText(getContext(),"Error",Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    public void setFecha(){

        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                SimpleDateFormat format = new SimpleDateFormat("dd 'de' MMMM 'de' yyyy",new Locale("es"));
                String f=format.format(myCalendar.getTime());
                txtFechaDesignada.setText(f);
            }

        };

        btnCambioFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

    }

    public void setTypeface(){
        Typeface bold=Typeface.createFromAsset(getActivity().getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(getActivity().getAssets(),avenirMedium);
        Typeface dem=Typeface.createFromAsset(getActivity().getAssets(),demibold);

        nombreSer.setTypeface(dem);
        descripServicio.setTypeface(dem);
        alumServi.setTypeface(medium);
        txtAlumnoS.setTypeface(medium);
        txtFechaServi.setTypeface(medium);
        txtFechaDesignada.setTypeface(medium);
        txtComentarios.setTypeface(medium);
        txtPrecio.setTypeface(dem);
        txtPrecioServicio.setTypeface(dem);
        btnCambioAlumno.setTypeface(medium);
        btnCambioFecha.setTypeface(medium);
        editComentarios.setTypeface(medium);

    }

    public void solicitar(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Servicio Solicitado")
                .setTitle("El colegio le noticará de recibido")
                .setCancelable(false)
                .setNegativeButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                getActivity().getSupportFragmentManager().popBackStack();

                            }
                        })
                .setPositiveButton("Cancelar servicio",
                        new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog, int id) {

                                cancelarServicio();

                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void cancelarServicio(){
        escuelaId=((ParseApplication)getActivity().getApplication()).getEscuelaId();

        ParseQuery<ParseObject> q=ParseQuery.getQuery("ServicioSolicitado");
        q.whereEqualTo("objectId",servicioId);
        q.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (e==null){
                    object.put("status",9);
                    object.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e==null){



                                Toast.makeText(getContext(),"Cancelado",Toast.LENGTH_SHORT).show();

                                params.put("statusString","Cancelado");
                                params.put("estudianteId",alumnoId);
                                params.put("escuelaObjId",escuelaId);

                                ParseCloud.callFunctionInBackground("servicioSolicitadoNotifyParentStatusChanged", params, new FunctionCallback<Object>() {

                                    @Override
                                    public void done(Object object, ParseException e) {
                                        if (e == null){
                                            Log.d("Cloudcode","Success");
                                        }else {
                                            Log.d("Error",e.getMessage());
                                        }
                                    }
                                });
                                getActivity().getSupportFragmentManager().popBackStack();

                            }


                        }
                    });
                }
            }
        });
    }

}
