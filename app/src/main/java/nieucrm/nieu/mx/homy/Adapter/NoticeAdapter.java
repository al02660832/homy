package nieucrm.nieu.mx.skola.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import nieucrm.nieu.mx.skola.DataModel.InfoData;
import nieucrm.nieu.mx.skola.DataModel.ProspectoData;
import nieucrm.nieu.mx.skola.Fragments.InformationViewFragment;
import nieucrm.nieu.mx.skola.Fragments.NewProspectoFragment;
import nieucrm.nieu.mx.skola.Fragments.ProspectoWebFragment;
import nieucrm.nieu.mx.skola.R;


/**
 * Created by Carlos Romero on 14/03/2017.
 */

public class NoticeAdapter extends BaseAdapter {

    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf",avenirDemibold="font/avenir-next-demi-bold.ttf";



    private Context context;
    LayoutInflater inflater;
    private List<InfoData> infoData=null;
    private ArrayList<InfoData> array=null;
    Fragment fragment;
    boolean web;

    public NoticeAdapter(Context context, List<InfoData> infoData) {
        this.context = context;
        this.infoData=infoData;
        try {
            inflater=LayoutInflater.from(context);

        }catch (Exception e){
            Log.d("Error",e.getMessage());
        }
        this.array=new ArrayList<>();
        this.array.addAll(infoData);
    }

    public class ViewHolder{
        TextView nombreNotice,tipoNotice;
    }


    @Override
    public int getCount() {
        return infoData.size();
    }

    @Override
    public Object getItem(int position) {
        return infoData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Typeface bold=Typeface.createFromAsset(context.getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(context.getAssets(),avenirMedium);
        Typeface demibold=Typeface.createFromAsset(context.getAssets(),avenirDemibold);

        ViewHolder holder;
        // TextView nombreEstudiante=(TextView)v.findViewById(R.id.nombreEstudiante);

        if(convertView==null){
            holder=new ViewHolder();
            convertView=inflater.inflate(R.layout.notice_item_layout,null);

            holder.nombreNotice=(TextView) convertView.findViewById(R.id.nombreNotice);

            holder.tipoNotice=(TextView) convertView.findViewById(R.id.tipoNotice);

            holder.nombreNotice.setTypeface(medium);
            holder.tipoNotice.setTypeface(medium);
            convertView.setTag(holder);
        }else {
            holder=(ViewHolder)convertView.getTag();
        }

        holder.tipoNotice.setText(infoData.get(position).getContenido());

        if (infoData.get(position).getContenido()==null||infoData.get(position).getContenido().equals("")){
            holder.tipoNotice.setVisibility(View.GONE);

        }else {
            holder.tipoNotice.setText(infoData.get(position).getContenido());
        }

        holder.nombreNotice.setText(infoData.get(position).getTipo());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment=new InformationViewFragment();

                Bundle b = new Bundle();
                b.putString("objectId",infoData.get(position).getObjectId());
                b.putString("tipo",infoData.get(position).getTipo());
                b.putString("content",infoData.get(position).getContenido());

                fragment.setArguments(b);
                FragmentTransaction transaction= ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
            }
        });

        return convertView;
    }





}

