package nieucrm.nieu.mx.skola.Fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.PNCallback;
import com.pubnub.api.models.consumer.PNPublishResult;
import com.pubnub.api.models.consumer.PNStatus;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import nieucrm.nieu.mx.skola.BuildConfig;
import nieucrm.nieu.mx.skola.Helpers.Constants;
import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;

/**
 * Created by Carlos Romero
 */

public class ScanCredentialFragment extends Fragment {

    ImageButton escaneo;
    List<ParseObject> fotos;
    ParseImageView fotoScan;
    TextView nombreEscaneado,statusEscaneado,accesoOtorgado,textEscanear,accesoEscaneado;
    String id;
    String alid;
    ParseObject acceso;
    ParseUser user;
    ParseObject student;
    PubNub pubNub;
    ParseObject paquete;
    LinearLayout layoutMonitor;
    int usertype;
    ParseUser usu;
    HashMap<String, Object> params;
    String accesoObjectId;
    int c;

    int width;
    int height;

    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf",demibold="font/avenir-next-demi-bold.ttf";

    TransferUtility utility;
    TransferObserver observer;
    File imageFile;
    File outputDir;

    String escuelaId;
    Activity a;

    public ScanCredentialFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            a=(Activity) context;
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        DisplayMetrics dm=new DisplayMetrics();
        a.getWindowManager().getDefaultDisplay().getMetrics(dm);
        width=dm.widthPixels/2;
        height=dm.heightPixels/2;

        return inflater.inflate(R.layout.fragment_scan_credential, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);

        escaneo = (ImageButton) v.findViewById(R.id.crearAcceso);
        accesoEscaneado=(TextView)v.findViewById(R.id.accesoEscaneado);
        fotoScan = (ParseImageView) v.findViewById(R.id.imagenEscaneada);
        nombreEscaneado=(TextView)v.findViewById(R.id.nombreEscaneado);
        accesoOtorgado=(TextView)v.findViewById(R.id.accesoOtorgado);
        textEscanear=(TextView)v.findViewById(R.id.textEscanear);
        statusEscaneado=(TextView)v.findViewById(R.id.estatusOtorgado);

        layoutMonitor=(LinearLayout)v.findViewById(R.id.layoutMonitor);

        fotoScan.getLayoutParams().height=height;
        fotoScan.getLayoutParams().width=width;
        typeface();

        escaneo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                crearEscaneo();
            }
        });

    }

    //Escanear la credencial
    public void crearEscaneo() {

        final CharSequence[] items = {"Cámara frontal", "Cámara trasera",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(a);
        builder.setTitle("Elige una cámara: ");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Cámara frontal")) {
                    IntentIntegrator sc = IntentIntegrator.forSupportFragment(ScanCredentialFragment.this);
                    sc.setPrompt("Presentar Credencial en Pantalla");
                    sc.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
                    sc.setCameraId(1);
                    sc.setBeepEnabled(true);
                    sc.setBarcodeImageEnabled(true);
                    c=1;
                    sc.initiateScan(IntentIntegrator.QR_CODE_TYPES);
                } else if (items[item].equals("Cámara trasera")) {
                    IntentIntegrator sc = IntentIntegrator.forSupportFragment(ScanCredentialFragment.this);
                    sc.setPrompt("Presentar Credencial en Pantalla");
                    sc.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
                    sc.setCameraId(0);
                    sc.setBeepEnabled(true);
                    sc.setBarcodeImageEnabled(true);
                    c=0;
                    sc.initiateScan(IntentIntegrator.QR_CODE_TYPES);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();


    }

    //Se obtiene el objectId del usuario y del padre
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult i = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (i != null) {
            String scan = i.getContents();
            if (scan != null && scan.length() >= 10) {
                createObjects(scan);
            }else{
                Toast.makeText(a,"Error al escanear QR",Toast.LENGTH_SHORT).show();
            }
        }else {
            super.onActivityResult(requestCode, resultCode, data);

        }
    }

    public void createObjects(String qr){

        id="";
        alid="";

        id = qr.substring(0, 10);

        if (qr.charAt(10)=='_'){

        }else{
            if (qr.charAt(10)=='-'){
                alid = qr.substring(11, 21);

            }
        }

        getUser(id);


    }

    //Registrar acceso en Parse
    private void saveAccess() throws ParseException {

        escuelaId=((ParseApplication)a.getApplication()).getEscuelaId();


        params = new HashMap<>();

        final ProgressDialog progressDialog = new ProgressDialog(a);

        a.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressDialog.setTitle("Registrando acceso");
                progressDialog.setMessage("Por favor espere");
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
        });

        Date d = new Date();

        user= ParseObject.createWithoutData(ParseUser.class, id);
        acceso = new ParseObject("Acceso");
        acceso.put("user", user);

        usertype=usu.getNumber("usertype").intValue();
        if (alid.equals("")){

            if (usertype==1){
                acceso.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e==null){
                            Toast.makeText(a,"Acceso registrado exitosamente",Toast.LENGTH_SHORT).show();
                        }else {
                            Log.d("Teacher error",e.getMessage());
                        }
                    }
                });
            }

            if (usertype==2){
                ParseQuery<ParseObject>q=ParseQuery.getQuery("Estudiantes").include("PersonasAutorizadas").include("paquete").whereEqualTo("PersonasAutorizadas",user);
                q.getFirstInBackground(new GetCallback<ParseObject>() {
                    @Override
                    public void done(ParseObject object, ParseException e) {
                        if (object==null){
                            ParseObject studentNull=new ParseObject ("NullStudent");
                            studentNull.put("user",user);
                            studentNull.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    if(e==null){
                                        Log.d("NullStudent Error","Success");
                                    }else {
                                        Log.d("NullStudent Error",e.getMessage());
                                    }
                                }
                            });
                        }else {
                            acceso.put("student",object);


                            if (object.getParseObject("paquete")==null){
                                Toast.makeText(a,"El estudiante no tiene paquete",Toast.LENGTH_SHORT).show();
                            }else{
                                paquete=object.getParseObject("paquete");
                                paquetePuntualidad();
                            }



                        }
                    }
                });
            }else {
                acceso.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e==null){
                            accesoObjectId = acceso.getObjectId();
                            params.put("accesoObjectId", accesoObjectId);
                            params.put("escuelaObjId",escuelaId);
                            ParseCloud.callFunctionInBackground("accesos", params, new FunctionCallback<Object>() {

                                @Override
                                public void done(Object object, ParseException e) {
                                    if (e == null) {
                                        Log.d("Cloudcode", "Success");
                                    } else {
                                        Log.d("Error", e.getMessage());
                                    }
                                }
                            });
                            Toast.makeText(a,"Acceso registrado exitosamente",Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(a,"Error al registrar el acceso",Toast.LENGTH_SHORT).show();

                        }
                    }
                });
            }
        }else {
            student = ParseObject.createWithoutData("Estudiantes", alid);
            acceso.put("student",student);
        }



        acceso.put("escaneoOcurrido", d);
        acceso.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                progressDialog.hide();
                if (e == null) {
                    subscribePubNub();
                    accesoObjectId = acceso.getObjectId();
                    params.put("accesoObjectId", accesoObjectId);
                    params.put("escuelaObjId",escuelaId);
                    ParseCloud.callFunctionInBackground("accesos", params, new FunctionCallback<Object>() {

                        @Override
                        public void done(Object object, ParseException e) {
                            if (e == null) {
                                Log.d("Cloudcode", "Success");
                            } else {
                                Log.e("Error", e.getMessage());
                            }
                        }
                    });
                    Toast.makeText(a,"Acceso registrado exitosamente",Toast.LENGTH_SHORT).show();
                    accesoOtorgado.setText("Otorgado");
                    layoutMonitor.setBackgroundColor(getResources().getColor(R.color.green_events));
                    resetTimer();

                } else {
                    Toast.makeText(a, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });

    }


    public void getUser(String ids){

        escuelaId=((ParseApplication)a.getApplication()).getEscuelaId();
        ParseObject escuela=ParseObject.createWithoutData("Escuela",escuelaId);
        ParseQuery<ParseUser> q = ParseUser.getQuery();
        q.whereEqualTo("escuela",escuela);
        q.whereEqualTo("objectId", ids);

        q.getFirstInBackground(new GetCallback<ParseUser>() {
            @Override
            public void done(ParseUser user, ParseException e) {
                if (e == null) {

                    if (user.getNumber("status")!=null){
                        if (user.getNumber("status").intValue() == 0) {
                            displayImage(user);
                            usu=user;

                        }else {
                            negarAcceso(user.getNumber("status").intValue());
                        }
                    }else {
                        try {
                            Toast.makeText(a,"Tipo de usuario invalido",Toast.LENGTH_SHORT).show();

                        }catch (Exception e1){
                            Log.e("Exception",e1.getMessage());
                        }
                    }



                } else {
                    negarAcceso(5);
                }


            }
        });






    }

    public void downloadImage(String objectId){
        outputDir = a.getCacheDir();
        try {
            imageFile= File.createTempFile(objectId, ".jpeg", outputDir);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            observer=utility.download(Constants.BUCKET_NAME,objectId,imageFile);
            observer.setTransferListener(new TransferListener() {
                @Override
                public void onStateChanged(int id, TransferState state) {
                    Log.d("Estatus:",state.toString());

                    if (TransferState.COMPLETED.equals(observer.getState())) {

                        Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());

                        //fotoPadre.setImageBitmap(bitmap);
                        //progress.setVisibility(View.GONE);
                        try {
                            Toast.makeText(a, "Foto lista", Toast.LENGTH_SHORT).show();
                        }catch (Exception e1){
                            Log.e("Exception",e1.getMessage());
                        }
                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                    long _bytesCurrent = bytesCurrent;
                    long _bytesTotal = bytesTotal;

                    float percentage =  ((float)_bytesCurrent /(float)_bytesTotal * 100);
                    Log.d("percentage","" +percentage);


                }

                @Override
                public void onError(int id, Exception ex) {

                    Log.d("Error al descargar", ex.getMessage());
                }
            });
        }catch (Exception e1){
            Log.e("Error",e1.getMessage());
        }

    }

    public void displayImage(ParseUser o) {

        String nombre = o.getString("nombre") + " " + o.getString("apellidos");
        nombreEscaneado.setText(nombre);

        final ParseQuery<ParseObject> photo = ParseQuery.getQuery("UserPhoto");
        photo.include("user");
        photo.whereEqualTo("user", o);

        photo.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (e == null) {
                    ParseFile foto = object.getParseFile("userPhoto");
                    if (foto != null) {
                        fotoScan.setParseFile(foto);
                        fotoScan.loadInBackground(new GetDataCallback() {
                            @Override
                            public void done(byte[] data, ParseException e) {
                            }
                        });

                        try {
                            saveAccess();
                        } catch (ParseException e1) {
                            e1.printStackTrace();
                        }
                    } else {
                        if (object.getBoolean("aws")){
                            try {
                                downloadImage(object.getObjectId());

                            }catch (Exception e1){
                                Log.e("Error",e1.getMessage());
                            }
                            try {
                                saveAccess();
                            } catch (ParseException e1) {
                                e1.printStackTrace();
                            }
                        }else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(a);
                            builder.setMessage("Usuario existente")
                                    .setTitle("Usuario sin foto digital. \nFavor de presentar una foto con administración")
                                    .setCancelable(false)
                                    .setPositiveButton("Continuar",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.dismiss();
                                                }
                                            });
                            AlertDialog alert = builder.create();
                            alert.show();

                            try {
                                saveAccess();
                            } catch (ParseException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }else {
                    try {
                        saveAccess();
                    } catch (ParseException e1) {
                        e1.printStackTrace();
                    }
                }

            }
        });

    }

    public void negarAcceso(int reason){
        switch (reason){
            case 0:
                statusEscaneado.setText("Usuario Activo. Puede pasar");
                break;
            case 1:
                statusEscaneado.setText("El Estudiante ha sido dado de Baja del Colegio");
                break;
            case 2:
                statusEscaneado.setText("El Estudiante ha sido Desactivado Temporalmente");
                break;
            case 3:
                statusEscaneado.setText("Esta Persona Autorizada ha sido Desactivada Temporalmente");
                break;
            case 4:
                statusEscaneado.setText("Esta Persona Autorizada ha sido Desactivada Permanentemente del Sistema");
                break;
            case 5:
                statusEscaneado.setText("Código Inválido - Este Usuario no Existe en la base de Datos");
                break;

            default:
                statusEscaneado.setText("NEGAR ACCESO USUARIO INEXISTENTE");
                break;
        }

    }

    public void paquetePuntualidad() {

        int puntualidad=0;
        long minute=60000;
        Date now=new Date();
        Date horaSalida=new Date(paquete.getDate("horaSalida").getTime()+minute);
        Date tolerancia=new Date(horaSalida.getTime()+(15*minute));

        if (now.before(horaSalida)){
            puntualidad=0;
        }else
        if (now.equals(horaSalida)){
            puntualidad=0;
        }else
        if (tolerancia.before(now)){
            puntualidad=1;
        }else
        if (tolerancia.equals(now)){
            puntualidad=1;
        }else
        if (tolerancia.after(now)){
            puntualidad=2;
        }

        acceso.put("puntualidad",puntualidad);
        

    }

    public void subscribePubNub(){
        PNConfiguration pnConfiguration=new PNConfiguration();
        pnConfiguration.setPublishKey(BuildConfig.pubNubPublish);
        pnConfiguration.setSubscribeKey(BuildConfig.pubNubSubscribe);
        pnConfiguration.setSecure(false);
        pubNub=new PubNub(pnConfiguration);
        pubNub.publish().channel("QRCodeScanner_Channel").message(ParseUser.getCurrentUser().getObjectId()).async(new PNCallback<PNPublishResult>() {
            @Override
            public void onResponse(PNPublishResult result, PNStatus status) {
                if (!status.isError()){
                    //Message successfully sent
                }
                else {
                    //Problem sending the message
                }
            }
        });


    }

    public void resetView(){
        fotoScan.setImageResource(android.R.color.transparent);
        nombreEscaneado.setText("");
        accesoOtorgado.setText("");
        layoutMonitor.setBackgroundColor(getResources().getColor(R.color.purple_access));
        IntentIntegrator sc = IntentIntegrator.forSupportFragment(ScanCredentialFragment.this);
        sc.setPrompt("Presentar Credencial en Pantalla");
        sc.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
        sc.setCameraId(c);
        sc.setBeepEnabled(true);
        sc.setBarcodeImageEnabled(true);
        sc.initiateScan(IntentIntegrator.QR_CODE_TYPES);
    }

    public void resetTimer(){
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                resetView();
            }
        }, 3000);
    }

    public void typeface(){
        Typeface bold=Typeface.createFromAsset(a.getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(a.getAssets(),avenirMedium);
        Typeface demi=Typeface.createFromAsset(a.getAssets(),demibold);

        nombreEscaneado.setTypeface(demi);
        statusEscaneado.setTypeface(demi);
        accesoOtorgado.setTypeface(demi);
        textEscanear.setTypeface(demi);
        accesoEscaneado.setTypeface(demi);


    }

}

