package nieucrm.nieu.mx.skola.Fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import info.hoang8f.android.segmented.SegmentedGroup;
import nieucrm.nieu.mx.skola.Activity.Home;
import nieucrm.nieu.mx.skola.Adapter.AnuncioAdapter;
import nieucrm.nieu.mx.skola.DataModel.AnuncioData;
import nieucrm.nieu.mx.skola.R;

/**
 * Created by Carlos Romero
 */

public class PerfilEstudianteFragment extends Fragment {
     //Segmented Todos, tareas, enfermeria, anuncios

    ArrayList<String>anuncioPhoto;
    RadioButton radioTodo,radioTareas,radioAnun,radioEnfer;
    ProgressBar progressLibreta;
    SegmentedGroup segmented;
    ListView anuncios;
    ArrayList<AnuncioData>anuncioData;
    ArrayList<AnuncioData>enfermeria;
    ArrayList<AnuncioData>tareas;
    ArrayList<AnuncioData>anuncio;
    AnuncioAdapter adapter;
    ParseQuery<ParseObject>query;
    String nombre,genero,seleccion,autorId,tipo,idEst,grupoEst,objectId,nombreEstudiante;
    TextView nombreEs;
    Toolbar toolbar;
    ParseQuery<ParseObject> mainQuery;
    boolean parseFile;

    AnuncioData data;
    Activity a;

    public PerfilEstudianteFragment() {
        // Required empty public constructor
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            a=(Activity) context;
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            nombre = bundle.getString("NombreEstudiante");
            genero=bundle.getString("generoEstudiante");
            idEst=bundle.getString("estudianteId");
            grupoEst=bundle.getString("grupoId");
            nombreEstudiante=bundle.getString("nombre");

        }

        nombreEs = (TextView) a.findViewById(R.id.textAction);
        toolbar=(Toolbar)a.findViewById(R.id.toolbar);
        nombreEs.setText(nombreEstudiante);

        setHasOptionsMenu(true);




        return inflater.inflate(R.layout.fragment_perfil_estudiante, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        radioTodo=(RadioButton)view.findViewById(R.id.radioTodo);
        radioTareas=(RadioButton)view.findViewById(R.id.radioTareas);
        radioAnun=(RadioButton)view.findViewById(R.id.radioAnun);
        radioEnfer=(RadioButton)view.findViewById(R.id.radioEnfer);
        progressLibreta=(ProgressBar)view.findViewById(R.id.progressLibreta);
        anuncios=(ListView)view.findViewById(R.id.listAnuncios);
        segmented=(SegmentedGroup)view.findViewById(R.id.segmentedGroup);
        anuncioData=new ArrayList<>();
        tareas=new ArrayList<>();
        anuncio=new ArrayList<>();
        enfermeria=new ArrayList<>();

        setTypeface();

        radioTodo.setChecked(true);

        if (genero.equals("M")){
            getView().setBackgroundColor(Color.parseColor("#5D9CEC"));
            segmented.setTintColor(Color.parseColor("#5D9CEC"));
            seleccion="boy";
            toolbar.setBackground(getResources().getDrawable(R.drawable.header_azul));
        }else {
            getView().setBackgroundColor(Color.parseColor("#EC87C0"));
            segmented.setTintColor(Color.parseColor("#EC87C0"));
            seleccion="girl";
            toolbar.setBackground(getResources().getDrawable(R.drawable.libreta_nina_header));

        }

        getAnuncioPhoto();



    }

    public void getAnuncios(){
        ParseObject es=ParseObject.createWithoutData("Estudiantes",idEst);
        ParseObject group=ParseObject.createWithoutData("grupo",grupoEst);


        ParseQuery<ParseObject>query1=ParseQuery.getQuery("anuncio").whereEqualTo("estudiante",es).whereEqualTo("aprobado",true);
        //.include("estudiante").include("tipo").include("estudiante").include("grupos").include("autor");

        ParseQuery<ParseObject>query2=ParseQuery.getQuery("anuncio").whereEqualTo("grupos",group).whereEqualTo("aprobado",true);
        //.include("grupos").include("tipo").include("estudiante").include("grupos").include("autor");
        List<ParseQuery<ParseObject>>queries=new ArrayList<>();
        queries.add(query1);
        queries.add(query2);

        mainQuery = ParseQuery.or(queries).include("estudiante").include("tipo")
                .include("estudiante").include("grupos").include("autor").orderByDescending("createdAt");
        mainQuery.setLimit(40);

        mainQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                progressLibreta.setVisibility(View.GONE);
                anuncios.setVisibility(View.VISIBLE);
                if(e==null){
                    for (ParseObject o:objects){
                        data=new AnuncioData();
                        data.setGenero(genero);

                        ParseObject est=o.getParseObject("estudiante");
                        ParseUser autor=o.getParseUser("autor");
                        ParseObject gru=o.getParseObject("grupos");
                        ParseObject od=o.getParseObject("tipo");

                        if(autor==null){
                            data.setEmisor("");
                        }else{
                            data.setEmisor(autor.getString("nombre")+" "+autor.getString("apellidos"));
                            data.setAutorId(autor.getObjectId());

                        }

                        if (o.getParseFile("attachment")==null){
                            if (o.getBoolean("awsAttachment")){
                                data.setAwsAttachment(true);
                            }else {
                                if (anuncioPhoto.size()==0){
                                    Log.e("Foto","Fotos en anuncio");
                                }else {
                                    if (anuncioPhoto.contains(o.getObjectId())){
                                        data.setTablaAnuncio(true);
                                        Log.e("Foto","Foto en tabla anuncio"+o.getObjectId());
                                    }
                                }
                            }
                        }else {


                            data.setParseUri(o.getParseFile("attachment").getUrl());

                        }

                        if (od==null){

                            if (o.getJSONObject("momento")!=null){
                                try {
                                    JSONObject obj=o.getJSONObject("momento");
                                    data.setPopo(obj.getString("popo"));
                                    data.setPipi(obj.getString("pipi"));
                                    data.setComida(obj.getString("comida"));
                                    data.setColacion(obj.getString("colacion"));
                                    data.setAviso(obj.getBoolean("avisoFuncion"));
                                    data.setDesayuno(obj.getString("desayuno"));
                                    data.setMerienda(obj.getString("merienda"));
                                    data.setDurmio(obj.getBoolean("descanso"));
                                    data.setTiempo(obj.getString("tiempoSiesta"));
                                    data.setLeche(obj.getString("leche"));
                                    data.setComentario(obj.getString("alimentosComentarios"));
                                    data.setHorario(obj.getString("horaSiesta"));

                                    /*
                                     "horaSiesta": "11:30",
                                      "alimentosComentarios": "se comió los vegetales",
                                      "avisoFuncion": true,
                                      "popo": "3",
                                      "tiempoSiesta": "33",
                                      "merienda": "Poco",
                                      "descanso": true,
                                      "leche": "33",
                                      "colacion": "Poco",
                                      "desayuno": "Poco",
                                      "estudiante": "1KmeaoGsZT",
                                      "comida": "Poco",
                                      "pipi": "22"
                                     */

                                    Log.e(o.getObjectId(),"Leche "+data.getLeche());
                                    Log.e(o.getObjectId(),"Comentario "+data.getComentario());
                                    Log.e(o.getObjectId(),"Horario "+ data.getHorario());

                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                }
                                data.setTipo("Momentos del día");
                                data.setMomento(true);
                            }else {
                                data.setTipo("Anuncio");
                            }

                            //
                        }else {
                            data.setTipo(od.getString("nombre"));
                        }
                        data.setID(o.getObjectId());

                        data.setMateria(o.getString("materia"));
                        data.setDescripcion(o.getString("descripcion"));

                        if (est==null){
                            if (o.get("grupos")==null){
                                data.setDestinatario("Todo el colegio");
                            }else {
                                List<ParseObject>g=o.getList("grupos");
                                StringBuilder a=new StringBuilder();
                                for (int i=0;i<g.size();i++){
                                    try {
                                        if (i == g.size() - 1) {
                                            a.append(g.get(i).getString("grupoId"));
                                        } else {
                                            a.append(g.get(i).getString("grupoId")).append(", ");
                                            //Error

                                        }
                                    }catch (Exception e1){
                                        Log.d("Error",e1.getMessage());
                                    }
                                    data.setDestinatario(a.toString());
                                }

                            }
                        }else {
                            data.setDestinatario(est.getString("NOMBRE")+" "+est.getString("APELLIDO"));
                        }

                        data.setFechaE(o.getUpdatedAt());


                        data.setFecha(o.getCreatedAt());

                        if(od==null){
                            if (o.getJSONObject("momento")!=null){
                                data.setTipo("Momentos del día");

                                data.setMateria("General");
                                anuncioData.add(data);

                            }

                        }else {
                            if (od.getString("nombre")==null){

                            }else {
                                if (od.getString("nombre").equals("Tarea")){
                                    tareas.add(data);
                                    anuncioData.add(data);
                                }else{
                                    if (od.getString("nombre").equals("Enfermería")){
                                        enfermeria.add(data);
                                        anuncioData.add(data);
                                    }else {
                                        if (od.getString("nombre").equals("Anuncio")){
                                            anuncio.add(data);
                                            anuncioData.add(data);
                                        }
                                    }

                                }if (!od.getString("nombre").equals("Tarea")&&!od.getString("nombre").equals("Enfermería")&&!od.getString("nombre").equals("Anuncio")){
                                    anuncioData.add(data);
                                }else{

                                }
                            }
                        }





                    }
                    try {
                        adapter=new AnuncioAdapter(a,anuncioData);
                        anuncios.setAdapter(adapter);
                    }catch (Exception ea){
                        Log.d("Error",ea.getMessage());
                    }

                }
            }
        });

        segmented.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.radioTodo:
                        if (genero.equals("M")){
                            getView().setBackgroundColor(Color.parseColor("#5D9CEC"));
                            segmented.setTintColor(Color.parseColor("#5D9CEC"));
                            seleccion="boy";
                            toolbar.setBackground(getResources().getDrawable(R.drawable.header_azul));

                        }else {
                            getView().setBackgroundColor(Color.parseColor("#EC87C0"));
                            segmented.setTintColor(Color.parseColor("#EC87C0"));
                            seleccion="girl";
                            toolbar.setBackground(getResources().getDrawable(R.drawable.libreta_nina_header));

                        }
                        tipo="RRedHmnzoO";
                        adapter=new AnuncioAdapter(a,anuncioData);
                        anuncios.setAdapter(adapter);
                        break;
                    case R.id.radioTareas:
                        ParseObject tar=ParseObject.createWithoutData("tipoAnuncio","lArUY0ns8S");
                        getView().setBackgroundColor(Color.parseColor("#A0D468"));
                        segmented.setTintColor(Color.parseColor("#A0D468"));
                        seleccion="Tareas";
                        tipo="lArUY0ns8S";
                        toolbar.setBackground(getResources().getDrawable(R.drawable.estudiantes_header_libreta));

                        adapter=new AnuncioAdapter(a,tareas);
                        anuncios.setAdapter(adapter);

                        break;
                    case R.id.radioEnfer:
                        ParseObject enf=ParseObject.createWithoutData("tipoAnuncio","A0wGB5B76x");
                        getView().setBackgroundColor(Color.parseColor("#ED5565"));
                        segmented.setTintColor(Color.parseColor("#ED5565"));
                        seleccion="Enfermería";
                        tipo="A0wGB5B76x";

                        toolbar.setBackground(getResources().getDrawable(R.drawable.libreta_enfermeria_header));

                        adapter=new AnuncioAdapter(a,enfermeria);
                        anuncios.setAdapter(adapter);

                        break;
                    case R.id.radioAnun:
                        ParseObject anun=ParseObject.createWithoutData("tipoAnuncio","eJZIOPloQH");
                        getView().setBackgroundColor(Color.parseColor("#4FC1E9"));
                        segmented.setTintColor(Color.parseColor("#4FC1E9"));
                        seleccion="Anuncio";
                        tipo="eJZIOPloQH";
                        toolbar.setBackground(getResources().getDrawable(R.drawable.libreta_anuncio_header));

                        adapter=new AnuncioAdapter(a,anuncio);
                        anuncios.setAdapter(adapter);

                        break;
                }
            }
        });
    }

    //Obtiene los mensajes que tienen foto en tabla AnuncioPhoto y los almacena en Arraylist
    public void getAnuncioPhoto(){
        anuncioPhoto=new ArrayList<>();
        ParseQuery<ParseObject>anuncioPhotos=ParseQuery.getQuery("AnuncioPhoto");
        anuncioPhotos.include("anuncio");
        anuncioPhotos.setLimit(5000);
        anuncioPhotos.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {

                if (e==null){
                    for (ParseObject o:objects){
                        if (o.getParseObject("anuncio")!=null){
                            anuncioPhoto.add(o.getParseObject("anuncio").getObjectId());
                        }
                    }
                    Log.e("Tamaño: ",String.valueOf(objects.size()));
                    Log.e("AnuncioPhoto",String.valueOf(anuncioPhoto.size()));
                }
                getAnuncios();
            }
        });
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.new_message,menu);
        inflater.inflate(R.menu.activity_calendar,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id=item.getItemId();
        if(id==R.id.write_mess){
            Fragment fragment = new NewMessageFragment();
            Bundle bundle=new Bundle();
            bundle.putString("seccion",seleccion);
            bundle.putString("tipo",tipo);
            bundle.putString("idEstudiante",idEst);
            fragment.setArguments(bundle);
            FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
            transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
        }

        if (id==R.id.historialPlaneacion){
            Fragment fragment = new ActivityFragment();
            Bundle b=new Bundle();
            b.putString("grupoId",grupoEst);
            fragment.setArguments(b);
            FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
            transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();


        }

        return super.onOptionsItemSelected(item);

    }



    public void setTypeface(){
        String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf",demibold="font/avenir-next-demi-bold.ttf";
        Typeface bold=Typeface.createFromAsset(a.getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(a.getAssets(),avenirMedium);
        Typeface dem=Typeface.createFromAsset(a.getAssets(),demibold);

        radioTodo.setTypeface(medium);
        radioTareas.setTypeface(medium);
        radioAnun.setTypeface(medium);
        radioEnfer.setTypeface(medium);

    }

}
