package nieucrm.nieu.mx.skola.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import nieucrm.nieu.mx.skola.Adapter.MonitorAdapter;
import nieucrm.nieu.mx.skola.BuildConfig;
import nieucrm.nieu.mx.skola.DataModel.AccessData;
import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;

/**
 * Created by Carlos Romero
 */

public class MonitorAccessFragment extends Fragment {

    ListView listView;
    MonitorAdapter adapter;
    ArrayList<AccessData> listAccesos;
    ProgressBar progressMonitor;
    AccessData data;
    PubNub pubNub;
    ParseObject grupo;
    int puntualidad=0;
    ParseObject g;
    String grup;

    String escuelaId;
    ParseObject escuela;

    Activity a;

    public MonitorAccessFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            a=(Activity) context;
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_monitor_access, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listView=(ListView)view.findViewById(R.id.listaMonitorAccesos);
        progressMonitor=(ProgressBar)view.findViewById(R.id.progressMonitor);
        getAccess();
        subscribePubNub();

    }

    public void getAccess(){

        listAccesos=new ArrayList<>();


        ParseQuery<ParseObject>query=ParseQuery.getQuery("Acceso");
        query.include("student").include("user").orderByDescending("createdAt");

        query.include("user.escuela");
        query.include("student.grupo");


        query.setLimit(20);

        escuelaId=((ParseApplication)a.getApplication()).getEscuelaId();
        escuela=ParseObject.createWithoutData("Escuela",escuelaId);

        query.findInBackground(new FindCallback<ParseObject>() {

            @Override
            public void done(final List<ParseObject> objectList, ParseException e) {

                progressMonitor.setVisibility(View.GONE);
                listView.setVisibility(View.VISIBLE);
                if(e==null){
                    for(ParseObject objects:objectList){
                        data=new AccessData();

                        ParseUser userObject=objects.getParseUser("user");
                        ParseObject estObjects=objects.getParseObject("student");
                        ParseObject esc=userObject.getParseObject("escuela");


                        if (userObject.getParseObject("escuela").getObjectId().equals(escuelaId)){
                            Log.d(escuelaId,esc.getObjectId());
                                    String nombreP=userObject.getString("nombre")+" "+userObject.getString("apellidos");
                                    String parentesco=userObject.getString("parentesco");
                                    data.setNombrePadre(nombreP);
                                    data.setParentesco(parentesco);

                                    if (estObjects==null){
                                        data.setNombreNino("Acceso Staff");
                                        data.setGrupo("");
                                    }else {
                                        String nombre=estObjects.getString("NOMBRE")+" "+estObjects.getString("APELLIDO");
                                        data.setNombreNino(nombre);
                                        grupo=estObjects.getParseObject("grupo");
                                        if (grupo == null) {
                                            data.setGrupo("Grupo");
                                        }else {
                                            data.setGrupo(grupo.getString("grupoId"));
                                        }



                                    }

                                    if (objects.getNumber("puntualidad")==null) {
                                        data.setColor(0);
                                    }else {

                                        puntualidad=objects.getNumber("puntualidad").intValue();

                                        if (puntualidad==0){
                                            data.setColor(0);

                                        }else {
                                            if (puntualidad==1){
                                                data.setColor(1);

                                            }else{
                                                if(puntualidad==2) {
                                                    data.setColor(2);

                                                }else {
                                                    data.setColor(0);

                                                }
                                            }
                                        }
                                    }




                                    //data.setFecha(objects.getUpdatedAt().getDate()+"/"+objects.getUpdatedAt().getMonth()+"/"+objects.getUpdatedAt().getYear());

                                    Date p =objects.getCreatedAt();
                                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                                    try {
                                        p = sdf.parse(sdf.format(p));
                                    } catch (java.text.ParseException e1) {
                                        e1.printStackTrace();

                                    }
                                    sdf.setTimeZone(TimeZone.getTimeZone("CST"));

                                    data.setFecha(sdf.format(p));

                                    DateFormat df=new SimpleDateFormat("HH:mm", Locale.US);
                                    Date d=new Date(objects.getCreatedAt().getTime());
                                    String format=df.format(d);
                                    data.setHora(format);

                                    listAccesos.add(data);
                                }

                        //Toast.makeText(getContext(),grupo,Toast.LENGTH_SHORT).show();
                        //g=ParseObject.createWithoutData("grupo",grupo);

                        /*
                        ParseQuery<ParseObject> grupos=ParseQuery.getQuery("grupo").whereEqualTo("objectId",grupo);
                        grupos.findInBackground(new FindCallback<ParseObject>() {
                            @Override
                            public void done(List<ParseObject> objects, ParseException e) {
                                if (e==null){
                                    for (ParseObject a:objects){
                                        if (e==null){
                                            data.setGrupo(a.getString("name"));

                                        }else {
                                            grup="Grupo";
                                            data.setGrupo(grup);
                                        }
                                    }

                                }else {
                                    grup="Grupo";
                                    data.setGrupo(grup);
                                }
                            }
                        });
                        */


                    }
                    adapter=new MonitorAdapter(a,listAccesos);
                    listView.setAdapter(adapter);
                }else {
                    Log.e("Error monitor",e.getMessage());
                }
            }
        });

    }


    public void subscribePubNub() {
        PNConfiguration pnConfiguration = new PNConfiguration();
        pnConfiguration.setPublishKey(BuildConfig.pubNubPublish);
        pnConfiguration.setSubscribeKey(BuildConfig.pubNubSubscribe);
        pnConfiguration.setSecure(false);
        pubNub = new PubNub(pnConfiguration);
        pubNub.subscribe().channels(Collections.singletonList("QRCodeScanner_Channel")).execute();
    }
}
