package nieucrm.nieu.mx.skola.Fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import in.myinnos.library.AppIconNameChanger;
import nieucrm.nieu.mx.skola.Activity.Home;
import nieucrm.nieu.mx.skola.BuildConfig;
import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;

/**
 * Created by Carlos Romero
 */

public class InformationFragment extends Fragment {

    private ImageButton btnMenu,btnReglamento,btnDirectorio,linkMom,btnAviso,btnPrograma,btnActividad;
    private TextView link,escuela;
    ImageView imageView3;
    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf";
    Bundle b;
    ProgressDialog p;
    String nombre;

    Fragment fragment=null;
    Activity a;

    public InformationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            a=(Activity) context;
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_information, container, false);

        btnMenu=(ImageButton)v.findViewById(R.id.btnMenu);
        btnDirectorio=(ImageButton)v.findViewById(R.id.btnDirectorio);
        btnReglamento=(ImageButton)v.findViewById(R.id.btnReglamento);
        link=(TextView)v.findViewById(R.id.link);
        escuela=(TextView)v.findViewById(R.id.nombreEscuela);
        linkMom=(ImageButton) v.findViewById(R.id.linkMom);
        btnAviso=(ImageButton) v.findViewById(R.id.btnAviso);
        btnPrograma=(ImageButton) v.findViewById(R.id.btnPrograma);
        btnActividad=(ImageButton) v.findViewById(R.id.btnActividad);

        imageView3=(ImageView)v.findViewById(R.id.imageView3);

        escuela.setText(BuildConfig.name);

        Typeface bold=Typeface.createFromAsset(a.getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(getActivity().getAssets(),avenirMedium);

        link.setTypeface(medium);
        escuela.setTypeface(medium);
        link.setText(BuildConfig.link);

        b=new Bundle();
        p=new ProgressDialog(getContext());
        p.setTitle("Cargando...");
        p.setMessage("Por favor espere");
        p.setCancelable(false);

        nombre=((ParseApplication)getActivity().getApplication()).getEscuelaNombre();

        if (nombre==null){
            Log.e("Error","Usuario sin escuela");
        }else {
            changeImage();

        }


        setListeners();

        if (BuildConfig.FLAVOR.equals("momsTotsTol")||BuildConfig.FLAVOR.equals("momsTotsMet")) {
            linkMom.setVisibility(View.VISIBLE);
            escuela.setVisibility(View.GONE);
            btnActividad.setVisibility(View.VISIBLE);
            btnPrograma.setVisibility(View.VISIBLE);
            btnAviso.setVisibility(View.VISIBLE);
        }

        return v;
    }

    public void setListeners(){
        btnReglamento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                p.show();
                fragment=new InformationViewFragment();
                // app.setTipoInformacion(1);
                b.putInt("tipo",1);
                fragment.setArguments(b);
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                p.hide();

            }
        });

        btnDirectorio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                p.show();

                fragment=new InformationViewFragment();
                // app.setTipoInformacion(2);
                b.putInt("tipo",2);
                fragment.setArguments(b);

                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                p.hide();

            }
        });


        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                p.show();

                fragment=new InformationViewFragment();
                // app.setTipoInformacion(3);
                b.putInt("tipo",3);
                fragment.setArguments(b);
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                p.hide();

            }
        });

        btnPrograma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment=new InformationViewFragment();
                // app.setTipoInformacion(3);
                b.putInt("tipo",4);
                fragment.setArguments(b);
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                p.hide();
            }
        });

        btnActividad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment=new InformationViewFragment();
                // app.setTipoInformacion(3);
                b.putInt("tipo",5);
                fragment.setArguments(b);
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                p.hide();
            }
        });

        btnAviso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment=new InformationViewFragment();
                // app.setTipoInformacion(3);
                b.putInt("tipo",6);
                fragment.setArguments(b);
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                p.hide();
            }
        });

        link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String url = "http://skola.nieu.mx/";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(BuildConfig.link));
                startActivity(i);
            }
        });
        linkMom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(BuildConfig.link));
                startActivity(i);
            }
        });
    }

    private void changeImage() {
        if (nombre.equals("Baúl Azúl Cancún")) {
            imageView3.setImageResource(R.drawable.sidemenu_baul);
            escuela.setText(nombre);


        }

        if (nombre.equals("Skola México")) {
            imageView3.setImageResource(R.drawable.sidemenu_logo);
            escuela.setText(nombre);


        }
        if (nombre.equals("BabyBoomers Metepec")) {
            imageView3.setImageResource(R.drawable.sidemenu_bb);

            escuela.setText(nombre);


        }

        if (nombre.equals("BabyBoomers Capultitlán")) {
            imageView3.setImageResource(R.drawable.sidemenu_bb);

            escuela.setText(nombre);


        }

        if (nombre.equals("BabyBoomers Colón")) {
            imageView3.setImageResource(R.drawable.sidemenu_bb);
            escuela.setText(nombre);


        }

        if (nombre.equals("Moms and Tots Metepec")) {
            imageView3.setImageResource(R.drawable.sidemenu_mtm);
            linkMom.setVisibility(View.VISIBLE);
            escuela.setVisibility(View.GONE);
            btnActividad.setVisibility(View.VISIBLE);
            btnPrograma.setVisibility(View.VISIBLE);
            btnAviso.setVisibility(View.VISIBLE);


        }

        if (nombre.equals("Moms and Tots Toluca")) {
            imageView3.setImageResource(R.drawable.sidemenu_mtm);
            linkMom.setVisibility(View.VISIBLE);
            escuela.setVisibility(View.GONE);
            btnActividad.setVisibility(View.VISIBLE);
            btnPrograma.setVisibility(View.VISIBLE);
            btnAviso.setVisibility(View.VISIBLE);

        }

        if (nombre.equals("Chibolines")) {
            imageView3.setImageResource(R.drawable.sidemenu_chib);
            escuela.setText(nombre);


        }

        if (nombre.equals("Little Feet")) {
            imageView3.setImageResource(R.drawable.sidemenu_lf);
            escuela.setText(nombre);


        }

        if (nombre.equals("Homy")) {
            imageView3.setImageResource(R.drawable.sidemenu_homy);
            escuela.setText(nombre);


        }

        if (nombre.equals("Colegio del Ángel")) {
            imageView3.setImageResource(R.drawable.sidemenu_cda);
            escuela.setText(nombre);

        }

        if (nombre.equals("Pashamama")) {
            imageView3.setImageResource(R.drawable.sidemenu_pasha);
            escuela.setText(nombre);
        }

        if (nombre.equals("Estancia Pandín")) {
            imageView3.setImageResource(R.drawable.sidemenu_pandin);
            escuela.setText(nombre);
        }

        if (nombre.equals("Summerville Polanco")) {
            imageView3.setImageResource(R.drawable.sidemenu_summer);
            escuela.setText(nombre);
        }

        if (nombre.equals("Educación Especial MAS")) {
            imageView3.setImageResource(R.drawable.sidemenu_edu);
            escuela.setText(nombre);

        }

        if (nombre.equals("Colegio Ciprés Metepec")) {
            imageView3.setImageResource(R.drawable.sidemenu_cipres);
            escuela.setText(nombre);

        }

        if (nombre.equals("Glenn Doman")) {
            imageView3.setImageResource(R.drawable.sidemenu_glenn);
            escuela.setText(nombre);


        }
        if (nombre.equals("Colegio Torreón")) {
            imageView3.setImageResource(R.drawable.sidemenu_torreon);
            escuela.setText(nombre);

        }

        if (nombre.equals("Kids Bee Happy")) {
            imageView3.setImageResource(R.drawable.sidemenu_kids);
            escuela.setText(nombre);
        }

        if (nombre.equals("Balance Cube Metepec")) {
            imageView3.setImageResource(R.drawable.sidemenu_cube);
            escuela.setText(nombre);
        }

        if (nombre.equals("Mi Mundo Mágico")) {
            imageView3.setImageResource(R.drawable.sidemenu_mundo);
            //textAction.setText("Colegio Ciprés Metepec");
            escuela.setText(nombre);

        }

        if (nombre.equals("Colegio Antara")){
            imageView3.setImageResource(R.drawable.sidemenu_ant);
            escuela.setText(nombre);
        }

        if (nombre.equals("Kids & Babies Toluca")){
            imageView3.setImageResource(R.drawable.sidemenu_kb);
            escuela.setText(nombre);
        }

    }




}
