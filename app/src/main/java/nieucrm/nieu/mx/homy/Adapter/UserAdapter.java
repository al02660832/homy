package nieucrm.nieu.mx.skola.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import nieucrm.nieu.mx.skola.DataModel.UserData;
import nieucrm.nieu.mx.skola.Fragments.NewUserFragment;
import nieucrm.nieu.mx.skola.R;


/**
 * Created by Carlos Romero  on 10/12/2016.
 */

public class UserAdapter extends BaseAdapter {

    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf";



    private Context context;
    LayoutInflater inflater;
    private List<UserData> data=null;
    private ArrayList<UserData>arrayList=null;




    public UserAdapter(Context context, List<UserData> data) {
        this.context = context;
        this.data=data;
        try {
            inflater=LayoutInflater.from(context);

        }catch (Exception e){
            Log.d("Error",e.getMessage());
        }
        this.arrayList=new ArrayList<>();
        this.arrayList.addAll(data);
    }

    public class ViewHolder{
        TextView nombreU;
        TextView userName;

    }


    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Typeface bold=Typeface.createFromAsset(context.getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(context.getAssets(),avenirMedium);

        ViewHolder holder;
       // TextView nombreEstudiante=(TextView)v.findViewById(R.id.nombreEstudiante);

        if(convertView==null){
            holder=new ViewHolder();
            convertView=inflater.inflate(R.layout.user_item_layout,null);
            holder.userName = (TextView) convertView.findViewById(R.id.userName);
            holder.nombreU = (TextView) convertView.findViewById(R.id.nombreU);


            holder.nombreU.setTypeface(medium);
            holder.userName.setTypeface(medium);

            convertView.setTag(holder);
        }else {
            holder=(ViewHolder)convertView.getTag();
        }

        /*
        if (estudianteData.get(position).getNoMensajes()==0){
            holder.mensajes.setVisibility(View.INVISIBLE);
        }else {
            holder.mensajes.setText(estudianteData.get(position).getNoMensajes());
        }
        */

        holder.userName.setText(data.get(position).getUsername());
        holder.nombreU.setText(data.get(position).getName());


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new NewUserFragment();
                Bundle bundle=new Bundle();
                bundle.putString("username",data.get(position).getUsername());
                bundle.putString("name",data.get(position).getName());
                bundle.putString("objectId",data.get(position).getObjectId());
                bundle.putString("nombre",data.get(position).getNombre());
                bundle.putString("apellidos",data.get(position).getApellido());
                bundle.putBoolean("existe",data.get(position).isExiste());
                fragment.setArguments(bundle);
                FragmentTransaction transaction= ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();

            }
        });


        return convertView;


    }

}
