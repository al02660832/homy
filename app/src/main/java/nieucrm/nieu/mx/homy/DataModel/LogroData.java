package nieucrm.nieu.mx.skola.DataModel;

/**
 * Created by mmac1 on 19/07/2017.
 */

public class LogroData {

    private String imagenLogro,nombre,descripcion;

    public String getImagenLogro() {
        return imagenLogro;
    }

    public void setImagenLogro(String imagenLogro) {
        this.imagenLogro = imagenLogro;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
