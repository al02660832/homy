package nieucrm.nieu.mx.skola.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import nieucrm.nieu.mx.skola.DataModel.PaqueteData;
import nieucrm.nieu.mx.skola.Fragments.NewPaqueteFragment;
import nieucrm.nieu.mx.skola.R;


/**
 * Created by Carlos Romero on 14/03/2017.
 */

public class PaqueteAdapter extends BaseAdapter {

    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf",avenirDemibold="font/avenir-next-demi-bold.ttf";

    String hourE,hourS;

    private Context context;
    LayoutInflater inflater;
    private List<PaqueteData> paqueteData=null;
    private ArrayList<PaqueteData> array=null;

    public PaqueteAdapter(Context context, List<PaqueteData> paqueteData) {
        this.context = context;
        this.paqueteData=paqueteData;
        try {
            inflater=LayoutInflater.from(context);

        }catch (Exception e){
            Log.d("Error",e.getMessage());
        }
        this.array=new ArrayList<>();
        this.array.addAll(paqueteData);
    }

    public class ViewHolder{
        TextView nivelPaquete,horaPaquete,numPaquete;
    }


    @Override
    public int getCount() {
        return paqueteData.size();
    }

    @Override
    public Object getItem(int position) {
        return paqueteData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        Typeface bold=Typeface.createFromAsset(context.getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(context.getAssets(),avenirMedium);
        Typeface demibold=Typeface.createFromAsset(context.getAssets(),avenirDemibold);

        ViewHolder holder;
        // TextView nombreEstudiante=(TextView)v.findViewById(R.id.nombreEstudiante);

        if(convertView==null){
            holder=new ViewHolder();
            convertView=inflater.inflate(R.layout.paquete_item_layout,null);

            holder.nivelPaquete=(TextView)convertView.findViewById(R.id.nivelPaquete);
            holder.horaPaquete=(TextView)convertView.findViewById(R.id.horaPaquete);
            holder.numPaquete=(TextView)convertView.findViewById(R.id.numPaquete);

            holder.nivelPaquete.setTypeface(medium);
            holder.horaPaquete.setTypeface(medium);
            holder.numPaquete.setTypeface(medium);

            convertView.setTag(holder);
        }else {
            holder=(ViewHolder)convertView.getTag();
        }

        holder.numPaquete.setText(paqueteData.get(position).getNum());
        holder.horaPaquete.setText(paqueteData.get(position).getHoras());
        holder.nivelPaquete.setText(paqueteData.get(position).getNivel());



        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hourE=(String)android.text.format.DateFormat.format("hh:mm",paqueteData.get(position).getHoraEntrada());
                hourS=(String)android.text.format.DateFormat.format("hh:mm",paqueteData.get(position).getHoraSalida());
                Fragment fragment=new NewPaqueteFragment();
                Bundle b=new Bundle();
                b.putString("nivel",paqueteData.get(position).getNivel());
                b.putString("num",paqueteData.get(position).getNum());
                b.putString("horas",paqueteData.get(position).getHoras());
                b.putString("precio",paqueteData.get(position).getPrecio());
                b.putString("alimentos",paqueteData.get(position).getAlimento());
                b.putString("pronto",paqueteData.get(position).getProntoPago());
                b.putBoolean("taller",paqueteData.get(position).isTaller());
                b.putString("horaEntrada",hourE);
                b.putString("horaSalida",hourS);
                b.putString("nivelId",paqueteData.get(position).getNivelId());
                b.putBoolean("nuevo",paqueteData.get(position).isNuevo());
                b.putString("objectId",paqueteData.get(position).getObjectId());
                b.putString("tipo",paqueteData.get(position).getTipo());
                b.putInt("horasServicio",paqueteData.get(position).getHorasServicio());
                fragment.setArguments(b);
                FragmentTransaction transaction= ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
            }
        });


        return convertView;
    }





}

