package nieucrm.nieu.mx.skola.Fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.io.File;
import java.io.IOException;

import nieucrm.nieu.mx.skola.Helpers.Constants;
import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.Helpers.Util;
import nieucrm.nieu.mx.skola.R;

/**
 * Created by Carlos Romero
 */

public class InformationViewFragment extends Fragment{

    //private TextView txtTitulo,txtContenido;
    private LinearLayout layoutInformacion;

    String pdfUrl;

    TextView infoType,contentInfo;

    String tipo, contenido,objectId;

    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf";
    private TextView nombreEs;
    //ScrollView scrollText;
    TransferUtility utility;

    ParseQuery<ParseObject>c=ParseQuery.getQuery("Informacion");
    String escuelaId;
    ParseObject escuela;

    ProgressBar progressInfo;

    File outputDir;
    File pdfFile;

    PDFView pdfView;
    Activity a;



    public InformationViewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            a=(Activity) context;
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View v= inflater.inflate(R.layout.fragment_information_view, container, false);

        Bundle b=this.getArguments();
        tipo=b.getString("tipo");
        contenido=b.getString("content");
        objectId=b.getString("objectId");

        progressInfo=(ProgressBar)v.findViewById(R.id.progressInfo);

        infoType=(TextView)v.findViewById(R.id.infoType);
        contentInfo=(TextView)v.findViewById(R.id.contentInfo);

        //txtContenido=(TextView)v.findViewById(R.id.contenidoInformacion);
        layoutInformacion=(LinearLayout)v.findViewById(R.id.layoutInformacion);

        nombreEs = (TextView) a.findViewById(R.id.textAction);

        pdfView=(PDFView)v.findViewById(R.id.pdfView);



        //scrollText=(ScrollView)v.findViewById(R.id.scrollText);


        //tipoInfo();

        typeface();

        downloadFromS3(objectId);


        return v;
    }

    /*
    public void tipoInfo(){

        switch (t){
            case 1:
                //setView("Reglamento");
                layoutInformacion.setBackgroundColor(getResources().getColor(R.color.red_rules));

                break;

            case 2:
                //

                //setView("Directorio");

                layoutInformacion.setBackgroundColor(getResources().getColor(R.color.blue_directory));
                break;

            case 3:
                //nombres.setText("Menú Semanal");
                //setView("Menú Semanal");
                layoutInformacion.setBackgroundColor(getResources().getColor(R.color.green_menu));

                break;

            case 4:
                //setView("Programa Semanal");
                layoutInformacion.setBackgroundColor(getResources().getColor(R.color.groups));
                break;

            case 5:
                //setView("Actividad Especial");

                layoutInformacion.setBackgroundColor(getResources().getColor(R.color.blue_payment));
                break;
            case 6:
                //setView("Avisos");

                layoutInformacion.setBackgroundColor(getResources().getColor(R.color.yellow_payments));
                break;

        }
    }
    */

    public void typeface(){
        Typeface bold=Typeface.createFromAsset(a.getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(a.getAssets(),avenirMedium);

        //txtContenido.setTypeface(medium);
        infoType.setTypeface(bold);
        contentInfo.setTypeface(medium);
    }

    public void downloadFromS3(String objectId){

        pdfView.setVisibility(View.VISIBLE);





        try {
            utility= Util.getTransferUtility(a);
        }catch (Exception e1){
            Log.e("Error credencial",e1.getMessage());
        }

        outputDir = a.getCacheDir();
        try {
            pdfFile= File.createTempFile("prefix", "extension", outputDir);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            TransferObserver observe= utility.download(Constants.BUCKET_NAME,objectId,pdfFile);
            observe.setTransferListener(new TransferListener() {
                @Override
                public void onStateChanged(int id, TransferState state) {
                    if (state==TransferState.COMPLETED){
                        pdfView.fromFile(pdfFile).defaultPage(1).enableSwipe(true).onLoad(new OnLoadCompleteListener() {
                            @Override
                            public void loadComplete(int nbPages) {
                                Log.d("Number of pages",String.valueOf(nbPages));
                                progressInfo.setVisibility(View.GONE);
                                infoType.setText(tipo);

                                if (contenido==null||contenido.equals("")){
                                    contentInfo.setVisibility(View.GONE);
                                }else {
                                    contentInfo.setText(contenido);
                                }

                            }
                        }).onPageChange(new OnPageChangeListener() {
                            @Override
                            public void onPageChanged(int page, int pageCount) {
                                Log.d("Page:",String.valueOf(page));
                            }
                        }).load();
                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                    long _bytesCurrent = bytesCurrent;
                    long _bytesTotal = bytesTotal;

                    float percentage =  ((float)_bytesCurrent /(float)_bytesTotal * 100);
                    Log.d("percentage","" +percentage);
                }

                @Override
                public void onError(int id, Exception ex) {
                    Log.d("Error de Amazon",ex.getMessage());
                }
            });

        }catch (Exception e){
            Log.e("Error",e.getMessage());
        }
    }


    /*
    public void setView(final String titulo){

        webView.setVisibility(View.VISIBLE);


        escuelaId=((ParseApplication)getActivity().getApplication()).getEscuelaId();
        escuela=ParseObject.createWithoutData("Escuela",escuelaId);

        c.whereEqualTo("escuela",escuela);

        c.whereEqualTo("tipo",titulo);


        c.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {

                if (e==null){
                    if (object==null){


                    }else {


                        if(object.getString("contenido")==null){

                        }else {
                            if (object.getString("contenido").equals("")||object.getString("contenido")==null){

                            }else{
                                String c=object.getString("contenido");


                            }
                        }

                        if (object.getBoolean("aws")){
                            webView.setVisibility(View.GONE);
                            downloadFromS3(object.getObjectId());
                            pdfView.setVisibility(View.VISIBLE);

                            Log.e("Visibilidad",String.valueOf(pdfView.getVisibility()));

                            Log.e("Archivo ","S3");

                        }else {
                            Log.e("Archivo","Parse");
                            ParseFile file=object.getParseFile("pdf");
                            if (file!=null){
                                pdfUrl = object.getParseFile("pdf").getUrl();

                                webView.loadData(pdfUrl,"text/html","UTF-8");
                                webView.loadUrl("http://docs.google.com/gview?embedded=true&url="+pdfUrl);
                                webView.setWebViewClient(new WebViewClient() {
                                    @Override
                                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                        view.loadUrl(url);
                                        return true;
                                    }
                                });
                                webView.setVisibility(View.VISIBLE);

                            }
                        }
                    }
                }else{
                    Toast.makeText(getContext(),"Por el momento no hay información",Toast.LENGTH_SHORT).show();
                }

            }
        });

    }
    */

}

