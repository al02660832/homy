package nieucrm.nieu.mx.skola.Fragments;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.github.chrisbanes.photoview.PhotoView;
import com.parse.DeleteCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import nieucrm.nieu.mx.skola.Helpers.Constants;
import nieucrm.nieu.mx.skola.Helpers.Util;
import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class SingleItemFragment extends Fragment {


    public SingleItemFragment() {
        // Required empty public constructor
    }
    String region="https://s3-us-east-2.amazonaws.com/skola-photos/";

    ProgressBar progressGallery;
    String image;
    PhotoView img;
    TextView borrarFoto;
    //ImageLoader imageLoader=new ImageLoader(getContext());
    TransferUtility utility;
    File imageFile;
    File outputDir;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment



        return inflater.inflate(R.layout.fragment_single_item, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle b=this.getArguments();
        image=b.getString("image");

        img=(PhotoView) view.findViewById(R.id.imagenP);
        borrarFoto=(TextView)view.findViewById(R.id.borrarFoto);
        progressGallery=(ProgressBar)view.findViewById(R.id.progressGallery);
        if (ParseUser.getCurrentUser().getNumber("usertype").intValue()==2){
            borrarFoto.setVisibility(View.GONE);
        }

        borrarFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eliminarFoto(image);
            }
        });

        downloadFromS3(image);

        img.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                final CharSequence[] items = {"Guardar imagen", "Compartir",
                        "Cancelar"};
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Selecciona una opción: ");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (items[item].equals("Guardar imagen")) {
                            savePhoto();
                        } else if (items[item].equals("Compartir")) {

                            sharePhoto();

                        } else if (items[item].equals("Cancel")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
                return false;
            }
        });

    }

    public void savePhoto() {

        //Funciona
        img.buildDrawingCache();

        Bitmap bmp = img.getDrawingCache();

        File storageLoc = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES); //context.getExternalFilesDir(null);

        File file = new File(storageLoc, System.currentTimeMillis() + ".jpg");

        try{
            FileOutputStream fos = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.close();

            scanFile(getContext(), Uri.fromFile(file));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void sharePhoto(){


        img.buildDrawingCache();

        Bitmap bmp = img.getDrawingCache();

        File storageLoc = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES); //context.getExternalFilesDir(null);

        File file = new File(storageLoc, System.currentTimeMillis() + ".jpg");

        try{
            FileOutputStream fos = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.close();

            scanFile(getContext(), Uri.fromFile(file));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/*");
        share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
        startActivity(Intent.createChooser(share,"Share via"));

    }

    private static void scanFile(Context context, Uri imageUri){
        Intent scanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        scanIntent.setData(imageUri);
        context.sendBroadcast(scanIntent);

    }

    public void eliminarFoto(String id){
        ParseQuery<ParseObject> pagosQuery=ParseQuery.getQuery("EventoGaleria");
        pagosQuery.whereEqualTo("objectId",id);
        pagosQuery.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (e==null){
                    object.deleteInBackground(new DeleteCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e==null){
                                try {
                                    Toast.makeText(getContext(),"Foto eliminada satisfactoriamente",Toast.LENGTH_SHORT).show();
                                    getActivity().getSupportFragmentManager().popBackStack();

                                }catch (Exception ex){
                                    Log.e("Error eliminar",ex.getMessage());
                                }
                            }else {
                                Log.e("Eliminar",e.getMessage());
                                try {
                                    Toast.makeText(getContext(),"Error al eliminar, intente de nuevo",Toast.LENGTH_SHORT).show();
                                }catch (Exception ex){
                                    Log.e("Error eliminar",ex.getMessage());
                                }
                            }
                        }
                    });
                }else {
                    Log.d("Error",e.getMessage());
                }
            }
        });
    }

    public void downloadFromS3(String objectId){

        try {
            utility= Util.getTransferUtility(getContext());
        }catch (Exception e1){
            Log.e("Error credencial",e1.getMessage());
        }

        outputDir = getContext().getCacheDir();
        try {
            imageFile= File.createTempFile("prefix", "extension", outputDir);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            TransferObserver observe= utility.download(Constants.BUCKET_NAME,objectId,imageFile);
            observe.setTransferListener(new TransferListener() {
                @Override
                public void onStateChanged(int id, TransferState state) {
                    if (state==TransferState.COMPLETED){
                        progressGallery.setVisibility(View.GONE);
                        Bitmap bitmap= BitmapFactory.decodeFile(imageFile.getAbsolutePath());
                        img.setImageBitmap(bitmap);
                        try {
                            //Toast.makeText(getContext(), "Foto lista", Toast.LENGTH_SHORT).show();
                        }catch (Exception e1){
                            Log.e("Exception",e1.getMessage());
                        }
                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                    long _bytesCurrent = bytesCurrent;
                    long _bytesTotal = bytesTotal;

                    float percentage =  ((float)_bytesCurrent /(float)_bytesTotal * 100);
                    Log.d("percentage","" +percentage);
                }

                @Override
                public void onError(int id, Exception ex) {
                    progressGallery.setVisibility(View.GONE);
                    Log.d("Error de Amazon",ex.getMessage());
                }
            });

        }catch (Exception e){
            Log.e("Error",e.getMessage());
        }
    }

}
