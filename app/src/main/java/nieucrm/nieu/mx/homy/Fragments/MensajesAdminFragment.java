package nieucrm.nieu.mx.skola.Fragments;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import nieucrm.nieu.mx.skola.Adapter.MensajeAdapter;
import nieucrm.nieu.mx.skola.DataModel.MensajeData;
import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class MensajesAdminFragment extends Fragment {

    String destino;
    ArrayList<MensajeData> mensajeDataArray,alumnos,menGroup;
    ArrayList<MensajeData> aprobar;
    MensajeAdapter mensajeAdapter, mensajeAprobar;
    ListView listAprobado, listaMensajeAprobar;
    ProgressBar progressPorAprobar, progressAprobados;
    TextView mensajesAprobar;
    String escuelaId;
    ParseObject escuela;
    String avenirBold = "font/avenir-next-bold.ttf", avenirMedium = "font/avenir-next-medium.ttf", demibold = "font/avenir-next-demi-bold.ttf";
    int usertype;
    ArrayList<MensajeData> maestros;
    ArrayList<String> groups,estu;
    boolean grupoM;

    ArrayList<ParseObject>nivArray, groupArray;
    ArrayList<String>nivNom;
    String[]arrayNiv;
    String niveles, nivelId, selection, seleccion;

    ArrayList<String>anuncioPhoto;
    Activity a;


    public MensajesAdminFragment() {
        // Required empty public constructor
    }

    //OnAttach
    //OnCreate


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);


        if (context instanceof Activity){
            a=(Activity) context;
        }


    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true);

        return inflater.inflate(R.layout.fragment_mensajes_admin, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);
        listAprobado = (ListView) v.findViewById(R.id.mensajeListaAprobado);

        progressAprobados = (ProgressBar) v.findViewById(R.id.progressAprobado);
        progressPorAprobar = (ProgressBar) v.findViewById(R.id.progressPorAprobar);
        listaMensajeAprobar = (ListView) v.findViewById(R.id.listaMensajeAprobar);

        mensajesAprobar = (TextView) v.findViewById(R.id.mensajesAprobar);
        usertype = ParseUser.getCurrentUser().getNumber("usertype").intValue();


        setTypeface();

        getAnuncioPhoto();

    }

    public void setAprobados() {

        maestros = new ArrayList<>();
        mensajeDataArray = new ArrayList<>();
        menGroup=new ArrayList<>();
        alumnos=new ArrayList<>();

        escuelaId = ((ParseApplication) a.getApplication()).getEscuelaId();
        escuela = ParseObject.createWithoutData("Escuela", escuelaId);
        ParseQuery<ParseUser>userQ=ParseUser.getQuery();
        userQ.include("escuela");
        userQ.whereEqualTo("usertype",2);
        userQ.whereEqualTo("escuela",escuela);

        ParseQuery<ParseObject> q = new ParseQuery<>("anuncio");
        q.whereEqualTo("aprobado", true);
        q.whereNotEqualTo("actionTaken", true);
        q.include("autor");
        q.include("estudiante");
        q.include("grupos");
        q.include("tipo");
        q.whereMatchesQuery("autor",userQ);


        if (usertype==1){
            q.whereContainedIn("estudiante",estu);
            Log.e("Usertype","Maestro");
        }

        q.orderByDescending("createdAt");





        q.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                progressAprobados.setVisibility(View.GONE);
                listAprobado.setVisibility(View.VISIBLE);

                if (e == null) {
                    Log.e("Mensajes #",String.valueOf(objects.size()));
                    for (ParseObject o:objects){
                        MensajeData mensajeData = new MensajeData();

                        ParseObject est = o.getParseObject("estudiante");
                        ParseUser autor = o.getParseUser("autor");
                        ParseObject od = o.getParseObject("tipo");

                        if (autor.getString("parentesco").equals("Admin")) {
                            mensajeData.setParentesco(autor.getString("parentesco") + " de");
                        } else {
                            mensajeData.setParentesco(autor.getString("parentesco") + " de");
                        }

                        if (est == null) {
                            if (o.get("grupos") == null) {
                                mensajeData.setDestinatario("Todo el colegio");
                            } else {
                                //fILTRAR GRUPOS DE MAESTRA
                                List<ParseObject> g = o.getList("grupos");

                                StringBuilder a = new StringBuilder();
                                for (int i = 0; i < g.size(); i++) {
                                    try {
                                        if (i == g.size() - 1) {
                                            a.append(g.get(i).getString("grupoId"));
                                        } else {
                                            a.append(g.get(i).getString("grupoId")).append(", ");
                                            //Error
                                        }
                                    } catch (Exception e1) {
                                        Log.d("Error append", e1.getMessage());
                                    }
                                    mensajeData.setEmisor(a.toString());
                                }
                            }
                        } else {
                            //Filtrar estudiante
                            mensajeData.setStudentId(est.getObjectId());

                            mensajeData.setEmisor(est.getString("NOMBRE") + " " + est.getString("APELLIDO"));
                        }
                        if (o.getParseFile("attachment")==null){
                            if (o.getBoolean("awsAttachment")){
                                mensajeData.setAwsAttachment(true);
                            }else {
                                if (anuncioPhoto.size()==0){
                                    Log.e("Foto","Fotos en anuncio");
                                }else {
                                    if (anuncioPhoto.contains(o.getObjectId())){
                                        mensajeData.setTablaAnuncio(true);
                                        Log.e("Foto","Foto en tabla anuncio"+o.getObjectId());
                                    }
                                }
                            }
                        }else {
                            mensajeData.setParseUri(o.getParseFile("attachment").getUrl());
                        }
                        Date p = o.getCreatedAt();
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                        try {
                            p = sdf.parse(sdf.format(p));
                        } catch (java.text.ParseException e1) {
                            e1.printStackTrace();
                        }
                        sdf.setTimeZone(TimeZone.getTimeZone("CST"));

                        mensajeData.setFechaEntrega(sdf.format(p));

                        mensajeData.setID(o.getObjectId());

                        DateFormat df = new SimpleDateFormat("HH:mm:ss", Locale.US);
                        Date d = new Date(o.getCreatedAt().getTime());
                        String format = df.format(d);
                        mensajeData.setHora(format);
                        mensajeData.setSel("aprobado");
                        if (od == null) {
                            mensajeData.setTipo("Anuncio");
                        } else {
                            mensajeData.setTipo(od.getString("nombre"));
                        }
                        mensajeData.setDescripcion(o.getString("descripcion"));

                        mensajeDataArray.add(mensajeData);

                    }
                    mensajeAdapter = new MensajeAdapter(getActivity(), mensajeDataArray);
                    listAprobado.setAdapter(mensajeAdapter);

                } else {
                    Toast.makeText(a, e.getMessage(), Toast.LENGTH_SHORT).show();
                    Log.e("Error query",e.getMessage());
                }


                //Siguiente
                if (usertype == 1) {
                    progressPorAprobar.setVisibility(View.GONE);
                } else {
                    setPorAprobar();
                }

            }
        });


    }

    public void setPorAprobar() {

        aprobar = new ArrayList<>();

        ParseQuery<ParseObject> q = new ParseQuery<>("anuncio");
        q.whereEqualTo("aprobado", false);
        q.include("autor");
        q.include("autor.escuela");
        q.include("estudiante");
        q.include("grupos");
        q.include("tipo");
        q.orderByDescending("createdAt");
        q.setLimit(50);
        escuelaId = ((ParseApplication) a.getApplication()).getEscuelaId();

        escuela = ParseObject.createWithoutData("Escuela", escuelaId);
        q.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                progressPorAprobar.setVisibility(View.GONE);
                listaMensajeAprobar.setVisibility(View.VISIBLE);

                if (e == null) {

                    for (ParseObject o : objects) {
                        MensajeData mensajeData = new MensajeData();

                        ParseObject est = o.getParseObject("estudiante");
                        ParseUser autor = o.getParseUser("autor");
                        ParseObject gru = o.getParseObject("grupos");
                        ParseObject od = o.getParseObject("tipo");


                        if (autor == null) {
                            mensajeData.setParentesco("General");
                        } else {

                            if (autor.getParseObject("escuela") != null) {

                                if (autor.getParseObject("escuela").getObjectId().equals(escuelaId)) {
                                    if (o.getString("descripcion") == null) {

                                    } else {

                                    }


                                    if (est == null) {
                                        if (o.get("grupos") == null) {
                                            mensajeData.setDestinatario("Todo el colegio");
                                        } else {
                                            List<ParseObject> g = o.getList("grupos");


                                            StringBuilder a = new StringBuilder();
                                            for (int i = 0; i < g.size(); i++) {
                                                if (g.get(i).getString("grupoId")==null){
                                                    Log.e("Grupo null",String.valueOf(i));
                                                }else {
                                                    try {
                                                        if (i == g.size() - 1) {
                                                            a.append(g.get(i).getString("grupoId"));
                                                        } else {
                                                            a.append(g.get(i).getString("grupoId")).append(", ");
                                                            //Error

                                                        }
                                                    } catch (Exception e1) {
                                                        Log.d("Error", e1.getMessage());
                                                    }
                                                    mensajeData.setEmisor(a.toString());
                                                }

                                            }
                                        }
                                    } else {
                                        mensajeData.setStudentId(est.getObjectId());
                                        mensajeData.setEmisor(est.getString("NOMBRE") + " " + est.getString("APELLIDO"));
                                    }


                                    if (o.getParseFile("attachment")==null){
                                        if (o.getBoolean("awsAttachment")){
                                            mensajeData.setAwsAttachment(true);
                                        }else {
                                            if (anuncioPhoto.size()==0){
                                                Log.e("Foto","Fotos en anuncio");
                                            }else {
                                                if (anuncioPhoto.contains(o.getObjectId())){
                                                    mensajeData.setTablaAnuncio(true);
                                                    Log.e("Foto","Foto en tabla anuncio"+o.getObjectId());
                                                }
                                            }
                                        }
                                    }else {
                                        mensajeData.setParseUri(o.getParseFile("attachment").getUrl());
                                    }

                                    Date p = o.getCreatedAt();
                                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                                    try {
                                        p = sdf.parse(sdf.format(p));
                                    } catch (java.text.ParseException e1) {
                                        e1.printStackTrace();
                                    }


                                    sdf.setTimeZone(TimeZone.getTimeZone("CST"));


                                    mensajeData.setFechaEntrega(sdf.format(p));
                                    mensajeData.setParentesco(autor.getUsername());
                                    mensajeData.setPara(" Para:");
                                    mensajeData.setID(o.getObjectId());

                                    DateFormat df = new SimpleDateFormat("HH:mm", Locale.US);
                                    Date d = new Date(o.getCreatedAt().getTime());
                                    String format = df.format(d);
                                    mensajeData.setHora(format);
                                    mensajeData.setSel("pendiente");
                                    if (od == null) {
                                        mensajeData.setTipo("Anuncio");
                                    } else {

                                        mensajeData.setTipo(od.getString("nombre"));
                                    }

                                    mensajeData.setDescripcion(o.getString("descripcion"));

                                    aprobar.add(mensajeData);

                                } else {

                                }
                            }
                        }
                        //Set hora
                        // mensajeData.setHora("Hora");
                    }
                    try {
                        mensajeAprobar = new MensajeAdapter(a, aprobar);
                        listaMensajeAprobar.setAdapter(mensajeAprobar);
                    } catch (Exception e1) {
                        Log.d("Error", e1.getMessage());
                    }
                } else {
                    //Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }


            }
        });

    }

    public void grupos() {

        groups = new ArrayList<>();
        groupArray=new ArrayList<>();
        ParseQuery<ParseObject> est = ParseQuery.getQuery("grupo").include("Maestros").include("escuela").whereEqualTo("Maestros", ParseUser.getCurrentUser());
        est.whereEqualTo("Maestros", ParseUser.getCurrentUser());

        est.findInBackground(new FindCallback<ParseObject>() {

            @Override

            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {

                    for (ParseObject o : objects) {

                        groups.add(o.getObjectId());
                        groupArray.add(o);
                    }
                    getEstudiantes();
                    Log.e("Grupos","Sin error");
                } else {
                    Log.e("Error grupo", e.getMessage());
                    Toast.makeText(a, "Error", Toast.LENGTH_SHORT).show();

                }

            }

        });



    }

    public void getEstudiantes(){
        estu=new ArrayList<>();
        escuelaId = ((ParseApplication) a.getApplication()).getEscuelaId();
        escuela = ParseObject.createWithoutData("Escuela", escuelaId);
        ParseQuery<ParseObject> estudiante = ParseQuery.getQuery("Estudiantes").include("grupo");
        estudiante.whereContainedIn("grupo",groups);
        estudiante.whereEqualTo("status",0);
        estudiante.whereEqualTo("escuela",escuela);
        estudiante.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e==null){
                    for (ParseObject a:objects){
                        estu.add(a.getObjectId());

                    }
                    setAprobados();
                    Log.e("Estudiantes","Sin error");
                    //Toast.makeText(getContext(),"Estudiantes "+est.size(),Toast.LENGTH_SHORT).show();
                }else {
                    Log.e("ExceEs",e.getMessage());
                }
            }
        });
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        if (ParseUser.getCurrentUser().getNumber("usertype").intValue()==0){
            inflater.inflate(R.menu.historial,menu);
            inflater.inflate(R.menu.new_message,menu);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id=item.getItemId();
        if(id==R.id.historial){
            Fragment fragment = new HistorialFragment();
            FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
            transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
        }

        if (id==R.id.write_mess){
            createDialog();
            //Fragment fragment = new NewMessageFragment();
            //FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
            //transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
            //transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
        }
        return super.onOptionsItemSelected(item);

    }

    public void setTypeface(){
        //Typeface bold=Typeface.createFromAsset(getActivity().getAssets(),avenirBold);
        //Typeface medium=Typeface.createFromAsset(getActivity().getAssets(),avenirMedium);
        Typeface dem=Typeface.createFromAsset(getActivity().getAssets(),demibold);

        mensajesAprobar.setTypeface(dem);
    }

    //Obtiene los mensajes que tienen foto en tabla AnuncioPhoto y los almacena en Arraylist
    public void getAnuncioPhoto(){
        anuncioPhoto=new ArrayList<>();
        ParseQuery<ParseObject>anuncioPhotos=ParseQuery.getQuery("AnuncioPhoto");
        anuncioPhotos.include("anuncio");
        anuncioPhotos.setLimit(1000);
        anuncioPhotos.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {

                if (e==null){
                    for (ParseObject o:objects){
                        if(o.getParseObject("anuncio")!=null){
                            anuncioPhoto.add(o.getParseObject("anuncio").getObjectId());
                        }
                    }
                    Log.e("AnuncioPhoto",String.valueOf(anuncioPhoto.size()));
                }
                //Llamar al primer metodo
                if (usertype==0){
                    Log.e("Primer m","Admin");
                    setAprobados();
                }else {
                    if (usertype==1){
                        grupos();
                        Log.e("Metodos","Maestra");
                    }
                }
            }
        });
    }

    public void createDialog(){
        nivNom=new ArrayList<>();
        nivArray=new ArrayList<>();
        ParseQuery<ParseObject>niv=new ParseQuery<ParseObject>("Nivel");
        ParseObject escuela=ParseObject.createWithoutData("Escuela",escuelaId);
        niv.whereEqualTo("escuela",escuela);
        niv.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e==null){
                    nivNom.add("Toda la escuela");
                    for (ParseObject o:objects){
                        nivNom.add(o.getString("nombre"));
                        nivArray.add(o);
                    }

                    arrayNiv=new String[nivNom.size()];
                    arrayNiv=nivNom.toArray(arrayNiv);

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    // Set the dialog title
                    builder.setTitle("Seleccione un nivel: ")
                            // Specify the list array, the items to be selected by default (null for none),
                            // and the listener through which to receive callbacks when items are selected
                            .setItems(arrayNiv, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    niveles=nivNom.get(which);

                                    if (niveles.equals("Toda la escuela")){

                                        selection="all";
                                    }else {
                                        nivelId=nivArray.get(which-1).getObjectId();
                                        selection=nivelId;
                                    }
                                    Fragment fragment = new NewMessageFragment();
                                    Bundle b=new Bundle();
                                    b.putString("seccion","niv");
                                    b.putString("niveles",selection);
                                    b.putString("nombre",niveles);
                                    fragment.setArguments(b);
                                    FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                                    transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                                    transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();

                                }

                                // Set the action buttons
                            }).create().show();
                }else {
                    Log.e("Error","Error al obtener datos");
                }
            }
        });
    }


}
