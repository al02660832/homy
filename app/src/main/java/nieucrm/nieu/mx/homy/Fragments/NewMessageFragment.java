package nieucrm.nieu.mx.skola.Fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import nieucrm.nieu.mx.skola.Helpers.Constants;
import nieucrm.nieu.mx.skola.Helpers.ImagePicker;
import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.Helpers.Util;
import nieucrm.nieu.mx.skola.R;

/**
 * Created by Carlos Romero
 */

public class NewMessageFragment extends Fragment {

    TextView asuntoMensaje, mensajeTxt, editAsunto, mensajePara,textView2;
    ParseFile photoFile;

    EditText mensajeMensaje;
    ImageView imagenMensaje;

    String nombre;

    Button enviarMensaje;

    ProgressDialog dialog;

    ImageButton adjuntar,crearLogro;

    String avenirBold = "font/avenir-next-bold.ttf", avenirMedium = "font/avenir-next-medium.ttf";

    String seleccion, estudiante, tipoMensaje;
    String asuntos;
    int usertype;
    String messageId;
    String name;
    ParseObject mensajes = new ParseObject("anuncio");
    ParseObject anuncios = new ParseObject("anuncio");
    HashMap<String, Object> params;

    ArrayList<ParseObject> tipoAnuncio = new ArrayList<>();
    ArrayList<String> nombreAnuncio = new ArrayList<>();
    ArrayList<ParseObject> pendientes = new ArrayList<>();
    ArrayList<String> alumnosPendientes = new ArrayList<>();
    ArrayList<ParseObject> envio = new ArrayList<>();
    ArrayList<ParseObject> grupos;
    String[] arrayAnuncio;
    LinearLayout layoutLogro;
    TransferObserver observe;

    String camino,pagoId,nivel;

    List<ParseObject> men;

    int i=0;

    ParseQuery<ParseObject> queryP = new ParseQuery<ParseObject>("Estudiantes");

    private static final int REQUEST_CAMERA = 20;
    BitmapFactory.Options btmapOptions;
    RelativeLayout layoutHeader;

    private static String APP_DIRECTORY = "MyPictureApp/";
    private static String MEDIA_DIRECTORY = APP_DIRECTORY + "PictureApp";

    private final int MY_PERMISSIONS = 100;
    private final int PHOTO_CODE = 200;
    private final int SELECT_PICTURE = 300;
    private String mPath;


    ProgressBar progressMessage;

    private static final int PICK_IMAGE_ID = 234; // the number doesn't matter

    TransferUtility utility;

    String escuelaId;

    boolean hasPhoto;

    String tipo;

    public NewMessageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        //ImagePicker.setMinQuality(600, 600);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            seleccion = bundle.getString("seccion");
            estudiante = bundle.getString("idEstudiante");
            nombre = bundle.getString("nombre");
            alumnosPendientes = bundle.getStringArrayList("arrayEstudiantes");
            nivel=bundle.getString("niveles");
            tipo=bundle.getString("tipo");

        }

        utility= Util.getTransferUtility(getContext());

        setHasOptionsMenu(true);

        return inflater.inflate(R.layout.fragment_new_message, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);

        asuntoMensaje = (TextView) v.findViewById(R.id.asuntoMensaje);
        mensajeTxt = (TextView) v.findViewById(R.id.mensajeTxt);
        enviarMensaje = (Button) v.findViewById(R.id.enviarMensaje);
        textView2=(TextView)v.findViewById(R.id.textView2);

        layoutLogro=(LinearLayout)v.findViewById(R.id.layoutLogro);

        editAsunto = (TextView) v.findViewById(R.id.editAsunto);
        editAsunto.setSaveFromParentEnabled(false);
        editAsunto.setSaveEnabled(true);
        mensajeMensaje = (EditText) v.findViewById(R.id.mensajeMensaje);
        mensajePara = (TextView) v.findViewById(R.id.mensajePara);

        adjuntar = (ImageButton) v.findViewById(R.id.adjunto);
        crearLogro = (ImageButton)v.findViewById(R.id.crearLogro);

        layoutHeader = (RelativeLayout) v.findViewById(R.id.layoutHeader);

        imagenMensaje = (ImageView) v.findViewById(R.id.fotoMensaje);

        progressMessage=(ProgressBar)v.findViewById(R.id.progressMessage);

        progressMessage.setVisibility(View.GONE);

        usertype = ParseUser.getCurrentUser().getNumber("usertype").intValue();

        if (usertype==2){
            layoutLogro.setVisibility(View.GONE);
        }

        if (seleccion == null) {
            getView().setBackgroundColor(Color.parseColor("#4FC1E9"));
        } else {
            if (seleccion.equals("boy")) {
                getView().setBackgroundColor(Color.parseColor("#5D9CEC"));
            }
            if (seleccion.equals("girl")) {
                getView().setBackgroundColor(Color.parseColor("#EC87C0"));
            }
            if (seleccion.equals("Tareas")) {
                getView().setBackgroundColor(Color.parseColor("#A0D468"));
            }
            if (seleccion.equals("Enfermería")) {
                getView().setBackgroundColor(Color.parseColor("#ED5565"));
            }
            if (seleccion.equals("Anuncio")) {
                getView().setBackgroundColor(Color.parseColor("#4FC1E9"));
            }
            if (seleccion.equals("ap")) {
                layoutHeader.setVisibility(View.GONE);
                mensajePara.setText("Mensaje para " + nombre);
                editAsunto.setText("Aviso de pago");
                editAsunto.setClickable(false);
                tipoMensaje = "M5WRrQAdra";
                //cloudCall();
            }
            if (seleccion.equals("mp")) {
                queryP.whereEqualTo("status", 0);

                queryP.whereNotContainedIn("objectId", alumnosPendientes);
                queryP.setLimit(250);

                queryP.findInBackground(new FindCallback<ParseObject>() {
                    @Override
                    public void done(List<ParseObject> objects, ParseException e) {
                        if (e == null) {
                            for (ParseObject o : objects) {
                                pendientes.add(o);
                            }
                        }
                    }
                });
            }
            if (seleccion.equals("niv")){
                mensajePara.setText("Mensaje para " + nombre);
                getGrupos();
            }
            if (seleccion.equals("aprobado")){
                mensajePara.setText("Mensaje para " + nombre);

            }
        }

        setTypeface();

        adjuntar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPicker();
            }
        });

        crearLogro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment=new LogrosFragment();
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();

            }
        });

        enviarMensaje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (seleccion.equals("mp")) {
                    mensajePendiente();
                } else {
                    sentMessage();
                }
            }
        });

        editAsunto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new ProgressDialog(getContext());
                dialog.setTitle("Cargando");
                dialog.setMessage("Por favor espere...");
                dialog.setCancelable(false);
                dialog.show();
                asunto();
            }
        });

    }

    //Enviar mensaje
    public void sentMessage() {
        escuelaId=((ParseApplication)getActivity().getApplication()).getEscuelaId();

        params = new HashMap<>();
        final String mensaje = mensajeMensaje.getText().toString().trim();


        ParseObject tipo = ParseObject.createWithoutData("tipoAnuncio", tipoMensaje);
        ParseObject student = ParseObject.createWithoutData("Estudiantes", estudiante);


        mensajes.put("autor", ParseUser.getCurrentUser());
        mensajes.put("descripcion", mensaje);
        mensajes.put("materia", "General");
        if (seleccion.equals("niv")){
            mensajes.put("grupos",grupos);
        }else {
            mensajes.put("estudiante", student);

        }
        mensajes.put("tipo", tipo);
        if (usertype == 0 || usertype == 2) {
            mensajes.put("aprobado", true);
        } else {
            if (usertype == 1) {
                mensajes.put("aprobado", false);
            }
        }
        mensajes.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    pagoId=mensajes.getObjectId();

                    if (hasPhoto){
                        saveToAnuncioPhoto(mensajes);
                    }


                    switch (usertype) {
                        case 0:
                            //Admin
                            messageId = mensajes.getObjectId();
                            params.put("anuncioObjectId", messageId);
                            params.put("escuelaObjId",escuelaId);
                            ParseCloud.callFunctionInBackground("adminApprovedAnuncio", params, new FunctionCallback<Object>() {

                                @Override
                                public void done(Object object, ParseException e) {
                                    if (e == null) {
                                        Log.d("Cloudcode", "Success");

                                    } else {
                                        Log.d("Cloudcode", e.getMessage());
                                    }
                                }
                            });
                            break;
                        case 1:
                            //Teacher
                            messageId = mensajes.getObjectId();
                            params.put("anuncioObjectId", messageId);
                            params.put("escuelaObjId",escuelaId);

                            ParseCloud.callFunctionInBackground("teacherAnuncioToBeApproved", params, new FunctionCallback<Object>() {

                                @Override
                                public void done(Object object, ParseException e) {
                                    if (e == null) {
                                        Log.d("Cloudcode", "Success");
                                    } else {
                                        Log.d("Error", e.getMessage());
                                    }
                                }
                            });
                            break;
                        case 2:
                            //Parent
                            messageId = mensajes.getObjectId();
                            params.put("anuncioObjectId", messageId);
                            params.put("escuelaObjId",escuelaId);

                            ParseCloud.callFunctionInBackground("parentAnuncioCreated", params, new FunctionCallback<Object>() {

                                @Override
                                public void done(Object object, ParseException e) {
                                    if (e == null) {
                                        Log.d("Cloudcode", "Success");
                                    } else {
                                        Log.d("Error", e.getMessage());
                                    }
                                }
                            });
                            break;

                        default:
                            break;
                    }



                    Toast.makeText(getContext(), "Mensaje enviado exitosamente", Toast.LENGTH_SHORT).show();


                } else {
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();

                }
            }
        });


    }

    //Mensaje para alumnos pendientes de pago
    public void mensajePendiente() {

        params = new HashMap<>();

        men = new ArrayList<>();

        String mensaje = mensajeMensaje.getText().toString().trim();

        tipoMensaje = "M5WRrQAdra";

        ParseObject tipo = ParseObject.createWithoutData("tipoAnuncio", tipoMensaje);

        for (ParseObject pago : pendientes) {
            pago = new ParseObject("anuncio");
            pago.put("autor", ParseUser.getCurrentUser());
            pago.put("descripcion", mensaje);
            pago.put("materia", "General");
            pago.put("estudiante", pendientes.get(i));
            pago.put("tipo", tipo);
            if (usertype == 0 || usertype == 2) {
                pago.put("aprobado", true);
            } else {
                if (usertype == 1) {
                    pago.put("aprobado", false);
                }
            }

            i++;
            men.add(pago);

        }
        ParseObject.saveAllInBackground(men, new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Toast.makeText(getContext(), "Mensajes enviados exitosamente", Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(getContext(), "Error " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });



            /*
            mensajes.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e==null){

                        messageId=mensajes.getObjectId();
                        params.put("objectId",messageId);

                        switch (usertype){
                            case 0:
                                //Admin
                                ParseCloud.callFunctionInBackground("adminApprovedAnuncio", params, new FunctionCallback<Object>() {

                                    @Override
                                    public void done(Object object, ParseException e) {
                                        if (e == null){
                                            Log.d("Cloudcode","Success");
                                        }else {
                                            Log.d("Error",e.getMessage());
                                        }
                                    }
                                });
                                break;
                            case 1:
                                //Teacher
                                ParseCloud.callFunctionInBackground("teacherAnuncioToBeApproved", params, new FunctionCallback<Object>() {

                                    @Override
                                    public void done(Object object, ParseException e) {
                                        if (e == null){
                                            Log.d("Cloudcode","Success");
                                        }else {
                                            Log.d("Error",e.getMessage());
                                        }
                                    }
                                });
                                break;
                            case 2:
                                //Parent
                                ParseCloud.callFunctionInBackground("parentAnuncioCreated", params, new FunctionCallback<Object>() {

                                    @Override
                                    public void done(Object object, ParseException e) {
                                        if (e == null){
                                            Log.d("Cloudcode","Success");
                                        }else {
                                            Log.d("Error",e.getMessage());
                                        }
                                    }
                                });
                                break;

                            default:
                                break;
                        }


                    }else{
                        Toast.makeText(getContext(),e.getMessage(),Toast.LENGTH_SHORT).show();

                    }
                }
            });
            */

    }

    public void asunto(){
        ParseQuery<ParseObject>asunto=ParseQuery.getQuery("tipoAnuncio");
        asunto.whereNotEqualTo("nombre",null);
        asunto.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                dialog.hide();
                if(e==null){
                    for (ParseObject o:objects){

                            if (o.getString("nombre")==null){
                                Log.d("Error","No hay asuntos");
                            }else {
                                nombre=o.getString("nombre");
                                nombreAnuncio.add(nombre);
                                tipoAnuncio.add(o);
                                Log.d("Error","Hay asuntos");

                            }

                        }
                }else {
                    Toast.makeText(getContext(),"Error",Toast.LENGTH_SHORT).show();
                }

                arrayAnuncio=new String[nombreAnuncio.size()];
                arrayAnuncio=nombreAnuncio.toArray(arrayAnuncio);

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                // Set the dialog title

                builder.setTitle("Seleccione un asunto: ")

                        // Specify the list array, the items to be selected by default (null for none),

                        // and the listener through which to receive callbacks when items are selected

                        .setItems(arrayAnuncio, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                asuntos=nombreAnuncio.get(which);
                                editAsunto.setText(asuntos);
                                tipoMensaje=tipoAnuncio.get(which).getObjectId();
                            }

                        // Set the action buttons
                        }).create().show();
            }
        });
    }

    public void setTypeface(){
        Typeface bold = Typeface.createFromAsset(getContext().getAssets(), avenirBold);
        Typeface medium = Typeface.createFromAsset(getContext().getAssets(), avenirMedium);
        mensajeTxt.setTypeface(medium);
        asuntoMensaje.setTypeface(medium);
        editAsunto.setTypeface(medium);
        enviarMensaje.setTypeface(medium);
        mensajeMensaje.setTypeface(medium);
        textView2.setTypeface(medium);

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);

    }


    public void showPicker(){
        PickImageDialog.build(new PickSetup().setCameraButtonText("Cámara").setGalleryButtonText("Galería").setTitle("Agregar foto").setCancelText("Cancelar"))
                .setOnPickResult(new IPickResult() {
                    @Override
                    public void onPickResult(PickResult r) {
                        //TODO: do what you have to...
                        if (r.getError() == null) {
                            //If you want the Uri.
                            //Mandatory to refresh image from Uri.
                            //getImageView().setImageURI(null);

                            //Setting the real returned image.
                            //getImageView().setImageURI(r.getUri());

                            //If you want the Bitmap.

                            Bitmap bitmap = r.getBitmap();
                            // TODO use bitmap

                            camino=r.getPath();
                            imagenMensaje.setImageBitmap(bitmap);
                            hasPhoto=true;

                            //Image path
                            //r.getPath();
                        } else {
                            //Handle possible errors
                            //TODO: do what you have to do with r.getError();
                            Toast.makeText(getContext(), r.getError().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                }).show(getActivity().getSupportFragmentManager());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case PICK_IMAGE_ID:
                Bitmap bitmap = ImagePicker.getImageFromResult(getContext(), resultCode, data);
                // TODO use bitmap



                camino=mPath;
                imagenMensaje.setImageBitmap(bitmap);
                hasPhoto=true;
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }


    public void saveToAnuncioPhoto(ParseObject anuncio){
        final ParseObject anuncioPhoto=new ParseObject("AnuncioPhoto");
        anuncioPhoto.put("anuncio",anuncio);
        anuncioPhoto.put("aws",true);
        anuncioPhoto.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e==null){
                    Log.d("AnuncioPhoto","Foto guardada");
                    attachMessage(anuncioPhoto.getObjectId());
                }else {
                    Toast.makeText(getContext(), "Verifique su conexión a internet e intente de nuevo",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void attachMessage(String anuncioId){
        observe= utility.upload(Constants.BUCKET_NAME,anuncioId,new java.io.File(camino ));
        observe.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (TransferState.COMPLETED.equals(observe.getState())) {

                    //progressMessage.setVisibility(View.GONE);
                    getActivity().getSupportFragmentManager().popBackStack();
                    Toast.makeText(getContext(), "Foto guardada", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                long _bytesCurrent = bytesCurrent;
                long _bytesTotal = bytesTotal;

                float percentage =  ((float)_bytesCurrent /(float)_bytesTotal * 100);
                Log.d("percentage","" +percentage);
                //progressMessage.setProgress((int) percentage);
            }

            @Override
            public void onError(int id, Exception ex) {
                Log.d("Error de Amazon",ex.getMessage());
                Toast.makeText(getContext(),"Error al subir la foto, intente de nuevo",Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getGrupos(){

        grupos=new ArrayList<>();
        ParseQuery<ParseObject> grupoMas=ParseQuery.getQuery("grupo");
        escuelaId = ((ParseApplication) getActivity().getApplication()).getEscuelaId();
        ParseObject escuela=ParseObject.createWithoutData("Escuela",escuelaId);
        grupoMas.whereEqualTo("escuela",escuela);
        if (nivel.equals("all")){
            Toast.makeText(getActivity(),"All",Toast.LENGTH_SHORT).show();
        }else {
            ParseObject niv=ParseObject.createWithoutData("Nivel",nivel);
            grupoMas.whereEqualTo("nivel",niv);
            Toast.makeText(getActivity(),nivel,Toast.LENGTH_SHORT).show();
        }

        grupoMas.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e==null){
                    grupos.addAll(objects);
                }else {
                    Log.e("Niveles",e.getMessage());

                }
            }
        });
    }

}
