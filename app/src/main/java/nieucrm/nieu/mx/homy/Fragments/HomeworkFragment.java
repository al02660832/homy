package nieucrm.nieu.mx.skola.Fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import nieucrm.nieu.mx.skola.Adapter.AnunciosAdapter;
import nieucrm.nieu.mx.skola.DataModel.AnnouncementData;
import nieucrm.nieu.mx.skola.DataModel.AnuncioData;
import nieucrm.nieu.mx.skola.R;

/**
 * Created by Carlos Romero
 */

public class HomeworkFragment extends Fragment {

    String groupId;
    ListView listaTarea;
    ArrayList<String> tareas;
    List<ParseObject> obj;
    ArrayAdapter<String> adapter;
    ImageButton crearNuevaTarea;
    Fragment fragment;
    String genero,materia,emisor,destinatario,descripcion,tipo;
    Date fecha,fechaEntrega;
    ProgressBar progressTareas;
    int position;
    String f,m;
    ParseUser autor;

    String nombreGrupo;
    AnunciosAdapter a;

    ArrayList<AnnouncementData>arrayList;

    ArrayList<String> anuncioPhoto;


    String tipoId;

    Activity ac;

    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf",demibold="font/avenir-next-demi-bold.ttf";

    public HomeworkFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            ac=(Activity) context;
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        Bundle b=this.getArguments();
        if (b!=null){
            groupId=b.getString("grupoId");
            nombreGrupo=b.getString("grupoNombre");
            tipo="Tarea";
            genero="G";
        }

        return inflater.inflate(R.layout.fragment_homework, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        listaTarea=(ListView)view.findViewById(R.id.listaTareas);
        progressTareas=(ProgressBar)view.findViewById(R.id.progressTarea);
        crearNuevaTarea=(ImageButton)view.findViewById(R.id.crearNuevaTarea);



        setTypeface();

        getAnuncioPhoto();

        crearNuevaTarea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment=new NewHomeworkFragment();
                Bundle b=new Bundle();
                b.putString("groupId",groupId);
                b.putString("grupoNombre",nombreGrupo);
                fragment.setArguments(b);
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
                //getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
            }
        });

    }

    public void setTypeface(){
        Typeface bold = Typeface.createFromAsset(ac.getAssets(), avenirBold);
        Typeface medium = Typeface.createFromAsset(ac.getAssets(), avenirMedium);
        Typeface dem=Typeface.createFromAsset(ac.getAssets(),demibold);


    }

    public void getTipos(){
        ParseQuery<ParseObject>tipos=ParseQuery.getQuery("tipoAnuncio");
        tipos.whereEqualTo("nombre","Tarea");
        tipos.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (e==null){
                    tipoId=object.getObjectId();
                    getTareas();
                }else {
                    Toast.makeText(ac,"Error, trate de nuevo",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void getTareas(){
        ParseObject o=ParseObject.createWithoutData("tipoAnuncio",tipoId);
        ParseObject g=ParseObject.createWithoutData("grupo",groupId);
        tareas=new ArrayList<>();
        arrayList=new ArrayList<>();

        position=1;
        ParseQuery<ParseObject> t=ParseQuery.getQuery("anuncio").include("tipo").include("autor").include("grupos").whereEqualTo("tipo",o).whereEqualTo("grupos",g);
        t.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                progressTareas.setVisibility(View.GONE);
                listaTarea.setVisibility(View.VISIBLE);
                obj=objects;
                if (e==null){
                    for (ParseObject o:objects){
                        autor=o.getParseUser("autor");
                        AnnouncementData data=new AnnouncementData();
                        data.setGenero(genero);
                        data.setTipo(tipo);
                        data.setFechaEntrega(o.getUpdatedAt());
                        data.setFecha(o.getCreatedAt());

                        data.setMateria(o.getString("materia"));
                        if (autor!=null){
                            data.setEmisor(autor.getString("nombre")+" "+autor.getString("apellidos"));
                        }else {
                            data.setEmisor("Genérico");
                        }

                        if (o.getList("grupos")==null){
                            Log.e("Error de: ","Sin grupos");
                        }else {
                            List<ParseObject>g=o.getList("grupos");
                            StringBuilder a=new StringBuilder();
                            for (int i=0;i<g.size();i++){

                                try{
                                    if (g.get(i).getString("grupoId")==null){
                                        Log.e("GrupoId: ","Nulo");
                                    }else {
                                        if(i==g.size()-1){
                                            a.append(g.get(i).getString("grupoId"));
                                        }else {
                                            a.append(g.get(i).getString("grupoId")).append(", ");

                                        }
                                        destinatario=a.toString();
                                    }
                                }catch (Exception e1){
                                    Log.e("Error","GRUPO NULO");
                                }


                            }
                        }
                        data.setDestinatario(destinatario);

                        if (o.getParseFile("attachment")==null){
                            if (o.getBoolean("awsAttachment")){
                                data.setAwsAttachment(true);
                                Log.e("Foto","Fotos en anuncio");
                            }else {
                                if (anuncioPhoto.size()==0){
                                    Log.e("Foto","Fotos en anuncio");
                                }else {
                                    if (anuncioPhoto.contains(o.getObjectId())){
                                        data.setTablaAnuncio(true);
                                        Log.e("Foto","Foto en tabla anuncio"+o.getObjectId());
                                    }
                                }
                            }
                        }else {
                            Log.e("Foto","En Parse Server");

                        }

                        data.setDescripcion(o.getString("descripcion"));
                        data.setObjectId(o.getObjectId());
                        data.setAprobado(o.getBoolean("aprobado"));

                        arrayList.add(data);
                    }
                    a=new AnunciosAdapter(ac,arrayList);
                    listaTarea.setAdapter(a);
                }else {
                    Log.e("Error","Error al obtener tareas");
                }



            }
        });
    }

    public void getAnuncioPhoto(){
        anuncioPhoto=new ArrayList<>();
        ParseQuery<ParseObject>anuncioPhotos=ParseQuery.getQuery("AnuncioPhoto");
        anuncioPhotos.include("anuncio");
        anuncioPhotos.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {

                if (e==null){
                    for (ParseObject o:objects){
                        if(o.getParseObject("anuncio")!=null){
                            anuncioPhoto.add(o.getParseObject("anuncio").getObjectId());
                        }
                    }
                    Log.e("AnuncioPhoto",String.valueOf(anuncioPhoto.size()));
                }
                getTipos();
            }
        });
    }

}
