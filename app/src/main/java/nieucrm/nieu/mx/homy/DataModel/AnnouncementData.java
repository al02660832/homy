package nieucrm.nieu.mx.skola.DataModel;

import java.util.Date;

/**
 * Created by mmac1 on 18/02/2018.
 */

public class AnnouncementData {
    String genero,materia,emisor,destinatario,descripcion,tipo,objectId;

    Date fecha,fechaEntrega;
    String f,m;
    boolean awsAttachment,tablaAnuncio;

    boolean aprobado;

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getMateria() {
        return materia;
    }

    public void setMateria(String materia) {
        this.materia = materia;
    }

    public String getEmisor() {
        return emisor;
    }

    public void setEmisor(String emisor) {
        this.emisor = emisor;
    }

    public String getDestinatario() {
        return destinatario;
    }

    public void setDestinatario(String destinatario) {
        this.destinatario = destinatario;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public String getF() {
        return f;
    }

    public void setF(String f) {
        this.f = f;
    }

    public String getM() {
        return m;
    }

    public void setM(String m) {
        this.m = m;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public boolean isAprobado() {
        return aprobado;
    }

    public void setAprobado(boolean aprobado) {
        this.aprobado = aprobado;
    }

    public boolean isAwsAttachment() {
        return awsAttachment;
    }

    public void setAwsAttachment(boolean awsAttachment) {
        this.awsAttachment = awsAttachment;
    }

    public boolean isTablaAnuncio() {
        return tablaAnuncio;
    }

    public void setTablaAnuncio(boolean tablaAnuncio) {
        this.tablaAnuncio = tablaAnuncio;
    }
}
