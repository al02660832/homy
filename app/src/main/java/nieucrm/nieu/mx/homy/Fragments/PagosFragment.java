package nieucrm.nieu.mx.skola.Fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickResult;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import nieucrm.nieu.mx.skola.Helpers.Constants;
import nieucrm.nieu.mx.skola.Helpers.ImagePicker;
import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.Helpers.Util;
import nieucrm.nieu.mx.skola.R;

/**
 * Created by Carlos Romero
 */

public class PagosFragment extends Fragment {

    TextView tomarFoto, reemplazarFoto, nombrePago, textConcepto,reeFoto;
    LinearLayout layoutFoto,layoutSegunda,layoutMadre;
    ImageButton btnMandarPago, btnCambiarAlumno, btnFoto;
    EditText concepto;
    ParseObject studentPago;
    int position = 0;
    ParseImageView fotoPago;
    ProgressBar progressBar;
    String studentId = "";
    ArrayList<ParseObject> arrayEstudiantes = new ArrayList<>();
    String nombreEstudiante;
    String fechaCloud;
    ParseFile photoFile;
    ParseObject pagos = new ParseObject("pagos");
    Toolbar toolbarFragment;
    HashMap<String, Object> pe,pagoA;
    String pagoId;
    ArrayList<String>valor;

    String escuelaId;

    private static final int PICK_IMAGE_ID = 234;

    TransferUtility utility;

    File file;

    int []i={1,2,3,4,5,6};
    String []num={"1","2","3","4","5","6"};

    int mes=0;


    ProgressDialog progressDialog;
    String avenirBold = "font/avenir-next-bold.ttf", avenirMedium = "font/avenir-next-medium.ttf";

    //Prueba
    private static String APP_DIRECTORY = "MyPictureApp/";
    private static String MEDIA_DIRECTORY = APP_DIRECTORY + "PictureApp";


    private final int PHOTO_CODE = 200;
    private final int SELECT_PICTURE = 300;
    private String mPath;
    String camino;
    TransferObserver observe;

    public PagosFragment() {
        // Required empty public constructor
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        utility= Util.getTransferUtility(getContext());

        return inflater.inflate(R.layout.fragment_pagos, container, false);
    }


    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);

        reeFoto=(TextView)v.findViewById(R.id.reFoto);
        nombrePago = (TextView) v.findViewById(R.id.nombrePago);
        textConcepto = (TextView) v.findViewById(R.id.txtConcepto);
        tomarFoto = (TextView) v.findViewById(R.id.tomarFoto);
        reemplazarFoto = (TextView) v.findViewById(R.id.reemplazarFoto);

        fotoPago = (ParseImageView) v.findViewById(R.id.foto);

        concepto = (EditText) v.findViewById(R.id.etConcepto);

        btnMandarPago = (ImageButton) v.findViewById(R.id.mandarPago);
        btnCambiarAlumno = (ImageButton) v.findViewById(R.id.cambiarAlumno);
        btnFoto = (ImageButton) v.findViewById(R.id.btnFoto);

        layoutMadre=(LinearLayout)v.findViewById(R.id.layoutMadre);

        layoutFoto = (LinearLayout) v.findViewById(R.id.layoutFoto);
        layoutSegunda=(LinearLayout)v.findViewById(R.id.linearFoto);
        progressBar=(ProgressBar)v.findViewById(R.id.progressParse);

        toolbarFragment = (Toolbar)getActivity().findViewById(R.id.toolbar);

        /*
        toolbarFragment.setBackgroundResource(R.drawable.pago_colegiatura_header);
        toolbarFragment.setTitle("Hola");


        ((Home)getActivity()).setSupportActionBar(toolbarFragment);
        */

        progressDialog = new ProgressDialog(getContext());

        progressDialog.setTitle("Cargando");
        progressDialog.setMessage("Por favor espere");
        progressDialog.setCancelable(false);
        progressDialog.show();

        initializeQuery();


        reeFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPicker();
            }
        });

        btnFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //showPicker();
                showPicker();
            }
        });

        SetTypeface();

        btnMandarPago.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setMeses();
            }
        });
    }

    /*
    private void selectImage() {
        final CharSequence[] items = {"Tomar una foto", "Elegir imagen de galería",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Agregar foto");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Tomar una foto")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File f = new File(android.os.Environment
                            .getExternalStorageDirectory(), "temp.jpg");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (items[item].equals("Elegir imagen de galería")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            SELECT_PICTURE);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
    */

    public void setMeses(){

        // Set the dialog title

        valor=new ArrayList<>();

        for (String c:num){
            valor.add(c);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // Set the dialog title

        builder.setTitle("Meses a pagar: ")

                // Specify the list array, the items to be selected by default (null for none),

                // and the listener through which to receive callbacks when items are selected

                .setItems(num, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mes=Integer.valueOf(valor.get(which));
                        savePayment();
                    }

                    // Set the action buttons
                }).create().show();

    }

    /*
    public static byte[] bitmapToByteArray(Bitmap bmp) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }
    */

    public void initializeQuery() {
        ParseQuery<ParseObject> est = ParseQuery.getQuery("Estudiantes").include("PersonasAutorizadas").whereEqualTo("PersonasAutorizadas", ParseUser.getCurrentUser());
        est.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                progressDialog.hide();
                if (e==null){
                    arrayEstudiantes.addAll(objects);
                    studentPago = arrayEstudiantes.get(position);
                    nombreEstudiante = studentPago.get("NOMBRE") + " " + studentPago.getString("APELLIDO");
                    btnMandarPago.setVisibility(View.GONE);
                    if (arrayEstudiantes.size() > 1) {
                        btnCambiarAlumno.setVisibility(View.VISIBLE);
                        btnCambiarAlumno.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if (position == arrayEstudiantes.size() - 1) {
                                    position = 0;
                                    studentPago = arrayEstudiantes.get(position);
                                    nombreEstudiante = studentPago.get("NOMBRE") + " " + studentPago.getString("APELLIDO");
                                    studentId = studentPago.getObjectId();
                                    nombrePago.setText(nombreEstudiante);

                                } else {
                                    position++;
                                    studentPago = arrayEstudiantes.get(position);
                                    nombreEstudiante = studentPago.get("NOMBRE") + " " + studentPago.getString("APELLIDO");
                                    studentId = studentPago.getObjectId();
                                    nombrePago.setText(nombreEstudiante);
                                }
                            }
                        });
                    }

                    nombrePago.setText(nombreEstudiante);
                }
            }
        });

    }

    public void SetTypeface() {
        Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), avenirBold);
        Typeface medium = Typeface.createFromAsset(getActivity().getAssets(), avenirMedium);
        nombrePago.setTypeface(bold);
        textConcepto.setTypeface(medium);
        tomarFoto.setTypeface(bold);
        reemplazarFoto.setTypeface(bold);
        concepto.setTypeface(bold);
        reeFoto.setTypeface(bold);

    }

    public void savePayment() {

        escuelaId=((ParseApplication)getActivity().getApplication()).getEscuelaId();

        Calendar despues=Calendar.getInstance();
        despues.add(Calendar.MONTH,mes);
        Date adelanto=despues.getTime();

        pe=new HashMap<>();
        pagoA=new HashMap<>();
        String concept = concepto.getText().toString().trim();

        SimpleDateFormat formateador = new SimpleDateFormat(
                "MMMM yyyy", new Locale("ES"));

        Calendar cal = Calendar.getInstance();

        cal.setTimeZone(TimeZone.getDefault());

        fechaCloud=formateador.format(new Date());

        pe.put("monthYear",fechaCloud);
        pe.put("escuelaObjId",escuelaId);

        studentId = arrayEstudiantes.get(position).getObjectId();

        ParseObject st = ParseObject.createWithoutData("Estudiantes", studentId);

        if (TextUtils.isEmpty(concept)){
            Toast.makeText(getContext(),"Ingrese el concepto del pago",Toast.LENGTH_SHORT).show();
        }else {
            pagos.put("user", ParseUser.getCurrentUser());
            pagos.put("concepto", concept);
            pagos.put("student", st);
            pagos.put("fechaAdelanto",adelanto);
            pagos.put("aws",true);

            pagos.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    pagoId=pagos.getObjectId();
                    pagoA.put("pagoId",pagoId);
                    pagoA.put("escuelaObjId",escuelaId);
                    if (e==null){
                        //getActivity().getSupportFragmentManager().popBackStack();

                        savePhoto(pagoId);




                        ParseCloud.callFunctionInBackground("updatePagosCount", pe, new FunctionCallback<Object>() {
                            @Override
                            public void done(Object object, ParseException e) {
                                if (e == null){
                                    Log.d("CloudcodePagos-Padres","Success");
                                }else {
                                    Log.d("Error",e.getMessage());
                                }
                            }
                        });


                        ParseCloud.callFunctionInBackground("pagoAdminNotification", pagoA, new FunctionCallback<Object>() {
                            @Override
                            public void done(Object object, ParseException e) {
                                if (e == null){
                                    Log.d("Cloudcode-sNotificacion","Success");
                                }else {
                                    Log.d("Error",e.getMessage());
                                }
                            }
                        });



                        Toast.makeText(getContext(),"Enviando pago",Toast.LENGTH_LONG).show();

                    }else {
                        Log.d("Error pago",e.getMessage());
                    }
                }
            });
        }


    }
/*
    public void invokeCamera(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, getPhotoFileUri(photoFileName)); // set the image file name



        // If you call startActivityForResult() using an intent that no app can handle, your app will crash.
        // So as long as the result is not null, it's safe to use the intent.
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Start the image capture intent to take photo
            startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
        }
    }

 */
    /*
    public Uri getPhotoFileUri(String fileName) {
        // Only continue if the SD Card is mounted
        if (isExternalStorageAvailable()) {
            // Get safe storage directory for photos
            // Use `getExternalFilesDir` on Context to access package-specific directories.
            // This way, we don't need to request external read/write runtime permissions.
            File mediaStorageDir = new File(
                    getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES),"Skola");

            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()){
               Toast.makeText(getContext(),"Failed to create directory",Toast.LENGTH_LONG).show();
            }

            // Return the file target for the photo based on filename
            return Uri.fromFile(new File(mediaStorageDir.getPath() + File.separator + fileName));
        }
        return null;
    }
    */

    // Returns true if external storage for photos is available
    private boolean isExternalStorageAvailable() {
        String state = Environment.getExternalStorageState();
        return state.equals(Environment.MEDIA_MOUNTED);
    }

    /*
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case PICK_IMAGE_ID:
                Bitmap bitmap = ImagePicker.getImageFromResult(getContext(), resultCode, data);
                // TODO use bitmap

                byte[] bytes = bitmapToByteArray(bitmap);
                photoFile = new ParseFile("photo.jpg", bytes);
                photoFile.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if(e==null){
                            Toast.makeText(getContext(), "Imagen guardada", Toast.LENGTH_LONG).show();
                            pagos.put("photo", photoFile);
                        }else {
                            Toast.makeText(getContext(), "Error, intente de nuevo", Toast.LENGTH_LONG).show();
                            e.printStackTrace();

                            Log.d("Error",e.getMessage());
                        }

                    }
                });
                fotoPago.setImageBitmap(bitmap);
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }
    */

    /*
    private void showOptions() {
        final CharSequence[] option = {"Tomar foto", "Elegir de galeria", "Cancelar"};
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Eleige una opción");
        builder.setItems(option, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(option[which] == "Tomar foto"){
                    openCamera();
                }else if(option[which] == "Elegir de galeria"){
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(intent.createChooser(intent, "Selecciona app de imagen"), SELECT_PICTURE);
                }else {
                    dialog.dismiss();
                }
            }
        });

        builder.show();
    }

    private void openCamera() {
        File file = new File(Environment.getExternalStorageDirectory(), MEDIA_DIRECTORY);
        boolean isDirectoryCreated = file.exists();

        if(!isDirectoryCreated)
            isDirectoryCreated = file.mkdirs();

        if(isDirectoryCreated){
            Long timestamp = System.currentTimeMillis() / 1000;
            String imageName = timestamp.toString() + ".jpg";

            mPath = Environment.getExternalStorageDirectory() + File.separator + MEDIA_DIRECTORY
                    + File.separator + imageName;

            File newFile = new File(mPath);

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(newFile));
            startActivityForResult(intent, PHOTO_CODE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK){
            switch (requestCode){
                case PHOTO_CODE:
                    MediaScannerConnection.scanFile(getContext(),
                            new String[]{mPath}, null,
                            new MediaScannerConnection.OnScanCompletedListener() {
                                @Override
                                public void onScanCompleted(String path, Uri uri) {
                                    Log.i("ExternalStorage", "Scanned " + path + ":");
                                    Log.i("ExternalStorage", "-> Uri = " + uri);
                                }
                            });


                    Bitmap bitmap = BitmapFactory.decodeFile(mPath);
                    camino=mPath;

                    btnCambiarAlumno.setVisibility(View.GONE);
                    layoutFoto.setClickable(false);
                    btnMandarPago.setVisibility(View.VISIBLE);
                    layoutMadre.setVisibility(View.GONE);
                    layoutSegunda.setVisibility(View.VISIBLE);
                    reeFoto.setVisibility(View.VISIBLE);
                    fotoPago.setVisibility(View.VISIBLE);
                    fotoPago.setImageBitmap(bitmap);
                    break;
                case SELECT_PICTURE:
                    Uri path = data.getData();
                    camino=path.getPath();

                    btnCambiarAlumno.setVisibility(View.GONE);
                    layoutFoto.setClickable(false);
                    btnMandarPago.setVisibility(View.VISIBLE);

                    layoutMadre.setVisibility(View.GONE);
                    layoutSegunda.setVisibility(View.VISIBLE);
                    reeFoto.setVisibility(View.VISIBLE);
                    fotoPago.setVisibility(View.VISIBLE);
                    fotoPago.setImageURI(path);
                    break;

            }
        }
    }
    */

    public void showPicker(){
        PickImageDialog.build(new PickSetup().setCameraButtonText("Cámara").setGalleryButtonText("Galería").setTitle("Agregar foto").setCancelText("Cancelar"))
                .setOnPickResult(new IPickResult() {
                    @Override
                    public void onPickResult(PickResult r) {
                        //TODO: do what you have to...
                        if (r.getError() == null) {
                            //If you want the Uri.
                            //Mandatory to refresh image from Uri.
                            //getImageView().setImageURI(null);

                            //Setting the real returned image.
                            //getImageView().setImageURI(r.getUri());

                            //If you want the Bitmap.

                            Bitmap bitmap = r.getBitmap();
                            // TODO use bitmap

                            camino=r.getPath();

                            btnCambiarAlumno.setVisibility(View.GONE);
                            layoutFoto.setClickable(false);

                            layoutMadre.setVisibility(View.GONE);
                            layoutSegunda.setVisibility(View.VISIBLE);
                            reeFoto.setVisibility(View.VISIBLE);
                            fotoPago.setVisibility(View.VISIBLE);
                            btnMandarPago.setVisibility(View.VISIBLE);
                            fotoPago.setImageBitmap(r.getBitmap());

                            //Image path
                            //r.getPath();
                        } else {
                            //Handle possible errors
                            //TODO: do what you have to do with r.getError();
                            Toast.makeText(getContext(), r.getError().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                }).show(getActivity().getSupportFragmentManager());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case PICK_IMAGE_ID:
                Bitmap bitmap = ImagePicker.getImageFromResult(getContext(), resultCode, data);
                // TODO use bitmap



                fotoPago.setImageBitmap(bitmap);
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    public static byte[] bitmapToByteArray(Bitmap bmp) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    public void savePhoto(String id){
        try{
            observe= utility.upload(Constants.BUCKET_NAME,id,new java.io.File(camino ));
            observe.setTransferListener(new TransferListener() {
                @Override
                public void onStateChanged(int id, TransferState state) {
                    Log.d("Estatus:",state.toString());

                    if (TransferState.COMPLETED.equals(observe.getState())) {

                        progressBar.setVisibility(View.GONE);

                        if (getActivity()==null){
                           Log.e("Contexto","Null");
                        }else {
                            getActivity().getSupportFragmentManager().popBackStack();
                        }


                        Toast.makeText(getContext(), "Pago registrado exitosamente", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                    long _bytesCurrent = bytesCurrent;
                    long _bytesTotal = bytesTotal;

                    float percentage =  ((float)_bytesCurrent /(float)_bytesTotal * 100);
                    Log.d("percentage","" +percentage);
                    progressBar.setProgress((int) percentage);
                }

                @Override
                public void onError(int id, Exception ex) {
                    Log.d("Error de Amazon",ex.getMessage());
                    Toast.makeText(getContext(),"Error,intente de nuevo",Toast.LENGTH_SHORT).show();
                }

            });
        }catch (Exception ex1){
            Log.e("Exception",ex1.getMessage());
        }
    }

}
