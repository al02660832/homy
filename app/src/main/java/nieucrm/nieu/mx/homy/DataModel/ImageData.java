package nieucrm.nieu.mx.skola.DataModel;

/**
 * Created by mmac1 on 19/07/2017.
 */

public class ImageData {

    private String autor,imagen,camino;

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getCamino() {
        return camino;
    }

    public void setCamino(String camino) {
        this.camino = camino;
    }
}
