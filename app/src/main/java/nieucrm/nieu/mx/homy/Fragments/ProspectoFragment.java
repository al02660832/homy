package nieucrm.nieu.mx.skola.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import nieucrm.nieu.mx.skola.Adapter.ProspectoAdapter;
import nieucrm.nieu.mx.skola.DataModel.ProspectoData;
import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProspectoFragment extends Fragment {

    ParseQuery<ParseObject>prospecto;

    ArrayList<ProspectoData>array;
    ProspectoData data;
    ProspectoAdapter adapter;

    ListView listaProspecto;
    String escuelaId;
    ParseObject escuela;

    public ProspectoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_prospecto, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        listaProspecto=(ListView)view.findViewById(R.id.listaProspecto);

        getProspecto();

    }

    private void getProspecto(){
        array=new ArrayList<>();
        prospecto=ParseQuery.getQuery("Prospecto");
        escuelaId=((ParseApplication)getActivity().getApplication()).getEscuelaId();
        escuela=ParseObject.createWithoutData("Escuela",escuelaId);
        prospecto.whereEqualTo("escuela",escuela);
        prospecto.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e==null){

                    for (ParseObject o:objects){
                        data=new ProspectoData();
                        data.setObjectId(o.getObjectId());
                        if (o.getString("fuente")==null){
                            //App
                            data.setWeb(false);
                            if (o.getString("estudianteNombre")!=null){
                                data.setEstudianteNombre(o.getString("estudianteNombre"));
                            }
                            if (o.getString("estudianteApellidos")!=null){
                                data.setEstudianteApellidos(o.getString("estudianteApellidos"));
                            }

                            if (o.getString("seguimiento")!=null){
                                data.setSeguimiento(o.getString("seguimiento"));
                            }

                            data.setCaptura(o.getString("captura"));

                            if (o.getDate("estudianteFechaNacimiento")!=null){

                                data.setEstudianteFechaNacimiento(o.getDate("estudianteFechaNacimiento"));

                                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy",new Locale("es"));
                                data.setFechaNacimiento(sdf.format(o.getDate("estudianteFechaNacimiento")));


                            }else {
                                data.setEstudianteFechaNacimiento(o.getCreatedAt());
                                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy",new Locale("es"));
                                data.setFechaNacimiento(sdf.format(o.getCreatedAt()));
                            }

                            data.setMamaEmail(o.getString("mamaEmail"));
                            data.setMamaNombre(o.getString("mamaNombre"));
                            data.setMamaApellidos(o.getString("mamaApellidos"));
                            data.setMamaTelefono(o.getString("mamaTelefono"));

                            data.setPapaNombre(o.getString("papaNombre"));
                            data.setPapaApellidos(o.getString("papaApellidos"));
                            data.setPapaEmail(o.getString("papaEmail"));
                            data.setPapaTelefono(o.getString("papaTelefono"));

                            data.setPromocion(o.getString("promocion"));
                        }else {
                            if (o.getString("fuente").equals("web")){
                                //Prospecto Web
                                data.setWeb(true);

                                //Convertir fecha

                                SimpleDateFormat format = new SimpleDateFormat("EEEE  dd 'de' MMMM",new Locale("es"));
                                data.setFechaCreacion(format.format(o.getCreatedAt()));

                                //Convertir hora
                                DateFormat df=new SimpleDateFormat("HH:mm",new Locale("es"));
                                data.setHoraCreacion(df.format(o.getCreatedAt()));

                                if (o.getString("nombre")!=null){
                                    data.setNombre(o.getString("nombre"));
                                }

                                if (o.getString("edadHijo")!=null){
                                    data.setEdad(o.getString("edadHijo"));
                                }

                                if (o.getString("fechaIngreso")!=null){
                                    data.setFechaIngreso(o.getString("fechaIngreso"));
                                }

                                if (o.getString("preguntas")!=null){
                                    data.setPreguntas(o.getString("preguntas"));
                                }

                                if (o.getString("telefono")!=null){
                                    data.setTelefono(o.getString("telefono"));
                                }

                                if (o.getString("correo")!=null){
                                    data.setMail(o.getString("correo"));
                                }


                            }else {
                                //Nada
                            }

                        }


                        array.add(data);
                    }
                    adapter=new ProspectoAdapter(getActivity(),array);
                    listaProspecto.setAdapter(adapter);
                }
            }
        });
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.crear_prospecto,menu);
        //inflater.inflate(R.menu.crear_usuario,menu);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.crearPros:
                Fragment fragment=new NewProspectoFragment();
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();

                break;
        }
        /*
        switch (item.getItemId()){
            case R.id.crearUser:
                Fragment fragment=new NewUserFragment();
                Bundle b=new Bundle();

                b.putInt("usertype",usertype);
                fragment.setArguments(b);
                FragmentTransaction transaction= getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();

                break;
        }
        */

        return super.onOptionsItemSelected(item);

    }


}
