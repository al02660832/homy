package nieucrm.nieu.mx.skola.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import nieucrm.nieu.mx.skola.DataModel.ServiciosData;
import nieucrm.nieu.mx.skola.Fragments.ServicesFragment;
import nieucrm.nieu.mx.skola.R;


/**
 * Created by Carlos Romero on 10/12/2016.
 */

public class ServicioAdminAdapter extends BaseAdapter {

    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf",demibold="font/avenir-next-demi-bold.ttf";
    String fecha, hour;

    private Context context;
    LayoutInflater inflater;
    private List<ServiciosData> serviciosData=null;
    private ArrayList<ServiciosData>arrayList=null;


    public ServicioAdminAdapter(Context context, List<ServiciosData> serviciosData) {
        this.context = context;
        this.serviciosData=serviciosData;
        try {
            inflater=LayoutInflater.from(context);

        }catch (Exception e){
            Log.d("Error",e.getMessage());
        }
        this.arrayList=new ArrayList<>();
        this.arrayList.addAll(serviciosData);
    }

    public class ViewHolder{
        TextView serviceTag,alumnoTag,fechaServicioTag,comentarioTag;
        TextView nombreTag,estadoTag,nombreAlumnoTag,fechaTag,horaTag,comentarioServicioTag;

    }


    @Override
    public int getCount() {
        return serviciosData.size();
    }

    @Override
    public Object getItem(int position) {
        return serviciosData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Typeface bold=Typeface.createFromAsset(context.getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(context.getAssets(),avenirMedium);
        Typeface dem=Typeface.createFromAsset(context.getAssets(),demibold);

        ViewHolder holder;
       // TextView nombreEstudiante=(TextView)v.findViewById(R.id.nombreEstudiante);

        if(convertView==null){
            holder=new ViewHolder();
            convertView=inflater.inflate(R.layout.historial_servicio_admin_item_layout,null);

            holder.serviceTag=(TextView)convertView.findViewById(R.id.serviceTag);
            holder.alumnoTag=(TextView)convertView.findViewById(R.id.alumnoTag);
            holder.fechaServicioTag=(TextView)convertView.findViewById(R.id.fechaServicioTag);
            holder.comentarioTag=(TextView)convertView.findViewById(R.id.comentarioTag);

            holder.nombreTag=(TextView)convertView.findViewById(R.id.nombreTag);
            holder.estadoTag=(TextView)convertView.findViewById(R.id.estadoTag);
            holder.nombreAlumnoTag=(TextView)convertView.findViewById(R.id.nombreAlumnoTag);
            holder.fechaTag=(TextView)convertView.findViewById(R.id.fechaTag);
            holder.horaTag=(TextView)convertView.findViewById(R.id.horaTag);
            holder.comentarioServicioTag=(TextView)convertView.findViewById(R.id.comentarioServicioTag);



            holder.serviceTag.setTypeface(dem);
            holder.nombreTag.setTypeface(dem);
            holder.estadoTag.setTypeface(dem);


            holder.alumnoTag.setTypeface(medium);
            holder.fechaServicioTag.setTypeface(medium);
            holder.comentarioTag.setTypeface(medium);
            holder.nombreAlumnoTag.setTypeface(medium);
            holder.fechaTag.setTypeface(medium);
            holder.horaTag.setTypeface(medium);
            holder.comentarioServicioTag.setTypeface(medium);

            convertView.setTag(holder);
        }else {
            holder=(ViewHolder)convertView.getTag();
        }

        SimpleDateFormat format = new SimpleDateFormat("dd 'de' MMMM",new Locale("es"));

        DateFormat df=new SimpleDateFormat("HH:mm",Locale.US);
        Date d=new Date(serviciosData.get(position).getDate().getTime());

        hour=df.format(d);


        fecha=format.format(serviciosData.get(position).getDate());
        serviciosData.get(position).setFecha(fecha);
        serviciosData.get(position).setHora(hour);

        switch (serviciosData.get(position).getStatus()){
            case 0:
                serviciosData.get(position).setEstado("Solicitado");
                break;
            case 1:
                serviciosData.get(position).setEstado("En Progreso");
                break;
            case 2:
                serviciosData.get(position).setEstado("Completo");
                break;
            case 3:
                serviciosData.get(position).setEstado("Pagado");
                break;
            case 8:
                serviciosData.get(position).setEstado("Rechazado");
                break;
            case 9:
                serviciosData.get(position).setEstado("Cancelado");
                break;
        }

        holder.nombreTag.setText(serviciosData.get(position).getNombre());
        holder.estadoTag.setText(serviciosData.get(position).getEstado());
        holder.nombreAlumnoTag.setText(serviciosData.get(position).getAlumno());
        holder.fechaTag.setText(serviciosData.get(position).getFecha());
        holder.horaTag.setText(serviciosData.get(position).getHora());
        holder.comentarioServicioTag.setText(serviciosData.get(position).getComentarios());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new ServicesFragment();
                Bundle bundle=new Bundle();
                bundle.putString("nombre",serviciosData.get(position).getNombre());
                bundle.putString("precio",serviciosData.get(position).getPrecio());
                bundle.putString("descripcion",serviciosData.get(position).getDescripcion());
                bundle.putString("objectid",serviciosData.get(position).getObjectId());
                bundle.putString("estado",serviciosData.get(position).getEstado());
                bundle.putString("comentarios",serviciosData.get(position).getComentarios());
                bundle.putString("alumnoId",serviciosData.get(position).getAlumnoId());
                bundle.putString("alumno",serviciosData.get(position).getAlumno());
                bundle.putString("fecha",serviciosData.get(position).getFecha());
                bundle.putInt("status",serviciosData.get(position).getStatus());


                fragment.setArguments(bundle);
                FragmentTransaction transaction= ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();
            }
        });

        return convertView;


    }

}
