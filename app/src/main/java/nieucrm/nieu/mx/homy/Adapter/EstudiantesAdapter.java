package nieucrm.nieu.mx.skola.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import nieucrm.nieu.mx.skola.DataModel.EstudianteData;
import nieucrm.nieu.mx.skola.Fragments.PerfilEstudianteFragment;
import nieucrm.nieu.mx.skola.R;


/**
 * Created by Carlos Romero  on 10/12/2016.
 */

public class EstudiantesAdapter extends BaseAdapter {

    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf";



    private Context context;
    LayoutInflater inflater;
    private List<EstudianteData> estudianteData=null;
    private ArrayList<EstudianteData>arrayList=null;




    public EstudiantesAdapter(Context context, List<EstudianteData> estudianteData) {
        this.context = context;
        this.estudianteData = estudianteData;
        try {
            inflater=LayoutInflater.from(context);

        }catch (Exception e){
            Log.d("Error",e.getMessage());
        }
        this.arrayList=new ArrayList<>();
        this.arrayList.addAll(estudianteData);
    }

    public class ViewHolder{
        TextView nombreE;
        TextView grupo;
        TextView mensajes;
        ImageView btnChild;
    }


    @Override
    public int getCount() {
        return estudianteData.size();
    }

    @Override
    public Object getItem(int position) {
        return estudianteData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Typeface bold=Typeface.createFromAsset(context.getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(context.getAssets(),avenirMedium);

        ViewHolder holder;
       // TextView nombreEstudiante=(TextView)v.findViewById(R.id.nombreEstudiante);

        if(convertView==null){
            holder=new ViewHolder();
            convertView=inflater.inflate(R.layout.estudiantes_item_layout,null);
            holder.grupo = (TextView) convertView.findViewById(R.id.grupoEstudiante);
            holder.nombreE = (TextView) convertView.findViewById(R.id.nombreChild);
            holder.mensajes=(TextView) convertView.findViewById(R.id.mensajeColor);
            holder.btnChild=(ImageView)convertView.findViewById(R.id.btnChild);

            holder.grupo.setTypeface(bold);
            holder.nombreE.setTypeface(medium);
            holder.mensajes.setTypeface(medium);

            convertView.setTag(holder);
        }else {
            holder=(ViewHolder)convertView.getTag();
        }




        if (estudianteData.get(position).getGenero().equals("M")){
            holder.btnChild.setImageDrawable(context.getResources().getDrawable(R.drawable.boy_button));
            holder.mensajes.setBackgroundResource(R.drawable.mensajes_pendientes);
        }else {
           holder.btnChild.setImageDrawable(context.getResources().getDrawable(R.drawable.girl_button));
            holder.mensajes.setBackgroundResource(R.drawable.rectangle_mensajes);
        }


        /*
        if (estudianteData.get(position).getNoMensajes()==0){
            holder.mensajes.setVisibility(View.INVISIBLE);
        }else {
            holder.mensajes.setText(estudianteData.get(position).getNoMensajes());
        }
        */

        holder.grupo.setText(estudianteData.get(position).getGrupo());
        holder.nombreE.setText(estudianteData.get(position).getNombreEstudiante());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new PerfilEstudianteFragment();
                Bundle bundle=new Bundle();
                bundle.putString("generoEstudiante",estudianteData.get(position).getGenero());
                bundle.putString("NombreEstudiante",estudianteData.get(position).getNombreEstudiante());
                bundle.putString("estudianteId",estudianteData.get(position).getIdEstudiante());
                bundle.putString("grupoId",estudianteData.get(position).getIdGrupo());
                bundle.putString("nombre",estudianteData.get(position).getNombre());
                fragment.setArguments(bundle);
                FragmentTransaction transaction= ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,R.anim.enter_from_left,R.anim.exit_to_right);
                transaction.replace(R.id.content_home,fragment,"fragment").addToBackStack(null).commit();

            }
        });

        return convertView;


    }

}
