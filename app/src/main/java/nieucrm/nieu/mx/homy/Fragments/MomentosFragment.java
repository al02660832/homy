package nieucrm.nieu.mx.skola.Fragments;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.ParseACL;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import info.hoang8f.android.segmented.SegmentedGroup;
import nieucrm.nieu.mx.skola.Adapter.MomentosAdapter;
import nieucrm.nieu.mx.skola.DataModel.MomentoData;
import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class MomentosFragment extends Fragment {

    ListView listaMomentos;
    String escuelaId, estudianteId;
    ParseObject escuela;
    String groupId;
    ArrayList<MomentoData>momentoList,listaEnviar;
    MomentoData data;
    ProgressBar progressMomento;

    ArrayList<ParseObject>listaMoments;

    HashMap<String, Object> params;

    Activity a;

    public MomentosFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            a=(Activity) context;
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Bundle b=this.getArguments();
        groupId=b.getString("grupoId");

        setHasOptionsMenu(true);

        return inflater.inflate(R.layout.fragment_momentos, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        listaMomentos=(ListView)view.findViewById(R.id.listaMomentos);
        progressMomento=(ProgressBar)view.findViewById(R.id.progressMomento);
        getMomentos();
    }

    public void getMomentos(){
        momentoList=new ArrayList<>();
        escuelaId=((ParseApplication)a.getApplication()).getEscuelaId();
        escuela= ParseObject.createWithoutData("Escuela",escuelaId);
        ParseObject o=ParseObject.createWithoutData("grupo",groupId);
        ParseQuery<ParseObject> query=ParseQuery.getQuery("Estudiantes").whereEqualTo("grupo",o);
        query.whereEqualTo("status",0);
        query.whereEqualTo("escuela",escuela);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                progressMomento.setVisibility(View.GONE);
                listaMomentos.setVisibility(View.VISIBLE);
                if (e==null){
                    for (ParseObject o:objects){
                        data=new MomentoData();
                        data.setNombre(o.getString("NOMBRE")+" "+o.getString("APELLIDO"));
                        data.setID(o.getObjectId());
                        momentoList.add(data);

                    }

                    MomentosAdapter ad=new MomentosAdapter(a,momentoList);
                    listaMomentos.setAdapter(ad);
                }else {
                    Log.e("Error",e.getMessage());
                }


            }
        });
    }

    public void saveMomentos(){
        listaMoments=new ArrayList<>();
        listaEnviar=new ArrayList<>();
        int count=listaMomentos.getCount();

        for (int i=0;i<count;i++){
            //View childView=listaMomentos.getAdapter().getView(i,listaMomentos.getChildAt(i),listaMomentos);
            MomentoData data=(MomentoData) listaMomentos.getAdapter().getItem(i);

            String nombre=data.getNombre();
            String desayuno=" ",comida=" ",colacion=" ",merienda=" ",tiempoSiesta=" ",horario=" ",pipi=" ",popo=" ",comentarios=" ",leche=" ";
            boolean durmio=false,aviso=false;
            estudianteId=data.getID();

            aviso=data.isAviso();
            durmio=data.isDurmio();

            desayuno=data.getDesayuno();
            colacion=data.getColacion();
            merienda=data.getMerienda();
            comida=data.getComida();
            leche=data.getMililitro();
            tiempoSiesta=data.getTiempo();
            horario=data.getHora();
            pipi=data.getPipi();
            popo=data.getPopo();
            comentarios=data.getComentario();

            if (desayuno==null||desayuno.equals("")){
                Log.e(nombre+" des","Vacio");
            }else {
                Log.e(nombre+" des",desayuno);
            }

            if (colacion==null ||colacion.equals("")){
                Log.e(nombre+" col","Vacia");
            }else {
                Log.e(nombre+" col",colacion);
            }

            if (comida==null||comida.equals("")){
                Log.e(nombre+" com","Vacio");
            }else {
                Log.e(nombre+" com",comida);
            }

            if (merienda==null||merienda.equals("")){
                Log.e(nombre+" mer","VACIO");
            }else {
                Log.e(nombre+" mer",merienda);
            }

            if (leche==null||leche.equals("")){
                Log.e(nombre+" mililitros","Vacio");
            }else {
                Log.e(nombre+" mililitros",leche);
            }

            if (tiempoSiesta==null||tiempoSiesta.equals("")){
                Log.e(nombre+" time","Vacio");
            }else {
                Log.e(nombre+" time",tiempoSiesta);
            }

            if (horario==null||horario.equals("")){
                Log.e(nombre+" hora ","Vacio");
            }else {
                Log.e(nombre+" hora ",horario);
            }

            if (pipi==null||pipi.equals("")){
                Log.e(nombre+" pip","Vacio");
            }else {
                Log.e(nombre+" pip",pipi);
            }

            if (popo==null||popo.equals("")){
                Log.e(nombre+" pop","vacio");
            }else {
                Log.e(nombre+" pop",popo);
            }

            if (comentarios==null||comentarios.equals("")){
                Log.e(nombre+" comen","Vacio");
            }else {
                Log.e(nombre+" comen",comentarios);
            }

            if (durmio){
                Log.d(nombre+" durm","Si");
            }else {
                Log.d(nombre+" durm","No");
            }

            if (aviso){
                Log.d(nombre+" avi","Si");
            }else {
                Log.d(nombre+" avi","No");
            }

            JSONObject o=new JSONObject();

            try {
                o.put("horaSiesta",horario);
                o.put("alimentosComentarios",comentarios);
                o.put("avisoFuncion",aviso);
                o.put("popo",popo);
                o.put("tiempoSiesta",tiempoSiesta);
                o.put("merienda",merienda);
                o.put("descanso",durmio);
                o.put("leche",leche);
                o.put("colacion",colacion);
                o.put("desayuno",desayuno);
                o.put("estudiante",estudianteId);
                o.put("comida",comida);
                o.put("pipi",pipi);
            }catch (JSONException j){
                Log.e("Error",j.getMessage());
            }

            ParseObject anuncio=new ParseObject("anuncio");
            ParseObject estudiante=ParseObject.createWithoutData("Estudiantes",estudianteId);
            anuncio.put("momento",o);
            anuncio.put("estudiante",estudiante);
            anuncio.put("aprobado",true);
            anuncio.put("autor", ParseUser.getCurrentUser());

            if (desayuno==null&&comida==null&&colacion==null&&merienda==null&&tiempoSiesta==null&&horario==null&&pipi==null&&popo==null&&comentarios==null&&leche==null&&!durmio&&!aviso){
                Log.e("Error","No hay momentos");
            }else {
                listaMoments.add(anuncio);
                Log.e("Tamaño",String.valueOf(listaMoments.size()));
            }
            Log.e("Objeto",o.toString());

            //Log.e("Objeto",o.toString());
            //Toast.makeText(getActivity(),desayuno,Toast.LENGTH_SHORT).show();

        }

        /*
        ParseObject.saveAllInBackground(listaMoments, new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e==null){
                    Log.d("Status","Envios exitosos");
                    Toast.makeText(getActivity(),"Envio exitoso",Toast.LENGTH_SHORT).show();
                }else {
                    Log.e("Error de envio",e.getMessage());
                }
            }
        });
        */
        for (ParseObject o:listaMoments){
            params=new HashMap<>();
            params.put("estudianteObjectId",o.getParseObject("estudiante").getObjectId());
            o.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e==null){
                        Log.d("Status","Envios exitosos");
                        ParseCloud.callFunctionInBackground("momentosParentNotification", params, new FunctionCallback<Object>() {
                            @Override
                            public void done(Object object, ParseException e) {
                                if (e == null) {
                                    Toast.makeText(a,"Envio exitoso",Toast.LENGTH_SHORT).show();
                                } else {
                                    Log.d("Error", e.getMessage());
                                }
                            }
                        });
                    }else {
                        Log.e("Error de envio",e.getMessage());
                    }
                }
            });
        }

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.guardar_momento,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.saveMoment:
                saveMomentos();
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);

    }

}
