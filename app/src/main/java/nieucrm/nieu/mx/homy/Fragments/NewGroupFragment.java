package nieucrm.nieu.mx.skola.Fragments;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;

import info.hoang8f.android.segmented.SegmentedGroup;
import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class NewGroupFragment extends Fragment {

    TextView crearGrupoTag,nombreGrupo,nombreCorto,nivelTag;
    EditText editGrupo,editCorto;
    SegmentedGroup segmentedNivel;
    ArrayList<ParseObject>niveles;
    SegmentedGroup.LayoutParams size = new SegmentedGroup.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

    String avenirBold = "font/avenir-next-bold.ttf", avenirMedium = "font/avenir-next-medium.ttf",demibold="font/avenir-next-demi-bold.ttf";
    Button btnGuardarGrupo;
    ParseObject niv;
    int i;
    String escuelaId;

    public NewGroupFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_new_group, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        escuelaId=((ParseApplication)getActivity().getApplication()).getEscuelaId();

        crearGrupoTag=(TextView)view.findViewById(R.id.crearGrupoTag);
        nombreGrupo=(TextView)view.findViewById(R.id.nombreGrupo);
        nombreCorto=(TextView)view.findViewById(R.id.nombreCorto);
        nivelTag=(TextView)view.findViewById(R.id.nivelTag);

        editGrupo=(EditText)view.findViewById(R.id.editGrupo);
        editCorto=(EditText)view.findViewById(R.id.editCorto);

        segmentedNivel=(SegmentedGroup)view.findViewById(R.id.segmentedNivel);

        btnGuardarGrupo=(Button)view.findViewById(R.id.btnGuardarGrupo);

        /*
        radioMaternal=(RadioButton)view.findViewById(R.id.radioMaternal);
        radioPreescolar=(RadioButton)view.findViewById(R.id.radioPreescolar);
        */



        fontFace();
        createRadio();

        btnGuardarGrupo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                saveGroup();
            }
        });

    }

    public void fontFace(){
        Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), avenirBold);
        Typeface medium = Typeface.createFromAsset(getActivity().getAssets(), avenirMedium);
        Typeface demi=Typeface.createFromAsset(getActivity().getAssets(),demibold);

        crearGrupoTag.setTypeface(demi);
        nombreGrupo.setTypeface(medium);
        nombreCorto.setTypeface(medium);
        nivelTag.setTypeface(medium);
        editGrupo.setTypeface(medium);
        editCorto.setTypeface(medium);
        /*
        radioPreescolar.setTypeface(medium);
        radioMaternal.setTypeface(medium);
        */


    }

    public void createRadio(){
        size.weight=1f;
        niveles=new ArrayList<>();
        final Typeface medium = Typeface.createFromAsset(getActivity().getAssets(), avenirMedium);

        ParseObject escuela=ParseObject.createWithoutData("Escuela",escuelaId);


        ParseQuery<ParseObject> nivel=ParseQuery.getQuery("Nivel");
        nivel.whereEqualTo("escuela",escuela);


        nivel.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e==null){
                    segmentedNivel.setWeightSum(objects.size());

                    for (ParseObject o:objects){
                        niveles.add(o);
                        RadioButton radioButton = (RadioButton)getActivity().getLayoutInflater().inflate(R.layout.radio_button_item,null);
                        radioButton.setText(o.getString("nombre"));
                        radioButton.setTextColor(getResources().getColor(R.color.white));
                        radioButton.setGravity(Gravity.CENTER);
                        radioButton.setLayoutParams(size);

                        radioButton.setTypeface(medium);
                        radioButton.setId(i);

                        i++;
                        segmentedNivel.addView(radioButton);
                        segmentedNivel.updateBackground();
                    }

                    segmentedNivel.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                            int checkedRadioButtonId=segmentedNivel.getCheckedRadioButtonId();
                            niv=niveles.get(checkedRadioButtonId);
                        }
                    });
                }else {
                    Log.e("Error",e.getMessage());
                }
            }
        });

    }

    public void saveGroup(){
        String grupoId=editCorto.getText().toString().trim();
        String name=editGrupo.getText().toString().trim();
        ParseObject escuela= ParseObject.createWithoutData("Escuela",escuelaId);
        ParseObject group=new ParseObject("grupo");
        group.put("grupoId",grupoId);
        group.put("escuela",escuela);
        group.put("name",name);
        group.put("nivel",niv);
        group.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e==null){
                    Toast.makeText(getActivity(),"Grupo creado exitosamente",Toast.LENGTH_SHORT).show();
                }else {
                    Log.d("Error",e.getMessage());
                }
            }
        });
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        return super.onOptionsItemSelected(item);

    }


}
