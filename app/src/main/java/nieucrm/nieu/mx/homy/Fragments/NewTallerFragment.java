package nieucrm.nieu.mx.skola.Fragments;


import android.app.TimePickerDialog;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import info.hoang8f.android.segmented.SegmentedGroup;
import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class NewTallerFragment extends Fragment {

    TextView tallerTag,nombreTallerTag,entradaTallerTag, txtEntradaTaller,
            salidaTallerTag,txtSalidaTaller,diasTag;
    EditText editTaller;
    SegmentedGroup segmentedDias;
    String h1,h2,horario;
    Date dateEntrada,dateSalida;
    Button btnGuardarTaller;
    RadioButton radioLunes, radioMartes,radioMiercoles,radioJueves,radioViernes;
    ArrayList<Integer>weekdays;
    String escuelaId;
    ParseObject escuela;
    ParseObject taller;

    String nombre;
    Integer[]week;

    public NewTallerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_new_taller, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);

        tallerTag=(TextView)v.findViewById(R.id.tallerTag);
        nombreTallerTag=(TextView)v.findViewById(R.id.nombreTallerTag);
        entradaTallerTag=(TextView)v.findViewById(R.id.entradaTallerTag);
        txtEntradaTaller=(TextView)v.findViewById(R.id.txtEntradaTaller);
        salidaTallerTag=(TextView)v.findViewById(R.id.salidaTallerTag);
        txtSalidaTaller=(TextView)v.findViewById(R.id.txtSalidaTaller);
        diasTag=(TextView)v.findViewById(R.id.diasTag);

        editTaller=(EditText) v.findViewById(R.id.editTaller);
        segmentedDias=(SegmentedGroup)v.findViewById(R.id.segmentedDias);
        btnGuardarTaller=(Button)v.findViewById(R.id.btnGuardarTaller);
        radioLunes=(RadioButton)v.findViewById(R.id.radioLunes);
        radioMartes=(RadioButton)v.findViewById(R.id.radioMartes);
        radioMiercoles=(RadioButton)v.findViewById(R.id.radioMiercoles);
        radioJueves=(RadioButton)v.findViewById(R.id.radioJueves);
        radioViernes=(RadioButton)v.findViewById(R.id.radioViernes);

        weekdays=new ArrayList<>();
        segmentedDias.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.radioLunes:
                        weekdays.add(1);
                        break;
                    case R.id.radioMartes:
                        weekdays.add(2);
                        break;
                    case R.id.radioMiercoles:
                        weekdays.add(3);
                        break;
                    case R.id.radioJueves:
                        weekdays.add(4);
                        break;
                    case R.id.radioViernes:
                        weekdays.add(5);
                        break;
                }
            }
        });

        txtEntradaTaller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                horaEntrada();
            }
        });

        txtSalidaTaller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                horaSalida();
            }
        });

        btnGuardarTaller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveTaller();
            }
        });

    }

    public void horaEntrada(){
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(getContext(),new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                txtEntradaTaller.setText( String.format(new Locale("es"),"%02d", selectedHour) + ":" + String.format(new Locale("es"),"%02d", selectedMinute));
                h1=String.format(new Locale("es"),"%02d", selectedHour) + ":" + String.format(new Locale("es"),"%02d", selectedMinute);
                dateEntrada= new Date();
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(dateEntrada);
                calendar.set(Calendar.HOUR_OF_DAY, selectedHour);// for hour
                calendar.set(Calendar.MINUTE, selectedMinute);// for  min
            }
        }, hour, minute,false);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    public void horaSalida(){
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(getContext(),new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                txtSalidaTaller.setText( selectedHour + ":" + String.format(new Locale("es"),"%02d", selectedMinute));
                h2=String.format(new Locale("es"),"%02d", selectedHour) + ":" + String.format(new Locale("es"),"%02d", selectedMinute);

                dateSalida = new Date();
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(dateSalida);
                calendar.set(Calendar.HOUR_OF_DAY, selectedHour);// for hour
                calendar.set(Calendar.MINUTE, selectedMinute);// for  min
                //Toast.makeText(getContext(),calendar.getTime().toString(),Toast.LENGTH_SHORT).show();
            }
        }, hour, minute,false);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    public void saveTaller(){
        week=new Integer[weekdays.size()];
        weekdays.toArray(week);

        taller=new ParseObject("Taller");
        nombre=editTaller.getText().toString().trim();
        escuelaId=((ParseApplication)getActivity().getApplication()).getEscuelaId();
        escuela= ParseObject.createWithoutData("Escuela",escuelaId);
        taller.put("horaEntrada",dateEntrada);
        taller.put("nombre",nombre);
        taller.put("weekdays",week);
        taller.put("horaSalida",dateSalida);
        taller.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e==null){
                    Log.d("Guardado","Taller registrado");
                    Toast.makeText(getContext(),"Taller registrado",Toast.LENGTH_SHORT).show();
                }else {
                    Log.e("Error taller",e.getMessage());
                }
            }
        });
    }

}
