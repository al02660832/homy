package nieucrm.nieu.mx.skola.Fragments;


import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickResult;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import info.hoang8f.android.segmented.SegmentedGroup;
import nieucrm.nieu.mx.skola.Helpers.Constants;
import nieucrm.nieu.mx.skola.Helpers.ImagePicker;
import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.Helpers.Util;
import nieucrm.nieu.mx.skola.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class NewUserFragment extends Fragment {

    ArrayList<ParseObject>grupos;

    ArrayList<ParseObject>selected;

    ParseUser user;
    String photoId;

    Map<String, Object> parame;

    ImageView imageUsuario;

    String username,apellidos,nombre;
    int usertype;
    String camino;

    HashMap<String, Object> params;

    ParseObject userPhoto=new ParseObject("UserPhoto");

    ParseUser u;

    String password;

    private static String APP_DIRECTORY = "MyPictureApp/";
    private static String MEDIA_DIRECTORY = APP_DIRECTORY + "PictureApp";

    private final int MY_PERMISSIONS = 100;
    private final int PHOTO_CODE = 200;
    private final int SELECT_PICTURE = 300;
    private String mPath;
    TransferUtility utility;

    private static final int PICK_IMAGE_ID = 234;



    TextView btnTomarFoto,usuarioTag,nombreUser,grupoUser,apellidoUser;
    EditText editUser,editName,editApellido;

    String avenirBold = "font/avenir-next-bold.ttf", avenirMedium = "font/avenir-next-medium.ttf",demibold="font/avenir-next-demi-bold.ttf";

    SegmentedGroup.LayoutParams size = new SegmentedGroup.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    SegmentedGroup segmentedGrupo;
    ParseRelation<ParseObject>relation;
    ArrayList<String>selectedId;
    Button btnGuardarUser,generateQr;

    EditText userInput;
    TextView textInput;

    boolean foto;

    String escuela;

    int i;

    ParseObject userObject;

    LayoutInflater li;
    View promptsView;

    boolean existe;
    String usuario,name,lastname, objectId;

    ArrayList<ParseObject>userGroups;
    File outputDir;
    File imageFile;

    public NewUserFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Bundle bundle=this.getArguments();
        usertype= bundle.getInt("usertype");

        utility= Util.getTransferUtility(getContext());


        return inflater.inflate(R.layout.fragment_new_user, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (usertype==0){
            try {
                view.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.purple_access));

            }catch (Exception e1){
               Log.e("Error",e1.getMessage());
            }

        }else {
            if (usertype==1){

                try {
                    view.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.groups));

                }catch (Exception e1){
                    Log.e("Error",e1.getMessage());
                }


            }
        }

        imageUsuario=(ImageView)view.findViewById(R.id.imageUsuario);

        segmentedGrupo=(SegmentedGroup)view.findViewById(R.id.segmentedGrupo);

        btnTomarFoto=(TextView)view.findViewById(R.id.btnTomarFoto);
        usuarioTag=(TextView)view.findViewById(R.id.usuarioTag);
        nombreUser=(TextView)view.findViewById(R.id.nombreUser);
        grupoUser=(TextView)view.findViewById(R.id.grupoUser);
        apellidoUser=(TextView)view.findViewById(R.id.apellidoUser);

        editUser=(EditText)view.findViewById(R.id.editUser);
        editName=(EditText)view.findViewById(R.id.editName);
        editApellido=(EditText)view.findViewById(R.id.editApellido);

        btnGuardarUser=(Button)view.findViewById(R.id.btnGuardarUser);
        generateQr=(Button)view.findViewById(R.id.generateQr);

        li = LayoutInflater.from(getContext());
        promptsView = li.inflate(R.layout.promps, null);
        userInput = (EditText) promptsView
                .findViewById(R.id.inputPassword);
        textInput=(TextView)promptsView.findViewById(R.id.textInput);

        getData();

        createRadio();

        getTypeface();

        btnGuardarUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPassword();
            }
        });

        btnTomarFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPicker();
            }
        });

    }

    public void createRadio(){
        i=0;
        size.weight=1f;
        selectedId=new ArrayList<>();
        grupos=new ArrayList<>();
        selected=new ArrayList<>();
        final Typeface medium = Typeface.createFromAsset(getActivity().getAssets(), avenirMedium);
        escuela=((ParseApplication)getActivity().getApplication()).getEscuelaId();

        ParseObject e=ParseObject.createWithoutData("Escuela",escuela);

        ParseQuery<ParseObject> q=ParseQuery.getQuery("grupo").include("Maestros").orderByAscending("name");
        q.whereEqualTo("escuela",e);
        q.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
            if (e==null){
                segmentedGrupo.setWeightSum(objects.size());
                for (ParseObject o:objects){
                    grupos.add(o);
                    final CheckBox checkBox = (CheckBox) getActivity().getLayoutInflater().inflate(R.layout.checkbox_button_item,null);
                    checkBox.setText(o.getString("grupoId"));
                    checkBox.setTextColor(getResources().getColor(R.color.white));
                    checkBox.setGravity(Gravity.CENTER);
                    checkBox.setLayoutParams(size);

                    checkBox.setTypeface(medium);
                    checkBox.setId(i);
                    checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            //relation=new ParseRelation<ParseObject>("Maestros");
                            if ((buttonView).isChecked()) {
                                int checkedRadioButtonId = checkBox.getId();
                                selectedId.add(grupos.get(checkedRadioButtonId).getObjectId());
                            }else {
                                int checkedRadioButtonId = checkBox.getId();
                                selectedId.remove(grupos.get(checkedRadioButtonId).getObjectId());
                            }
                        }
                    });

                    i++;
                    segmentedGrupo.addView(checkBox);
                    segmentedGrupo.updateBackground();
                }
            }
            }
        });
    }

    public void getTypeface(){
        Typeface medium = Typeface.createFromAsset(getActivity().getAssets(), avenirMedium);
        Typeface demi = Typeface.createFromAsset(getActivity().getAssets(), demibold);

        btnTomarFoto.setTypeface(medium);
        usuarioTag.setTypeface(medium);
        nombreUser.setTypeface(medium);
        grupoUser.setTypeface(medium);
        apellidoUser.setTypeface(medium);
        editUser.setTypeface(medium);
        editName.setTypeface(medium);
        editApellido.setTypeface(medium);
        btnGuardarUser.setTypeface(demi);
        generateQr.setTypeface(demi);
        userInput.setTypeface(medium);
        textInput.setTypeface(demi);

    }

    public void setPassword(){


        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                getContext());

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);



        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // get user input and set it to result
                                // edit text
                                //result.setText(userInput.getText());
                                password=userInput.getText().toString().trim();
                                saveUser();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }

    public void saveUser() {
        params = new HashMap<>();

        username = editUser.getText().toString();
        nombre = editName.getText().toString().trim();
        apellidos = editApellido.getText().toString().trim();
        //user=new ParseUser();

        //user.setUsername(username);

        /*
        username
        password
        usertype
        nombre
        apellidos
        parentesco
        escuela objectId
         */
        //Ya furula
        params.put("username", username);
        params.put("password", password);
        if (usertype == 0) {
            params.put("usertype", usertype);
            params.put("parentesco", "Admin");
        } else {
            if (usertype == 1) {
                params.put("usertype", usertype);
                params.put("parentesco", "Docente");
            } else {

            }
        }


        params.put("nombre", nombre);
        params.put("apellidos", apellidos);
        params.put("escuela", escuela);
        //user.put("username",username);

        //params.put("newUser",parame);


        ParseCloud.callFunctionInBackground("newSchoolUserSignUp", params, new FunctionCallback<String>() {

            @Override
            public void done(String object, ParseException e) {
                if (e==null){

                    try {
                        userObject = ParseObject.createWithoutData(ParseUser.class, object);
                        userObject.fetchIfNeededInBackground(new GetCallback<ParseObject>() {
                            @Override
                            public void done(ParseObject object, ParseException e) {
                                if (e==null){
                                    addTeacherToRelation();
                                }else {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }catch (Exception e1){
                        Log.e("Error",e1.getMessage());
                    }

                    if (foto){
                        userPhoto.put("user",userObject);
                        userPhoto.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e==null){
                                    Log.d("Foto","Agregada");
                                    uploadFile(userPhoto.getObjectId());
                                }else {
                                    Log.e("Error",e.getMessage());
                                }
                            }
                        });
                    }
                    /*
                    u=(ParseUser) ParseObject.createWithoutData("User",object);
                    u.fetchIfNeededInBackground(new GetCallback<ParseObject>() {
                        @Override
                        public void done(ParseObject object, ParseException e) {

                            if (e==null){
                                addTeacherToRelation();
                            }else {
                                e.printStackTrace();
                            }

                        }
                    });

                    if (foto){
                        userPhoto.put("user",u);
                        userPhoto.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e==null){
                                    Log.d("Foto","Agregada");
                                    uploadFile(userPhoto.getObjectId());
                                }else {
                                    Log.e("Error",e.getMessage());
                                }
                            }
                        });
                    }
                    */

                }else {
                    Log.e("Error cloud",e.getMessage());
                            e.printStackTrace();
                }
            }
        });


        /*user.signUpInBackground(new SignUpCallback() {
            @Override
            public void done(ParseException e) {
                if (e==null){


                }else {
                    Log.e("Error sign up",e.getMessage());
                }
            }
        });
        */







    }

    public void addTeacherToRelation(){
        ParseQuery<ParseObject> g=ParseQuery.getQuery("grupo");
        g.whereContainedIn("objectId",selectedId);
        g.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e==null){
                    for (ParseObject o:objects){
                        relation=o.getRelation("Maestros");
                        relation.add(userObject);
                        o.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e==null){
                                    Log.d("Success","Sin errores");
                                }else {
                                    Log.d("Error",e.getMessage());
                                }
                            }
                        });
                    }
                }else {
                    Log.e("Error",e.getMessage());
                }
            }
        });
    }

    public void removeTeacher(String grupoSelected){
        ParseQuery<ParseObject> g=ParseQuery.getQuery("grupo");
        g.whereEqualTo("grupoId",grupoSelected);
        g.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (e==null){
                    ParseRelation<ParseObject> relation=object.getRelation("Maestros");
                    relation.remove(userObject);
                    object.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e==null){
                                Log.d("User","Removed");
                            }else {
                                Log.e("Error",e.getMessage());
                            }
                        }
                    });
                }else {
                    Log.e("Error","Relacion");
                }
            }
        });
    }

    public void uploadFile(String id){
        TransferObserver observe= utility.upload(Constants.BUCKET_NAME,id,new java.io.File(camino ));
        observe.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                Toast.makeText(getContext(), "Foto guardada", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                long _bytesCurrent = bytesCurrent;
                long _bytesTotal = bytesTotal;

                float percentage =  ((float)_bytesCurrent /(float)_bytesTotal * 100);
                Log.d("percentage","" +percentage);
            }

            @Override
            public void onError(int id, Exception ex) {
                Log.d("Error de Amazon",ex.getMessage());
            }
        });
    }

    public void showPicker(){
        PickImageDialog.build(new PickSetup().setCameraButtonText("Cámara").setGalleryButtonText("Galería").setTitle("Agregar foto").setCancelText("Cancelar"))
                .setOnPickResult(new IPickResult() {
                    @Override
                    public void onPickResult(PickResult r) {
                        //TODO: do what you have to...
                        if (r.getError() == null) {
                            //If you want the Uri.
                            //Mandatory to refresh image from Uri.
                            //getImageView().setImageURI(null);

                            //Setting the real returned image.
                            //getImageView().setImageURI(r.getUri());

                            //If you want the Bitmap.

                            Bitmap bitmap = r.getBitmap();
                            // TODO use bitmap
                            imageUsuario.setImageBitmap(bitmap);

                            camino=r.getPath();




                            //Image path
                            //r.getPath();
                        } else {
                            //Handle possible errors
                            //TODO: do what you have to do with r.getError();
                            Toast.makeText(getContext(), r.getError().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                }).show(getActivity().getSupportFragmentManager());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case PICK_IMAGE_ID:
                Bitmap bitmap = ImagePicker.getImageFromResult(getContext(), resultCode, data);
                // TODO use bitmap



                //fotoPago.setImageBitmap(bitmap);
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    /*
    public void getUserInfo(){
        //Username, nombre, appleidos, grupos y foto
        ParseQuery<ParseUser>usersQuery=ParseUser.getQuery();
        //usersQuery.whereEqualTo("objectId","");
        usersQuery.getFirstInBackground(new GetCallback<ParseUser>() {
            @Override
            public void done(ParseUser object, ParseException e) {

            }
        });

    }
    */


    public void getData(){
        Bundle b=this.getArguments();
        if (b!=null){
            name=b.getString("nombre");
            lastname=b.getString("apellidos");
            usuario=b.getString("username");
            objectId=b.getString("objectId");
            existe=b.getBoolean("existe");

            editUser.setText(usuario);
            editApellido.setText(lastname);
            editName.setText(name);
            getUserPhoto(objectId);

        }

    }

    public void getUserPhoto(String id){
        ParseUser u=ParseUser.createWithoutData(ParseUser.class,id);
        ParseQuery<ParseObject>userPhoto=ParseQuery.getQuery("UserPhoto");
        userPhoto.include("user");
        userPhoto.whereEqualTo("user",u);
        userPhoto.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (e==null){
                    if (object.getBoolean("aws")){
                        //getPhoto from AWS
                        downloadFromS3(object.getObjectId());
                    }
                }

            }
        });
    }

    public void downloadFromS3(String objectId){

        try {
            utility= Util.getTransferUtility(getContext());
        }catch (Exception e1){
            Log.e("Error credencial",e1.getMessage());
        }

        outputDir = getContext().getCacheDir();
        try {
            imageFile= File.createTempFile("prefix", "extension", outputDir);
        } catch (IOException ei) {
            ei.printStackTrace();
        }

        try {
            TransferObserver observe= utility.download(Constants.BUCKET_NAME,objectId,imageFile);
            observe.setTransferListener(new TransferListener() {
                @Override
                public void onStateChanged(int id, TransferState state) {
                    if (state==TransferState.COMPLETED){

                        Bitmap bitmap= BitmapFactory.decodeFile(imageFile.getAbsolutePath());
                        imageUsuario.setImageBitmap(bitmap);


                        try {
                            Toast.makeText(getContext(), "Foto lista", Toast.LENGTH_SHORT).show();
                        }catch (Exception e1){
                            Log.e("Exception",e1.getMessage());
                        }
                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                    long _bytesCurrent = bytesCurrent;
                    long _bytesTotal = bytesTotal;

                    float percentage =  ((float)_bytesCurrent /(float)_bytesTotal * 100);
                    Log.d("percentage","" +percentage);
                }

                @Override
                public void onError(int id, Exception ex) {

                    Log.d("Error de Amazon",ex.getMessage());
                    Toast.makeText(getContext(),"Error, intente de nuevo",Toast.LENGTH_SHORT).show();
                }
            });

        }catch (Exception e1){
            Log.e("Error",e1.getMessage());
        }
    }

}

