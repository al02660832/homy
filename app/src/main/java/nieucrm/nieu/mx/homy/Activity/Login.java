package nieucrm.nieu.mx.skola.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.onesignal.OneSignal;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import nieucrm.nieu.mx.skola.Helpers.ParseApplication;
import nieucrm.nieu.mx.skola.R;

/**
 * Created by Carlos Romero
 */

public class Login extends AppCompatActivity {

    String avenirBold="font/avenir-next-bold.ttf",avenirMedium="font/avenir-next-medium.ttf",demibold="font/avenir-next-demi-bold.ttf";
    EditText etPassword,etUsuario;
    ImageButton btnAmarillo,btnVerde;
    TextView txtBienvenido,textCredencial,textIngresar;
    String userName="";
    String parentesco;
    ArrayList<String> tag;
    JSONObject t;
    int usertype;
    ParseObject escuela;
    String escuelaId,nombre;

    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Full screen is set for the Window
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();

        setContentView(R.layout.activity_login);


        etUsuario=(EditText)findViewById(R.id.editUsuario);
        etPassword=(EditText)findViewById(R.id.editContraseña);
        txtBienvenido=(TextView)findViewById(R.id.txtBienvenido);
        btnAmarillo=(ImageButton)findViewById(R.id.btnAmarillo);
        btnVerde=(ImageButton)findViewById(R.id.btnVerde);

        textCredencial=(TextView)findViewById(R.id.textCredencial);
        textIngresar=(TextView)findViewById(R.id.textIngresar);

        dialog=new ProgressDialog(getApplicationContext());

        setTypeface();

        OneSignal.getTags(new OneSignal.GetTagsHandler() {
            @Override
            public void tagsAvailable(JSONObject tags) {
                if (tags!=null){

                    Log.d("Tag",tags.toString());

                }else {
                    Log.e("Tags","No more");

                }
            }
        });

        btnAmarillo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                userLogin();
            }
        });

        btnVerde.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scanCredencial();
            }
        });

    }

    public void userLogin(){
        String usuario=etUsuario.getText().toString().trim();
        String contraseña=etPassword.getText().toString().trim();

        ParseUser.logInInBackground(usuario,contraseña,new LogInCallback(){
            @Override
            public void done(ParseUser user, ParseException e) {
                if(user!=null){
                    usertype=ParseUser.getCurrentUser().getNumber("usertype").intValue();
                    Toast.makeText(getApplicationContext(),"Registro exitoso",Toast.LENGTH_LONG).show();

                    ParseQuery<ParseUser> userQuery=ParseUser.getQuery().include("escuela");
                    userQuery.whereEqualTo("objectId",ParseUser.getCurrentUser().getObjectId());
                    userQuery.getFirstInBackground(new GetCallback<ParseUser>() {
                        @Override
                        public void done(ParseUser object, ParseException e) {
                            if(e==null) {
                                if (object.getParseObject("escuela")==null){

                                }else {
                                    escuela = object.getParseObject("escuela");
                                    escuelaId=escuela.getObjectId();
                                    nombre=escuela.getString("nombre");

                                    ((ParseApplication)getApplication()).setEscuelaId(escuelaId);
                                    ((ParseApplication)getApplication()).setEscuelaNombre(nombre);



                                }
                                //Toast.makeText(getContext(),escuela.getObjectId(),Toast.LENGTH_SHORT).show();
                                //Toast.makeText(getApplicationContext(),escuela.getObjectId(),Toast.LENGTH_SHORT).show();

                            } else {
                                Log.e("Error",e.getMessage());

                            }
                        }
                    });

                    finish();
                    Intent intent=new Intent(getApplicationContext(),Home.class);
                    startActivity(intent);
                }else{
                    Toast.makeText(getApplicationContext(),"Error",Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void scanCredencial(){

        IntentIntegrator scanIntegrator=new IntentIntegrator(this);
        scanIntegrator.setPrompt("Escanee su creedencial");

        scanIntegrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
        scanIntegrator.setCameraId(0);
        scanIntegrator.setBeepEnabled(true);
        scanIntegrator.setBarcodeImageEnabled(true);
        scanIntegrator.initiateScan();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        IntentResult result=IntentIntegrator.parseActivityResult(requestCode,resultCode,data);
        if(result!=null){
            String scannedCode=result.getContents();
            if(scannedCode!=null && scannedCode.length()>=10){

                beginLoginWithQR(scannedCode);

            }
            else {
                Toast.makeText(getApplicationContext(),"Error al escanear QR",Toast.LENGTH_SHORT).show();

            }
        }
        else  {
            super.onActivityResult(requestCode, resultCode, data);
        }



    }

    private void beginLoginWithQR(String qr){

        final ProgressDialog progressDialog=new ProgressDialog(this);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressDialog.setTitle("Iniciando sesión");
                progressDialog.setMessage("Por favor espere");
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
        });


        String id=qr.substring(0,10);

        ParseQuery<ParseUser>userQuery=ParseUser.getQuery().include("escuela");
        userQuery.whereEqualTo("objectId",id);
        userQuery.getFirstInBackground(new GetCallback<ParseUser>() {
            @Override
            public void done(ParseUser object, ParseException e) {
                progressDialog.hide();
                if(e==null) {
                    parentesco = object.getString("parentesco");



                    if (parentesco.equals("Mamá") || parentesco.equals("Papá")) {
                        userName = object.getUsername();

                    }else {
                        userName = "";

                    }


                    ParseUser.logInInBackground(userName,"Angel550",new LogInCallback(){
                        @Override
                        public void done(ParseUser user, ParseException e) {

                            if(user!=null){

                                usertype=ParseUser.getCurrentUser().getNumber("usertype").intValue();
                                ParseQuery<ParseUser> userQuery=ParseUser.getQuery().include("escuela");
                                userQuery.whereEqualTo("objectId",ParseUser.getCurrentUser().getObjectId());
                                userQuery.getFirstInBackground(new GetCallback<ParseUser>() {
                                    @Override
                                    public void done(ParseUser object, ParseException e) {
                                        if(e==null) {

                                            if (object.getParseObject("escuela")!=null){
                                                escuela = object.getParseObject("escuela");
                                                escuelaId=escuela.getObjectId();
                                                nombre=escuela.getString("nombre");

                                                ((ParseApplication)getApplication()).setEscuelaId(escuelaId);
                                                ((ParseApplication)getApplication()).setEscuelaNombre(nombre);

                                            }else {
                                                Toast.makeText(getApplicationContext(),"El usuario carece de escuela establecidad",Toast.LENGTH_SHORT).show();

                                            }




                                            //Toast.makeText(getContext(),escuela.getObjectId(),Toast.LENGTH_SHORT).show();
                                            //Toast.makeText(getApplicationContext(),escuela.getObjectId(),Toast.LENGTH_SHORT).show();

                                        } else {
                                            Log.e("Error",e.getMessage());

                                        }
                                    }
                                });
                                finish();
                                Intent intent=new Intent(getApplicationContext(),Home.class);
                                startActivity(intent);
                                Toast.makeText(getApplicationContext(),"Registro exitoso",Toast.LENGTH_LONG).show();

                            }else{

                                Toast.makeText(getApplicationContext(),"Error al registrarse",Toast.LENGTH_LONG).show();

                            }
                        }
                    });

                }else{
                    Toast.makeText(getApplicationContext(),"Error",Toast.LENGTH_SHORT).show();

                }
            }
        });
    }

    public void setTypeface(){
        Typeface bold=Typeface.createFromAsset(getAssets(),avenirBold);
        Typeface medium=Typeface.createFromAsset(getAssets(),avenirMedium);
        Typeface demi=Typeface.createFromAsset(getAssets(),demibold);


        txtBienvenido.setTypeface(bold);
        etPassword.setTypeface(medium);
        etUsuario.setTypeface(medium);
        textCredencial.setTypeface(demi);
        textIngresar.setTypeface(demi);
    }

    public void registerTags() throws JSONException {
        t=new JSONObject();
        if (ParseUser.getCurrentUser()==null){

        }else{
            if (usertype==0){
                OneSignal.sendTag("admin","admin");
                Log.d("Tag","Admin");

            }else{
                if (usertype==1){
                    ParseQuery<ParseObject>gruposMaestro=ParseQuery.getQuery("grupo");
                    gruposMaestro.whereEqualTo("Maestros",ParseUser.getCurrentUser());

                    ParseQuery<ParseObject>estudiantes=ParseQuery.getQuery("Estudiantes");
                    estudiantes.whereMatchesQuery("grupo",gruposMaestro);
                    estudiantes.findInBackground(new FindCallback<ParseObject>() {
                        @Override
                        public void done(List<ParseObject> objects, ParseException e) {
                            for (ParseObject o:objects){
                                String channel="tuser_"+o.getObjectId();
                                try {
                                    t.put(channel,channel);
                                    Log.d("Tag",t.toString());

                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                }
                                OneSignal.sendTags(t);
                            }
                        }
                    });

                }
            }if (usertype==2){
                ParseQuery<ParseObject>q=ParseQuery.getQuery("Estudiantes");
                q.whereEqualTo("PersonasAutorizadas",ParseUser.getCurrentUser());
                q.findInBackground(new FindCallback<ParseObject>() {
                    @Override
                    public void done(List<ParseObject> objects, ParseException e) {
                        for (ParseObject ob:objects){
                            String channel="user_"+ob.getObjectId();
                            try {
                                t.put(channel,channel);
                                Log.d("Tag",t.toString());
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }
                        }
                        OneSignal.sendTags(t);

                    }
                });
            }
        }
    }


}
